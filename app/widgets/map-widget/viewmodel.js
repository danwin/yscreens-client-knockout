    define([
        'durandal/composition',
        'durandal/app',
        'underscore',
        'jquery',
        'leaflet',
        'viewmodels/model-display'
        ],
    function(composition, app, _, $, L, mdDisplay) {

        var itemRef = app.IO.gw.reflections['Devices'];

        var ctor = function() {   };

        var enumMarkers = function (map) {
            var customDefault = L.icon({
                iconUrl: 'vendor/leaflet/css/images/marker-icon.png',
                shadowUrl: 'vendor/leaflet/css/images/marker-shadow.png',
            });
            itemRef.enum(app.rqOptions)
                .then(function (list) {
                    _.each(list, function (device) {

                            var label = device.Label.peek();
                            var address = device.Address.peek();
                            var lat = device.Lat.peek();
                            var lon = device.Lon.peek();
                            var latlng = new L.LatLng(lat, lon);
                            var Leaflet = L;
                            if (address) label = label + '<hr>'+ address;
                            var addDevice = function () {
                                Leaflet.marker(latlng, {
                                    icon: customDefault,
                                    title: label,
                                    alt: label,
                                    riseOnHover: true,
                                    draggable: false
                                }).addTo(map);
                                console.log('addDevice', lat, lon, label)
                            }
                            // addDevice()
                            setTimeout(addDevice, 1);
                    })
                })
                .catch(app.handleException)
        }

        ctor.prototype.activate = function(settings) {
            this.settings = settings;

        };

        ctor.prototype.getHeaderText = function(item) {
            if (this.settings.headerProperty) {
                return this.settings.headerProperty;
            }

            return 'Your Map';
        };

        ctor.prototype.compositionComplete = function () {
            // body...
            console.log('compositionComplete: map-view:::::', $('#map-view'));
            this.map = new L.Map('map-view');

            // create the tile layer with correct attribution
            var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
            var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
            var osm = new L.TileLayer(osmUrl, {minZoom: 3, maxZoom: 12, attribution: osmAttrib, subdomains: 'abc'});

            // start the map in Sweden:
            this.map.setView(new L.LatLng(63, 17),4);
            this.map.addLayer(osm);

            enumMarkers(this.map);

        }

        return ctor;
    });

