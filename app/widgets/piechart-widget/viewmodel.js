    define(['durandal/composition', 
		    'durandal/app', 
    		'knockout', 
    		'underscore.all',
    		'jquery',
    		'viewmodels/model-user',
    		'chart', 
    		'knockout.chart'], 
    	function(
    		composition,
    		app, 
    		ko,
    		_,
    		$,
    		mdUser) {

        var ctor = function() {  
        	var self = this;

	        var _toMBytes = function (value) {
	        	// return value;
				var mbSize = value / 1048576 //1,048,576 Bytes
				if (mbSize < 1) return Math.ceil(mbSize * 100) / 100; 
				if (mbSize < 10) return Math.ceil(mbSize * 10) / 10; 
				return Math.ceil(mbSize);
	        }

        	var mandatory = _.assertDefined;

			self.ForImages = ko.observable(70);
			self.FreeSpace = ko.observable(900);
			self.ForVideo = ko.observable(30);

			self.ForImagesMB = ko.pureComputed(function () {return _toMBytes(self.ForImages())});
			self.FreeSpaceMB = ko.pureComputed(function () {return _toMBytes(self.FreeSpace())});
			self.ForVideoMB = ko.pureComputed(function () {return _toMBytes(self.ForVideo())});

			self.labelImages = ko.observable('Images');
			self.labelFreeSpace = ko.observable('Free Space');
			self.labelVideo = ko.observable('Video');

			self.SpaceData = [ //SpaceData
				{
					value: self.ForImagesMB,
					color:"#464AF7",
					highlight: "#5A5EFF",
					label: self.labelImages
				},
				{
					value: self.FreeSpaceMB,
					color: "#46BFBD",
					highlight: "#5AD3D1",
					label: self.labelFreeSpace
				},
				{
					value: self.ForVideoMB,
					color: "#FDB45C",
					highlight: "#FFC870",
					label: self.labelVideo
				}
			];

			self.legend = ko.observable('');
			// self.legend = ko.pureComputed(function () {
			// 	return 
			// 		"Free Space: "+self.FreeSpace()+" MB" 
			// 		"Video: "+self.ForVideo()+" MB" 
			// 		"Images: "+self.ForImages()+" MB" 
			// });

			self.fmtLegend  = ko.pureComputed(function () {
				return
					'<div class="badge" style="background-color:#46BFBD;">Free Space: '+self.FreeSpaceMB()+' MB</div>' +
					'<div class="badge" style="background-color:#FDB45C;">Video: '+self.ForVideoM()+' MB</div>' +
					'<div class="badge" style="background-color:#464AF7;">Images: '+self.ForImagesMB()+' MB</div>' 
			});
     
	        self.activate = function(settings) {
	            self.settings = settings;

	            // Run requests
				var AccountEmail = mandatory(app.auth.user.email, 
					'Cannot extract user key (email) from app.user (Piechart-Widget)');
				var oUser = new mdUser.User({'AccountEmail': AccountEmail});

				var measureFreeSpace = function () {
		            // [1] Compute quota
					return new Promise(function (resolve, reject) {
						// body...
						app.IO.gw.reflections['AdmUsers'].load(oUser, app.rqOptions)
							.then(function () {
								// body...
								var spaceQuota = oUser.SpaceQuota.peek();
								var spaceUsed = oUser.SpaceUsed.peek();
								console.log('Quota: ', spaceQuota, spaceUsed, spaceQuota - spaceUsed)
								self.FreeSpace(spaceQuota - spaceUsed);
								resolve()
							})
							.catch(reject)
					})
				}

				var measureAssets = function () {
		            // [2] Compute space by asset categories
					return new Promise(function (resolve, reject) {
						// body...
			            var assets = [];
			            app.IO.gw.reflections['Assets'].enum(app.rqOptions).then(function (list) {
			            	// compute images
			            	var imgSpace = _.reduce(list, function (result, o2) {
			            		return (o2.Type.peek() === 'Image') ? result + o2.FileSize.peek() : result
			            	}, 0)
			            	self.ForImages(imgSpace)
			            	// compute images
			            	var videoSpace = _.reduce(list, function (result, o2) {
			            		return (o2.Type.peek() === 'Video') ? result + o2.FileSize.peek() : result
			            	}, 0)
			            	self.ForVideo(videoSpace)
			            	resolve()
			            })
			            .catch(reject)
					})
				}

				var updateLegend = function () {

					self.legend(
							'<div class="badge" style="background-color:#46BFBD;color:#000000;">Free Space: '+self.FreeSpaceMB()+' MB</div><br>' +
							'<div class="badge" style="background-color:#FDB45C;color:#000000;">Video: '+self.ForVideoMB()+' MB</div><br>' +
							'<div class="badge" style="background-color:#464AF7;color:#ffffff;">Images: '+self.ForImagesMB()+' MB</div><br>' 
						)
				}

				return measureFreeSpace()
						.then(measureAssets)
						.then(updateLegend)
						.catch(app.handleException)
	        };
	     
	        self.getHeaderText = function(item) {
	            if (self.settings.headerProperty) {
	                return self.settings.headerProperty;
	            }
	     
	            return 'Your Space';
	        };

	        self.compositionComplete = function () {
	            // body...
	        }
        };
          
        return ctor;
    });

