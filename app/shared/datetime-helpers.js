//datetime.js


(function(root, factory) {
	var _modname = 'datetime_helpers';
	if (typeof define === "function" && define.amd) { // AMD mode
		define([], factory);
	} else if (typeof exports === "object") { // CommonJS mode
		module.exports = factory();
	} else {
		root[_modname] = factory(); // Plain JS, "rtl" is in window scope
	}
}(this, function() {

	var _zero = (new Date(Date.UTC(1970, 0, 1, 0, 0 ,0, 0))).getTime()
	var twoDigitStr = function (value) {
		return value < 10 ? '0'+value : ''+value;
	}

	var module = {}


	module.fmtTimeInterval = function(msInterval) {

		// Use: 1 Jan 1970 as a reference ()
		var 
			elapsed = new Date(msInterval)
			,rYears = elapsed.getUTCFullYear() - 1970
			,rMonths = elapsed.getUTCMonth()
			,rDays = elapsed.getUTCDate() - 1
			,rHours = elapsed.getUTCHours()
			,rMinites = elapsed.getUTCMinutes()
			,rSeconds = elapsed.getUTCSeconds()
			,txt = [];

		// console.log(rYears, rMonths, rDays, rHours, rMinites, rSeconds)

	 	if(rYears) txt.push( rYears+'y')
	 	if(rMonths) txt.push( rMonths+'m')
	 	if(rDays) txt.push( rDays+'d')
	 	if(rHours) txt.push( twoDigitStr(rHours)+'h')
	 	if(rMinites) txt.push( twoDigitStr(rMinites)+'mn')
		if(rSeconds) txt.push( twoDigitStr(rSeconds)+'s');

		return txt.length > 0  ? txt.join('') : '0';
	}

	// schedule start, -> trigger start
	module.fmtHours = function (msInterval, zero) {
		// "zero" can be any datetime in ms or "undefined"
		var 
			d = new Date(msInterval + (zero === void 0) ? _zero : zero)
			,h = d.getUTCHours()
			,m = d.getUTCMinutes()
			,mm = (m<10) ? '0'+m : m;
		// console.log('...date...', valueMs, d)
		return h + ':' + mm
	}

	module.extractHoursMinutes = function (msInterval, zero) {
		// "zero" can be any datetime in ms or "undefined"
		var 
			d = new Date(msInterval + (zero === void 0) ? _zero : zero)
			,h = d.getUTCHours()
			,m = d.getUTCMinutes();
		// console.log('...date...', valueMs, d)
		return [h,m]
	}

	module.extractHoursMinutesStr = function (msInterval, zero) {
		// "zero" can be any datetime in ms or "undefined"
		var value = module.extractHoursMinutes(msInterval, zero);
		if (value[1] < 10) value[1] = '0'+value[1]; 
		return value.join(':')
	}

	// schedule start, -> trigger start
	/*
	extract<Time>
	extract<Time>Str
	*/

	module.extractWeekDay = function (msInterval, zero) {
		var 
			// Date for previos Monday before start of epoch:
			_zero = new Date(Date.UTC(1969, 11, 28, 0, 0 ,0, 0))
			,d = new Date(msInterval + (zero === void 0) ? _zero : zero);
		return d.getUTCDay()
	}

	module.extractWeekDayStr = function (msInterval, zero) {
		var	
			_wdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
			,wd = module.extractWeekDay(msInterval, zero);
		return _wdays[wd]
	}

	module.extractWeekNumber = function (msInterval, zero) {
		return Math.ceil((msInterval + zero)/(7*24*3600*1000))
	}
	
	module.extractWeekNumberStr = function (msInterval, zero) {
		return 'Week ' + (module.extractWeekNumber(msInterval, zero)+1)
	}
	
	module.extractMonthDate = function (msInterval, zero) {
		// "zero" can be any datetime in ms or "undefined"
		var 
			d = new Date(msInterval + (zero === void 0) ? _zero : zero);
		return d.getUTCDate()
	}

	module.extractMonthDateStr = function (msInterval, zero) {
		return module.extractMonthDate(msInterval, zero).toString()
	}

	module.extractMonth = function (msInterval, zero) {
		// "zero" can be any datetime in ms or "undefined"
		var 
			d = new Date(msInterval + (zero === void 0) ? _zero : zero);
		return d.getUTCMonth()
	}

	module.extractMonthStr = function (msInterval, zero) {
		var	
			_months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Oct','Nov','Dec']
			,m = module.extractMonth(msInterval, zero);
		return _months[m]
	}

	// XMLTV format
	module.toXMLTVString = function (msInterval) {
		// "20161208030000 +0300"
		var oDate = new Date(msInterval)
			,fullYear = oDate.getUTCFullYear()
			,month = twoDigitStr(oDate.getUTCMonth())
			,date = twoDigitStr(oDate.getUTCDate())
			,h = twoDigitStr(oDate.getUTCHours())
			,m = twoDigitStr(oDate.getUTCMinutes())
			,s = '00';

		return [fullYear, month, date, h, m, s, ' +0000'].join('')
		
	}

	return module;

}))


