// To-do: [1] iter(), accepts 3 tpe: array, generator function, and objects with __iter__ or __next__ methods (see as it in Python)
// To-do: [2] functional programming for iterators (compose iterators): each, map, find, reduce, 
// To-do: [3] overwrite underscrore for iterators
(function (root, factory) {
	var _modname = 'iterator';
    if (typeof define === "function" && define.amd) { // AMD mode
        define([], factory);
    } else if (typeof exports === "object") { // CommonJS mode
        module.exports = factory();
    } else {
        root[_modname] = factory(); // Plain JS
    }
}(this, function () {

	/**
	 * Class Iterator. A generic tool to handle arrays and "endless" sequences (defined by recursive expression).
	 * @param {array/function} listOrFunction Defines underlying collection
	 * @param {any/function} sentinel       "Terminator" value. Optional. Default value - 'undefined'. Defines value of the first excluded element. Can be a function which returns "true" to stop iterations. To work with sparse collections (which contains undefined values), set sentinel value to some defined value which is not belongs to meaningful values of items (e.g., for some custom object). For iterators initialized by array, iterations can be stopped also when length-1 item reached.  
	 */

	Iterator.DONE = {}; // Some object, only as an unique constant to check equality '==='


	Iterator.iter = function (listOrFunction, sentinel) {
		return new Iterator(listOrFunction, sentinel)
	}

	/**
	 * Class which implements iterator protocol.
	 * Supports sequential access for items by calling "next" method.
	 * Method .next is compatible with ES6 iterator protocol - for () loop
	 * For es3 - use pattern "while(iterator.next().hasValue){var item=iterator.value()...}"
	 * @constructor Iterator
	 * @param {array/function} listOrFunction Generator value for iterator. Can be a lst or a recurrent expression with arguments (item, index), where item is a value of previous item (so, item[i+1]=expr(item[i], i+1), where i = [-1, ...] and item=[null, ...] for the first call of expression value of "item" argument will be null)
	 * @param {any/function} sentinel       "Terminator" value. Optional. Default value - 'undefined'. Defines value of the first excluded element. Can be a function which returns "true" to stop iterations. To work with sparse collections (which contains undefined values), set sentinel value to some defined value which is not belongs to meaningful values of items (e.g., for some custom object). For iterators initialized by array, iterations can be stopped also when length-1 item reached.
	 */
	function Iterator(listOrFunction, sentinel) {

		var self = this;
		var recursiveExpression = (typeof listOrFunction === 'function') ? listOrFunction : null;
		var length = (recursiveExpression) ? null : listOrFunction.length;
		var index = -1;
		var item = null;
		// var value; // undefined, for iterator where item is not Iterator instance - equals to item itself
		var done = false;
		var stopOn = (typeof sentinel === 'function') 
			? sentinel 
			: function (value) {return value === sentinel}

		var _items = (recursiveExpression) ? null : listOrFunction;

		self.done = function () {
			return done;
		}

		self.value = function () {
			return item;
		}

		/**
		 * "Next" method which compatible with es6 iterator protocol
		 * @return {object} Object with attributes: "done", "value". When "done" is true, "value" is undefined
		 */
		self.next = function () {
			index++;
			// if (item instanceof Iterator && item.next().hasValue) {
			// 	value = item.value()
			// } else {
			// 	index++;
			// 	item = (recursiveExpression) ? recursiveExpression(item, index) : _items[index];
			// 	if (item instanceof Iterator) {
			// 		// index--;
			// 		return self.next();
			// 	} else {
			// 		value = item;
			// 	}
			// }

			// was: for plain iterators (without upper block {}):
			item = (recursiveExpression) ? recursiveExpression(item, index) : _items[index];
			done = done || stopOn(item, index) || index === length;

			if (done) {
				// value = void 0;
				return {
						'done': true, /* <-attribute compatible with es5/6 iterator protocol */
						'hasValue': false /* extended attribute */
					}
				}
			return {
				'done': false, /* <-attribute compatible with es5/6 iterator protocol */
				'value': item, /* <-attribute compatible with es5/6 iterator protocol */
				'hasValue': true /* extended attribute */
			}
		}

		self.nextValue = function () {
			self.next();
			// index++;
			// item = (recursiveExpression) ? recursiveExpression(item, index) : _items[index];
			// done = done || stopOn(item, index) || index === length;
			if (done) return Iterator.DONE;
			return item
		}

		/**
		 * Apply callback to the next item of iterator
		 * @param  {function} iteratee Callback with arguments: (value, index). You can return explicit "false" to stop subsequent iterations
		 * @return {boolean}          Return false when iterator is done.
		 */
		self.forNext = function (iteratee) {
			self.next();

			// index++;
			// item = (recursiveExpression) ? recursiveExpression(item, index) : _items[index];
			// done = done || stopOn(item, index) || index === length;

			if (done || iteratee(item, index) === false) return false;
			return true 
		}
		
		/**
		 * Apply "iteratee" to each item until sentinel or until iteratee returns false. Note: it is similar to "each" in particular case: if : iterator has finite number of items and will be stopped by array.length or by "sentinel". Be careful with infinite iterators (be sure to return "false" from iteratee to avoid endless loop).
		 * @param  {function} iteratee Code to apply for item. Arguments: item, index. Return explicit "false" when you want to break iterations
		 * @return {undefined}          None
		 */
		self.forMany = function (iteratee) {
			while( self.forNext(iteratee) );
		}

		/**
		 * Return array to use it in functional programming (underscore.js, ...) where defined "length" value is necessary
		 * @return {array} Plain javascript array
		 */
		self.asList = function (maxIterations) {
			maxIterations = maxIterations || null;
			var OVERFLOW = Number.MAX_VALUE-1;
			var buffer = [], count = 0;

			while(self.next().hasValue){
				if (maxIterations) {
					if (count === maxIterations) break; // <-- stop iteration 
				} else {
					if (count === OVERFLOW) throw new Error('Iterator cannot be converted to array: too many items')
				} 
				buffer.push(item); // <-- contains current value after call of "next"
				count++;
			}

			return buffer;
		}

		// self.asList = function (maxIterations) {
		// 	maxIterations = maxIterations || null;
		// 	var OVERFLOW = Number.MAX_VALUE-1;
		// 	var buffer = [], count = 0;
		// 	// if (_items) return _items; 
		// 	while(self.forNext(function (item, index) {
		// 		if (maxIterations) {
		// 			if (count === maxIterations) return false; // <-- stop iteration 
		// 		} else {
		// 			if (count === OVERFLOW) throw new Error('Iterator cannot be converted to array: too many items')
		// 		} 
		// 		buffer.push(item);
		// 		count++;
		// 	})) {;}

		// 	return buffer;
		// }


		// self.each = function (iteratee, context) {
		// 	var items = _items,
		// 	step = (context === void 0) 
		// 	? iteratee 
		// 	: function(value, index) {
		//         return iteratee.call(context, value, index);
		// 	};
		// 	for (var index=0, value; index < length, value=items[index]; index++) {
		// 		done = done || stopOn(item);
		// 		if (done) break;
		// 		step(value, index)
		// 	}
		// }
	}



	// Export Iterator type:
	return Iterator
}))

