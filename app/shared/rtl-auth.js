//rtl-auth.js
var __rtl_preloader__ = (function (_, IO, config) {
	console.log('Preloader installed...Ok')
	return new Promise(function (resolve, reject) {
		var auth = IO.AuthFactory(config.storages.config)

		var retrieveSession = function () {
			return auth.retriveSession()
		}

		// Re-login is session is expired:
		var handleSessionStatus = function () {
			if (auth.user) {
				console.log('Session retrieved, using existing one...')
				resolve(auth)
				return
			}
			console.log('Opening new session as a player...')
			return auth.signIn({
					'email': 'player@device.service',
					'password': 'playback-password'
				})
		}

		// resolve and pass "auth" as a result to the external modules
		var resolveWithAuth = function () {
			return resolve(auth)
		}

		try {
			retrieveSession()
				.then(handleSessionStatus)
				.then(resolveWithAuth)
				.catch(reject)
		} catch (e) {
			reject(e)
		}
	})
})(this._, this.IO, this.MODULES.config);