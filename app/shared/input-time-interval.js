// "time-interval" custom type for input 
// Based on: https://github.com/liorwohl/html5-simple-date-input-polyfill
// 
// uses the same html5-simple-date-input-polyfill.css

(function(root, factory) {
    if (typeof define === "function" && define.amd) { // AMD mode
      define(['datetime', 'html5-simple-date-input-polyfill'], factory);
    } else if (typeof exports === "object") { // CommonJS mode
      module.exports = factory(require('datetime'));
    } else {
      factory(root.datetime); // Plain JS
    }

}(this, function(datetime) {

  var DateTimeInterval = datetime.DateTimeInterval;

 // use isNaN polyfill
  !(typeof Number.isNaN == 'function') ||
    (Number.isNaN = function (value) {
      return value !== null // Number(null) => 0
        && (value != value // NaN != NaN
          || +value != value // Number(falsy) => 0 && falsy == 0...
        )
  });

  // Custome Event polyfill for IE (details: )
  (function () {

    if ( typeof window.CustomEvent === "function" ) return false;

    function CustomEvent ( event, params ) {
      params = params || { bubbles: false, cancelable: false, detail: undefined };
      var evt = document.createEvent( 'CustomEvent' );
      evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
      return evt;
     }

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
  })();

  if (typeof DateTimeInterval === 'undefined') 
    throw new TypeError('"input-time-interval" extender requires "datetime.js" module');
  // if (typeof DateTimeExtender === 'undefined') 
  //   throw new TypeError('"input-time-interval" extender requires "html5-simple-date-input-polyfill.js" module');

  var _2digits = function (value) {
    return (value < 10) ? '0' + value : value.toString()
  }

  var renderIntervalStr = function (dateObj, hasWeeks, hasSeconds) {
    return dateObj.toPersistentString(hasWeeks)
    //-------------------
    // var result = [];

    // if (hasWeeks) {
    //   result.push(dateObj.weeks)
    //   result.push('w ')
    // }
    // result.push([
    //       dateObj.days,
    //       'd ',
    //       _2digits(dateObj.hours),
    //       ':',
    //       _2digits(dateObj.minutes)
    //     ].join('')
    //   )
    // if (hasSeconds) {
    //   result.push(':')
    //   result.push(_2digits(dateObj.seconds))
    // }
    // //~console.log(result)
    // return result.join('');
  }


  // string Id of the current visible editorsContainer;
  var _activeSId = null;

  var hideOpened = function (e) {
    var editorsContainer, sId;
    e = e ? e : window.event;
    //~console.log('GLOBAL blur', e.target, e.relatedTarget)
    if (e.target && /*e.relatedTarget &&*/ e.target.getAttribute('data-interval-element-sid') !== _activeSId /*e.relatedTarget.getAttribute('data-interval-element-sid')*/) {
      //~console.log(e.target.getAttribute('data-interval-element-sid'), e.relatedTarget.getAttribute('data-interval-element-sid'))
      // sId = e.target.getAttribute('data-interval-element-sid');
      sId = _activeSId;
      editorsContainer = document.getElementById('timeinterval-editor-group-'+sId);
      if (editorsContainer) {
        console.log('!editor found, closing:', editorsContainer) 
        editorsContainer.style.display = 'none';
        _activeSId = null
      }
    }
  }

  function timeIntervalExtender (theInput) {

    var self = this;
    // DateTimeExtender.call(this, theInput)

    // Set or update last Id (class-wide value)
    timeIntervalExtender.lastId = timeIntervalExtender.lastId ? (++timeIntervalExtender.lastId) : 1;

    // Own value to create unique Id's for elements
    this.sId = timeIntervalExtender.lastId.toString();

    this.theInput = theInput;
    // [2017-01-18:]
    // this.theInput.setAttribute('data-datetime-sid', self.sId);
    this.theInput.setAttribute('data-interval-element-sid', self.sId);
    this.container = null;
    this.theDateEditorDiv = null;
    this.selectedTimeInterval = new DateTimeInterval();

    // Choose editors by  input[type] check     
    var inputType = this.theInput.getAttribute('data-x-type') || this.theInput.getAttribute('type'); //<- warning: for browsers which have no support for input[type=date] the value element.type will be "text"! but the attribute value for 'type' will be as specified!
    //~console.log(inputType)
    this.hasWeeks = (this.theInput.hasAttribute('data-x-show-all-parts'));

    this.init = function () {
      if (!this.updateBufferFromInput()) { 
        this.selectedTimeInterval.setTime(new DateTimeInterval().getTime());
        this.theInput.value = renderIntervalStr(this.selectedTimeInterval, this.hasWeeks, this.hasSeconds)
      }
      this.createContainer();
    };

    // To-do: call this method after changes in "this.selectedTimeInterval"
    this.alignIntervalToRange = function () {
      var minValue = this.theInput.getAttribute('min');
      var maxValue = this.theInput.getAttribute('max');
      var value, module;

      if (minValue || maxValue) {
        value = self.selectedTimeInterval.module();
        if (minValue) {
          marginValue = new DateTimeInterval(minValue);
          if (value < marginValue.module()) self.selectedTimeInterval = marginValue
        }
        if (maxValue) {
          marginValue = new DateTimeInterval(maxValue);
          if (value > marginValue.module()) self.selectedTimeInterval = marginValue
        }
      }
    }

    //update selectedTimeInterval with the date from the input, return true if changed
    this.updateBufferFromInput = function () {

      if (this.theInput.value) {
        // var cleanedText = this.theInput.value.replace(/-$/,''); // remove trailing "-" when input text is a partial
        var possibleNewDate = new DateTimeInterval(this.theInput.value);
        var module = possibleNewDate.module();
        if (!Number.isNaN(module) && module !== this.selectedTimeInterval.module()) {
          this.selectedTimeInterval = possibleNewDate;
          return true;
        }
      }
      return false;
    };

    //update the calendar and/or time if the date changed manually in the input
    this.updateControls = function () {
      if (self.updateBufferFromInput()) {
        self.updateTimeDisplay();
      }
    }

    this.incrementTime = function (timeUnits, increment) {
      // var multiplier = {
      //   'weeks' : 604800000,
      //   'days' : 86400000,
      //   'hours' : 3600000,
      //   'minutes' : 60000,
      //   'seconds' : 1000
      // }
      var minmax = {
        'weeks' : [0, 52],
        'days' : [0,6],
        'hours' : [0,23],
        'minutes' : [0,59],
        'seconds' : [0,59]
      }
      var //oldValue = self.selectedTimeInterval.getTime(), 
        numUnits = increment + self.selectedTimeInterval[timeUnits],
        // value,
        min = minmax[timeUnits][0],
        max = minmax[timeUnits][1];

      if (numUnits < min) numUnits = max;
      if (numUnits > max) numUnits = min;

      // value = multiplier[timeUnits] * increment + oldValue;

      self.selectedTimeInterval[timeUnits] = numUnits;

      // // Apply restrictions if any:
      // if (self.maxLimit !== void 0 && self.selectedTimeInterval > self.maxLimit) self.selectedTimeInterval = self.maxLimit;

      // if (self.minLimit !== void 0 && self.selectedTimeInterval < self.minLimit) self.selectedTimeInterval = self.minLimit;

      // if (minValue || maxValue) {
      //   value = self.selectedTimeInterval.module();
      //   if (minValue) {
      //     marginValue = new DateTimeInterval(minValue);
      //     if (value < marginValue.module()) self.selectedTimeInterval = marginValue
      //   }
      //   if (maxValue) {
      //     marginValue = new DateTimeInterval(maxValue);
      //     if (value > marginValue.module()) self.selectedTimeInterval = marginValue
      //   }
      // }
      // 
      
      // Relay event to external listeners:
      self.theInput.dispatchEvent(new CustomEvent("datevaluechange", {
        'detail': {
          'value': self.selectedTimeInterval.module()
        }
      }));

      unitDisplay = document.getElementById(timeUnits+'-display-'+self.sId);
      if (unitDisplay) unitDisplay.innerHTML = _2digits(numUnits)

      self.fromBufferToInputValue();
      // self.updateTimeDisplay();
    }

    this.resetTime = function () {
      self.selectedTimeInterval = new DateTimeInterval(0);
      // Relay event to external listeners:
      self.theInput.dispatchEvent(new CustomEvent("datevaluechange", {
        'detail': {
          'value': self.selectedTimeInterval.module()
        }
      }));

      // self.theInput.value = renderIntervalStr(self.selectedTimeInterval, self.hasWeeks, self.hasSeconds)

      self.fromBufferToInputValue()
      self.updateTimeDisplay();

    }

    var appentTabPage = function (parent, className) {
      // Group with tab handle, lable and page for content
      var element = document.createElement('div');
      element.className = className;
      parent.appendChild(element)
      return element;

    }

    //create the calendar html and events
    this.createContainer = function () {
      var element;
      //creating a container div around the input, the calendar will also be there
      this.container = document.createElement('div');
      this.container.className = 'calanderContainer';
      this.container.style.display = 'inline-block';
      this.container.setAttribute('data-interval-element-sid', self.sId); //<- prevent auto-close on click
      this.theInput.parentNode.replaceChild(this.container, this.theInput);
      this.container.appendChild(this.theInput);


      // //the calendar div
      // this.theDateEditorDiv = document.createElement('div');
      // this.theDateEditorDiv.className = 'calendar';
      // this.theDateEditorDiv.style.display = 'none';
      // this.container.appendChild(this.theDateEditorDiv);

      // CHANGES ARE HERE:::::::::::::::::::
      // Create pull-down:
      this.editorsContainer = document.createElement('div')
      this.editorsContainer.setAttribute('data-interval-element-sid', self.sId); //<- prevent auto-close on click
      this.editorsContainer.id = 'timeinterval-editor-group-'+self.sId;
      this.editorsContainer.className = 'calendar';
      this.editorsContainer.style.display = 'none'; // <-

      this.container.appendChild(this.editorsContainer)

      this.theTimeEditorDiv = appentTabPage(this.editorsContainer, 'time-editor-container');
      this.createTimeIntervalEditor();

      // Special event which allows to set value directly by custom event:
      this.theInput.addEventListener('setDateValue', function (e) {
        // self.selectedTimeInterval.setTime(e.details.date.getTime())
        var interval = e.detail.interval;
        if (interval instanceof DateTimeInterval) {
          self.theInput.value = renderIntervalStr(interval, self.hasWeeks, self.hasSeconds)
          self.updateControls()
        } else {
          console.error('setDateValue" event invalid data type:', interval);
          throw new TypeError('"input-time-interval" extender supports only DateTimeInterval values in "setDateValue" event');
        }
      });

      function openEditor () {
        if (_activeSId !== self.sId) {
          var editorsContainer = document.getElementById('timeinterval-editor-group-'+_activeSId);
          if (editorsContainer) editorsContainer.style.display = 'none';
          _activeSId = self.sId
        }
        // Update editor
        self.updateControls()        
        // Display editor
        self.editorsContainer.style.display = '';
      }
      //open the calendar when the input get focus, also on various click events to capture it in all corner cases
      this.theInput.addEventListener('focus', openEditor);
      this.theInput.addEventListener('mouseup', openEditor);
      this.theInput.addEventListener('mousedown', openEditor);

      // Align string to mask with leading zeros if date is in a short form:
      this.theInput.addEventListener('blur', function (e) {
        var text = self.theInput.value;
        // if (text.length < 10) {
          //~console.log('Updating!!!')
          self.theInput.value = renderIntervalStr(self.selectedTimeInterval, self.hasWeeks, self.hasSeconds)
        // }

        // e = e ? e : window.event;
        // // if focus moves out of controls group:
        // //~console.log('blur', e.target, e.relatedTarget)
        // if (!e.relatedTarget.getAttribute('data-interval-element-sid') || e.relatedTarget.getAttribute('data-interval-element-sid' !== self.sId)) {
        //   //~console.log('Loosing focus, hiding editor:')
        //   self.editorsContainer.style.display = 'none';
        // }
      })

      this.theInput.addEventListener('keyup', self.updateControls);

      // "Ctrl" listener:
      this._isCtrlKeyDown = false;

      this.theInput.addEventListener('keyup',   function (evt) {
        var e = evt ? evt : window.event;        
        if (17 === e.keyCode) {
          self._isCtrlKeyDown = false;
          //~console.log('Ctrl OFF')
        } 
      })

      var _stopEvent = function (e) { //Stop the event
          //e.cancelBubble is supported by IE - this will kill the bubbling process.
          e.cancelBubble = true;
          e.returnValue = false;
  
          //e.stopPropagation works in Firefox.
          if (e.stopPropagation) {
            e.stopPropagation();
            e.preventDefault();
          }
          return false
      }

      // TO-DO: issues remains: FIRST CHAR not validated???
      this.theInput.addEventListener('keydown', function (evt) {
        function isTextSelected(input){ // <--- http://stackoverflow.com/questions/9065828/javascript-check-if-text-selected
           var startPos = input.selectionStart;
           var endPos = input.selectionEnd;
           var doc = document.selection;

           if(doc && doc.createRange().text.length != 0) {
              return true;
           } else if (!doc && input.value.substring(startPos,endPos).length != 0) {
              return true;
           }
           return false;
        }
        
        var e = evt ? evt : window.event;
        var key = e.charCode || e.keyCode || e.which;
        var keychar = String.fromCharCode(key);

        // ----------- 8< --------------------
        // Simple stub to allow only Tab key
        if (key === 9) return true;
        return _stopEvent(e)
        // ----------- 8< --------------------

        // Always allow Ctrl key:
        if (key === 17) {
          self._isCtrlKeyDown = true;
          //~console.log('Ctrl ON')
          return true;
        }

        // Allow combinations: Ctrl+C, Ctrl+A, Ctrl+Z, Ctrl+X:
        if (self._isCtrlKeyDown && [65,67,88,90,99].indexOf(key)>-1) return true;
        // Allow tab, Enter, End, Home, <-, ->, Backspace, Delete:
        //~console.log('[].indexOf', key, [9, 13, 35, 36, 37, 39].indexOf(key))
        if ([9, 13, 35, 36, 37, 39, 8, 46].indexOf(key)>-1) return true;


        var maxLen = 9; //'01d 00:00'
        if (self.hasWeeks) maxLen += 4; //'01w '+...
        if (self.hasSeconds) maxLen += 3; // ...+':00'

        // if complete and no text is selected:
        if (self.theInput.value.length >=maxLen && !isTextSelected(self.theInput))  return _stopEvent(e); 

        // handle weeks edge:
        if (self.hasWeeks) {
          if (self.theInput.value.length === 2) {
            self.theInput.value += (keychar === 'w') ? ' ': 'w ';
          }
        }

        // handle days edge: ('01w 00d...' or '00d...')
        if (self.hasWeeks && self.theInput.value.length === 6 || self.theInput.value.length === 2) {
          self.theInput.value += (keychar === 'd') ? ' ': 'd ';
        }

        // handle hours, minutes, seconds '01d 00:00'
        var threshold = (self.hasSeconds) ? [6] : [6, 9];
        if (self.hasWeeks) {
          for (var i = 0; i < threshold.length; i++) {threshold[i] += 4}
        } 

        if ( threshold.indexOf (self.theInput.value.length) >-1 ) {
          self.theInput.value += (keychar === ':') ? '': ':';
        }

        if (/[0-9]/.test(keychar)) return true; // <-- allow digits
        return _stopEvent(e); // <- in all another cases - prohibit all
      })

      this.theInput.addEventListener('change', self.updateControls)

      //close the calendar when clicking outside of the input or calendar
      // TO-DO: handle also "blur"/"focus" events to hide menu...
      // document.getElementsByTagName('body')[0].addEventListener('focus', function (e) {
      //   // //~console.log('..we are here: ',e.target, e.target.parentNode, e.target.parentNode.parentNode)

      //   // CHECK whether a target belongs to this control!! fires handlers in all instances of <input[date]>

      //   // switch (e.target.getAttribute('data-fire-action')) {
      //   //   case 'hours-increase':
      //   //     self.incrementTime('hours', +1)
      //   //     break
      //   //   case 'hours-decrease':
      //   //     self.incrementTime('hours', -1)
      //   //     break
      //   //   case 'minutes-increase':
      //   //     self.incrementTime('minutes', +1)
      //   //     break
      //   //   case 'minutes-decrease':
      //   //     self.incrementTime('minutes', -1)
      //   //     break
      //   //   case 'apply-now':
      //   //     self.resetTime()
      //   //     break
      //   //   case 'apply-today':
      //   //     self.resetDate()
      //   //     break;
      //   // }
      //   console.log('document.blur', e.target)
      //   if (e.target === self.theInput || e.target && e.target.getAttribute('data-interval-element-sid') === self.sId) return;
      //   // if (e.target.parentNode === self.container ||
      //   //     e.target.parentNode.parentNode === self.container || 
      //   //     e.target.parentNode.parentNode === self.editorsContainer ||
      //   //     e.target.parentNode === self.theDateEditorDiv ||
      //   //     (e.target.parentNode && e.target.parentNode.hasAttribute('data-interval-element-sid')) || 
      //   //     (e.target.parentNode.parentNode && e.target.parentNode.parentNode.hasAttribute('data-interval-element-sid'))
      //   // ) {
      //   //   return
      //   // }
      //   self.editorsContainer.style.display = 'none';
      // }, true);
    };


    //update the year and month selects with the right selected value (if date changed externally)
    this.updateSelecteds = function () {
      console.warn('updateSelecteds called!')
    };

    this.updateTimeDisplay = function () {
      var hoursDisplay, minutesDisplay;
      weeksDisplay = document.getElementById('weeks-display-'+self.sId);
      if (weeksDisplay) weeksDisplay.innerHTML = _2digits(self.selectedTimeInterval.weeks);
      daysDisplay = document.getElementById('days-display-'+self.sId);
      if (daysDisplay) daysDisplay.innerHTML = _2digits(self.selectedTimeInterval.days);
      hoursDisplay = document.getElementById('hours-display-'+self.sId);
      if (hoursDisplay) hoursDisplay.innerHTML = _2digits(self.selectedTimeInterval.hours);
      minutesDisplay = document.getElementById('minutes-display-'+self.sId)
      if (minutesDisplay) minutesDisplay.innerHTML = _2digits(self.selectedTimeInterval.minutes);
      secondsDisplay = document.getElementById('seconds-display-'+self.sId)
      if (secondsDisplay) secondsDisplay.innerHTML = _2digits(self.selectedTimeInterval.seconds);
    }

    this.createTimeIntervalEditor = function () {
      function createButton(caption, handler) {
        var button = document.createElement('button');
        button.innerHTML = caption;
        button.setAttribute('data-interval-element-sid', self.sId);
        button.onclick = handler;
        return button;
      }
      var editor, element, row, cell, button;
      var cellCount = 3 + (self.hasWeeks ? 1 : 0) + (self.hasSeconds ? 1 : 0);
      var cellClass = 'cell cell-'+cellCount; // 3 cells for hours, minutes, seconds
      var container = self.theTimeEditorDiv;
      // container.setAttribute('data-interval-element-sid', self.sId);

      var topBar = document.createElement('div');
      topBar.className = 'time-editor-topbar';

      cell = document.createElement('div')
      cell.className = 'cell'
      button = createButton('Reset', self.resetTime);
      cell.appendChild(button)
      topBar.appendChild(cell)

      cell = document.createElement('div')
      cell.className = 'cell'
      button = createButton('Close', hideOpened);
      button.setAttribute('data-interval-element-sid', 'false');
      cell.appendChild(button)
      topBar.appendChild(cell)

      container.appendChild(topBar)

      editor = document.createElement('div');
      editor.className = 'time-edit';
      editor.setAttribute('data-interval-element-sid', self.sId);

      row = document.createElement('selection');
      row.className = 'newline';

      // Weeks ++
      if (self.hasWeeks) {
        cell = document.createElement('div');
        cell.className = cellClass;
        button = createButton('+', function () {
          self.incrementTime('weeks', +1)
        })
        cell.appendChild(button)
        row.appendChild(cell)
      }

      // Days ++
      cell = document.createElement('div');
      cell.className = cellClass;
      button = createButton('+', function () {
        self.incrementTime('days', +1)
      })
      cell.appendChild(button)
      row.appendChild(cell)

      // Hours ++
      cell = document.createElement('div');
      cell.className = cellClass;
      button = createButton('+', function () {
        self.incrementTime('hours', +1)
      })
      cell.appendChild(button)
      row.appendChild(cell)

      // Minutes ++
      cell = document.createElement('div');
      cell.className = cellClass;
      button = createButton('+', function () {
        self.incrementTime('minutes', +1)
      })
      cell.appendChild(button)
      row.appendChild(cell)


      // Newline
      // element = document.createElement('div')
      // element.className = 'newline'
      editor.appendChild(row)

      // Time display
      element = document.createElement('div');
      element.className = 'time-display';
      element.innerHTML = 
            ((self.hasWeeks) ? '<div class="'+cellClass+'" id="weeks-display-'+self.sId+'">00</div>' : '') 
            +'\
            <div class="'+cellClass+'" id="days-display-'+self.sId+'">00</div>\
            <div class="'+cellClass+'" id="hours-display-'+self.sId+'">00</div>\
            <div class="'+cellClass+'" id="minutes-display-'+self.sId+'">00</div>' + 
            ((self.hasSeconds) ? '<div class="'+cellClass+'" id="seconds-display-'+self.sId+'">00</div>' : '') 
      row.appendChild(element);

      // Newline
      element = document.createElement('div');
      element.className = 'newline'
      editor.appendChild(element)

      row = document.createElement('selection');
      row.className = 'newline';

      // Add legent row
      // Weeks
      cell = document.createElement('div');
      cell.className = cellClass;
      cell.innerHTML = 'weeks';
      row.appendChild(cell)

      // Days
      cell = document.createElement('div');
      cell.className = cellClass;
      cell.innerHTML = 'days';
      row.appendChild(cell)

      // Hours
      cell = document.createElement('div');
      cell.className = cellClass;
      cell.innerHTML = 'hours';
      row.appendChild(cell)

      // Minutes
      cell = document.createElement('div');
      cell.className = cellClass;
      cell.innerHTML = 'minutes';
      row.appendChild(cell)

      // Newline
      // element = document.createElement('div');
      // element.className = 'newline'
      editor.appendChild(row)

      row = document.createElement('selection');
      row.className = 'newline';

      // Weeks --
      if (self.hasWeeks) {
        cell = document.createElement('div');
        cell.className = cellClass;
        button = createButton('-', function () {
          self.incrementTime('weeks', -1)
        })
        cell.appendChild(button)
        row.appendChild(cell)
      }

      // Days --
      cell = document.createElement('div');
      cell.className = cellClass;
      button = createButton('-', function () {
        self.incrementTime('days', -1)
      })
      cell.appendChild(button)
      row.appendChild(cell)


      // Hours --
      cell = document.createElement('div');
      cell.className = cellClass;
      button = createButton('-', function () {
        self.incrementTime('hours', -1)
      })
      cell.appendChild(button)
      row.appendChild(cell)

      // Minutes --
      cell = document.createElement('div');
      cell.className = cellClass;
      button = createButton('-', function () {
        self.incrementTime('minutes', -1)
      })
      cell.appendChild(button)
      row.appendChild(cell)

      editor.appendChild(row)

      container.appendChild(editor)
      this.updateTimeDisplay();


    }

    //copy the selected date to the input field
    this.fromBufferToInputValue = function () {

      this.theInput.value = renderIntervalStr(this.selectedTimeInterval, this.hasWeeks, this.hasSeconds);

      //make angular see the change
      
      var fakeEvent = document.createEvent('KeyboardEvent');
      fakeEvent.initEvent("change", true, false);
      this.theInput.dispatchEvent(fakeEvent);
    };


    this.init();
  }

  function applyTimeIntervalExtender(dateInput) {
    new timeIntervalExtender(dateInput);

    console.log('Applying extender: ', dateInput)
    //mark that it have extender installed:
    dateInput.classList.add('has-timeinterval-extender');
    dateInput.setAttribute('data-has-timeinterval-extender', 'true');
  }

  //will add the timeIntervalExtender to all inputs in the page
  function addTimeIntervalExtenderToInputs () {
    //get and loop all the input[type=date]s in the page that dont have "has-timeinterval-extender" class yet
    var dateInputs = document.querySelectorAll('input[data-x-type=time-interval]:not(.has-timeinterval-extender)');
    [].forEach.call(dateInputs, function (tiInput) {
      //call timeIntervalExtender function on the input
      applyTimeIntervalExtender(tiInput);
    });
  }

  var isTimeIntervalInput = function (element) {
    var inputType = element.getAttribute('data-x-type') || ''; //<- warning: for browsers which have no support for input[type=date] the value element.type will be "text"! but the attribute value for 'type' will be as specified!
    //~console.log(inputType)
    return inputType === 'time-interval';
  }

  //run the above code on any <input data-x-type='time-interval'> in the document, also on dynamically created ones 
  // addTimeIntervalExtenderToInputs();
  //this is also on mousedown event so it will capture new inputs that might joined to the dom dynamically
  document.getElementsByTagName('body')[0].addEventListener('focus', function (event) {
      var e = event || window.event, target = e.target;
      if (target && target.tagName === 'INPUT' 
          && isTimeIntervalInput(target) 
          && !target.hasAttribute('data-has-timeinterval-extender')) applyTimeIntervalExtender(target) 
  }, true);

  // Add delegated listener to handle status when editor group loses focus:
  document.getElementsByTagName('body')[0].addEventListener('focus', hideOpened, true) //<- pass true to ensure event visibility in nested elements


  // // Add delegated listener to handle status when editor group loses focus:
  // document.getElementsByTagName('body')[0].addEventListener('blur', function (e) {
  //   var editorsContainer, sId;
  //   e = e ? e : window.event;
  //   //~console.log('GLOBAL blur', e.target, e.relatedTarget)
  //   if (e.target && e.relatedTarget && e.target.getAttribute('data-interval-element-sid') !== e.relatedTarget.getAttribute('data-interval-element-sid')) {
  //     //~console.log(e.target.getAttribute('data-interval-element-sid'), e.relatedTarget.getAttribute('data-interval-element-sid'))
  //     sId = e.target.getAttribute('data-interval-element-sid');
  //     editorsContainer = document.getElementById('timeinterval-editor-group-'+sId);
  //     if (editorsContainer) {
  //       console.log('editor found, closing:', editorsContainer) 
  //       editorsContainer.style.display = 'none';
  //     }
  //   }
  // }, true) //<- pass true to ensure event visibility in nested elements

  // var _exported = {
  //   'applyTimeIntervalExtender': applyTimeIntervalExtender,
  //   'TimeIntervalExtender': timeIntervalExtender
  // }
  // window._timeinterval_input_element_extender_ = _exported;
  // return _exported; // <-- allow to use manual control on any input

}));