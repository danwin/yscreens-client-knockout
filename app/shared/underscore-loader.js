(function (root, factory) {
    if (typeof define === "function" && define.amd) { // AMD mode
        define(["underscore", "shared/underscore.ext"], factory);
    } else if (typeof exports === "object") { // CommonJS mode
    	var _ = require("underscore");
    	var _ = require('shared/underscore.ext');
    	// this module extends _, not export anything:
    	module.exports = factory(_);
    } else {
    	// Module does not define some globals, 
    	// so ignore returned value:
        factory(root._); // Plain JS, "rtl" is in window scope
    }
}(this, function (_) { return _}))


// define([
// 	'underscore', 
// 	'shared/underscore.ext' // <-- custom extensions ("mixins")
// 	], 
// 	function(_){
// 		return _;
// 	}
// );