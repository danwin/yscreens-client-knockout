// jest.unmock('shared/datetime');

const Iterator = require.requireActual('shared/iterator');

describe('Iterator', () => {

	describe('(1) constructor should accept specified types of arguments...', ()=>{

		it('(1.1) array...', ()=>{
			var obj = new Iterator([1,2,3]);
			var asList = obj.asList();
			expect(asList).toEqual([1,2,3])
		})

		it('(1.2) recursive function...', ()=>{
			var obj = new Iterator(function (item){if (item < 3) return item+1});
			var asList = obj.asList();
			expect(asList).toEqual([1,2,3])
		})

	})

	describe('(2)  sentinel should be the "cut-off" value (excluded)', ()=>{

		it('(2.1) for array...', ()=>{
			var sentinel = 4;
			var list = [1,2,3,4];
			var obj = new Iterator(list, sentinel);
			var buffer = [];
			var item;
			while (true) {
				item = obj.nextValue();
				if (item === Iterator.DONE) break
				buffer.push(item)
			}
			expect(buffer).toEqual([1,2,3])
		})

		it('(2.2) for expression...', ()=>{
			var sentinel = 4;
			var expression = function (item){return item+1};
			var obj = new Iterator(expression, sentinel);
			var buffer = [];
			var item;
			while (true) {
				item = obj.nextValue();
				if (item === Iterator.DONE) break
				buffer.push(item)
			}
			expect(buffer).toEqual([1,2,3])
		})
	})

	describe('(3) sentinel can be...', ()=>{

		it('(3.1) a "static" value...', ()=>{
			var sentinel = 4;
			var list = [1,2,3,4];
			var obj = new Iterator(list, sentinel);
			var asList = obj.asList();
			expect(asList).toEqual([1,2,3])
		})

		it('(3.2) a function which returns "true" to stop iterator ...', ()=>{
			var sentinel = function (item) {
				return item === 4;
			};
			var list = [1,2,3,4];
			var obj = new Iterator(list, sentinel);
			var asList = obj.asList();
			expect(asList).toEqual([1,2,3])
		})
	})

	describe('(4)  .nextValue() should run value of next item......', ()=>{

		it('(4.1) until "sentinel" item reached...', ()=>{
			var sentinel = 'STOP!';
			var obj = new Iterator([1,2,3,'STOP!',4,5,6], sentinel);
			var buffer = [];
			var item;

			while (obj.next().hasValue){
				buffer.push(obj.value())
			}

			// while (true) {
			// 	item = obj.nextValue();
			// 	if (item === Iterator.DONE) break
			// 	buffer.push(item)
			// }
			expect(buffer).toEqual([1,2,3])
		})

		it('(4.2) until "length" for array reached...', ()=>{
			var sentinel = 'item not in list';
			var obj = new Iterator([1,2,3], sentinel);
			var buffer = [];
			while (true) {
				item = obj.nextValue();
				if (item === Iterator.DONE) break
				buffer.push(item)
			}
			expect(buffer).toEqual([1,2,3])
		})
	})

	describe('(5) .forNext() should run "iteratee"...', ()=>{

		it('(5.1) until "sentinel" item reached...', ()=>{
			var sentinel = 'STOP!';
			var obj = new Iterator([1,2,3,'STOP!',4,5,6], sentinel);
			var buffer = [];
			while (obj.forNext(function (item, index) {
				buffer.push(item)
			})){;}
			expect(buffer).toEqual([1,2,3])
		})

		it('(5.2) until "length" for array reached...', ()=>{
			var sentinel = void 0;
			var obj = new Iterator([1,2,3,'STOP!',4,5,6], sentinel);
			var buffer = [];
			while (obj.forNext(function (item, index) {
				buffer.push(item)
			})){;}
			expect(buffer).toEqual([1,2,3,'STOP!',4,5,6])
		})
	})

	describe('(6) .asList() should return plain array...', ()=>{
		it('(6.1) ... which trimmed before first "sentinel" value', ()=>{
			var sentinel =  4;
			var list = [1,2,3,4, 5,6];
			var obj = new Iterator(list, sentinel);
			var asList = obj.asList();
			expect(asList).toEqual([1,2,3])			
		})

		it('(6.2) ... which trimmed when maxCount reached', ()=>{
			var sentinel = 'IGNORED!!!';
			var list = [1,2,3,4, 5,6];
			var maxCount = 3;
			var obj = new Iterator(list, sentinel);
			var asList = obj.asList(maxCount);
			expect(asList).toEqual([1,2,3])			
		})

	})

	describe('(7) details...', ()=>{

		it('initial value for "item" argument for the first call of a recursive expression should be null and "index" should be 0', ()=>{
			var expression = function (item, index){return {'result':item, 'i':index}};
			var obj = new Iterator(expression);
			var item = obj.nextValue();
			expect(item.result).toEqual(null)
			expect(item.i).toEqual(0)
		})
	})

	xdescribe('(8) nested iterator...', ()=>{

		it('iterator should sequentially iterate when items are Iterators itself (flatten)', ()=>{
			var childList1 = [1.1,1.2,1.3];
			var childList2 = [2.1,2.2,2.3, new Iterator([2.4])];
			var list = [1,new Iterator(childList1), new Iterator(childList2),3,4, 5,6];
			var obj = new Iterator(list);
			var asList = obj.asList();
			expect(asList).toEqual( [1, 1.1, 1.2, 1.3, 2.1, 2.2, 2.3, 2.4, 3, 4, 5, 6])			

		})
	})

})