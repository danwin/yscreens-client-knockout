jest.unmock('shared/datetime');

const 
	Pulse = require.requireActual('shared/pulse'),
	datetime = require.requireActual('shared/datetime'),
	Iterator = require.requireActual('shared/iterator');

jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000; // 10 second timeout

console.log('promise', Promise)

// Use fake timers!
jest.useFakeTimers();

function MockTimer() {
	var self = this;
	this.timeLeft = 0;
	this.calls = []
	this.setTimeout = function (action, timeout) {
		var timing = -(new Date()).getTime();
		var wrapper = function () {
			action();
			timing += (new Date()).getTime();
			timeLeft += timing;
		}
		self.timeLeft += timeout
		self.calls.push({action: action, timeout: timeout, timeLeft: self.timeLeft})
		setTimeout(action, 0)
		return 1;
	}
	this.mockClear = function () {
		this.timeLeft = 0;
		this.calls = [];
	}
}
var mockTimer = new MockTimer();
Pulse.schedulerFunc = mockTimer.setTimeout;

var handleException = function (e) {
	console.error('Exception occurs:', e)
}


describe('Pulse', () => {

	// beforeAll(() => {
	//   global.Promise = require.requireActual('promise');
	// });

	describe('(1) basic tests...', ()=>{

		beforeEach(()=>{
			// Use fake timers!
			// jest.useFakeTimers();
			// jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
			mockTimer.mockClear();
		})

		afterEach(()=>{
			jest.clearAllTimers();
			setTimeout.mockClear();
			// jest.resetAllMocks();		
			// jest.useFakeTimers();
		})

		it('(1.1) Pulse.wait() should wait X miliseconds then execute the subsequent code', ()=>{
			const 
				action = jest.fn(),
				TEST_INTERVAL = 999;

			var p = Pulse.wait(TEST_INTERVAL);

			expect(action).not.toBeCalled(); // <-- before timer - NOT to be called

			console.warn('1.1 - BEFORE TIMER:', p)

			p = p.then(action);

			// Set "time" to "0 + TEST_INTERVAL":
			jest.runTimersToTime(TEST_INTERVAL);

			console.warn('1.1 - AFTER TIMER:', p)

			// expect(action).toBeCalled(); // <-- on timer - TO BE called

			return p.then(()=>{
					console.warn('1.1 - p.then():', p)
				    expect(action).toBeCalled();
				    expect(mockTimer.calls[0].timeout).toEqual(TEST_INTERVAL)
				    expect(mockTimer.calls[0].timeLeft).toEqual(TEST_INTERVAL)
				    expect(mockTimer.timeLeft).toEqual(TEST_INTERVAL)
				    // expect(setTimeout.mock.calls[0][1]).toBe(TEST_INTERVAL);
				})
		    	//.catch(handleException)


		})

		it('(1.2) Pulse.runAsync() should resolve promise immediatelly with start of callback (not waiting for its execution)', ()=>{
			const 
				action = jest.fn(),
				nextAction = jest.fn();

			var p = Pulse.runAsyncPromise(action);
				
			console.warn('1.2 - BEFORE TIMER:', p)
			// Fast-forward until all timers have been executed

			expect(action).not.toBeCalled(); // <-- before timer - NOT to be called

			jest.runAllTimers();
			console.warn('1.2 - AFTER TIMER:', p)

			p = p.then(nextAction)

			expect(nextAction).not.toBeCalled(); // <-- after timer immediatelly - NOT to be called

			return p.then(()=>{

				    expect(action.mock.calls.length).toBe(1);
				    expect(nextAction.mock.calls.length).toBe(1);

				    expect(mockTimer.calls[0].timeout).toEqual(0)
				    expect(mockTimer.calls[0].timeLeft).toEqual(0)

				    // expect(setTimeout.mock.calls[0][1]).toBe(0);
				})
		    	//.catch(handleException)

		})


		xit('(1.2*) Pulse.wait().then(runAsync) should wait X miliseconds then execute the subsequent code', ()=>{
			const 
				callback1 = jest.fn(),
				TEST_INTERVAL = 999;

			var p = Pulse.wait(TEST_INTERVAL)
				.then(()=>{return Pulse.runAsync(callback1)});

			expect(callback1).not.toBeCalled(); // <-- before timer - NOT to be called

			console.warn('1.2* - BEFORE TIMER:', p)

			// Fast-forward until all timers have been executed
			// jest.runTimersToTime(TEST_INTERVAL);
			jest.runAllTimers();

			console.warn('1.2* - AFTER TIMER:', p)

			// expect(callback1).toBeCalled(); // <-- on timer - TO BE called

			return p.then(()=>{
					console.warn('1.2* - p.then():', p)
				    expect(callback1).toBeCalled();
				    expect(setTimeout.mock.calls[0][1]).toBe(TEST_INTERVAL);
				})
		    	//.catch(handleException)


		})



		it('(1.3) Pulse.runDeferred() should wait X milliseconds and resolve promise immediatelly with start of callback (not waiting for its execution)', ()=>{
			const 
				action = jest.fn(),
				nextAction = jest.fn(),
				TEST_INTERVAL = 333
				;

			var p = Pulse.runDeferred(action, TEST_INTERVAL)
				// .then(callback2)

			console.warn('1.3 - BEFORE TIMER:', p)

			p = p.then(nextAction);

			expect(nextAction).not.toBeCalled(); // <-- before timer - NOT to be called

			// Set "time" to "0 + TEST_INTERVAL":
			jest.runTimersToTime(TEST_INTERVAL);
			// jest.runOnlyPendingTimers();

			console.warn('1.3 - AFTER TIMER:', p)

			return p.then(()=>{
					// callback2();

				    expect(action.mock.calls.length).toBe(1);
				    expect(nextAction.mock.calls.length).toBe(1);
				    // expect(setTimeout.mock.calls[0][1]).toBe(TEST_INTERVAL);
				    expect(mockTimer.calls[0].timeout).toBeCloseTo(TEST_INTERVAL, 2)
				    expect(mockTimer.calls[0].timeLeft).toBeCloseTo(TEST_INTERVAL, 2)
				})
		    	//.catch(handleException)

		})

		it('(1.4) Pulse.repeatInterval() should wait X milliseconds and resolve promise immediatelly with start of callback (not waiting for its execution)', ()=>{
			var 
				action = jest.fn(),
				nextAction = jest.fn(),
				TEST_INTERVAL = 333,
				runCount = 3,
				// totalTime = 0,
				// startTiming = function () {
				// 	totalTime = -(new Date()).getTime();
				// },
				// stopTiming = function () {
				// 	totalTime += (new Date()).getTime();
				// }

				stopFunc = function () {
					return !(runCount--)
				};

			// startTiming()
			var p = Pulse.repeatInterval(action, TEST_INTERVAL, stopFunc)
				// .then(callback2)

			console.warn('1.4 - BEFORE TIMER:', p)

			// p = p.then(stopTiming);
			p = p.then(nextAction);

			expect(action).not.toBeCalled(); // <-- before timer - NOT to be called

			// Run all timers:
			jest.runAllTimers();
			// jest.runOnlyPendingTimers();

			console.warn('1.4 - AFTER TIMER:', p)

			return p.then(()=>{
					// expect(totalTime).toEqual(TEST_INTERVAL*3)
				    expect(action.mock.calls.length).toBe(3);
				    expect(nextAction.mock.calls.length).toBe(1);
				    expect(nextAction.mock.calls[0][0]).toBe(3); // <-- promise resolved with "count"

				    expect(mockTimer.calls[0].timeout).toEqual(0)
				    expect(mockTimer.calls[0].timeLeft).toEqual(0)

				    expect(mockTimer.calls[1].timeout).toEqual(TEST_INTERVAL)
				    expect(mockTimer.calls[1].timeLeft).toEqual(TEST_INTERVAL)

				    expect(mockTimer.calls[2].timeout).toEqual(0)
				    expect(mockTimer.calls[2].timeLeft).toEqual(TEST_INTERVAL)

				    expect(mockTimer.calls[3].timeout).toEqual(TEST_INTERVAL)
				    expect(mockTimer.calls[3].timeLeft).toEqual(TEST_INTERVAL*2)

				    expect(mockTimer.calls[4].timeout).toEqual(0)
				    expect(mockTimer.calls[4].timeLeft).toEqual(TEST_INTERVAL*2)

				    expect(mockTimer.calls[5].timeout).toEqual(TEST_INTERVAL)
				    expect(mockTimer.calls[5].timeLeft).toEqual(TEST_INTERVAL*3)

				    expect(mockTimer.timeLeft).toEqual(TEST_INTERVAL*3)

				    // expect(setTimeout.mock.calls[0][1]).toBe(0); // "async" setTimeout(..., 0)
				    // expect(setTimeout.mock.calls[1][1]).toBeLessThanOrEqual(TEST_INTERVAL); // <- TEST_INTERVAL - "execution time"
				    // expect(setTimeout.mock.calls[2][1]).toBe(0); // "async" setTimeout(..., 0)
				    // expect(setTimeout.mock.calls[3][1]).toBeLessThanOrEqual(TEST_INTERVAL);
				    // expect(setTimeout.mock.calls[4][1]).toBe(0); // "async" setTimeout(..., 0)
				    // expect(setTimeout.mock.calls[5][1]).toBeLessThanOrEqual(TEST_INTERVAL);
				})
		    	//.catch(handleException)

		})


		it('(1.5.1) Pulse.iterate() should map actions from queue to keyframe intervals in keyframes queue', ()=>{
			var keyframes = new Iterator([1000, 5000, 10000]);
			const 
				callback1 = jest.fn(),
				callback2 = jest.fn(),
				callback3 = jest.fn(),
				callback4 = jest.fn()
				;
			var actions = new Iterator([
				callback1,
				callback2,
				callback3
				])

			var p = Pulse.iterate(actions, keyframes)

		    jest.runAllTimers();

		    // Fast-forward until all timers have been executed

		    expect(callback1.mock.calls.length).toBe(1); //<--
		    expect(callback2.mock.calls.length).toBe(1); //<--
		    expect(callback3.mock.calls.length).toBe(1); //<--

		    // expect(mockTimer.calls[0].timeout).toEqual(0)
		    // expect(mockTimer.calls[0].timeLeft).toEqual(0)

		    expect(mockTimer.calls[1].timeout).toBeCloseTo(1000, 1)
		    expect(mockTimer.calls[1].timeLeft).toBeCloseTo(1000, 1)

		    // expect(mockTimer.calls[2].timeout).toEqual(0)
		    // expect(mockTimer.calls[2].timeLeft).toEqual(0)

		    expect(mockTimer.calls[3].timeout).toBeCloseTo(5000, 1)
		    expect(mockTimer.calls[3].timeLeft).toBeCloseTo(6000, 1)

		    // expect(mockTimer.calls[4].timeout).toEqual(0)
		    // expect(mockTimer.calls[4].timeLeft).toEqual(0)

		    expect(mockTimer.calls[5].timeout).toBeCloseTo(10000, 1)
		    expect(mockTimer.calls[5].timeLeft).toBeCloseTo(16000, 1)

		    // expect(setTimeout.mock.calls[0][1]).toBe(0); // "async" setTimeout(..., 0)
		    // expect(setTimeout.mock.calls[1][1]).toBe(1000);
		    // expect(setTimeout.mock.calls[2][1]).toBe(0); // "async" setTimeout(..., 0)
		    // expect(setTimeout.mock.calls[3][1]).toBe(5000);
		    // expect(setTimeout.mock.calls[4][1]).toBe(0); // "async" setTimeout(..., 0)
		    // expect(setTimeout.mock.calls[5][1]).toBe(10000);

		})

		it('(1.5.2) Pulse.iteratePromise() should map actions from queue to keyframe intervals in keyframes queue', ()=>{
			var keyframes = new Iterator([1000, 5000, 10000]);
			const 
				callback1 = jest.fn(),
				callback2 = jest.fn(),
				callback3 = jest.fn(),
				callback4 = jest.fn()
				;
			var actions = new Iterator([
				callback1,
				callback2,
				callback3
				])

			var p = Pulse.iteratePromise(actions, keyframes)

		    jest.runAllTimers();

		    return p.then(()=>{

				    // Fast-forward until all timers have been executed

				    expect(callback1.mock.calls.length).toBe(1); //<--
				    expect(callback2.mock.calls.length).toBe(1); //<--
				    expect(callback3.mock.calls.length).toBe(1); //<--

				    expect(mockTimer.calls[0].timeout).toEqual(0)
				    expect(mockTimer.calls[0].timeLeft).toEqual(0)

				    expect(mockTimer.calls[1].timeout).toEqual(1000)
				    expect(mockTimer.calls[1].timeLeft).toEqual(1000)

				    expect(mockTimer.calls[2].timeout).toEqual(0)
				    expect(mockTimer.calls[2].timeLeft).toEqual(0)

				    expect(mockTimer.calls[3].timeout).toEqual(5000)
				    expect(mockTimer.calls[3].timeLeft).toEqual(6000)

				    expect(mockTimer.calls[4].timeout).toEqual(0)
				    expect(mockTimer.calls[4].timeLeft).toEqual(0)

				    expect(mockTimer.calls[5].timeout).toEqual(10000)
				    expect(mockTimer.calls[5].timeLeft).toEqual(16000)

				    // expect(setTimeout.mock.calls[0][1]).toBe(0); // "async" setTimeout(..., 0)
				    // expect(setTimeout.mock.calls[1][1]).toBe(1000);
				    // expect(setTimeout.mock.calls[2][1]).toBe(0); // "async" setTimeout(..., 0)
				    // expect(setTimeout.mock.calls[3][1]).toBe(5000);
				    // expect(setTimeout.mock.calls[4][1]).toBe(0); // "async" setTimeout(..., 0)
				    // expect(setTimeout.mock.calls[5][1]).toBe(10000);
				})
		    	//.catch(handleException)

		})

		xit('(1.5) Pulse.runQueue() should execute mapped actions at appropriate time points', ()=>{

		})
	})

	describe('(1.1) constructor should support specified types of arguments...', ()=>{

		xit('', ()=>{


		})
	})
})