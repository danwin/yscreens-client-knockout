//datetime-helpers-test.js
'use strict'
// unmock to use the actual implementation of sum

// jest.unmock(vendorRoot+modules_underscore);
// jest.unmock('underscore');
jest.unmock('shared/datetime-helpers');

var lib = require.requireActual('shared/datetime-helpers');


xdescribe('fmtTimeInterval', () => {
  it('(1) should return "0" when interval iz sero', () => {

    expect(lib.fmtTimeInterval(0)).toEqual('0');

  });

  it('(2) should return "01mn" when interval iz 60*1000ms', () => {

    expect(lib.fmtTimeInterval(60*1000)).toEqual('01mn');

  });

  it('(3) should return "01h" when interval iz 3600*1000ms', () => {

    expect(lib.fmtTimeInterval(3600*1000)).toEqual('01h');

  });

  it('(4) should return "1d01h59s" when interval iz 3600*1000ms', () => {

    expect(lib.fmtTimeInterval(25*3600*1000 + 59*1000)).toEqual('1d01h59s');

  });

});


xdescribe('extractHoursMinutes', () => {
  it('(2.1) should return "[0,0]" when interval iz 0 and "zero" arg is undefined', () => {

    expect(lib.extractHoursMinutes(0)).toEqual([0,0]);

  });
});

xdescribe('extractWeekDay', () => {
  it('(3.1) should return "0" when interval iz 0 and "zero" arg is undefined', () => {

    expect(lib.extractWeekDay(0)).toEqual(0);

  });
});

xdescribe('extractWeekDayStr', () => {
  it('(4.1) should return "Sun" when interval iz 0 and "zero" arg is undefined', () => {

    expect(lib.extractWeekDayStr(0)).toEqual('Sun');

  });
  it('(4.2) should return "Mon" when interval iz (1*24+1)*3600*1000 (6th day, 23:00) and "zero" arg is undefined', () => {

    expect(lib.extractWeekDayStr((1*24+1)*3600*1000)).toEqual('Mon');

  });
  it('(4.3) should return "Tue" when interval iz (2*24+1)*3600*1000 (6th day, 23:00) and "zero" arg is undefined', () => {

    expect(lib.extractWeekDayStr((2*24+1)*3600*1000)).toEqual('Tue');

  });
});

xdescribe('extractWeekNumber', () => {
  it('(5.1) should return "0" when interval iz 0 and "zero" arg is undefined', () => {

    expect(lib.extractWeekNumber(0)).toEqual(0);

  });
  it('(5.2) should return "0" when interval iz "[1 day,...2day]" in milliseconds and "zero" arg is undefined', () => {

    expect(lib.extractWeekNumber(24*3600*1000)).toEqual(0);

  });
});

xdescribe('extractWeekNumberStr', () => {
  it('(6.1) should return "Week 1" when interval iz 0 and "zero" arg is undefined', () => {

    expect(lib.extractWeekNumberStr(0)).toEqual('Week 1');

  });
  it('(6.2) should return "Week 1" when interval iz "[1 day,...2day]" in milliseconds and "zero" arg is undefined', () => {

    expect(lib.extractWeekNumberStr(24*3600*1000)).toEqual('Week 1');

  });
});

xdescribe('extractMonthDate', () => {
  it('(7.1) should return 1 when interval iz 0 and "zero" arg is undefined', () => {

    expect(lib.extractMonthDate(0)).toEqual(1);

  });
});

xdescribe('extractMonth', () => {
  it('(8.1) should return 0 when interval iz 0 and "zero" arg is undefined', () => {

    expect(lib.extractMonth(0)).toEqual(0);

  });
});

xdescribe('extractMonthStr', () => {
  it('(9.1) should return "Jan" when interval iz 0 and "zero" arg is undefined', () => {

    expect(lib.extractMonthStr(0)).toEqual('Jan');

  });
});
