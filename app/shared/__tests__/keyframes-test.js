// jest.unmock('shared/datetime');

const Keyframes = require.requireActual('shared/rtl.keyframes');

describe('Keyframes', () => {

	describe('(1) resolveRange', ()=>{
		it('(1.1) should return unchanged [lo, hi] range when no clipping...', ()=>{
			var lo = 10, hi = 20, clipLo = void 0, clipHi = void 0; 
			var range = Keyframes.resolveRange(lo, hi, clipLo, clipHi);
			expect(range.lo).toEqual(lo)
			expect(range.hi).toEqual(hi)
		})

		it('(1.2) should return unchanged [lo, hi] range when clipping region contains range [lo, hi] ...', ()=>{
			var lo = 10, hi = 20, clipLo = lo-1, clipHi = hi+1; 
			var range = Keyframes.resolveRange(lo, hi, clipLo, clipHi);
			expect(range.lo).toEqual(lo)
			expect(range.hi).toEqual(hi)
		})

		it('(1.3) should return unchanged [lo, hi] range when clipping region is equal to range [lo, hi] ...', ()=>{
			var lo = 10, hi = 20, clipLo = lo, clipHi = hi; 
			var range = Keyframes.resolveRange(lo, hi, clipLo, clipHi);
			expect(range.lo).toEqual(lo)
			expect(range.hi).toEqual(hi)
		})

		it('(1.4) should replace lower bound of [lo, hi] with clippingLo  when "clipLo > lo"  ...', ()=>{
			var lo = 10, hi = 20, clipLo = lo + 1, clipHi = hi; 
			var range = Keyframes.resolveRange(lo, hi, clipLo, clipHi);
			expect(range.lo).toEqual(clipLo)
			expect(range.hi).toEqual(hi)
		})

		it('(1.5) should replace upper bound of [lo, hi] with clippingHi  when "clipHi < hi"  ...', ()=>{
			var lo = 10, hi = 20, clipLo = lo, clipHi = hi - 1; 
			var range = Keyframes.resolveRange(lo, hi, clipLo, clipHi);
			expect(range.lo).toEqual(lo)
			expect(range.hi).toEqual(clipHi)
		})

		it('(1.6) should replace entire [lo, hi] by [clipLo, clipHi]  when "clipLo > lo and clipHi < hi"  ...', ()=>{
			var lo = 10, hi = 20, clipLo = lo + 1, clipHi = hi - 1; 
			var range = Keyframes.resolveRange(lo, hi, clipLo, clipHi);
			expect(range.lo).toEqual(clipLo)
			expect(range.hi).toEqual(clipHi)
		})

		it('(1.7) should return "null" when initial range and clipping range are not intersecting and clipLo > hi...', ()=>{
			var lo = 10, hi = 20, clipLo = 100, clipHi = 200; 
			var range = Keyframes.resolveRange(lo, hi, clipLo, clipHi);
			expect(range).toEqual(null)
		})

		it('(1.8) should return "null" when initial range and clipping range are not intersecting and clipHi < lo...', ()=>{
			var lo = 10, hi = 20, clipLo = 1, clipHi = 2; 
			var range = Keyframes.resolveRange(lo, hi, clipLo, clipHi);
			expect(range).toEqual(null)
		})

		it('(1.9) should return "null" when lo >= hi regardless value of clipping range...', ()=>{
			var lo = 10, hi = 20; 
			expect(Keyframes.resolveRange(lo, lo)).toEqual(null)
			expect(Keyframes.resolveRange(hi, lo)).toEqual(null)
		})

		it('(1.10) should return "null" when clipLo >= cliphi regardless value of range...', ()=>{
			var lo = 10, hi = 20, clipLo = 1000, clipHi = 1; 
			var range = Keyframes.resolveRange(lo, hi, clipLo, clipHi);
			expect(range).toEqual(null)
		})
	})

	describe('(2) .iter, NO repetitions', ()=>{
		it('(2.1) should return iterator with initial range [lo, hi] when no repetitions and no clipping...', ()=>{
			var lo = 10, duration = 5, 
				period = null, numOfRepetitions = 0;
			var clipLo = void 0, clipHi = void 0; 
			var kf = Keyframes.keyframes(lo, duration, period, numOfRepetitions);
			var iter = kf.iter(clipLo, clipHi)
			var dump = iter.asList()
			expect(dump).toEqual([{'lo':lo, 'hi':lo+duration}])
		})

		it('(2.2) should return iterator with [clipLo, hi] range when clipping region intersects right bound of range [lo, hi] ...', ()=>{
			var lo = 10, duration = 5, 
				period = null, numOfRepetitions = 0;
			var clipLo = lo + 1, clipHi = lo + duration + 1; 
			var kf = Keyframes.keyframes(lo, duration, period, numOfRepetitions);
			var iter = kf.iter(clipLo, clipHi)
			var dump = iter.asList()
			expect(dump).toEqual([{'lo':clipLo, 'hi':lo+duration}])
		})

		it('(2.3) should return iterator with [lo, clipHi] range when clipping region intersects left bound of range [lo, hi] ...', ()=>{
			var lo = 10, duration = 5, 
				period = null, numOfRepetitions = 0;
			var clipLo = lo - 1, clipHi = lo + duration - 1; 
			var kf = Keyframes.keyframes(lo, duration, period, numOfRepetitions);
			var iter = kf.iter(clipLo, clipHi)
			var dump = iter.asList()
			expect(dump).toEqual([{'lo':lo, 'hi':clipHi}])
		})

	})


	describe('(3) .iter, WITH repetitions', ()=>{
		it('(3.1) should return iterator with N repetitions and unchanged range [lo + i*period, hi + i*period] when no clipping...', ()=>{
			var lo = 10, duration = 5, 
				period = 20, numOfRepetitions = 3;
			var clipLo = void 0, clipHi = void 0; 
			var kf = Keyframes.keyframes(lo, duration, period, numOfRepetitions);
			var iter = kf.iter(clipLo, clipHi)
			var dump = iter.asList()
			expect(dump).toEqual([
				{'lo':lo, 'hi':lo+duration},
				{'lo':lo + 1 * period , 'hi':lo+duration  + 1 * period},
				{'lo':lo + 2 * period , 'hi':lo+duration  + 2 * period}
			])
		})

		it('(3.2) should return iterator with N repetitions and clipped first occurence of range [lo + i*period, hi + i*period] when clipping intersects left bound of range...', ()=>{
			var lo = 10, duration = 5, 
				period = 20, numOfRepetitions = 3;
			var 
				clipLo = 12, 
				clipHi = void 0; 
			var kf = Keyframes.keyframes(lo, duration, period, numOfRepetitions);
			var iter = kf.iter(clipLo, clipHi)
			var dump = iter.asList()
			expect(dump).toEqual([
				{'lo':clipLo, 'hi':lo+duration},
				{'lo':lo + 1 * period , 'hi':lo+duration  + 1 * period},
				{'lo':lo + 2 * period , 'hi':lo+duration  + 2 * period}
			])
		})

		it('(3.3) should return iterator with N repetitions and drop the first occurence of range [lo + i*period, hi + i*period] when clipping does not contain the first occurence of range...', ()=>{
			var lo = 10, duration = 5, 
				period = 20, numOfRepetitions = 3;
			var 
				clipLo = lo + 5, 
				clipHi = void 0; 
			var kf = Keyframes.keyframes(lo, duration, period, numOfRepetitions);
			var iter = kf.iter(clipLo, clipHi)
			var dump = iter.asList()
			expect(dump).toEqual([
				// {'lo':clipLo, 'hi':lo+duration}, // <-- clipped region
				{'lo':lo + 1 * period , 'hi':lo+duration  + 1 * period},
				{'lo':lo + 2 * period , 'hi':lo+duration  + 2 * period}
			])
		})

		it('(3.4) should return iterator with repetitions where last occurence intersects with clipping region...', ()=>{
			var lo = 10, duration = 5, 
				period = 20, numOfRepetitions = 3;
			var 
				clipLo = void 0, 
				clipHi = 32; 
			var kf = Keyframes.keyframes(lo, duration, period, numOfRepetitions);
			var iter = kf.iter(clipLo, clipHi)
			var dump = iter.asList()
			expect(dump).toEqual([
				{'lo':lo, 'hi':lo+duration}, 
				{'lo':lo + 1 * period , 'hi': clipHi}
				// {'lo':lo + 2 * period , 'hi':lo+duration  + 2 * period} // <-- clipped region
			])
		})

	})

	xdescribe('test case when period<duration?', ()=>{})
	xdescribe('test case when period>0 but numOfRepetitions < 2?', ()=>{})
//---

})