(function(root, factory) {
	// var _modname = 'IO';
	if (typeof define === "function" && define.amd) { // AMD mode
		define(["underscore", "jquery", "knockout", "shared/message.bus", "datetime"], factory);
	} else if (typeof exports === "object") { // CommonJS mode
		var _ = require("underscore");
		var $ = require("jquery");
		var ko = require("knockout");
		var messageBus = require("shared/message.bus");
		var datetime = require("datetime");
		module.exports = factory(_, $, ko, messageBus, datetime);
	} else {
		// Use global class LocalDateTime
		factory(root._, root.$, root.ko, root.messageBus, root.datetime); // Plain JS
	}

}(this, function (_, $, ko, messageBus, datetime) {
	'use strict';

	var
		LocalDateTime = datetime.LocalDateTime,
		DateTimeInterval = datetime.DateTimeInterval;

	// Shortcuts for factories

	function ko_inject(attr, value){
		if (!_.isUndefined(ko[attr])) throw "ko."+attr+" already defined in 'ko' namespace!";
		ko[attr] = value;
	};

	ko_inject('_obs_', ko.observable);
	ko_inject('_obsA_', ko.observableArray);
	ko_inject('_obsC_', ko.computed);
	ko_inject('_fromObs_', ko.utils.unwrapObservable);

	// proxy for message bus in a "ko" namespace:
	// ko._notify_(event).tell(data)
	ko_inject('_notify_', messageBus);

	ko_inject('_observeChanges_', function (handlerOrMessage, propsArray, comment) {
		var hType = typeof handlerOrMessage,
			handler, message;

		if (hType === 'string') {
			var message = handlerOrMessage;
			handler = function () {
				console.log('NOTIFICATION', message);
				messageBus(message).tell();
			}
		} else if (hType === 'function') {
			handler = handlerOrMessage;
		} else {
			throw new Error(
				'Error in _observeChanges_: invalid type of argument "handlerOrMessage"');
		}

		_.each(propsArray, function (item) {

			if (ko.isObservable(item)) {
				// if item is observable:
				item.subscribe(handler);
				// console.log('Listening to observables: ', propsArray, comment);
			} else if (typeof item === 'string') {
				// if item is a message name:
				messageBus(item).listen(handler);
			} else {
				throw '_observeChanges_ error: item is not observable: '
					+ typeof item + ' at point: ' + comment;
			}
		});
	});

	ko_inject('_FieldFactory_', function (namespace, obsConstructor, extenders) {
		return function (
				name, // field name
				defValue, // default value, if not found in "args"
				args, // object which holds current values {name: value}
				itemFactory // constructor if item is instance of a custom class
				) {
			// arguments: namespace, name, defValue, args
			function unwrap(v) { return _.isFunction(v) ? v() : v; };
			var
				undefined,
				isComputed = (obsConstructor === ko.computed || obsConstructor === ko.pureComputed),
				isObsArray = (obsConstructor === ko.observableArray),
				argValue = (args) ? unwrap(args[name]) : undefined,
				value = isComputed ? defValue : (_.isUndefined(argValue) ? defValue : argValue),
				newField;

			value = unwrap(value); // in case if value is "observable"

			if (_.isUndefined(value))
				throw '_FieldFactory_ error: required value is missed for ' + name;

			// if( value === null && isObsArray) value = [];

			// instantiate items if value is array and if factory specified:
			if (typeof itemFactory === 'function') {
				if (_.isArray(value)) {
					var buffer = [];
					_.each(value, function (data) {
						buffer.push(itemFactory(data))
					});
					value = buffer;
				} else {
					if (isObsArray) {
						if (!value) {
							value = [];
						} else throw new Error('Invalid default value for array: ' + value)
					} else {
						// scalar value
						value = itemFactory (value);
					}
				}
			}

			// create or update observable field:
			if (ko.isObservable(namespace[name])) {
				// field exists and is observable:
				namespace[name] (value);
			} else {
				// create new field
				newField = obsConstructor(value);
				// Apply ko extenders if any:
				extenders = extenders || [];
				_.each(extenders, function (extender) {
					newField = newField.extend(extender);
				});
				namespace[name] = newField;
			}

			// return new observable field for chaining:
			return namespace[name]; // <- for chaining
		}
	});

	// creats read-only property, emulates observable behaviour on read operations
	ko_inject('_lookup_', // for read-only props, closure factory
		function (data){
			return function(){
				return data;
			}
		});

	ko_inject('_traverseTree_', function(root, tagarray, silent) {
			/*
					root, // object
					tagarray, // "path", ['prop1 = 'prop2'] means root['prop1']['prop2']
					silent // if true - do not raise error on intermediate "nulls"
			*/
			var
				current = root,
				handleErr = silent ? _TRACE_ : _ERR_,
				tag;

			for (var i=0, len = tagarray.length, len_1=len-1; i<len; i++) {

				tag = tagarray[i];

				if (_.isNull(current)) {
					handleErr('Warning: getNested: intermediate member is NULL: ', tag);
					return null;
				}
				if (_.isUndefined(current)) {
					handleErr('Warning: getNested: intermediate member is UNDEFINED: ', tag);
					return undefined;
				}
				// do not unwrap last object from observable (if i == len-2)
				current = (len_1 == i) ? current[tag] : ko._fromObs_(current[tag]);
			}
			return current;
		});

	ko_inject('_bindCollectionPropsTo_', function (Obj, childrenPropName) {
			var children = _.assertDefined(childrenPropName, 'bindCollectionPropsTo() error: childrenPropName is required!');
			// Ensure that children is observable array:
			if (!Obj[children] || !ko.isObservable(Obj[children]) || !Obj[children].splice ){
				console.log('parent dump ', Obj);
				throw new Error('bindCollectionPropsTo() error in parent object: "'+Obj._rtti+'": specified  "'
					+children+ '" property is not observable array: ' + typeof Obj[children]+ ' '+ko.isObservable(Obj[children]));
			}
			// add basic properties and methods for object (suppose it has .Items() observable array)

			Obj.selectedItem = ko._obs_(null);

			Obj.itemMoveUp = function (item) {
				// body...
			}

			Obj.itemMoveDown = function (item) {
				// body...
			}

			Obj.length = function () 			{ return Obj[children]().length;	} // Frames
			Obj.select = function (item) 		{ Obj.selectedItem(item);	}
			Obj.IsSelected = function (item) 	{ return ( Obj.selectedItem() === item )	}
			Obj.each = function (iterator)	{ _.each(Obj[children](), iterator);	}

			Obj.findFirst = function (rule) {
				// search frist item which satisfy for condition / rule:
				// {prop1:val1, prop2:val2, ...}
				// rule can contain any attr of Item (like Label, DomID, ...)
				var found = null, match;
				_.assertTrue(typeof rule == 'object',
					'Invalid argument type for .findFirst(): '+typeof(rule)+
					'. Allowed type: object.');

				_.find(Obj[children](), function (item) {
					// return false if found to stop iterations
					match = true;
					for (var name in rule) {
						if (_.isUndefined(item[name]))
							throw new Error('Error in .findFirst() - invalid rule name: '+name);
						match = match && (ko._fromObs_(item[name]) == rule[name]);
					}
					if (match) {
						found = item;
						return found; // do not continue iterations, item found
					}
				});
				return found; // return undefined to continue iterations...
			}
		});

	// EXTENDERS


	// Numeric-type extender for observables
	/*
	Sample usage:
	function AppViewModel(one, two) {
		this.myNumberOne = ko.observable(one).extend({ numeric: 0 });
		this.myNumberTwo = ko.observable(two).extend({ numeric: 2 });
	}

	ko.applyBindings(new AppViewModel(221.2234, 123.4525));
	*/



	ko.extenders.numeric = function(target, precision) {
		//create a writable computed observable to intercept writes to our observable
		var result = ko.pureComputed({
			read: target,  //always return the original observables value
			write: function(newValue) {
				var current = target(),
					roundingMultiplier = Math.pow(10, precision),
					newValueAsNum = Number.isNaN(newValue) ? 0 : parseFloat(+newValue),
					valueToWrite = Math.round(newValueAsNum * roundingMultiplier) / roundingMultiplier;

				//only write if it changed
				if (valueToWrite !== current) {
					// console.log('Writing value: ', valueToWrite, typeof valueToWrite);
					target(valueToWrite);
				} else {
					//if the rounded value is the same, but a different value was written, force a notification for the current field
					if (newValue !== current) {
						target.notifySubscribers(valueToWrite);
					}
				}
			}
		}).extend({ notify: 'always' });

		//initialize with current value to make sure it is rounded appropriately
		result(target());

		//return the new computed observable
		return result;
	};

	ko.extenders.ntfChange = function (target, eventName) {
		target.isDirty = ko.observable(false);
		target.originalValue = target();
		target.subscribe(function (newValue) {
			// use != not !== so numbers will equate naturally
			var changed = newValue != target.originalValue;
			target.isDirty(changed);
			messageBus(eventName).tell({sender: target, value: newValue});
		});
		return target;
	};

	ko.extenders.ntfListen = function (target, options) {
		var eventName = _.assertDefined( options.eventName ),
			newValueOrHandler = _.assertDefined( options.newValueOrHandler );
		if (typeof newValueOrHandler == 'function') {
			messageBus( eventName ).listen(newValueOrHandler);
		} else {
			_.assertTrue(_.isBoolean(target.peek()),
				'ko.ntfListen extender with constant instead of handler can be applied only to observable with boolean type!');
			messageBus(eventName).listen(function () {
				// assign value on event
				target(newValueOrHandler);
			});
		}
		return target;
	};


	/*
	Bindings
	 */

	// <input id="text" type="text" data-bind="numeric: number, value: number, valueUpdate: 'afterkeydown'">
	// source: http://stackoverflow.com/a/17060212

	ko.bindingHandlers.numeric = {
	    init: function (element, valueAccessor) {
			var _stopEvent = function (e) { //Stop the event
			  //e.cancelBubble is supported by IE - this will kill the bubbling process.
			  e.cancelBubble = true;
			  e.returnValue = false;

			  //e.stopPropagation works in Firefox.
			  if (e.stopPropagation) {
			    e.stopPropagation();
			    e.preventDefault();
			  }
			  return false
			}

	        element.addEventListener("keydown", function (evt) {
    	        var event = evt ? evt : window.event;
		        var key = event.charCode || event.keyCode || event.which;

	            // Allow: backspace, delete, tab, escape, and enter
	            if (key == 46 || key == 8 || key == 9 || key == 27 || key == 13 ||
	                // Allow: Ctrl+A
	                (key == 65 && event.ctrlKey === true) ||
	                // Allow: . ,
	                (key == 188 || key == 190 || key == 110) ||
	                // Allow: home, end, left, right
	                (key >= 35 && key <= 39)) {
	                // let it happen, don't do anything
	                return true;
	            } else {
	                // Ensure that it is a number and stop the keypress
	                if (event.shiftKey || (key < 48 || key > 57) && (key < 96 || key > 105)) {
	                    // event.preventDefault();
	                    return _stopEvent(event)
	                }
	                return true;
	            }
	        });
	    }
	};

	// fileUpload binding (from: http://stackoverflow.com/a/35800382)
	ko.bindingHandlers.fileUpload = {
			init: function (element, valueAccessor) {
					$(element).change(function () {
							valueAccessor()(element.files[0]);
					});
			},
			update: function (element, valueAccessor) {
					if (ko.unwrap(valueAccessor()) === null) {
							$(element).wrap('<form>').closest('form').get(0).reset();
							$(element).unwrap();
					}
			}
	};


ko.bindingHandlers.hidden = {
    update: function(element, valueAccessor) {
        ko.bindingHandlers.visible.update(element, function() { return !ko.utils.unwrapObservable(valueAccessor()); });
    }
};

ko.bindingHandlers.clickToEdit = {
    init: function(element, valueAccessor) {
        var observable = valueAccessor(),
            link = document.createElement("a"),
            input = document.createElement("input"),
            staticText = document.createElement("span");

        link.innerHTML = '&nbsp;<i class="fa fa-pencil"></i>';
        link.setAttribute('href','javascript:void(0)');
        input.className = 'inline-edit';
        staticText.className = 'inline-edit-value'
        element.appendChild(staticText);
        element.appendChild(link);
        element.appendChild(input);

        observable.editing = ko.observable(false);

        ko.applyBindingsToNode(link, {
            // text: observable,
            hidden: observable.editing,
            click: function() { observable.editing(true); }
        });

        ko.applyBindingsToNode(input, {
            value: observable,
            visible: observable.editing,
            hasfocus: observable.editing,
            // valueUpdate: 'afterkeydown',
            event: {'keyup': function (data, e) {
            	var evt = e || window.event,
            		keyCode = evt.keyCode || evt.charCode || evt.which;
            		if (keyCode === 13 || keyCode === 27) {
            			evt.target.blur();
            		}
            		return true;
            }}
        });

        ko.applyBindingsToNode(staticText, {
        	text: observable,
            hidden: observable.editing
        })
    }
};

// ko.applyBindings({ message: ko.observable("Welcome to camp!") });

	// Date binding (better works together with a modified version of "html5-simple-date-input-polyfill")
	var _2digits = function (value) {
		return (value < 10) ? '0'+value : value
	}

	var raise = function (message) {throw new Error(message)};

	// use isNaN polyfill
	!(typeof Number.isNaN == 'function') ||
	  (Number.isNaN = function (value) {
	    return value !== null // Number(null) => 0
	      && (value != value // NaN != NaN
	        || +value != value // Number(falsy) => 0 && falsy == 0...
	      )
	});

	var toInt = function (value, minVal, maxVal, errMessage) {
		var result = parseInt(value)
		minVal = minVal || 0;
		maxVal = maxVal || Number.MAX_VALUE;
		if (Number.isNaN(result) || result < minVal || result >maxVal )
			throw new TypeError(errMessage)
	}

	var _getBufferDate = function (dataType, valueUnwrapped) {
		switch (dataType) {
			case 'number':
			case 'string':
				return new Date(valueUnwrapped);
			case 'Date':
				return valueUnwrapped;
			default:
				throw new TypeError('timestampValue binding error: unsupported type of value: '+dataType);
		}
	}

	var _updateDateObservable = function (observable, dateBuffer, dataType) {
		switch (dataType) {
			case 'Date':
				observable(dateBuffer); break;
			case 'number':
				observable(dateBuffer.getTime()); break;
			case 'string':
				observable(dateBuffer.toString()); break;
		}
	}

	ko.bindingHandlers['jsDate'] = {
		init: function(element, valueAccessor, allBindingsAccessor) {
			var value = valueAccessor();
			var errorMessageObservable = allBindingsAccessor().errorMessageObservable
									? ko.utils.unwrapObservable(allBindingsAccessor().errorMessageObservable) : function () {};
			var valueUnwrapped = ko.utils.unwrapObservable(value);
			var useType = allBindingsAccessor().useType;
			var valueType = (valueUnwrapped instanceof Date) ? 'Date' : ((valueUnwrapped instanceof LocalDateTime) ? 'LocalDateTime' : useType);

			// if (element.tagName == 'INPUT' || element.tagName == 'SELECT')
			if (!valueType) {
				if (!useType) throw new TypeError('jsDate binding requires defined value of observable or a value type specified in "useType" attribute');
				valueType = window[useType];
			}
			// if (window._datetime_input_element_extender_ && typeof window._datetime_input_element_extender_.applyDateTimeExtender === 'function') {
			// 	window._datetime_input_element_extender_.applyDateTimeExtender(element);
			// }

			element.addEventListener('change', function () {
				var
					valueUnwrapped = ko.utils.unwrapObservable(value)
					// Parse text value with LocalDateTime.parse to handle "patial" strings with a time-only values:
					,dateBuffer
					//new valueType(LocalDateTime.parse(element.value))
					,testNumValue; //<-- contains NaN when strng canot be converted to a date
				if (valueType === 'Date') {
					dateBuffer = (new LocalDateTime(element.value)).toDate();
				} else if (valueType === 'LocalDateTime') {
					dateBuffer = new LocalDateTime(element.value)
				} else {
					throw new TypeError('Invalid type of observable value');
				}
				testNumValue = dateBuffer.getTime();
				// console.log('@@@ Value FROM input, text: ', element.value, 'dateBuffer', dateBuffer, 'parse~d date:', (new LocalDateTime(element.value)).toString())
				if (Number.isNaN(testNumValue)) {
					errorMessageObservable('Invalid time value');
					console.log('Error updating time observable:', element.value)
				} else {
					// console.log(value, valueUnwrapped)
					value(dateBuffer)
				}
			})
		},
		update: function(element, valueAccessor, allBindingsAccessor, viewModel) {
			var value = valueAccessor();
			var valueUnwrapped = ko.utils.unwrapObservable(value);
			var newValue;
			var buffer = [];

		    var inputType = element.getAttribute('data-x-type') || element.getAttribute('type'); //<- warning: for browsers which have no support for input[type=date] the value element.type will be "text"! but the attribute value for 'type' will be as specified!

		    var hasDateEditor = ('date,datetime,datetime-local'.split(',').indexOf(inputType) >-1);
		    var hasTimeEditor = ('datetime,datetime-local,time'.split(',').indexOf(inputType) >-1);
		    var hasDateTimeEditor = hasDateEditor && hasTimeEditor;

		    if (hasDateTimeEditor) {
		    	newValue = (new LocalDateTime(valueUnwrapped)).toDelimitedString(true); // true: omitSeconds
		    } else if (hasDateEditor) {
		    	newValue = (new LocalDateTime(valueUnwrapped)).toDelimitedDateString();
		    } else if (hasTimeEditor) {
		    	newValue = (new LocalDateTime(valueUnwrapped)).toDelimitedTimeString(true); // true: omitSeconds
		    } else {
		    	newValue = '<binding error>'
		    }

		    element.value = newValue;

			// // If element has "html5-simple-date-input-polyfill" extender applied:
			// if (element.hasAttribute('data-has-dateinput-extender')) {
			// 	var event = new CustomEvent('setDateValue', { 'detail': {'date': valueUnwrapped} });
			// 	element.dispatchEvent(event);
			// 	console.log('@@@ Value TO input', valueUnwrapped);
			// } else {
			// 	// Without extender:
			// 	if (['date', 'datetime-local'].indexOf(element.type) > -1) {
			// 		buffer = [
			// 			[
			// 				valueUnwrapped.getFullYear(),
			// 				valueUnwrapped.getMonth(),
			// 				valueUnwrapped.getDate()
			// 			].join('-')
			// 		]
			// 	}
			// 	if (['datetime-local', 'time'].indexOf(element.type) > -1) {
			// 		buffer.push([
			// 			valueUnwrapped.getHours(),
			// 			valueUnwrapped.getMinutes(),
			// 			valueUnwrapped.getSeconds()
			// 			].join(':'))
			// 	}
			// 	element.value = buffer.join(' ');
			// }
		}
	}

	/**
	 * Compatible with DateTimeInterval from "datetime.js" module
 	 */
	ko.bindingHandlers['jsTimeInterval'] = {
		init: function(element, valueAccessor, allBindingsAccessor) {
			var value = valueAccessor();
			var errorMessageObservable = allBindingsAccessor().errorMessageObservable
									? ko.utils.unwrapObservable(allBindingsAccessor().errorMessageObservable) : function () {};
			// if (element.tagName == 'INPUT' || element.tagName == 'SELECT')

			// if (window._timeinterval_input_element_extender_ && typeof window._timeinterval_input_element_extender_.applyTimeIntervalExtender === 'function') {
			// 	window._timeinterval_input_element_extender_.applyTimeIntervalExtender(element);
			// } else {
			// 	throw new Error('"jsTimeInterval" binding requires "_timeinterval_input_element_extender_"')
			// }

			element.addEventListener('change'/*'datevaluechange'*/, function (e) {
				var
					// evt = e || window.event,
					valueUnwrapped = ko.utils.unwrapObservable(value)
					// ,dateBuffer = new DateTimeInterval(evt.detail.value)
					,dateBuffer = new DateTimeInterval(element.value)
					,testNumValue = dateBuffer.module(); //<-- contins NaN when strng canot be converted to a date
				console.log('@@@ Value changed', dateBuffer)
				if (Number.isNaN(testNumValue)) {
					errorMessageObservable('Invalid time value');
					console.log('Error updating time observable:', element.value)
				} else {
					console.log(value, valueUnwrapped)
					value(dateBuffer)
				}
			})
		},
		update: function(element, valueAccessor, allBindingsAccessor, viewModel) {
			var value = valueAccessor();
			var valueUnwrapped = ko.utils.unwrapObservable(value);
			var forceAll = element.hasAttribute('data-x-show-all-parts')
			element.value = (new DateTimeInterval(valueUnwrapped).toPersistentString(forceAll))
			// var event = new CustomEvent('setDateValue', { 'detail': {'interval': valueUnwrapped} });
			// console.log('@@@ Dispathicng event: ', element.dispatchEvent(event));
		}
	}



	ko.bindingHandlers['timestampValue'] = {

		init: function(element, valueAccessor, allBindingsAccessor) {
			var IYEAR = 0, IMONTH=1, IDATE=2, IHOURS = 0, IMINUTES = 1, ISECONDS = 2;
			// <input type="time">, which part modified by input (hours, minutes, seconds):
			var timeEditor = allBindingsAccessor().timeEditor
									? ko.utils.unwrapObservable(allBindingsAccessor().timeEditor) : null;
			var dataType = allBindingsAccessor().dataType
									? ko.utils.unwrapObservable(allBindingsAccessor().dataType) : 'Date';
			var errorMessageObservable = allBindingsAccessor().errorMessageObservable
									? ko.utils.unwrapObservable(allBindingsAccessor().errorMessageObservable) : function () {};

			if (element.tagName == 'INPUT' || element.tagName == 'SELECT') element.onchange = function() {
				var dateObservable = valueAccessor();
				var valueUnwrapped = ko.utils.unwrapObservable(dateObservable);
				var dateBuffer = _getBufferDate(dataType, valueUnwrapped);

				var value = element.value;
				var args, year, month, date, hours, minutes, seconds;

				if (!value) return

				var eType = element.type, eExtType = element.getAttribute('data-input-type');
				if ('date' === eType || 'date' === timeEditor || 'date' === eExtType) {
					try {
						args = value.split('-');
						year = toInt(args[IYEAR], 1970, null, 'Invalid year value: '+args[IYEAR]);
						month = toInt(args[IMONTH], 1, 12, 'Invalid month value: '+args[IMONTH]) - 1;
						date = toInt(args[IDATE], 1, 31, 'Invalid date value: '+args[IDATE])

						dateBuffer.setFullYear(year);
						dateBuffer.setMonth(month);
						dateBuffer.setDate(date)
					} catch (e) {
						errorMessageObservable('timestampValue binding warning: cannot convert to date: ' + e.message)
					}
				} else if ('time' === eType || 'time' === timeEditor || 'time' === eExtType) {
					try {
						var args = value.split(':');
						hours = toInt(args[IHOURS], 0, 23, 'Invalid hours value: '+args[IHOURS])
						minutes = toInt(args[IMINUTES], 0, 59, 'Invalid minutes value: '+args[IMINUTES])
						seconds = toInt(args[ISECONDS] || 0, 0, 59, 'Invalid seconds value: '+args[ISECONDS])

						dateBuffer.setHours(hours);
						dateBuffer.setMinutes(minutes);
						dateBuffer.setSeconds(seconds);
					} catch (e) {
						errorMessageObservable('timestampValue binding warning: cannot convert to time: '+e.message);
					}
				} else if ('hours' === timeEditor || 'hours' === eExtType) {
					try {
						hours = toInt(value, 0, 23, 'Invalid hours value: '+value)
						dateBuffer.setHours(hours);
					} catch (e) {
						errorMessageObservable('timestampValue binding warning: cannot convert to hours: '+e.message);
					}
				} else if ('minutes' === timeEditor || 'minutes' === eExtType) {
					try {
						minutes = toInt(value, 0, 59, 'Invalid minutes value: '+value)
						dateBuffer.setMinutes(minutes);
					} catch (e) {
						errorMessageObservable('timestampValue binding warning: cannot convert to minutes: '+e.message);
					}
				} else if ('seconds' === timeEditor || 'seconds' === eExtType) {
					try {
						seconds = toInt(value, 0, 59, 'Invalid seconds value: '+value)
						dateBuffer.setSeconds(seconds);
					} catch (e) {
						errorMessageObservable('timestampValue binding warning: cannot convert to seconds: '+e.message);
					}
				} else {
						console.error('Invalid input for timestampValue binding:', eType, eExtType)
						throw new Error('Invalid input type for timestampValue binding');
				}

				// switch (element.type) {
				// 	case 'date':
				// 		try {
				// 			var args = value.split('-');
				// 			dateBuffer.setFullYear(parseInt(args[IYEAR]))
				// 			dateBuffer.setMonth(parseInt(args[IMONTH] - 1))
				// 			dateBuffer.setDate(parseInt(args[IDATE]))
				// 		} catch (e) {
				// 			// Date format is incomplete?
				// 			console.warn('timestampValue binding warning: cannot convert to date: ', value, 'value ignored');
				// 		}
				// 		break
				// 	case 'month':
				// 	case 'week':
				// 		throw new Error('Not implemented')
				// 		break
				// 	case 'time':
				// 		try {
				// 			switch (element.getAttribute('data-time-fragment')) {
				// 				case 'hours':
				// 					dateBuffer.setHours(parseInt(value)); break
				// 				case 'minutes':
				// 					dateBuffer.setMinutes(parseInt(value)); break
				// 				case 'seconds':
				// 					dateBuffer.setSeconds(parseInt(value)); break
				// 				default:
				// 					throw new Error('timestampValue binding for input[type="time"] requires attribute "data-time-fragment=hours|minutes|seconds"')
				// 			}
				// 			break
				// 		} catch (e) {
				// 			// Date format is incomplete?
				// 			console.warn('timestampValue binding warning: cannot convert to time: ', value, 'value ignored');
				// 		}
				// 	default:
				// 		throw new Error('Invalid input type for timestampValue binding');
				// }
				_updateDateObservable (dateObservable, dateBuffer, dataType)
			}
		},

		update: function(element, valueAccessor, allBindingsAccessor, viewModel) {
			var value = valueAccessor();
			var valueUnwrapped = ko.utils.unwrapObservable(value);
			var timeEditor = allBindingsAccessor().timeEditor
									? ko.utils.unwrapObservable(allBindingsAccessor().timeEditor) : null;
			var dataType = allBindingsAccessor().dataType
									? ko.utils.unwrapObservable(allBindingsAccessor().dataType) : 'Date';
			var dateBuffer = _getBufferDate(dataType, valueUnwrapped);
			var output = '';

			var fmtDate = function () {
				return [
					_2digits(dateBuffer.getFullYear()),
					_2digits(dateBuffer.getMonth() + 1),
					_2digits(dateBuffer.getDate())
				].join('-');
			}
			var fmtTime = function () {
				return [
					_2digits(dateBuffer.getHours()),
					_2digits(dateBuffer.getMinutes()),
					_2digits(dateBuffer.getSeconds())
				].join(':');
			}

			if (valueUnwrapped) {

				var eType = element.type, eExtType = element.getAttribute('data-input-type'), isZero = (dateBuffer.getTime() === 0);

				if ('date' === eType || 'date' === timeEditor || 'date' === eExtType) {
					output = fmtDate();
				} else if ('time' === eType || 'time' === timeEditor || 'time' === eExtType) {
					output = fmtTime()
				} else if ('hours' === timeEditor || 'hours' === eExtType) {
					output = dateBuffer.getHours();
				} else if ('minutes' === timeEditor || 'minutes' === eExtType) {
					output = dateBuffer.getMinutes();
				} else if ('seconds' === timeEditor || 'seconds' === eExtType) {
					output = dateBuffer.getSeconds();
				} else {
						console.error('Invalid input for timestampValue binding:', eType, eExtType)
						throw new Error('Invalid input type for timestampValue binding');
				}

				if (element.tagName == 'INPUT' || element.tagName == 'SELECT') {
					element.value = (dateBuffer.getTime() === 0) ? '' : output
				} else {
					element.textContent = fmtDate();
				}
			}
		}

	};


	// // Date-Time combo component
	// // <div data-bind="component:{name:'datetime-combo',params:{datetime:$root.dObservable}}"></div>
	// ko.components.register('datetime-combo', {
	// 	'viewModel': function (params) {
	// 		var
	// 			dataType = params.dataType || 'Date',
	// 			dateObservable = params['value'],
	// 			errorMessageObservable = params.errorMessageObservable || function () {};

	// 		if (!dateObservable) throw new Error('"datetime-combo" component error: "params.dateObservable" attribute is missed!');

	// 		this.datetime = dateObservable; // <-- make accessible from template

	// 		this.hours = ko.pureComputed({
	// 			'read': function () {
	// 				var valueUnwrapped = ko.utils.unwrapObservable(dateObservable)
	// 				var dateBuffer = _getBufferDate(dataType, valueUnwrapped)
	// 				return (dateBuffer) ? _2digits (dateBuffer.getHours()) : '';
	// 			},
	// 			'write': function (value) {
	// 				var
	// 					valueUnwrapped = ko.utils.unwrapObservable(dateObservable)
	// 					,dateBuffer = _getBufferDate(dataType, valueUnwrapped)
	// 					,hours;
	// 				if (value !== void 0) {
	// 					try {
	// 						hours = parseInt(value);
	// 						if (Number.isNaN(hours) || hours <0 || hours > 23) throw new Error('Invalid time value')
	// 						dateBuffer.setHours(hours)
	// 						_updateDateObservable(dateObservable, dateBuffer, dataType)
	// 					} catch (e) {
	// 						errorMessageObservable(e.message)
	// 					}
	// 				}
	// 			},
	// 			'owner': this
	// 		});

	// 		this.minutes = ko.pureComputed({
	// 			'read': function () {
	// 				var valueUnwrapped = ko.utils.unwrapObservable(dateObservable)
	// 				var dateBuffer = _getBufferDate(dataType, valueUnwrapped)
	// 				return (dateBuffer) ? _2digits (dateBuffer.getMinutes()) : '';
	// 			},
	// 			'write': function (value) {
	// 				var
	// 					valueUnwrapped = ko.utils.unwrapObservable(dateObservable)
	// 					,dateBuffer = _getBufferDate(dataType, valueUnwrapped)
	// 					,minutes;
	// 				if (value !== void 0) {
	// 					try {
	// 						minutes = parseInt(value);
	// 						if (minutes <0 || minutes > 59) throw new Error('Invalid time value')
	// 						dateBuffer.setMinutes(minutes)
	// 						_updateDateObservable(dateObservable, dateBuffer, dataType)
	// 					} catch (e) {
	// 						errorMessageObservable(e.message)
	// 					}
	// 				}
	// 			},
	// 			'owner': this
	// 		});

	// 		this.lkpHours = _.range(0,24).map(function (value) {
	// 			return {'label': _2digits(value), 'value':value }
	// 		});

	// 		this.lkpMinutes = _.range(0,60).map(function (value) {
	// 			return {'label': _2digits(value), 'value':value }
	// 		});

	// 	},

	// 	'template': '<div class="date-time-combo">\
	// 		<input type="date" class="date-input" size="10" maxlength="10" data-bind="timestampValue:datetime,timeEditor:\'date\'">\
	// 		<select data-bind="options:$component.lkpHours,optionText:\'label\',optionsValue:\'value\',value:$component.hours"></select>\
	// 			:\
	// 		<select data-bind="options:$component.lkpMinutes,optionText:\'label\',optionsValue:\'value\',value:$component.minutes"></select>\
	// 	</div>'
	// });

	/*
				<input type="time" data-bind="timestampValue:datetime,timeEditor:\'hours\'">\
			<input type="time" data-bind="timestampValue:datetime,timeEditor:\'minutes\'">
	 */

	// Receipt from: http://www.knockmeout.net/2013/06/knockout-debugging-strategies-plugin.html
		(function() {
			var existing = ko.bindingProvider.instance;

				ko.bindingProvider.instance = {
						nodeHasBindings: existing.nodeHasBindings,
						getBindings: function(node, bindingContext) {
							var bindings;
							try {
								 bindings = existing.getBindings(node, bindingContext);
							}
							catch (ex) {
								 if (window.console && console.error) {
									 console.error("binding error", ex.message, node, bindingContext);
								 }
							}

							return bindings;
						}
				};

		})();



}))