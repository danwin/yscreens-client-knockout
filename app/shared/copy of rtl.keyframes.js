//rtl.keyframes.js
(function (root, factory) {
	var _modname = 'rtl_keyframes';
    if (typeof define === "function" && define.amd) { // AMD mode
        define(["underscore", "iterator", "datetime"], factory);
    } else if (typeof exports === "object") { // CommonJS mode
        module.exports = factory(
        	require("underscore"), 
        	require("iterator"), 
        	require("datetime"));
    } else {
        root[_modname] = factory(root._, root.iter, root.datetime); // Plain JS
    }
}(this, function (_, iterator, datetime) {

	var LocalDateTime = datetime.LocalDateTime;
	var DateTimeInterval = datetime.DateTimeInterval;
	var iter = iterator.iter;

	// input object - array with numbers and / or LocalDateTime, Intervals, generators, iterators,

	function isDefined(value) {
		return ! (value === void 0 || value === null)
	}
	Keyframes.resolveRange = function (lo, hi, clipLo, clipHi) {
		if (isDefined(lo) && isDefined(hi)) {
			var hasLoLimit = isDefined(clipLo), hasHiLimit = isDefined(clipHi);
			// Align against "clipLo" limit, if any:
			var left = 	isDefined(clipLo) ? ((lo < clipLo) ? clipLo : lo) : lo;
			// Align against "clipHi" limit, if any:
			var right = isDefined(clipHi) ? ((hi > clipHi) ? clipHi : hi) : hi;

			// Ranges have no inersection:
			if (hasLoLimit && hi < clipLo || hasHiLimit && lo > clipHi) return null;
			// Return region if left, right defines valid, non-empty region:
			return (left < right) ?  {'lo': left, 'hi': right} : null;
		} else {throw new Error('"resolveRange" - mandatory argument is undefined [lo, hi]: ' + lo + ', ' + hi)}
	}

	/**
	 * Factory function
	 * @param  {number|LocalDateTime|DateTimeInterval} lo               [description]
	 * @param  {number|DateTimeInterval} duration         [description]
	 * @param  {number|undefined} period           [description]
	 * @param  {number|undefeined} numOfRepetitions [description]
	 * @return {Keyframes}                  Instance of Keyframes object
	 */
	Keyframes.keyframes = function (lo, duration, period, numOfRepetitions, children) {
		return new Keyframes({
			'lo': lo,
			'duration': duration,
			'period': period,
			'numOfRepetitions': numOfRepetitions,
			'children': children
		})
	}

	// Keyframes, Keyframes, COMPLEMENT, use recurrent expressions to enumerate nested... function(){ loop with iterator? }

	// Load from JS object / JSON
	function Keyframes(data) {
		data = data || {};
		this.lo = data.lo || 0;
		this.duration = (data.duration || 0);
		this.hi = this.lo + this.duration;
		this.period = data.period || null;
		this.numOfRepetitions = isDefined(data.numOfRepetitions) ? data.numOfRepetitions : null;
		this.children = (data.children) 
			? _.map(data.children, function (itemData) {return new Keyframes(itemData)}) 
			: null;
	}
	Keyframes.prototype.toJS = function () {
		var data = {
			'lo': this.lo,
			'duration': this.duration
		}

		if (this.period) data['period'] = this.period
		if (this.numOfRepetitions) data['numOfRepetitions'] = this.numOfRepetitions
		if (this.children) data['children'] = _.map(this.children, function (c) {return c.toJS()})

		return data;
	}

	Keyframes.prototype.isEndless = function () {
		return (this.period !== null && this.numOfRepetitions === null)
	}

	Keyframes.prototype.lastPoint = function () {
		if (this.period) {
			if (this.numOfRepetitions) {
				// periodical + has limited num of repetitions:
				return this.lo + (this.numOfRepetitions * this.period);
			}
			return NaN; // <-- periodical, endless
		}
		return this.hi; // <-- single occurence, the rightmost point is a "hi"
	}

	// !!! arithmetic + and .add() duality
	Keyframes.prototype.iter = function (clippingLo, clippingHi) {
		var lo = this.lo;
		var hi = this.hi;

		var duration = this.duration;

		var period = this.period;
		var numRepeat = this.numOfRepetitions || false;
		var indexOffset = 0; // <-- offset when keaframes periodical and first occurences are clipped
		
		var itemRange = Keyframes.resolveRange(lo, hi, clippingLo, clippingHi);
		var firstVisibleProjection = itemRange;

		var noLoRestriction = (clippingLo === void 0 || clippingLo === null);
		var hasLoRestriction = !noLoRestriction;
		var noHiRestriction = (clippingHi === void 0 || clippingHi === null);


		var hasLoRestriction = isDefined(clippingLo);
		var hasHiRestriction = isDefined(clippingHi);

		var windowing = clippingLo !== void 0 || clippingHi !== void 0;

		if (period === null) { /* Single range only: */

			// Define iterator by finite array:
			return iter( itemRange === null ? [] : [itemRange])

		} else { /* Has repetitions: */

			var sentinelFunc, recursiveExpression;

			// Deternine the first "visible" non-empty range
			// 1. Try the first item:
			if (firstVisibleProjection === null) { // First item is out of clipping range entirely
				if (hasLoRestriction) {
					// 1. First item is on left, try another occurences on the right
					if (this.isEndless() || this.lastPoint() > clippingLo) {

						// // [1]
						// var i_lo = Math.ceil((clippingLo - hi)/period);
						// var i_hi = (hasHiRestriction) ? Math.floor((clippingHi - lo)/period) : Number.MAX_VALUE;
						// i_eff = Math.min(i_lo, i_hi)
						// console.log('i_eff found',i_lo, i_hi, 'i_eff: ', i_eff);
						// lo = lo + period * i_eff;
						// hi = hi + period * i_eff;
						// numRepeat -= i_eff;
						// console.log('updated values: lo:', lo, 'hi', hi, 'numRepeat', numRepeat);


						// [2]
						var offsetLocal = (clippingLo - hi) % period;
						var offsetGlobal = (offsetLocal > 0) 
							? clippingLo + offsetLocal
							: clippingLo + offsetLocal + period;
						indexOffset = Math.floor(offsetGlobal / period);

						var phase = indexOffset * period;
						// lo += indexOffset * period;
						// hi += indexOffset * period;

						console.log('offsetLocal', offsetLocal, 'offsetGlobal', offsetGlobal, 'indexOffset', indexOffset, 'new lo:', lo + phase, 'new hi:', hi + phase)

						// // Find the first "hi" point inside the clipping region:
						// // var hi0 = clippingLo + (clippingLo - lo) % period;
						// var hi0 = clippingLo + (clippingLo - hi) % period;
						// // find the related left bound (even if it outside the clipping range):
						// var lo0 = hi - duration;
						// !!! index also changed???? 
						firstVisibleProjection = Keyframes.resolveRange(lo + phase, hi + phase, clippingLo, clippingHi);
					} // all repetitions are outside clipping range (on the "left")
					if (!firstVisibleProjection) return iter([]) // <-- clipping range lower that "period - duration"? first visible item is on the right of clipping range 
				} else {
					return iter([]) // <-- first item is on the right of clipping range or the whole range is empty
				}
			}

			sentinelFunc = function (item, index) {
				index += indexOffset;
				// console.log('sentinelFunc: ', index, numRepeat)
				return (item === null) || (numRepeat && (index >= numRepeat))
			}

			recursiveExpression = function (item, index) {
				index += indexOffset;
				if (index === indexOffset) 
					return firstVisibleProjection;
				else {
					// Note: always count against initial lo, hi, because the first item can be partially clipped!
					return Keyframes.resolveRange(lo + index * period, hi + index * period, clippingLo, clippingHi);
					// return Keyframes.resolveRange(item.lo + period, item.hi + period, null, clippingHi);
				}
			}

			return iter(recursiveExpression, sentinelFunc)

		}

		//---


	}

	// // ----- 8< -----
	// Keyframes.prototype.childrenIter = function (argument) {
	// 	// body...
	// }
	// while (parentKeyframes.next().hasValue) {
	// 	...
	// 	// Parent interval:
	// 	var r = parentKeyframes.value() // <---
	// 	childIter = children.iter(r.lo, r.hi) 
	// 	while (childIter.next().hasValue) {
	// 		// Child interval:
	// 		rCh = childIter.value()
	// 		// ... nested loop
	// 	}
	// }
	// ----- 8< -----

	// ******************************************
	// to-do: 1) offset children, 2) enum children in __iter__(), see "clipping" also
	// flatten collection? (from tree?) NO

	// ******************************************


	// DRAFTS : Pulse, conpare with "pulse.js"?

	// Pulse.wait = function (intervalMs) {
	// 	return new Promise(function (resolve, reject) {
	// 		setTimeout(resolve, +intervalMs)
	// 	})
	// }
	// Pulse.runAsync = function (func, intervalMs) {
	// 	function bless() {
	// 		setTimeout(func, 0)
	// 	}
	// 	return Pulse.wait(intervalMs || 0).then(bless)
	// }

	// Pulse.runQueue = function (iterator, actionQueue) {
	// 	function runAfter(intervalMs) {
	// 		Pulse.runAsync(+intervalMs, actionQueue.nextValue())
	// 	}
	// 	return new Promise(function (resolve, reject) {
	// 		var action;
	// 		try {
	// 			while (iterator.forNext(Pulse.wait)) {
	// 				action = actionQueue.nextValue()
	// 				Pulse.runAsync(action)
	// 			}
	// 			resolve(true)
	// 		} catch (e) {
	// 			reject(e)
	// 		}
	// 	})
	// }

	// function Pulse (argument) {}

	// DRAFTS of keyframes?
	
	// var Keyframes = function (array, options) {
	// 	var self = this;


	// 	self.items = [];
	// 	// Object must have "nextFor" method?, function must receive dattime argument
	// 	self.generators = [];

	// 	self.unwrap = function (array) {
	// 		_.each(array, function (item) {
	// 			if (item instanceof DateTimeInterval) {

	// 			} else if (typeof item === 'function' || typeof item = 'object') {
	// 				self.generators.push(item)
	// 			}
	// 		})
	// 	}

	// 	self.next = function () {
	// 		// body...
	// 	}	

	// 	/**
	// 	 * Find the current keyframes for specified moment
	// 	 * @return {[type]} [description]
	// 	 */
	// 	self.find = function (datetime) {
	// 		var keyframes = _.find()
	// 	}	

	// }


	return Keyframes;
}))