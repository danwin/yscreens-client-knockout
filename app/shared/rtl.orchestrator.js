(function (root, factory) {
	var _modname = 'rtl_orchestrator';
    if (typeof define === "function" && define.amd) { // AMD mode
        define(["underscore", "datetime", "rtl.engine"], factory);
    } else if (typeof exports === "object") { // CommonJS mode
        module.exports = factory(require("underscore"), require("datetime"), require("rtl.engine"));
    } else {
        root[_modname] = factory(root._, root.datetime, root.rtl_player); // Plain JS
    }
}(this, function (_, datetime, Player) {
	var 
		DateTimeInterval = datetime.DateTimeInterval,
		LocalDateTime = datetime.LocalDateTime;
/*
		self.fromJS = function (jsObject) {
			var html = jsObject.scene,
				motion = jsObject.motion;

			console.log('JSON:', jsObject );

			self.stop(); // pause the scheduled activity if any
			self.setScene(html, self.targetSelector);

			for (var i=0, len=motion.length; i<len; i++) {
				self.addBehavior(motion[i]);
				// console.log('Adding :', motion[i] );
			}
		}
//============ schedule rendering:
			compiled = {
				'start': 	self.Start.peek(),
				'duration': self.Duration.peek(),
				'scene': geometry,
				'motion': keyframes // unwrap array - it contain single item 
			};
//========== sample JSON:
---JSON--- {"start":"20170118T093818","duration":10800000,"scene":"<!-- Stream: ptsll90uukbr91psk1yis (start) --> <!-- slide --> <section id=\"null\" class=\"schedule-frame\"></section> <!-- slide end --> <!-- Stream: ptsll90uukbr91psk1yis (end) -->","motion":[{"id":null,"lo":0,"hi":7200000}]}
 */

 	var testJS =  {
 		"start":"20170119T202018",
 		"duration":10800000,
 		"scene":"<!-- Stream: ptsll90uukbr91psk1yis (start) --> <!-- schedule-frame \"item 1\"\" start --> <section id=\"bm156cce8pux1gp3kfzvh\" class=\"schedule-frame\"></section> <!-- schedule-frame stop --> <!-- Stream: ptsll90uukbr91psk1yis (end) -->",
 		"motion":[{"id":"bm156cce8pux1gp3kfzvh","mediaId":"wk1gosm3i1ahjg9ya6zfw","lo":0,"hi":7200000}]}

 	function Orchestrator(targetDomID, options) {
 		options = options || {};
 		this.DomID = targetDomID;
 		// argument: playback ID, returns promise
 		this.contentRequestDispatcher = options.contentRequestDispatcher;
 		this.observerInterval = options.observerInterval || 1000;

 		// contains keyframes;
 		this.players = {}; // domId-> player instance
 		this.loadedKeyframes = {}; // 
 		this.startTime = null;
 		this.duration = null;
 	}

 	Orchestrator.workingRange = new DateTimeInterval({'weeks': 2}).module();

 	Orchestrator.prototype.observeTasksList = function () {
 		var self = this;
 		var startTime = self.startTime;
 		var record;
 		var distance;
 		_.each(self.loadedKeyframes, function (kfRecord, id) {
	 		var now = LocalDateTime.now();
 			distance = startTime - now;
 			console.log('>>>startTime', startTime)
 			console.log('>>>now', now)
 			console.log('observeTasksList', distance, distance + kfRecord.hi <= Orchestrator.workingRange)
 			if (distance < 0) {
 				// handle case with "past" start
 				console.log('distance is negative', distance, startTime.toString(), now.toString())
 			} else if (distance + kfRecord.hi <= Orchestrator.workingRange) {
 				// ^ Check the most "distant" point is in pragrammable range:
 				console.log('==== observeTasksList, self:', self)
 				self.createAnimatedQueue(kfRecord);
 				// Mark as scheduled
 				kfRecord.inQueue = true;
 			} else console.log('unknow case')
 		})
 		return true;
 	}

 	Orchestrator.prototype.fromJS = function(jsObject) {
		var 
			self = this,
			html = jsObject.scene,
			startTime = new LocalDateTime(jsObject.start),
			duration = jsObject.duration,
			motion = jsObject.motion,
			rootDOM = document.getElementById(this.DomID);
		// [1] setup DOM
		console.log('DomID', this.DomID)
		rootDOM.innerHTML = html;
		// [2] parse keyframes
		return this.parseMotion(startTime, duration, motion)
			.then(function () {
				self.startTime = startTime;
				self.duration = duration;
				return true
			})
 	};

 	Orchestrator.prototype.fromJSON = function (serializedStr) {
 		var jsObject = JSON.parse(serializedStr)
 		return this.fromJS(jsObject)
 	}

 	Orchestrator.prototype.parsePlaylistInstance = function (kfRecord) {
 		var self = this;
		// each plain record contains {'id': id, 'mediaId': mediaId, 'lo': slot.lo, 'hi': slot.hi}
		return self.contentRequestDispatcher(kfRecord.mediaId)
			.then(function (response) {
				var playerInstance = new Player('#'+kfRecord.id);
				var js = response['js'];
				// create new player and associate it with DOM contaner with content:
	 			self.players[kfRecord.id] = playerInstance;
	 			self.loadedKeyframes[kfRecord.id] = kfRecord
				console.log('json is:', js);
				playerInstance.fromJS(js);
				return true;
			})
 	}

 	Orchestrator.prototype.parseMotion = function (start, duration, motion) {
 		
 		var queue = [], record;

 		for (var i = 0, len = motion.length; i < len; i++) {
 			console.log('parseMotion, record:::', motion[i])
 			record = motion[i];
 			queue.push(this.parsePlaylistInstance(record))
 		}
 		return Promise.all(queue);

 		/////

 		// [1] create player instances
 		// [2] schedule triggers
 	}

 	Orchestrator.prototype.createAnimatedQueue = function (record) {
 		// create variables environment for closure here...
 		var self = this;
 		var now = new LocalDateTime(new Date());
 		var interval = self.startTime - now
 		var itemId = record.id;
 		var playerInstance = self.players[itemId]
 		var domNode = document.getElementById(itemId);

 		console.log('createAnimatedQueue', record)

 		// Trigger START
 		setTimeout(function () {
 			console.log('triggers: START [fired]', itemId, domNode, playerInstance)
 			domNode.style.display = '';
 			playerInstance.run()
 		}, interval + record.lo)
 		console.log('scheduling triggers: START', interval + record.lo, itemId, domNode, playerInstance)

 		// Trigger STOP
 		setTimeout(function () {
 			console.log('triggers: STOP [fired]', itemId, domNode, playerInstance)
 			domNode.style.display = 'none';
 			playerInstance.stop()
 		}, interval + record.hi)
 		console.log('scheduling triggers: STOP', interval + record.hi, itemId, domNode, playerInstance)

 	}

//  	Orchestrator.prototype.observe = function () {

// 		// see idea source: http://stackoverflow.com/a/8795434
// 		// function redraw() {
// 		//     var interval = 30000;

// 		//     // work out current frame number
// 		//     var now = new Date().getTime();
// 		//     var frame = Math.floor(now / interval) % 2; // 0 or 1

// 		//     // do your stuff here
// 		//     .. some time passes

// 		//     // retrigger
// 		//     now = new Date().getTime();
// 		//     Pulse.setTrigger(redraw, interval - (now % interval));
// 		// }

// 		// redraw();

// // ***
// // 	Sample ations are:
// 			// action: function () {document.getElementById('ID').style.display="block"}
// 			// action: function () {document.getElementById('ID').style.display=""}
// // Then run iterator
// 			// var p = Pulse.iterate(actions, keyframes)
// // ***

//  		var self = this;
//  		var observer = function () {
// 	 		var now = new LocalDateTime(new Date());
// 	 		var record;
// 	 		var distance;
// 	 		for (var i = this.programmes.length - 1; i >= 0; i--) {
// 	 			record = this.programmes[i]
// 	 			// Ignore records which already scheduled:
// 	 			if (record.inQueue) continue;
// 	 			distance = record.start - now;
// 	 			if (distance < 0) {
// 	 				// handle case with "past" start
// 	 			} else if (distance <= Orchestrator.workingRange) {
// 	 				setTimeout(Orchestrator.createAnimatedQueue(record), distance)
// 	 				// Mark as scheduled
// 	 				record.inQueue = true;
// 	 			}
// 	 		}
//  		}

//  	}
	
	//---

	var eventEmitter = function (argument) {
		// body...
	}

	console.log("Orchestrator")
	return Orchestrator;


	
}))