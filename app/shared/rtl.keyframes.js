//rtl.keyframes.js
(function (root, factory) {
	var _modname = 'rtl_keyframes';
    if (typeof define === "function" && define.amd) { // AMD mode
        define(["underscore", "iterator", "datetime"], factory);
    } else if (typeof exports === "object") { // CommonJS mode
        module.exports = factory(
        	require("underscore"), 
        	require("iterator"), 
        	require("datetime"));
    } else {
        root[_modname] = factory(root._, root.iter, root.datetime); // Plain JS
    }
}(this, function (_, iterator, datetime) {

	var LocalDateTime = datetime.LocalDateTime;
	var DateTimeInterval = datetime.DateTimeInterval;
	var iter = iterator.iter;

	// input object - array with numbers and / or LocalDateTime, Intervals, generators, iterators,

	function isDefined(value) {
		return ! (value === void 0 || value === null)
	}
	/**
	 * Returns region [x1, x2] which is intersection of 2 regions: [lo, hi] and [clipLo, clipHi]
	 * @param  {[type]} lo     [description]
	 * @param  {[type]} hi     [description]
	 * @param  {[type]} clipLo [description]
	 * @param  {[type]} clipHi [description]
	 * @return {[type]}        [description]
	 */
	Keyframes.resolveRange = function (lo, hi, clipLo, clipHi) {
		if (isDefined(lo) && isDefined(hi)) {
			var hasLoLimit = isDefined(clipLo), hasHiLimit = isDefined(clipHi);
			// Align against "clipLo" limit, if any:
			var left = 	isDefined(clipLo) ? ((lo < clipLo) ? clipLo : lo) : lo;
			// Align against "clipHi" limit, if any:
			var right = isDefined(clipHi) ? ((hi > clipHi) ? clipHi : hi) : hi;

			// Ranges have no inersection:
			if (hasLoLimit && hi < clipLo || hasHiLimit && lo > clipHi) return null;
			// Return region if left, right defines valid, non-empty region:
			return (left < right) ?  {'lo': left, 'hi': right} : null;
		} else {throw new Error('"resolveRange" - mandatory argument is undefined [lo, hi]: ' + lo + ', ' + hi)}
	}

	/**
	 * Factory function
	 * @param  {number|LocalDateTime|DateTimeInterval} lo               [description]
	 * @param  {number|DateTimeInterval} duration         [description]
	 * @param  {number|undefined} period           [description]
	 * @param  {number|undefeined} numOfRepetitions [description]
	 * @return {Keyframes}                  Instance of Keyframes object
	 */
	Keyframes.keyframes = function (lo, duration, period, numOfRepetitions, children) {
		return new Keyframes({
			'lo': lo,
			'duration': duration,
			'period': period,
			'numOfRepetitions': numOfRepetitions,
			'children': children
		})
	}

	// Keyframes, Keyframes, COMPLEMENT, use recurrent expressions to enumerate nested... function(){ loop with iterator? }

	// Load from JS object / JSON
	function Keyframes(data) {
		data = data || {};
		this.lo = data.lo || 0;
		this.duration = (data.duration || 0);
		this.hi = this.lo + this.duration;
		this.period = data.period || null;
		this.numOfRepetitions = isDefined(data.numOfRepetitions) ? data.numOfRepetitions : null;
		this.children = (data.children) 
			? _.map(data.children, function (itemData) {return new Keyframes(itemData)}) 
			: null;
	}
	Keyframes.prototype.toJS = function () {
		var data = {
			'lo': this.lo,
			'duration': this.duration
		}

		if (this.period) data['period'] = this.period
		if (this.numOfRepetitions) data['numOfRepetitions'] = this.numOfRepetitions
		if (this.children) data['children'] = _.map(this.children, function (c) {return c.toJS()})

		return data;
	}

	Keyframes.prototype.isEndless = function () {
		return (this.period !== null && this.numOfRepetitions === null)
	}

	// Keyframes.prototype.isRecursive = function() {
	// 	return (this.period !== null && this.numOfRepetitions > 1)
	// };

	Keyframes.prototype.lastPoint = function () {
		if (this.period) {
			if (this.numOfRepetitions) {
				// periodical + has limited num of repetitions:
				return this.lo + (this.numOfRepetitions * this.period);
			}
			return NaN; // <-- periodical, endless
		}
		return this.hi; // <-- single occurence, the rightmost point is a "hi"
	}

	// !!! arithmetic + and .add() duality
	Keyframes.prototype.iter = function (clipLo, clipHi) {
		var lo = this.lo;
		var hi = this.hi;

		var duration = this.duration;

		var period = this.period;
		var numRepeat = this.numOfRepetitions || false;
		var indexOffset = 0; // <-- offset when keyframes periodical and first occurences are clipped
		
		var itemRange = Keyframes.resolveRange(lo, hi, clipLo, clipHi);
		var firstVisibleProjection = itemRange;

		if (period === null) { /* Single range only: */

			// Define iterator by finite array:
			return iter( itemRange === null ? [] : [itemRange])

		} else { /* Has repetitions: */

			var sentinelFunc, recursiveExpression;

			// Deternine the first "visible" non-empty range
			// 1. Try the first item:
			if (firstVisibleProjection === null) { // First item is out of clipping range entirely
				if (isDefined(clipLo)) {
					// 1. First item is on left, try another occurences on the right
					if (this.isEndless() || this.lastPoint() > clipLo) {

						// [2]
						var offsetLocal = (clipLo - hi) % period;
						var offsetGlobal = (offsetLocal > 0) 
							? clipLo + offsetLocal
							: clipLo + offsetLocal + period;
						indexOffset = Math.floor(offsetGlobal / period);

						var phase = indexOffset * period;

						// console.log('offsetLocal', offsetLocal, 'offsetGlobal', offsetGlobal, 'indexOffset', indexOffset, 'new lo:', lo + phase, 'new hi:', hi + phase)

						firstVisibleProjection = Keyframes.resolveRange(lo + phase, hi + phase, clipLo, clipHi);
					} // all repetitions are outside clipping range (on the "left")
					if (!firstVisibleProjection) return iter([]) // <-- clipping range lower that "period - duration"? first visible item is on the right of clipping range 
				} else {
					return iter([]) // <-- first item is on the right of clipping range or the whole range is empty
				}
			}

			sentinelFunc = function (item, index) {
				index += indexOffset;
				// console.log('sentinelFunc: ', index, numRepeat)
				return (item === null) || (numRepeat && (index >= numRepeat))
			}

			recursiveExpression = function (item, index) {
				index += indexOffset;
				if (index === indexOffset) 
					return firstVisibleProjection;
				else {
					// Note: always count against initial lo, hi, because the first item can be partially clipped!
					return Keyframes.resolveRange(lo + index * period, hi + index * period, clipLo, clipHi);
					// return Keyframes.resolveRange(item.lo + period, item.hi + period, null, clipHi);
				}
			}

			return iter(recursiveExpression, sentinelFunc)

		}

		//---


	}

	Keyframes.prototype.iterFilter = function(rangesIter) {
		var self = this,
			clipIter = rangesIter, 
			clip = null,
			ownIter = null, SENTINEL = null,
			recursiveExpression = function (item, index) {
				while (clip || clipIter.next().hasValue) {
					clip = clipIter.value()
					ownIter = ownIter || self.iter(clip.lo, clip.hi)
					if (ownIter.next().hasValue) 
						return ownIter.value();
					ownIter = null
					clip = null
				}
				return SENTINEL
			};
		return iter(recursiveExpression, SENTINEL)
	};

	// Keyframes.prototype.multiplyIters = function (clipIters) {
	// 	// repeatIn
	// 	var areas = clipKeyframes.


	// 	var lo = this.lo;
	// 	var hi = this.hi;

	// 	var duration = this.duration;

	// 	var period = this.period;
	// 	var numRepeat = this.numOfRepetitions || false;
	// 	var indexOffset = 0; // <-- offset when keyframes periodical and first occurences are clipped
		
	// 	var itemRange = Keyframes.resolveRange(lo, hi, clipLo, clipHi);
	// 	var firstVisibleProjection = itemRange;

	// 	if (period === null) { /* Single range only: */

	// 		// Define iterator by finite array:
	// 		return iter( itemRange === null ? [] : [itemRange])

	// 	} else { /* Has repetitions: */

	// 		var sentinelFunc, recursiveExpression;

	// 		// Deternine the first "visible" non-empty range
	// 		// 1. Try the first item:
	// 		if (firstVisibleProjection === null) { // First item is out of clipping range entirely
	// 			if (isDefined(clipLo)) {
	// 				// 1. First item is on left, try another occurences on the right
	// 				if (this.isEndless() || this.lastPoint() > clipLo) {

	// 					// [2]
	// 					var offsetLocal = (clipLo - hi) % period;
	// 					var offsetGlobal = (offsetLocal > 0) 
	// 						? clipLo + offsetLocal
	// 						: clipLo + offsetLocal + period;
	// 					indexOffset = Math.floor(offsetGlobal / period);

	// 					var phase = indexOffset * period;

	// 					// console.log('offsetLocal', offsetLocal, 'offsetGlobal', offsetGlobal, 'indexOffset', indexOffset, 'new lo:', lo + phase, 'new hi:', hi + phase)

	// 					firstVisibleProjection = Keyframes.resolveRange(lo + phase, hi + phase, clipLo, clipHi);
	// 				} // all repetitions are outside clipping range (on the "left")
	// 				if (!firstVisibleProjection) return iter([]) // <-- clipping range lower that "period - duration"? first visible item is on the right of clipping range 
	// 			} else {
	// 				return iter([]) // <-- first item is on the right of clipping range or the whole range is empty
	// 			}
	// 		}

	// 		sentinelFunc = function (item, index) {
	// 			index += indexOffset;
	// 			// console.log('sentinelFunc: ', index, numRepeat)
	// 			return (item === null) || (numRepeat && (index >= numRepeat))
	// 		}

	// 		recursiveExpression = function (item, index) {
	// 			index += indexOffset;
	// 			if (index === indexOffset) 
	// 				return firstVisibleProjection;
	// 			else {
	// 				// Note: always count against initial lo, hi, because the first item can be partially clipped!
	// 				return Keyframes.resolveRange(lo + index * period, hi + index * period, clipLo, clipHi);
	// 				// return Keyframes.resolveRange(item.lo + period, item.hi + period, null, clipHi);
	// 			}
	// 		}

	// 		return iter(recursiveExpression, sentinelFunc)

	// 	}

	// 	//---


	// }


	// // ----- 8< -----
	// Keyframes.prototype.childrenIter = function (argument) {
	// 	// body...
	// }
	// while (parentKeyframes.next().hasValue) {
	// 	...
	// 	// Parent interval:
	// 	var r = parentKeyframes.value() // <---
	// 	childIter = children.iter(r.lo, r.hi) 
	// 	while (childIter.next().hasValue) {
	// 		// Child interval:
	// 		rCh = childIter.value()
	// 		// ... nested loop
	// 	}
	// }
	// ----- 8< -----

	// ******************************************
	// to-do: 1) offset children, 2) enum children in __iter__(), see "clipping" also
	// flatten collection? (from tree?) NO

	// ******************************************


	// DRAFTS : Pulse, conpare with "pulse.js"?

	// Pulse.wait = function (intervalMs) {
	// 	return new Promise(function (resolve, reject) {
	// 		setTimeout(resolve, +intervalMs)
	// 	})
	// }
	// Pulse.runAsync = function (func, intervalMs) {
	// 	function bless() {
	// 		setTimeout(func, 0)
	// 	}
	// 	return Pulse.wait(intervalMs || 0).then(bless)
	// }

	// Pulse.runQueue = function (iterator, actionQueue) {
	// 	function runAfter(intervalMs) {
	// 		Pulse.runAsync(+intervalMs, actionQueue.nextValue())
	// 	}
	// 	return new Promise(function (resolve, reject) {
	// 		var action;
	// 		try {
	// 			while (iterator.forNext(Pulse.wait)) {
	// 				action = actionQueue.nextValue()
	// 				Pulse.runAsync(action)
	// 			}
	// 			resolve(true)
	// 		} catch (e) {
	// 			reject(e)
	// 		}
	// 	})
	// }

	// function Pulse (argument) {}

	// DRAFTS of keyframes?
	
	// var Keyframes = function (array, options) {
	// 	var self = this;


	// 	self.items = [];
	// 	// Object must have "nextFor" method?, function must receive dattime argument
	// 	self.generators = [];

	// 	self.unwrap = function (array) {
	// 		_.each(array, function (item) {
	// 			if (item instanceof DateTimeInterval) {

	// 			} else if (typeof item === 'function' || typeof item = 'object') {
	// 				self.generators.push(item)
	// 			}
	// 		})
	// 	}

	// 	self.next = function () {
	// 		// body...
	// 	}	

	// 	/**
	// 	 * Find the current keyframes for specified moment
	// 	 * @return {[type]} [description]
	// 	 */
	// 	self.find = function (datetime) {
	// 		var keyframes = _.find()
	// 	}	

	// }


	return Keyframes;
}))