// Damaru Campan Bell Rhythm
(function (root, factory) {
    if (typeof define === "function" && define.amd) { // AMD mode
        define(["datetime", "iterator"], factory);
    } else if (typeof exports === "object") { // CommonJS mode
        module.exports = factory(require("datetime"), require("iterator"));
    } else {
        root['Pulse'] = factory(root.datetime, root.Iterator); // Plain JS
    }
}(this, function (datetime, Iterator) {

	if (window.Promise === void 0) throw new Error(
		'Pulse requires promises! Use a newer browser or a promise polyfill!')


/*
Player protocol

		self.fromJS = function (js) {
			var html = js.scene,
				motion = js.motion;
...
 */


	function timer(initialValue) {
		var timer = {_time: +(new Date()) + (initialValue || 0)};
		timer.valueOf = function () {
			return +(new Date()) - timer._time
		}
		return timer;
	}

	/*
	CLASS-level properties
	 */

	// Constants
	Pulse.ERROR_DELAY_NEGATIVE = -2;
	Pulse.ERROR_DELAY_OVERFLOW = -1;
	Pulse.ERROR_OUT_OF_RANGE = -3;
	// ... make local instances, expose via "Pulse." namespace

	// Approximately 3.5 week: To-do: reset weekly at least.
	MAX_DELAY = 2147483647; // See https://developer.mozilla.org/en-US/docs/Web/API/WindowTimers/setTimeout#Reasons_for_delays_longer_than_specified

	Pulse.rejectedPoints = [];

	Pulse.schedulerFunc = setTimeout;

	// Pulse.wrap = function (func, timeslot) {
	// 	return function (argument) {
	// 		// body...
	// 	}
	// }

	/**
	 * Make post-rendering (schedule call for some function)
	 * @param {[type]} func       [description]
	 * @param {[type]} intervalMs [description]
	 */
	Pulse.setTrigger = function (func, intervalMs, driftMonitor) {
		driftMonitor = driftMonitor || 0;
		intervalMs = +intervalMs; // <-- cast to "number" type
		if (intervalMs < 0 || intervalMs >= MAX_DELAY) return 0;
		Pulse.schedulerFunc(func, intervalMs - +driftMonitor); // <-- setTimeout
		return 1;
	} 

	Pulse.setTriggerPromise = function (func, intervalMs, driftMonitor) {
		var p;
		driftMonitor = driftMonitor || 0;
		intervalMs = +intervalMs; // <-- cast to "number" type
		if (intervalMs < 0) {
			Pulse.rejectedPoints.push(intervalMs)
			return Promise.reject(Pulse.ERROR_DELAY_NEGATIVE);
		} 
		if (intervalMs < 0 || intervalMs >= MAX_DELAY) {
			Pulse.rejectedPoints.push(intervalMs)
			return Promise.reject(Pulse.ERROR_DELAY_OVERFLOW);
		} 
		Pulse.schedulerFunc(func, intervalMs - +driftMonitor); // <-- setTimeout
		return Promise.resolve(+timer()); // <-- means 'Ok'
	} 

	/*
	INSTANCE-level properties
	 */
	
	Pulse.timezoneOffsetMs = (new Date().getTimezoneOffset()*60000);

	Pulse.now = function () {
		return new Date().getTime() - Pulse.timezoneOffsetMs;
	}

	/*
	Helpers	
	 */
	
	Pulse.runAsync = function (func) {
		Pulse.schedulerFunc(func, 0)
		console.log('Pulse.runAsync called')
	}
	
	Pulse.runAsyncPromise = function (func) {
		Pulse.schedulerFunc(func, 0)
		return Promise.resolve()
	}

	/*
	Relative steps
	 */

	Pulse.wait = Pulse.pause = function (intervalMs) {
		return new Promise(function (resolve, reject) {
			Pulse.setTriggerPromise(function (){}, +intervalMs)
				.then(resolve)
				.catch(reject)
		})
	}

	Pulse.runAfter = Pulse.runDeferred = function (func, intervalMs) {
		var alignMs = (new Date()).getTime();
		intervalMs = intervalMs || 0;
		if (intervalMs >= MAX_DELAY) return Promise.reject(Pulse.ERROR_DELAY_OVERFLOW)

		return new Promise(function (resolve, reject) {
			Pulse.setTrigger(function(){
				Pulse.setTrigger(func, 0)
				resolve(true)
			}, +intervalMs + (new Date()).getTime() - alignMs)
		})
	}

	Pulse.runAt = function (func, timeslot) {
		if (timelsot >= MAX_DELAY) return Promise.reject(Pulse.ERROR_DELAY_OVERFLOW)
		return new Promise(function (resolve, reject) {
		    var now = Pulse.now();
			Pulse.setTrigger(function(){
				Pulse.setTrigger(func, 0)
				resolve(true)
			}, timeslot - (now % timelsot))
		})
	}

	Pulse.repeatInterval = function (func, intervalMs, stopFunc) {
		stopFunc = stopFunc || false;
		if (intervalMs >= MAX_DELAY) return Promise.reject(Pulse.ERROR_DELAY_OVERFLOW)
		// var startTime = 
		// see idea source: http://stackoverflow.com/a/8795434
		return new Promise(function (resolve, reject) {
			var count = 0;
			function task() {
				var timeLoss = -(new Date()).getTime();
				if (stopFunc && stopFunc()) {
					return resolve(count); 
				}
			    // do your stuff here
			    Pulse.setTrigger(func, 0)
			    count++;
			    // .. some time passes?

			    // retrigger
			    timeLoss += (new Date()).getTime();
			    // handle case with "negative" interval!:
			    Pulse.setTrigger(task, intervalMs - timeLoss);
			}
			task();
		})
	}

	// TEST THIS!:
	// Align and Repeat each N-th timeslot;
	
	/**
	 * Run action each time which is divisible by timelsot value
	 * @param  {[type]} func     [description]
	 * @param  {[type]} timeslot [description]
	 * @param  {[type]} stopFunc [description]
	 * @return {[type]}          [description]
	 */
	Pulse.repeatTimeslot = function (func, timeslot, stopFunc) {
		stopFunc = stopFunc || false;
		if (timelsot >= MAX_DELAY) return Promise.reject(Pulse.ERROR_DELAY_OVERFLOW)
		// see idea source: http://stackoverflow.com/a/8795434
		return new Promise(function (resolve, reject) {
			var count = 0;
			function task() {
				var now;
				console.warn('.repeat: step')
				if (stopFunc && stopFunc()) {
					console.warn('.repeat: stopFunc')
					resolve(count); 
					return;
				}
			    // do your stuff here
			    Pulse.setTrigger(func, 0)
			    count++;
			    // .. some time passes?

			    // retrigger
			    now = Pulse.now();
			    Pulse.setTrigger(task, intervalMs - (now % intervalMs));
			}
			task();
		})
	}

	// Pulse.runAndDelay = function (func, intervalMs) {
	//     // do your stuff here
	//     Pulse.setTrigger(func, 0)

	// 	return new Promise(function (resolve, reject) {
	// 	    // retrigger
	// 	    var now = Pulse.now();
	// 	    Pulse.setTrigger(resolve, intervalMs - (now % intervalMs));
	// 	})
	// }

	Pulse.NO_MORE_ACTIONS = 'No more actions';
	Pulse.NO_MORE_INTERVALS = 'No more intervals';

	/**
	 * Run periodical actions
	 * @param  {[type]} actionsIter   [description]
	 * @param  {[type]} intervalsIter [description]
	 * @param  {[type]} sentinel      [description]
	 * @return {[type]}               [description]
	 */
	Pulse.iterate = function (actionsIter, intervalsIter, driftMonitor) {

		// Extract first interval of time ??? to allow delay the first action?
		var outerDrift = timer(driftMonitor || 0);

		function passOne() {
			var 
				drift = timer(outerDrift),
				func, interval, adjustedInterval;

			if (actionsIter.next().hasValue) {
				func = actionsIter.value(); // <-- get current value
				if (typeof func === 'function') {
			    	Pulse.setTrigger(func, 0);
					if (intervalsIter.next().hasValue) {
					    Pulse.setTrigger(passOne, intervalsIter.value(), drift);
					} 
			    } else console.error('Iterator item (actions) is not a function:', func)
			}
		}
		passOne()
		outerDrift = 0;
	}

	/**
	 * Run periodical actions
	 * @param  {[type]} actionsIter   [description]
	 * @param  {[type]} intervalsIter [description]
	 * @param  {[type]} sentinel      [description]
	 * @return {Promise}               [description]
	 */
	Pulse.iteratePromise = function (actionsIter, intervalsIter, driftMonitor) {

		return new Promise(function (resolve, reject) {

			function passOne() {
				var 
					drift = timer(driftMonitor),
					func, interval, adjustedInterval;

				if (actionsIter.next().hasValue) {
					func = actionsIter.value(); // <-- get current value
					if (typeof func === 'function') {
						// console.warn('>>>> .iterate: func', typeof func)
				    	Pulse.setTrigger(func, 0);
				    } else {
				    	return reject(
				    		'Iterator item (actions) is not a function')
				    }
				} else {
					// console.warn('>>>> .iterate: func==sentinel, END')
			    	return resolve(Pulse.NO_MORE_INTERVALS)
				}

				if (intervalsIter.next().hasValue) {

				    Pulse.setTrigger(passOne, intervalsIter.value(), drift);

				} else {
					// console.warn('>>>> .iterate: func==sentinel, END')
			    	return resolve(Pulse.NO_MORE_INTERVALS)
				}

			}
			passOne()
			driftMonitor = 0;
		})
	}

	/*
	Absolute actions
	 */
	Pulse.waitFor = function (date) {
		/*
		Pulse.waitFor(date)
			.then(()=>{Pulse.repeat(func,interval)})
			.catch(()=>{Pulse.rewind(...)})
		 */
		var now = Pulse.now();
		var target = (+date) - Pulse.timezoneOffsetMs;
		var intervalMs = target - now;
		return (intervalMs < 0) 
			? Promise.reject(-1) /* moment is passed!*/
			: Pulse.runDeferred(func, intervalMs)
	}


	// Pulse.runAt = function (func, date) {
	// 	var now = new Date().getTime();
	// 	var target = date.getTime();
	// 	var intervalMs = target - now;
	// 	return (intervalMs < 0) 
	// 		? Promise.reject(-1) /* moment is passed!*/
	// 		: Pulse.runDeferred(func, intervalMs)
	// }

	// To-do: implement exact method runInTimeslot, intercept cases when duration of "action"> length of tmeslot!
		// Player function
		// see idea source: http://stackoverflow.com/a/8795434
		// function redraw() {
		//     var interval = 30000;

		//     // work out current frame number
		//     var now = new Date().getTime();
		//     var frame = Math.floor(now / interval) % 2; // 0 or 1

		//     // do your stuff here
		//     .. some time passes

		//     // retrigger
		//     now = new Date().getTime();
		//     Pulse.setTrigger(redraw, interval - (now % interval));
		// }

		// redraw();
	

	Pulse.runQueue = function (iterator, actionQueue) {
		return new Promise(function (resolve, reject) {
			var action;
			try {
				while (iterator.forNext(Pulse.wait)) {
					action = actionQueue.nextValue()
					Pulse.runAsync(action)
				}
				resolve(true)
			} catch (e) {
				reject(e)
			}
		})
	}

	function Pulse (argument) {}

	return Pulse;

}))