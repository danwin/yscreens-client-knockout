//device-probe.js
(function (argument) {
	// body...
		self.MIMETypes = {
			'MT_WEBM'	: 'video/webm',
			'MT_MP4'	: 'video/mp4',
			'MT_OGG'	: 'video/ogg',
			'MT_3GP' : 'video/3gp',
			'MT_GIF'	: 'image/gif',
			'MT_PNG'	: 'image/png',
			'MT_JPEG'	: 'image/jpg',
		}

		function _testMediaCap(mimeType) {
			var test = self.deviceCaps[mimeType];
			if (typeof test === 'undefined' && self.videotest) {
				test = self.videotest.canPlayType(mimeType).replace(/no/gi, '');
				self.deviceCaps[mimeType] = test;
				_trace_('testing format: '+ mimeType);
				_trace_('result: '+ self.videotest.canPlayType(mimeType));
			}
			return test && test.length > 0;
		}

//-----
		// run in init, before DOM ready
		self.deviceCaps = {};
		try {
			self.videotest = document.createElement("video");
			document.body.appendChild(self.videotest); // <-- for chrome
			self.deviceCaps.VideoSupport = true;
			self.deviceCaps.autoplay = undefined; // override later by device test
		} catch (e) {
			self.videotest = null;
			self.deviceCaps.VideoSupport = false;
			_trace_('Error in deviceCaps detection:');
			_trace_(e);
		}

//------
	// execute before DOM-ready:

	(function() {

		// File data URIs from comments in https://github.com/richtr/NoSleep.js/commit/4cc289bea129b0351e6290fba9bdb2ad0bd127da
		var mp4 = 'data:video/mp4;base64,AAAAHGZ0eXBpc29tAAACAGlzb21pc28ybXA0MQAAAAhmcmVlAAAAG21kYXQAAAGzABAHAAABthADAowdbb9/AAAC6W1vb3YAAABsbXZoZAAAAAB8JbCAfCWwgAAAA+gAAAAAAAEAAAEAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAIVdHJhawAAAFx0a2hkAAAAD3wlsIB8JbCAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAQAAAAAAIAAAACAAAAAABsW1kaWEAAAAgbWRoZAAAAAB8JbCAfCWwgAAAA+gAAAAAVcQAAAAAAC1oZGxyAAAAAAAAAAB2aWRlAAAAAAAAAAAAAAAAVmlkZW9IYW5kbGVyAAAAAVxtaW5mAAAAFHZtaGQAAAABAAAAAAAAAAAAAAAkZGluZgAAABxkcmVmAAAAAAAAAAEAAAAMdXJsIAAAAAEAAAEcc3RibAAAALhzdHNkAAAAAAAAAAEAAACobXA0dgAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAIAAgASAAAAEgAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABj//wAAAFJlc2RzAAAAAANEAAEABDwgEQAAAAADDUAAAAAABS0AAAGwAQAAAbWJEwAAAQAAAAEgAMSNiB9FAEQBFGMAAAGyTGF2YzUyLjg3LjQGAQIAAAAYc3R0cwAAAAAAAAABAAAAAQAAAAAAAAAcc3RzYwAAAAAAAAABAAAAAQAAAAEAAAABAAAAFHN0c3oAAAAAAAAAEwAAAAEAAAAUc3RjbwAAAAAAAAABAAAALAAAAGB1ZHRhAAAAWG1ldGEAAAAAAAAAIWhkbHIAAAAAAAAAAG1kaXJhcHBsAAAAAAAAAAAAAAAAK2lsc3QAAAAjqXRvbwAAABtkYXRhAAAAAQAAAABMYXZmNTIuNzguMw==';
		var webm = 'data:video/webm;base64,GkXfo0AgQoaBAUL3gQFC8oEEQvOBCEKCQAR3ZWJtQoeBAkKFgQIYU4BnQI0VSalmQCgq17FAAw9CQE2AQAZ3aGFtbXlXQUAGd2hhbW15RIlACECPQAAAAAAAFlSua0AxrkAu14EBY8WBAZyBACK1nEADdW5khkAFVl9WUDglhohAA1ZQOIOBAeBABrCBCLqBCB9DtnVAIueBAKNAHIEAAIAwAQCdASoIAAgAAUAmJaQAA3AA/vz0AAA=';
		var body = document.getElementsByTagName('body')[0];
		var root = document.getElementsByTagName('html')[0];

		try {
			Plugin.deviceCaps.autoplay = false;
			var video = document.createElement('video');
			video.src = video.canPlayType('video/webm') ? webm : mp4;
			video.autoplay = true;
			video.loop = true;
			video.volume = 0;
			video.style.visibility = 'hidden';

			// attach video to body
			body.appendChild(video);

			// lock jQuery(document).ready()
			$.holdReady(true);

			// Set a timeout for the video element removal
			var removeVideoTimeout = setTimeout(function() {
				// unlock jQuery(document).ready()
				$.holdReady(false); 
				// remove video element from page
				body.removeChild(video);
			}, 100);
			
			// test preloading capability
			video.addEventListener('loadeddata', function() {
				Plugin.deviceCaps.preload = true;
			});
			
			// this will only be triggered if autoplay works
			video.addEventListener('play', function() {
				// set autoplay flag in plugin:
				Plugin.deviceCaps.autoplay = true;
				// unlock jQuery(document).ready()
				$.holdReady(false); 
				_trace_('Autoplay support - Ok.');
				// cancel timeout
				clearTimeout(removeVideoTimeout);
				// remove video element from page
				body.removeChild(video);
			}, false);

			// video.play() seems to be required for it to work,
			// despite the video having an autoplay attribute.
			// This could be due to run this code before 
			// the DOM is ready.
			video.play();
			
		} catch(e) {
			_trace_('[AUTOPLAY-ERROR]', e);
		}

	})();
})()
// caps probe