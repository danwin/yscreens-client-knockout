//ko.resizable.js
define(['underscore','jquery', 'knockout', 'jquery-ui/resizable'], 
function (_, $, ko) {
	ko.bindingHandlers.resizable = {
	    init: function(element, valueAccessor) {
	         var options = valueAccessor();
	         var Selement = $(element);
	         var height = Selement.height();
	         options = _.extend(options, {
	         	'minHeight':height, 
	         	'maxHeight':height
	         })
	         Selement.resizable(options);

	         // element.addEventListener('resize', function (argument) {
	         // 	console.log(argument)
	         // })
	    }  
	};
});