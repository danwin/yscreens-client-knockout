// io-config.js
(function(root, factory) {
	// var _modname = 'IO';
	if (typeof define === "function" && define.amd) { // AMD mode
		define(["underscore.all",
			"shared/io.middleware",
			"shared/config",
			"viewmodels/storage-reflections",
			"shared/io.firebase" // <-- connect Firebase driver here!
			], factory);
	} else if (typeof exports === "object") { // CommonJS mode
		var _ = (typeof root._ === 'undefined') ? require("underscore.all") : root._;
		var IO = (typeof root.IO === 'undefined') ? require("shared/io.middleware") : root.IO;
		var config = (! root.MODULES || typeof root.MODULES.config === 'undefined') ? require("shared/config") : root.MODULES.config;
		var storage_reflections = (! root.MODULES || typeof root.MODULES.storage_reflections === 'undefined') ? require("viewmodels/storage-reflections") : root.MODULES.storage_reflections;
		root.IO = require("shared/io.firebase") //<-- it extends the global IO variable
		module.exports = factory(_, IO, config, storage_reflections);
	} else {
	// This module extends "IO" (which already exists as a global variable)
		factory(root._, root.IO, root.MODULES.config, root.MODULES.storage_reflections); // Plain JS, "rtl" is in window scope
		// root[_modname] = factory(root._, root.IO, root.firebase); // Plain JS, "rtl" is in window scope
	}
}(this, function(_, IO, config, mdReflections) {

	var TRACE_MODULE = false;

	// Reflection -> Service -> Endpoint, Transport
	if (TRACE_MODULE) console.log('IO driver:', IO)
	var AuthFactory = IO.AuthFactory;
	var Endpoint = IO.Endpoint;
	var Service = IO.Service;

	function Gateway(cfg) {
		var
			_cfg = cfg || config,
			// data storage (db):
			_dataTsp = IO.Transport.factory(_cfg.storages.db, {}).init(_cfg.storages.config),
			// media storage:
			_blobTsp = IO.Transport.factory(_cfg.storages.media, {}).init(_cfg.storages.config),
			// multimedia transport dispatcher:
			_mmediaTsp = IO.MultiMediaDispatcher(_dataTsp, _blobTsp);

		//------------- DEV LOGIN
 		// _dataTsp.signIn({email:'d_z@mail.ru', password:'passwd'})
			// .then(function (result) {
			// 	// success:
			// 	console.log('Auth OK: ', result);
			// 	return result;
			// })
			// .catch(function (reason) {
			// 	alert(reason.toString());
			// 	console.warn('??? Auth Error: ', reason);
			// 	return reason;
			// })

		//-------------

		var self = {};
		var newService = function (epAlias, tspAlias) {
			return Service(
				_.assertDefined(self.endpoints[epAlias], 'No endpoint for alias: '+epAlias),
				_.assertDefined(self.transports[tspAlias], 'No transport for alias: '+tspAlias)
				)
		}
		/**
		 * Creates new instance of reflection, which wraps service related to the entityName
		 * @method newReflection
		 * @param  {[type]}      refName    Name of reflection (as in "storage-reflection")
		 * @param  {[type]}      entityName [description]
		 * @return {[type]}                 [description]
		 */
		var newReflection = function (refName, entityName) {
			var rfl = _.assertDefined(mdReflections[refName], 'Cannot find reflection for: '+entityName)
			var svc = _.assertDefined(self.services[entityName], 'Cannot find service for: '+entityName)
			return rfl.wrap(svc)
		}

		// console.log('...io-config... instance of Auth...')
		self.sessions = {
			'default': AuthFactory(_cfg.storages.config)
		}
		// console.log('...done...')

		// Mark resources which have to be removed on user removal also:
		self.userResources = ['Assets', 'Timelines', 'Channels', 'Playlists', 'Devices', 'Expositions', 'UserPhoto']

		self.endpoints = {
				'Assets': Endpoint('usr/:userId/assets'),
				'Timelines': Endpoint('usr/:userId/timelines'),
				'Channels': Endpoint('usr/:userId/channels'),
				'Playlists': Endpoint('usr/:userId/playlists'),
				'Devices': Endpoint('usr/:userId/devices'),

				/* Per-user data: */
				'UserInfo': Endpoint('adm/users/:userEmail'),

				'AdmUsers': Endpoint('adm/users'), //<-- here userId is optional

				'UserPhoto': Endpoint('adm/user-photo'), // <-- only for user photos

				/*^ URL format ???*/
				'Expositions': Endpoint('usr/:userId/records'),

				'Schedules': Endpoint('usr/:userId/schedules'),

				'Sequencers': Endpoint('usr/:userId/sequencers'),

				/* Bind Display logical ID to StorageID, contains data and command bus for screens*/
				'ScreenBindings': Endpoint('usr/:userId/magneto-network/displays')

				// 'Shares': ... // "Share means copy?"

				// //
				// 'RootAssets': Endpoint('assets'),
				// 'RootTimelines': Endpoint('timelines'),
				// 'RootChannels': Endpoint('channels'),
				// 'RootPlaylists': Endpoint('playlists'),
				// 'RootDevices': Endpoint('devices'),
				// 'RootUsers': Endpoint('users'),

				// 'RootExpositions': Endpoint('records')
			};
		self.transports = {
				// data storage
				'data' : _dataTsp,
				// media storage:
				'blob' : _blobTsp,
				// multimedia transport dispatcher:
				'mmedia' : _mmediaTsp
			};
		self.services = {
				'Assets': newService('Assets', 'mmedia'),
				'Timelines': newService('Timelines', 'data'),
				'Channels': newService('Channels', 'data'),
				'Playlists': newService('Playlists', 'data'),
				'Devices': newService('Devices', 'data'),

				'UserInfo': newService('UserInfo', 'data'),

				'AdmUsers': newService('AdmUsers', 'data'),

				'UserPhoto': newService('UserPhoto', 'blob'),

				'Expositions': newService('Expositions', 'data'),

				'Schedules': newService('Schedules', 'data'),

				'Sequencers': newService('Sequencers', 'data'),

				/* Bind Display logical ID to StorageID, contains data and command bus for screens*/
				'ScreenBindings': newService('ScreenBindings', 'data')

			},
		self.reflections = {
				'Assets': newReflection('AssetReflection', 'Assets'),

				'Timelines': newReflection('TimelineReflection', 'Timelines'),
				'Playlists': newReflection('PlaylistReflection', 'Playlists'),

				'Channels': newReflection('ChannelReflection', 'Channels'),
				'Devices': newReflection('DeviceReflection', 'Devices'),
				'AdmUsers': newReflection('AdmUserReflection', 'AdmUsers'),

				'UserPhoto': newReflection('UserPhotoReflection', 'UserPhoto'),

				'Expositions': newReflection('ExpositionReflection', 'Expositions'),

				'Schedules': newReflection('ScheduleReflection', 'Schedules'),

				'Sequencers': newReflection('SequencerReflection', 'Sequencers'),

				'ScreenBindings': newReflection('ScreenBindingReflection', 'ScreenBindings')
		};


			// _get: function (eName) {
			// 	return _.assertDefined(self._services[eName],
			// 		'Cannot find service for entity: '+eName)
			// }

		return self;

	};


	return _.extend(IO, {
		gw: Gateway()
	})

}));