// model-auth
define(['underscore.all', 'knockout.all', 'viewmodels/model-base',
], function(_, ko, BaseModel){

	var StructUser = function(scope, data) {
		// Persistent fields for I/O
		scope = scope || {};
		var
			Field = ko._FieldFactory_(scope, ko.observable),
			FieldA = ko._FieldFactory_(scope, ko.observableArray);

		/* Field('StorageID'), 
		*  Field('CreatedTime'), 
		*  Field('ModifiedTime'): */ 
		BaseModel.StructPersistent(scope, data); 

		Field( 'UID', null, data);
		Field( 'Label', '-', data); //<- here is displayName

		Field( 'AccountEmail', '', data);

		Field( 'FirstName', '', data);
		Field( 'LastName', '', data);

		Field( 'IsLocked', false, data);

		Field( 'IsAdmin', false, data);

		Field( 'ContactEmail', '', data);
		Field( 'ContactPhone', '', data);
		Field( 'BillingAddress', '', data);

		Field( 'Comment', '', data);

		/* Photo fields */
		Field( 'PhotoUrl', null, data);
		Field( 'uploadFile', null); //<-- temporary field only for upload transaction

		/* Quota Management */
		Field( 'SpaceQuota', 1048576, data); // 1Mb default (?)
		Field( 'SpaceUsed', 0, data);

		// FieldA( 'Groups', ['user'], data);
		return scope;
	}

	var User = function (data) {
		var self = this;

		StructUser(self, data);

		// Implement "updateValues" method which binded with the appropriate factory:
		BaseModel.IFUpdate(self, StructUser);

		// *** Constant fields ***

		self._rtti = 'class:User';
		_.assertTrue(self !== window, 
			'"'+self._rtti+'"" function is a constructor, call it with "new"!');

		// *** Computed fields ***
		self.fmtIsLocked = ko.pureComputed(function () {
			return (self.IsLocked()) ? '<i class="fa fa-lock"></i>': '';
		})
		self.fmtIsAdmin = ko.pureComputed(function () {
			return (self.IsAdmin()) ? '<i class="fa fa-check"></i>': '';
		})

		self.getPhotoUrl = ko.pureComputed(function () {
            var photoUrl = self.PhotoUrl();
            return photoUrl ? photoUrl : 'img/person-icon.png'
        })

        var _fmtMBytes = function (value) {
			var mbSize = value / 1048576 //1,048,576 Bytes
			if (mbSize < 1) return Math.ceil(mbSize * 100) / 100 + ' MB'; 
			if (mbSize < 10) return Math.ceil(mbSize * 10) / 10 + ' MB'; 
			return Math.ceil(mbSize) + ' MB';
        }

        self.fmtSpaceInfo = ko.pureComputed(function () {
        	return _fmtMBytes(self.SpaceUsed()) + '/' + _fmtMBytes(self.SpaceQuota())
        })
		
	}

	// ---

	return {
		StructUser: StructUser,
		User: User
	}

});
