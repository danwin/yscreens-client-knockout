// schedule-assistant.js
//schedule-editor.js
define([
	'durandal/app',
	'plugins/dialog',
	'plugins/router',
	'underscore.all',
	'jquery',
	'knockout.all',
	'viewmodels/model-sequencer',
	'viewmodels/model-exposition',
	'datetime',
	'shared/datetime-helpers',
	'nouislider',

	'knockout.sortable',
	'shared/ko.resizable'
],
function (app, dialog, router, _, $, ko, mdSequencer, mdExposition, mdDateTime, dt) {

	var
		// Schedule = mdSchedule.Schedule
		// ,PlaybackItem = mdSchedule.PlaybackItem
		// ,PlaybackFrame = mdSchedule.PlaybackFrame
		// ,PlaybackStream = mdSchedule.PlaybackStream
		// ,StructPlaybackItem = mdSchedule.StructPlaybackItem
		// ,StructPlaybackFrame = mdSchedule.StructPlaybackFrame
		// ,StructPlaybackStream = mdSchedule.StructPlaybackStream
		DateTimeInterval = mdDateTime.DateTimeInterval
		,LocalDateTime = mdDateTime.LocalDateTime

		,Exposition = mdExposition.Exposition;


	var vecToMs = DateTimeInterval.vectorToMilliseconds;

	var ctor = function () {
		var self = this;
		this.displayName = 'Schedule Assistant';

		this.DAYS = [
			{'label':'Sun', 'value':0},
			{'label':'Mon', 'value':1},
			{'label':'Tue', 'value':2},
			{'label':'Wed', 'value':3},
			{'label':'Thu', 'value':4},
			{'label':'Fri', 'value':5},
			{'label':'Sat', 'value':6}
		];

		this.TRIGGER_ACTIONS = _.map(mdSequencer.TRIGGER_ACTIONS, function (label, index) {
			return {'label':label, 'value':index}
		})

		this.Changed = ko.observable(false)

		var itemRef = app.IO.gw.reflections['Sequencers'];
		var recordRef = app.IO.gw.reflections['Expositions'];

		var scheduleRef = app.IO.gw.reflections['Schedules'];

		this.sequencer = ko.observable( new mdSequencer.Sequencer({
					'SequenceRange': 'DAY'
				}))

		this.lookups = {};

		var _throttleOptions = { rateLimit: { timeout: 500, method: "notifyWhenChangesStop" } };
												// LocalDateTime - override valueOf to return module also!





		var MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		var DAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

		function strMonth(index) {
			return MONTHS[index-1]
		}
		function strDay(index) {
			return DAYS[index]
		}
		function strWeek(index) {
			return 'W'+index
		}


	/*////////////////////////////////////////////////////////
	//
	// Lookups
	//
	*/////////////////////////////////////////////////////////



	/*////////////////////////////////////////////////////////
	//
	// Subscriptions
	//
	*/////////////////////////////////////////////////////////



		self.sortableElementOptions = {
				handle: '.sort-handle'
				,axis: 'y'
				,helper: function (event, element) {
					return $('<div>')
						.css({})
						// .addClass('sort-helper')
						.html('<span class="badge"><i class="fa fa-arrows-v"></i>&nbsp;Select order...</span>');
				}
			}




		this.lookups.records = ko.observableArray([]);

		this.lookups.sequencers = ko.observableArray([]);

		this.stateLegend = ko.observable('');

		this.fmtContentLegend = function (item) {
			function findRecordById(id) {
				return _find(self.lookups.sequencers, function (item) {
					return (item.StorageID.peek() === id)
				}).Label()
			}
			var sqId = item.NestedSequencerId()
			if (sqId) return findRecordById(sqId);
			return item.Label()
		}

		this.markedItems = ko.observableArray([]);
		this.markedItems.subscribe(function () {
			if (self.markedItems().length > 0) {
				self.stateLegend('&nbsp;You have selected <span class="badge">'+ self.markedItems().length + '</span> element(s)')
			} else {
				self.stateLegend('')
			}
		});
		this.mark = function (item) {
			var index = self.markedItems.indexOf(item);
			if (index < 0) {
				self.markedItems.push(item);
			} else {
				self.markedItems.splice(index, 1);
			}
			console.log(ko.toJS(self.markedItems));
			return true;
		};


		var _populate = function () {
			var buffer = []
			for (var i=0; i<3; i++) {
				buffer.push(new mdSequencer.PlayAndTrigger({
					'RecordInfo':{'Label': 'Record#'+i},
					// 'TriggerRange': 'ORDERED',
					'Time': new LocalDateTime({'hours': i}),
					'TriggerAction': 1
				}))
			}
			self.sequencer.peek().Items(buffer)
			// self.schedule.updateValues({'Items': buffer})
		}

		this.activate = function (scheduleId) {
			console.log('...schedules...activate...')

			var queue = [];
			// Enum schedules
			self.sequencer.peek().StorageID(scheduleId || 0);

			// self.sequencer.StorageID(scheduleId || 0);

			if (scheduleId) {
				queue.push(
					itemRef
						.load(self.sequencer.peek(), app.rqOptions)
						.catch(app.handleException)
						// .then(function (data) {
						// 	if (data.length == 0) _populate(data);
						// 	self.items(data)
						// 	return data
						// })
					);
			} else _populate();

			// Enum sequencers
			queue.push(
				itemRef.enum(app.rqOptions)
				.then(function (data) {
					self.lookups.sequencers(data)
				})
			)

			// Enum records
			queue.push(
				recordRef
					.enum(app.rqOptions)
					.then(function (data) {
						var records = _.map(data, function (item) {
							return new Exposition(item)
						})
						// var lookup = _.map(data, function (item) {
						// 	return {'label': item.Label.peek(), 'id': item.StorageID.peek()}
						// })
						// console.log('...record found: ...', lookup)
						console.log('Exposition records: ', records)
						self.lookups.records(records)
						// return data
					})
				)
			return Promise.all(queue)
				.then(function (resuls) {
					console.log('...then...', resuls)
				})
				.catch(function (reason) {
					console.error('...schedules...', reason)
				})
				.catch(app.handleException);
		}

		this.compositionComplete = function () {
			// updateTimetable()
		}

		/*
		CRUD
		*/

		// Convert sequence type
	            // app.showMessage(
	            //     'Are you sure you want to remove element \"'+ item.Label() + '\"?',
	            //     'Confirm', ['Yes', 'No']
	            // ).then(function (result) {
	            //     if (result == 'Yes') {


		// [new]
		this.createNew = function () {
			self.sequencer( new mdSequencer.Sequencer({
				'SequenceRange': 'DAY'
			}))
		}



		this.addPlaylist = function () {
			var item = new mdSequencer.PlayAndTrigger();
			self.assignPlaylist(item)
		}

		this.addSequncer = function () {
			var item = new mdSequencer.PlayAndTrigger();
			self.assignSequencer(item)
		}


		this.assignPlaylist = function (item) {
			// var item = new mdSchedule.PlaybackStream();

			app.customDialog({
				'buffer': item,
				'attrFilter': mdSequencer.StructPlayAndTrigger,
				// 'constructorFunc': mdSequencer.PlayAndTrigger,
				'viewUrl': 'views/schedule-editor-dialogs/dlg-assign-playback',
				'lookups': {
					'records': self.lookups.records.peek(),
					'formatOptionsLegend': function (item) {
						return item.Label() +' [' + (new DateTimeInterval( item.Duration())) + ']'
					}
				}
			}).waitConfirm()
			.then(function (Ok) {
				console.log('Confirmed????')
				if (Ok) {
					var items = self.sequencer.peek().Items;
					if (items.indexOf(item)<0) items.push(item)
					console.log(ko.toJS(item))
					self.Changed(true)
				}
			})
			.catch(app.handleException)
		}

		this.assignSequencer = function (item) {
			// var item = new mdSchedule.PlaybackStream();

			app.customDialog({
				'buffer': item,
				'attrFilter': mdSequencer.StructPlayAndTrigger,
				'constructorFunc': mdSequencer.PlayAndTrigger,
				'viewUrl': 'views/schedule-editor-dialogs/dlg-assign-sequencer',
				'lookups': {
					'records': self.lookups.records.peek(),
					'formatOptionsLegend': function (item) {
						return item.Label() +' [' + (new DateTimeInterval( item.Duration())) + ']'
					}
				}
			}).waitConfirm()
			.then(function (Ok) {
				if (Ok) {
					var items = self.sequencer.peek().Items;
					if (items.indexOf(item)<0) items.push(item)
					self.Changed(true)
				}
			})
			.catch(app.handleException)
		}

		this.addRecursiveStream = function () {
			// var buffer = {
			// 	StreamDuration: ko.observable(new DateTimeInterval(0)).extend({ numeric: 0 }),
			// 	Period: ko.observable(new DateTimeInterval(0)).extend({ numeric: 0 }),

			// }
			var buffer = new PlaybackStream({'SlotDuration':3600000, 'Period':3600000})
			buffer['viewUrl'] = 'views/dlg-edit-repetition';
			buffer['lookups'] = {
					'lkpPeriods': self.lkpPeriods,
					'lkpFmtPeriodLabel': function(item){
							console.log('lkpPeriods======>', item, parseInt(item.value/1000))
							switch ( parseInt(item.value) ) {
								case 3600:
								case 86400:
								case 604800:
									return item.label;
								default:
									return 'Exactly...';
							}
					}
				}

			app.showDialogEx(buffer)
			.then(function (Ok) {
				console.log('dialog result', Ok)
				if (Ok) {

					var item = new mdSchedule.PlaybackStream(buffer.toJS()); // <--
					self.schedule.createRecursivePattern(item); // <- item, period, times
					self.Changed(true)
				}
			})
			.catch(app.handleException)
		}

		/**
		 * Dialog data to edit trigger start/offset:
		 * @param {[type]} data [description]
		 */
		function TriggerData(data) {
				this.Start = ko.observable(data.Start);
				this.Offset = ko.observable(data.Offset);
				this.IsAbsolute = data.IsAbsolute;
				this['viewUrl'] = 'views/schedule-editor-dialogs/dlg-edit-trigger';
		}

		// Also make: editTrigger?
		this.addTriggeredStream = function () {
			var buffer = new TriggerData({
				'Start': new LocalDateTime.now(),
				'Offset': new DateTimeInterval(0), /*Initial value: edge start*/
				'IsAbsolute': self.schedule.IsAbsolute.peek()
			})

			app.showDialogEx(buffer)
			.then(function (Ok) {
				console.log('dialog result', Ok)
				if (Ok) {
					var offset = buffer.IsAbsolute
						? buffer.Start.peek().subtract(self.schedule.Start.peek())
						: buffer.Offset.peek();//.add(self.schedule.Offset.peek());
					var item = new mdSchedule.PlaybackStream({
						'Offset': offset,
						'IsOffsetLocked': true
					}); // <--
					console.log('Offset:', offset.toString(),
						'was: ', buffer.Start.peek(), self.schedule.Start.peek())
					self.schedule.Items.push(item); // <- item, period, times
					self.schedule.sortTriggers()
					self.Changed(true)
				}
			})
			.catch(app.handleException)
		}

		this.editTrigger = function (item) {
			var
					itemIsSchedule = (item === self.schedule),
					IsAbsolute = itemIsSchedule || self.schedule.IsAbsolute.peek(),
					Offset = itemIsSchedule ? 0 : item.Offset.peek(),
					scheduleStart = self.schedule.Start.peek(),
					Start = (itemIsSchedule) ? scheduleStart : scheduleStart.add(Offset);
			var buffer = new TriggerData({
				'IsAbsolute': IsAbsolute,
				'Start': Start,
				'Offset': Offset
			})
			return app.showDialogEx(buffer)
			.then(function (Ok) {
				if (Ok) {
					var Offset, parent;
					if (itemIsSchedule) {
						self.schedule.IsStartLocked(true)
						self.schedule.Start(buffer.Start.peek())
						self.schedule.updateItems()
					} else {
						if (IsAbsolute) {
							Offset = buffer.Start.peek().subtract(scheduleStart)
						} else {
							Offset = buffer.Offset
						}
						console.log('Offset:', Offset.toString())
						item.Offset(Offset)
						item.IsOffsetLocked(true)

						// If item is not on the top level:
						parent = self.schedule.getParentFor(item)
						if (parent && parent !== self.schedule) {
							parent.Items.remove(item);
							self.Items.push(item)
						}
						self.schedule.sortTriggers()
					}
					// If schedule itself:
					self.Changed(true)
				}
			})
			.catch(app.handleException)
		}

		this.removeTrigger = function (item) {
			if (item instanceof Schedule) {
				item.IsStartLocked(false);
			} else {
				item.IsOffsetLocked(false);
			}
			self.schedule.sortTriggers()
			self.schedule.updateItems();
			self.Changed(true)
		}
//---

		this.editDuration = function (item) {
			// var item = new mdSchedule.PlaybackStream();

			app.customDialog({
				'buffer': item,
				'attrFilter': mdSchedule.StructPlaybackFrame,
				'constructorFunc': PlaybackFrame,
				'viewUrl': 'views/schedule-editor-dialogs/dlg-edit-duration'
			}).waitConfirm()
			.then(function (Ok) {
				if (Ok) {
					self.Changed(true)
				}
			})
			.catch(app.handleException)
			// body...
		}


		this.splitItem = function (item) {
			var dlgBuffer = (function () {
					var model = {};
					var duration = item.RequiredDuration.peek();
					model.Duration = ko.observable(duration)
					model.maxSubitemDuration = Math.floor(duration/2)
					model.SplitCount = ko.observable(2)
					model.SubitemDuration = ko.observable(model.maxSubitemDuration)
					model.UseSplitCount = ko.observable("yes")
					model.Remainder = ko.pureComputed(function () {
							return new DateTimeInterval( model.Duration() % model.SubitemDuration() )
					})
					model.CopyContents = ko.observable(true);

					model.SplitCount.subscribe(function () {
						if (model.UseSplitCount.peek() === 'yes') {
							model.SubitemDuration(new DateTimeInterval( Math.floor( model.Duration.peek() / model.SplitCount.peek())) )
						}
					})

					model.SubitemDuration.subscribe(function () {
						if (model.UseSplitCount.peek() === 'no') {
							model.SplitCount(Math.floor( model.Duration.peek() / model.SubitemDuration.peek()))
						}
					})

					model.SubitemDuration.subscribe(function (newValue) {
						if (newValue > model.maxSubitemDuration || newValue < 60000) { // <-- loop over the "max" value
							model.SubitemDuration(new DateTimeInterval( model.maxSubitemDuration ))
						}
					})

					model.SplitCount.subscribe(function (newValue) {
						if (newValue < 2) {
							model.SplitCount(2)
						}
					})

					return model
				})()
			dlgBuffer['viewUrl'] = 'views/schedule-editor-dialogs/dlg-timeslot-split';

			app.showDialogEx(dlgBuffer)
			.then(function (Ok) {
				var
					parent = self.schedule.getParentFor(item),
					collection = parent.Items.peek(),
					index = parent.Items.indexOf(item),
					dontDuplicateContents = !dlgBuffer.CopyContents.peek(),
					child, data, contents;

				console.log('dialog result', Ok)

				if (Ok) {
					// Remove original item from collection:
					collection.splice(index, 1);

					// Set new duration:
					item.SlotDuration(dlgBuffer.SubitemDuration.peek())

					data = item.toJS();

					if (item instanceof PlaybackFrame) {
						for (var i=0, len=dlgBuffer.SplitCount.peek(); i<len; i++) {
							// Clear contents for 2nd+ child if necessary:
							if (dontDuplicateContents && i === 1) data.RecordInfo = {}
							child = new PlaybackItem(data);
							collection.splice(index, 0, child)
						}
					} else if (item instanceof PlaybackStream) {
						for (var i=0, len=dlgBuffer.SplitCount.peek(); i<len; i++) {
							// Clear contents for 2nd+ child if necessary:
							if (dontDuplicateContents && i === 1) data.Items = [];
							child = new PlaybackStream(data);
							collection.splice(index, 0, child)
						}
					}

					parent.Items(collection)

					self.Changed(true)
				}
			})
			.catch(app.handleException)
		}

		this.addDailyGrid = function () {
			var buffer = {};
			var period = new DateTimeInterval({'hours': 24});

			buffer.Start = ko.observable(new LocalDateTime({'hours':0, 'minutes':0}))
			buffer.Stop = ko.observable(new LocalDateTime({'hours':23, 'minutes': 59}))
			buffer.SlotDuration = ko.pureComputed(function () {
				var interval = buffer.Stop().subtract(buffer.Start());
				if (interval < 0) {
					interval = interval.add(new DateTimeInterval({'hours': 24}))
				}
				return interval;
			})
			buffer.fmtDuration = ko.pureComputed(function () {
				return buffer.SlotDuration().toString();
			})

			buffer.CloneByReference = ko.observable(false)
			buffer.viewUrl = 'views/dlg-edit-shd-daygrid'
			buffer.Cancel = function () {dialog.close(buffer)}
			buffer.Ok = function () {dialog.close(buffer, true)}

			app.showDialog(buffer)
			.then(function (Ok) {
				if (Ok) {
					var item = new mdSchedule.PlaybackStream({
						'Offset': buffer.Start.peek(),
						'SlotDuration': buffer.SlotDuration.peek(),
						'IsDurationLocked': true,
						'IsOffsetLocked': true
					});
					self.schedule.createRecursivePattern(item, period, 7);
					self.Changed(true)
				}
			})
			// .catch(app.handleException)
		}

		this.editComment = function (item) {

			app.customDialog({
				'buffer': item,
				'attrFilter': mdSchedule.StructPlaybackItem,
				'constructorFunc': PlaybackItem,
				'viewUrl': 'views/schedule-editor-dialogs/dlg-edit-comment'
			}).waitConfirm()
			.then(function (Ok) {
				if (Ok) {
					self.Changed(true)
				}
			})
			.catch(app.handleException)
			// body...
		}

		this.renderSchedule = function () {
			// WAS: schedule.publish
			self.publish()
				.then(function () {
					self.stateLegend('Schedule rendered')
				})
				.catch(app.handleException);
		}

		// common code to remove item
		var _doRemove = function (item, collectionList, selectionList) {
			return new Promise(function (resolve, reject) {
				try {
					console.log('Removing item:::', item);
					// To-do: use PropertyReflection here to operate directly on collection...

					var index = collectionList.indexOf(item);
					if (index >-1) {
						collectionList.splice(index, 1);
						self.Changed(true)
					}
					// remove from selection ("markedItems"):
					index = selectionList.indexOf(item);
					if (index >-1) selectionList.splice(index,1)
					resolve(true)
				} catch (e) {
					reject(e)
				}
			})
		}

		self.remove = function (item) {

			// ask confirm:
			app.showMessage(
				'Are you sure you want to remove element \"'+ item.Label() + '\"?',
				'Confirm', ['Yes', 'No']
			).then(function (result) {
				if (result == 'Yes') {
					self.sequencer.peek().Items.remove(item);
				}
			});
		};

		self.removeMany = function (item) {
			// body...
			var
				_selection = self.markedItems.peek();
				_collection = self.sequencer.peek().Items.peek();
			if (!_selection.length) return;
			// ask confirm:
			app.showMessage(
				'Are you sure you want to remove '+ _selection.length + ' element(s)?',
				'Confirm', ['Yes', 'No']
			).then(function (result) {
				if (result != 'Yes') return;

				// Promise.all(_.map(_selection, function (item) {
				// 	return _doRemove(item, _collection, _selection)
				// }))

				// Do sych execution due to external variables are not thread-safe
				_.reduce(_selection, function (promise, item) {
					return promise.then(function () {
						return _doRemove(item, _collection, _selection)
					})
				}, Promise.resolve())
				.then(function () {
					self.sequencer.peek().Items(_collection)
					self.markedItems(_selection)
				})
				.catch(app.handleException)

			});
		};

		self.save = function () {
			itemRef.save(self.sequencer.peek(), app.rqOptions)
				.then(function () {
					self.Changed(false)
					self.stateLegend('Saved')
				})
				.catch(app.handleException)
		}

		// Publish rendered movie
		self.publish = function () {
			var sequencer = self.sequencer.peek()
			var js = sequencer.renderJSON();

			var moviesSvc = app.IO.gw.services['Expositions'];
			var itemRef = app.IO.gw.reflections['Expositions'];

			var xp = new mdExposition.Exposition()
			// Apply values:
			// Use same Id as for original playist:
			xp.StorageID(sequencer.StorageID.peek())
			// Mark type of the source entity - Playlists
			xp.SourceEntity('Schedules')
			xp.json(js)
			xp.Label(sequencer.Label.peek())
			xp.js(JSON.parse(js))

		  return moviesSvc.signIn()
			  .then(function () {
			  	return itemRef.save(xp, app.rqOptions);
			  })
		}

		self.linkToPreview = ko.pureComputed(function () {
			var uId = app.auth.user && app.auth.user.uid || '';
			return 'app/test/test-player-edl.html?schId='+self.sequencer.peek().StorageID() + '&usr='+uId;
		})


		self.edit = function (item) {
			// body...
		}

		self.closeEditor = function () {
			router.navigate('schedules', {trigger: true, replace:true})
		}

		self.canDeactivate = function (argument) {

			if (!self.Changed()) return true;

			var title = 'Warning';
			var msg = 'Do you want to leave this page and lose all of your edits to this form?';
			return app.showMessage(msg, title, ['Yes', 'No'])
				.then(function (selectedOption) {
					return selectedOption === 'Yes';
				});
		}

		/*
		End of CRUD
		*/
	}



	//Note: This module exports a function. That means that you, the developer, can create multiple instances.
	//This pattern is also recognized by Durandal so that it can create instances on demand.
	//If you wish to create a singleton, you should export an object instead of a function.
	//See the "flickr" module for an example of object export.

	return ctor;
});