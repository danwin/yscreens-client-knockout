define([
    'durandal/app',
    'knockout.all',
    'viewmodels/model-channel',
    'viewmodels/model-schedule',
    'shared/tableview'],
		function (app, ko, ModelLib, mdSchedule, TableViewFactory) {
    //Note: This module exports an object.
    //That means that every module that "requires" it will get the same object instance.
    //If you wish to be able to create multiple instances, instead export a function.
    //See the "welcome" module for an example of function export.

    var scheduleRef = app.IO.gw.reflections['Schedules'];
    var itemRef = app.IO.gw.reflections['Channels'];
    var Schedule = mdSchedule.Schedule;
    // Implement relation Channel <- Schedule(from lookup)!

    var options = {

        displayName : 'My Channels',
        itemClass : ModelLib.Channel,
        entityName : 'Channels', // <-- data partition on the server
        attrFilter : ModelLib.StructChannel,
        dialogViews : {'edit':'views/dlg-edit-channel'},
        columns : [
            // name, sortBy, sortState
            {name:'Label'},
            {name:'Created', sortBy:'CreatedTime'},
            {name:'Comment'},
            {name:'Channel ID', sortBy:'fmtShortId'},
            {name:'Assigned Schedule', sortBy: 'ScheduleLabel'}
        ],
        endpoints : {
            'create':'',
            'read':'',
            'readMany':'',
            'update':'',
            'delete':''
        },
        populate : [
            {'Label':'Channel #1', 'CreatedTime': '2016-01-01 00:00:00', 'Comment': 'For children'},
            {'Label':'Channel #2', 'CreatedTime': '2015-12-31 00:00:00', 'Comment': 'Business channel'},
            {'Label':'Channel #3', 'CreatedTime': '2015-11-01 00:00:00', 'Comment': 'Retail'}
        ],
        'addMethods': {
            'assignSchedule': function (item) {
                var buffer = new (function () {
                    var self = this;
                    self.ChannelLabel = item.Label.peek();
                    self.Schedules = ko.observableArray([]);
                    self.SelectedSchedule = ko.observable(null);
                    self.activate = function () {
                        return scheduleRef.enum(app.rqOptions)
                            .then(function (data) {
                                self.Schedules( _.map(data, function (item) {
                                    return new Schedule(item)
                                }))
                                console.log('Schedules records: ', data)
                                return true
                            })
                        };
                    self.viewUrl = 'views/dlg-assign-schedule-to-channel';
                });

                app.showDialogEx(buffer)
                    .then(function (Ok) {
                        if (Ok) {
                            var selection = buffer.SelectedSchedule.peek() || null;
                            item.ScheduleInfo(selection)
                        }
                        return itemRef.save(item, app.rqOptions)
                    })
                    .catch(app.handleException)
            }
        }
    };

    return TableViewFactory(options);

});