// model-trigger.js
//model-schedule.js
if (typeof define !== "function" || !define.amd) {
	define = function (ignore, modfunc) {
		MODULES.model_schedule = modfunc(
			_, ko,
			MODULES.model_base,
			MODULES.model_exposition
			);
	}
}

define([
	'underscore.all',
	'knockout.all',
	'viewmodels/model-base',
	'viewmodels/model-exposition',
	'shared/datetime-helpers',
	'datetime',
	'iterator',
	'shared/rtl.keyframes'
  // 'viewmodels/io-config',
	],

function(_, ko, BaseModel, mdRecord, dt, datetime, Iterator, Keyframes){

	var SequencerError = _.createExceptionClass('Schedule.Error', Error)

	// Import types
	var LocalDateTime = datetime.LocalDateTime,
		DateTimeInterval = datetime.DateTimeInterval;

	// Helpers
	var mandatoryInterval = function (args) {
		return new DateTimeInterval(args);
	}
	var mandatoryDate = function (args) {
		return new LocalDateTime(args || new Date()); // Use args or "today"
	}
	var optionalInterval = function (args) {
		return (args === null) ? null : new DateTimeInterval(args);
	}
	var optionalDate = function (args) {
		return (args === null) ? null : new LocalDateTime(args);
	}
	var optionalDateOrInterval = function (args) {
		if (typeof args === 'number') { // <--- number means this is interval!
			return new DateTimeInterval(args)
		}
		return optionalDate(args); // <-- can be null or instance of LocalDateTime
	}
	var mandatoryDateOrInterval = function (args) {
		if (typeof args === 'number') { // <--- number means this is interval!
			return new DateTimeInterval(args)
		}
		return mandatoryDate(args); // <-- can be null or instance of LocalDateTime
	}


/* ??? */


	/*////////////////////////////////////////////////////////
	//
	// PlaybackItem
	//
	*/////////////////////////////////////////////////////////

	var StructPlayAndTrigger = function (scope, data) {
		// Persistent fields for I/O
		scope = scope || {};
		var
			Field = ko._FieldFactory_(scope, ko.observable);

		console.log('trace===1')

		Field( 'NestedSequencerId', null, data);

		Field( 'RecordInfo', null, data, function (args) {
			return (args)
				? _.omit(mdRecord.StructExposition({}, args), 'json', 'js')
				: null;
		});

		Field( 'DayOfWeek', 0, data);
		Field( 'Time', new LocalDateTime({'hours':0, 'minutes':0}), data, mandatoryDate);
		Field( 'DateTime', LocalDateTime.now(), data, mandatoryDate);

		Field( 'TriggerAction', 1, data) // 0 - loop, 1 - next

		// Field( 'Start', null, data, optionalDateOrInterval)
		console.log('trace===2')

		return self;

	}

	var TRIGGER_ACTIONS = ['go to loop', 'go to next'];
	var ACTION_LOOP = TRIGGER_ACTIONS.indexOf('go to loop');
	var ACTION_NEXT = TRIGGER_ACTIONS.indexOf('go to next');

	var PlayAndTrigger = function (data) {
		var self = this;
		StructPlayAndTrigger(this, data);

		// *** Constant fields ***
		self._rtti = 'class:PlayAndTrigger';

		// *** Computed ***
		self.Label = ko.pureComputed(function () {
			var record = self.RecordInfo();
			return (record) ? record.Label.peek() : 'empty'
		})

		self.fmtTriggerAction = ko.pureComputed(function () {
			return TRIGGER_ACTIONS[self.TriggerAction()]
		});

		self.hasNestedSequencer = ko.pureComputed(function () {
			return !!self.NestedSequencerId()
		});

		self.changesVector = ko.pureComputed(function () {
			console.warn('changesVector')
			return {
				'Time': self.Time(),
				'DayOfWeek': self.DayOfWeek(),
				'DateTime': self.DateTime()
			}
		})

	}


	var StructSequencer = function (scope, data) {
		// Persistent fields for I/O
		scope = scope || {};
		var
			Field = ko._FieldFactory_(scope, ko.observable);
			FieldA = ko._FieldFactory_(scope, ko.observableArray);

		/* Field('StorageID'),
		*  Field('CreatedTime'),
		*  Field('ModifiedTime'): */
		BaseModel.StructPersistent(scope, data);

		Field( 'Label', 'Noname', data);

		FieldA( 'Items', [], data, function (args) {
			console.log('Sequencer', '1.1.1', args)
			return new PlayAndTrigger(args)
		});

		Field( 'SequenceRange', 'ORDERED', data, function (value) {
			if (value === 'DAY' || value === 'WEEK' || value === 'DATE' || value === 'ORDERED')
				return value;
			throw new Error('Invalid value for SequenceRange')
		})

		return self;
	}


	var Sequencer = function (data) {
		var self = this;
		console.log('Sequencer', 1)
		StructSequencer(this, data);
		console.log('Sequencer', 2)

		BaseModel.IFUpdate(self, StructSequencer);
		console.log('Sequencer', 3)

		// *** Constant fields ***
		self._rtti = 'class:Sequencer';

		self._sortFunc = function (a, b) {return 0}

		// Selector for _sortFunc
		ko.computed(function () {
			var mode = self.SequenceRange();
			function wDayAndTimeToMs(item) {
				return +(item.Time.peek().timeModule()) + item.DayOfWeek.peek() * 86400000
			}
			switch (self.SequenceRange.peek()) {
				case 'DAY':
					self._sortFunc = function (a, b) {
						return (+a.Time.peek()) - (+b.Time.peek())
					}
					break;
				case 'WEEK':
					self._sortFunc = function (a, b) {
						return wDayAndTimeToMs(a) - wDayAndTimeToMs(b)
					}
					break;
				case 'DATE':
					self._sortFunc = function (a, b) {
						return (+a.DateTime.peek().dateModule()) + (+a.Time.peek().timeModule())
							 - ((+b.DateTime.peek().dateModule()) + (+b.Time.peek().timeModule()))
					}
					break;
				case 'ORDERED':
				default:
					self._sortFunc = function (a, b) {
						return 0
					}
					break;
			}
		})

		var _sortItems = function (array) {
			var mode = self.SequenceRange.peek();
			var orderPreserved = (mode === 'ORDERED' || !mode);
			if (orderPreserved) {
				console.log('_sortItems: not sotring, ORDERED')
			} else {
				array.sort(self._sortFunc)
				console.log('_sortItems: sotring, ', mode)
			}
			// Last item trigger - always "loop":
			var len = array.length;
			if (len>0) array[len-1].TriggerAction(ACTION_LOOP) // <-- always loop
			return !orderPreserved
		}

		var _itemPropsChanged = function () {
			var items = self.Items.peek();
			if (_sortItems(items)) self.Items(items)
		}

		var _subscriptions = [];

		self.Items.subscribe(function (newValue) {
			var mode = self.SequenceRange.peek();
			_.each(_subscriptions, function (subscription) {
				subscription.dispose();
			})

			_subscriptions = [];

			if (newValue === null) newValue = [];
			_.each(newValue, function (item) {
				_subscriptions.push(item.changesVector.subscribe(_itemPropsChanged))
				item.TriggerAction(ACTION_NEXT) // <-- for all items except the last
			});

			_sortItems(newValue)
		})


		/**
		 * Create new item but not push it into Items collection
		 * @return {[type]} [description]
		 */
		self.createItemInstance = function () {
			var mode = self.SequenceRange.peek();
			return new PlayAndTrigger({'TriggerRange': mode})
		}

		console.log('Sequencer', 4)

	}

	// ////////////////////////////////////////////////////
	//


	/*////////////////////////////////////////////////////////
	//
	// PlaybackItem
	//
	*/////////////////////////////////////////////////////////

	/**
	 * PlaybackItem - the root type for Schedule entities
	 * @constructor
	 * @param {object} data Initial values for fields
	 */
	var StructPlaybackItem = function (scope, data) {
		// Persistent fields for I/O
		scope = scope || {};
		var
			Field = ko._FieldFactory_(scope, ko.observable);

		/* Field('StorageID'),
		*  Field('CreatedTime'),
		*  Field('ModifiedTime'): */
		BaseModel.StructPersistent(scope, data);

		Field( 'Label', '', data);
		/**
		 * Actual duration (duration of assigned playback or summary of children)
		 * For recursive items - duration of a single-step playback
		 */
		Field( 'Duration', 0, data, mandatoryInterval);
		/**
		 * Scheduled duration (target duration)
		 */
		Field( 'SlotDuration', null, data, optionalInterval);
		Field( 'Offset', 0, data, mandatoryInterval);

		// Field( 'IsDurationLocked', false, data);
		Field( 'IsResizable', true, data); // <--- UI resizable
		Field( 'IsMovable', true, data); // <--- UI movable

		Field( 'IsOffsetLocked', false, data);
		Field( 'IsEndLocked', false, data);

		Field( 'StartOnTrigger', '', data); // <- Id of PlaybackStream?

		Field ('EditUri', '', data);

		// If item is cloned from template
		Field( 'TemplateItemId', null, data);
		// When item generated by seed item (e.g.: pause between repetitions has no template but depends on the seed position, duration)
		Field( 'SeedItemId', null, data);

		// For repetitive objects:
		Field( 'Period', null, data, optionalInterval);
		Field( 'RepeatCount', null, data).extend({ numeric: 0 });

		Field( 'Comment', '', data)

		return scope;
	}

	var PlaybackItem = function (data) {
		var self = this;
		StructPlaybackItem(this, data);

		BaseModel.IFUpdate(self, StructPlaybackItem);

		// *** Constant fields ***
		self._rtti = 'class:PlaybackItem';

		// // Notifications:
		// self.IsDurationLocked.subscribe(function (value) {
		// 	// var
		// 	// 	duration = self.SlotDuration.peek(),
		// 	// 	locked = self.IsDurationLocked.peek();
		// 	// if (locked) {
		// 	// 	if (!duration) self.SlotDuration(new DateTimeInterval(0));
		// 	// } else {
		// 	// 	if (duration) self.SlotDuration(null);
		// 	// }
		// })

		self.HasTrigger = ko.pureComputed(function () {
			return self.IsOffsetLocked()
		})

		self.IsDurationLocked = ko.pureComputed({
			'read': function () {
				return (!!self.SlotDuration())
			},
			'write': function (locked) {
				var
					duration = self.SlotDuration.peek();
				if (locked) {
					if (!duration) self.SlotDuration(new DateTimeInterval(0));
				} else {
					if (duration) self.SlotDuration(null);
				}
			}
		})

		//subscribe(function (value) {
			// var
			// 	duration = self.SlotDuration.peek(),
			// 	locked = self.IsDurationLocked.peek();
			// if (locked) {
			// 	if (!duration) self.SlotDuration(new DateTimeInterval(0));
			// } else {
			// 	if (duration) self.SlotDuration(null);
			// }
		//})

		// Computed:
		self.fmtOffset = ko.pureComputed(function () {
			return self.Offset().toString()
		})

		self.RequiredDuration = ko.pureComputed(function () {
			return self.SlotDuration() || self.Duration()
		})

		// Alias?
		self.fmtDuration = self.RequiredDuration;

		// Session lifecycle fields

		// Computed
		self.IsClone = ko.pureComputed(function () {
			return !!self.TemplateItemId();
		})

		self.IsRecursive = ko.pureComputed(function () {
			return !!self.Period()
		})

		// self.IsResizable = ko.pureComputed(function () {
		// 	return !self.IsDurationLocked()
		// })

		self.toggleResizable = function () {
			self.IsResizable(!self.IsResizable.peek())
		}

		self.toggleMovable = function () {
			self.IsMovable(!self.IsMovable.peek())
		}

		self.IsSortable = ko.pureComputed(function () {
			return !(self.TemplateItemId() || self.SeedItemId())
		})

		self.TimeRemainder = ko.pureComputed(function () {
			return new DateTimeInterval(
					(self.SlotDuration())
					? Math.max(0, self.SlotDuration() - self.Duration())
					: 0);
		})

		self.TimeOverrun = ko.pureComputed(function () {
			return new DateTimeInterval(
					(self.SlotDuration())
					? Math.max(0, self.Duration() - self.SlotDuration())
					: 0);
		})

		self.cmpPeriodSeconds = ko.pureComputed({
			'read': function () {
				return (self.Period() === null) ? null : parseInt(self.Period.peek()/1000)
			},
			'write': function (value) {
				self.Period((value === null || value === void 0) ? null : new DateTimeInterval(value * 1000))
			}
		})

		/**
		 * Computed obervable "IsSlotEqPeriod". True when slot duration equals to period.
		 * @type {boolean} Read/Write
		 */
		self.IsSlotEqPeriod = ko.pureComputed({
			'read': function () {
				return (self.Period() && +self.SlotDuration() === +self.Period())
			},
			'write': function (value) {
				if (self.Period.peek()) {
					self.SlotDuration(self.Period.peek())
				}
			}
		})

		/**
		 * Computed obervable "IsEndlessLoop". True when number of repetitions is not limited.
		 * @type {boolean} Read/Write
		 */
		self.IsEndlessLoop = ko.pureComputed({
			'read': function () {
				return (self.Period() && !self.RepeatCount())
			},
			'write': function (value) {
				if (value) {
					if (self.RepeatCount.peek()) self.RepeatCount(null)
				} else {
					if (!self.RepeatCount.peek()) self.RepeatCount(2)
				}
			}
		})

		// self.Duration.subscribe(function () {
		// 	if (!self.IsDurationLocked()) self.SlotDuration(self.Duration.peek())
		// })

		self.keyframes = function () {
			return Keyframes.keyframes(
					self.Offset.peek(),
					self.RequiredDuration.peek(),
					self.Period.peek(),
					self.RepeatCount.peek()
				)
		}

		self.iterKeyframes = function (clipLo, clipHi) {
			return self.keyframes().iter(clipLo, clipHi)
		}

		// [absolete:]
		self.iterRanges = function (selectionLo, selectionHi) {
			var offset = self.Offset.peek();
			var duration = self.RequiredDuration.peek();
			var itemRange = TimeRange(offset, duration);
			var period = self.Period.peek();
			var numRepeat = self.RepeatCount.peek() || false;
			var windowing = selectionLo !== void 0 || selectionHi !== void 0;
			var insideRange = true;
			var sentinelFunc, recursiveExpression;

			// Single range only:
			if (period === null) {
				// check limits: ...
				insideRange = windowing &&
					(selectionLo === void 0 || itemRange.hi >= selectionLo ) &&
					(selectionHi === void 0 || itemRange.lo <= selectionHi )
				return iter( insideRange ? [itemRange] : [])
			}

			// Has repetitions:
			sentinelFunc = function (item, index) {
				return (selectionHi && (item.lo > selectionHi)) || (numRepeat && (index > numRepeat))
			}

			recursiveExpression = function (item, index) {
				// find first item ???
				// nearest "further" item is:
				// intervalMs - (now % intervalMs)
				// period - ((selectionLo || offset) % period )
				var offset = (index === 0)
					? period - ((selectionLo || offset) % period )
					: item.lo;
				return TimeRange(offset, duration);
			}
			return iter(recursiveExpression, sentinelFunc)
		}

	}


	/*////////////////////////////////////////////////////////
	//
	// PlaybackFrame
	//
	*/////////////////////////////////////////////////////////
	var StructPlaybackFrame = function (scope, data) {
		// Persistent fields for I/O
		scope = StructPlaybackItem(scope, data);
		var
			Field = ko._FieldFactory_(scope, ko.observable);

		Field( 'RecordInfo', null, data, function (args) {
			return (args)
				? _.omit(mdRecord.StructExposition({}, args), 'json', 'js')
				: null;
		});

		return scope;
	}

	var PlaybackFrame = function (data) {
		var self = this;
		PlaybackItem.call(this, data);
		StructPlaybackFrame(this, data);
		BaseModel.IFUpdate(self, StructPlaybackFrame);

		// *** Constant fields ***
		self._rtti = 'class:PlaybackFrame';

		// *** Notifications ***
		// Set own duration to the duration of underluying record
		self.RecordInfo.subscribe(function () {
			if (self.RecordInfo() && self.RecordInfo.Duration) {
				self.Duration(self.RecordInfo.Duration.peek())
			}
		})

		/**
		 * PlaybackFrame.render
		 *
		 * Renders playback into specified streams.
		 * Wraps assigned record into layer,
		 * adds keyframes to start assigned record.
		 * @param  {array} scene     Mutable list of DOM obects
		 * @param  {array} keyframes Mutable list of events
		 * @param {object} options Clipping settings (optional attributes: clipLo, cipHi)
		 * @return {undefined}           None
		 */

		self.render = function (objects, keyframes, options) {
			var
				media = this.RecordInfo.peek(),
				id = self.StorageID.peek(),
				mediaId = (media) ? media.StorageID.peek() : null;

			options = options || {};

			if (media) {
				var
					count = 0,
					queue = self.keyframes().iter(options.clipLo, options.clipHi);

				while (queue.next().hasValue) {
					var slot = queue.value();
					keyframes.push({'id': id, 'mediaId': mediaId, 'lo': slot.lo, 'hi': slot.hi})
					count++
				}

				if (count > 0) {
					objects.push('<!-- schedule-frame "'+self.Label.peek()+'"" start -->');
					objects.push('<section id="'+id+'" class="schedule-frame"></section>')
					objects.push('<!-- schedule-frame stop -->');
				}
			}
		}


	}


	/*////////////////////////////////////////////////////////
	//
	// PlaybackStream
	//
	*/////////////////////////////////////////////////////////

	var StructPlaybackStream = function (scope, data) {
		// Persistent fields for I/O
		scope = StructPlaybackItem(scope, data);
		var
			Field = ko._FieldFactory_(scope, ko.observable),
			FieldA = ko._FieldFactory_(scope, ko.observableArray);

		// Reference to StructPlaybackStream which uses a "live template"

		// ??? Run updateItems here ????????
		FieldA( 'Items', [], data, function (args) {
			var
				// If child item has "Items" - it is a stream object:
				itemClass = (('Items' in args) ? PlaybackStream : PlaybackFrame);

			return new itemClass(args);
		}).extend({ rateLimit: 50 });

		// Field( 'Comment', '', data);

		FieldA( 'Tags', [], data); // <-- To-do: move "Tags" field to StructPlaybackItem

		Field( 'TriggerAtEnd', false, data);
		Field( 'TriggerActionLoop', false, data);
		Field( 'TriggerActionJumpTo', '', data);


		// // Default playlist (visible when no schedule defined)
		// Field( 'Channel', null, data);

		return scope;
	}

	var PlaybackStream = function (data) {
		var self = this;
		PlaybackItem.call(this, data);
		StructPlaybackStream(this, data);

		BaseModel.IFUpdate(self, StructPlaybackStream);

		// *** Constant fields ***
		self._rtti = 'class:PlaybackStream';

		/**
		 * Expand TreeNode if true
		 * @type {boolean}
		 */
		self.ChildrenViewExpanded = ko.observable(true);
		self.toggleExpanded = function () {
			self.ChildrenViewExpanded(!self.ChildrenViewExpanded.peek())
		}

		/**
		 * Enumerates labels for programmes in the schedule
		 * @param  {[type]} ) {			var      items [description]
		 * @return {string}   List of comma-separated labels
		 */
		self.Contents = ko.pureComputed(function () {
			var items = self.Items();
			return _.map(items, function (item) {
				return item.Label.peek()
			}).join(',')
		})

		// // "Changes monitoring": observable which reflect changes
		// self.stateVector = ko.computed(function () {
		// 	console.log('PlaybackStream.stateVector changes', self.Label.peek());
		// 	return {
		// 		Duration: self.Duration(),
		// 		Offset: self.Offset(),
		// 		Items: self.Items()
		// 	}
		// });

		self.updateItems = function () {
			// sync
			var newValue = self.Items.peek();
			// former arguments was: startIndex, endIndex
			var edgeOffset = self.Offset.peek();
			var lastAnchor = edgeOffset; // <--- last "reper" point to allign next item
			var duration = new DateTimeInterval(0);

			// console.log('-->updateItems for: ', self.Label.peek())

			// console.log('|--| running updateItems: ', self.Label.peek())


			_.each(newValue, function (item, index) {
				// |-- add nested!!!!
				if (item.IsOffsetLocked.peek()) {
					var currentOffset = item.Offset.peek();
					// "Air" duration
					// // If previoous item exists: set it "broadcasting" duration:
					// if (index > 0) {
					// 	var prevItem = newValue[index-1];
					// 	prevItem.Duration(currentOffset.subtract(prevItem.Offset.peek()))
					// }

					// adjust self: duration:
					duration.add(currentOffset.subtract(edgeOffset))
					edgeOffset =  currentOffset
				} else {
					item.Offset(edgeOffset);
				}
				if (typeof item.updateItems === 'function') {
					item.updateItems()
				}
				var increment = item.SlotDuration.peek() || item.Duration.peek();
				// console.log('|--| ', item.Label(), ', edgeOffset', edgeOffset.module(), increment.module())
				edgeOffset = edgeOffset.add(increment);
				duration = duration.add(increment)
			})

			// Update own duration:
			// if (!self.IsDurationLocked.peek())
			self.Duration(duration)
			// Set SlotDuration to Duration if not IsDurationLocked
			if (!self.IsDurationLocked.peek()) self.SlotDuration(duration)
		}

		self.Items.subscribe(self.updateItems);

		self.Offset.subscribe(self.updateItems)

		self.addFrame = function () {
			self.Items.push(new PlaybackFrame());
		}
		self.addStream = function () {
			self.Items.push(new PlaybackStream());
		}
		self.removeItem = function (item) {
			self.Items.remove(item)
		}

		// Convert to iter()
		// Think how to unify wuth iterator? Unique and cobed items, difference, ...
		self.repeatItem = function (item, period, times, complement) {
			// What to do with subsequent items???
			// Move them to start as "shaded"???
			// remain as-is...
			// or: move to subgroup with fixed duration...
			var
				buffer = self.Items.peek(),
				tail, intervalStream,
				itemDuration = item.SlotDuration.peek() || item.Duration.peek(),
				_periodObj = (period instanceof DateTimeInterval)
					? period
					: ((typeof period === 'number')
							? new DateTimeInterval(period)
							: new DateTimeInterval(0)),
				pauseDuration = (_periodObj > 0)
					? _periodObj.subtract(itemDuration) : null,
				itemId = item.StorageID.peek(),
				itemCloneData = _.extend(item.toJS(), {
					'TemplateItemId': itemId,
					'Period' : _periodObj,
					'SlotDuration': itemDuration,
					'IsDurationLocked': true
				}),
				pauseData = (pauseDuration > 0) ? {
					'SeedItemId': itemId,
					'SlotDuration': pauseDuration,
					'Label': 'interval',
					// 'Period': _periodObj,
					'IsDurationLocked': true
				} : null,
				itemClass; // <- must support IFUpdate interface

			times = times || 5;

			if (item instanceof PlaybackFrame) {
				itemClass = PlaybackFrame
			} else if (item instanceof PlaybackStream) {
				itemClass = PlaybackStream
			} else {
				throw new Error('Cannot clone item of this class')
			}

			// Extract the trailing items after the target item:
			var itemIndex = buffer.indexOf(item);
			tail = buffer.splice(itemIndex + 1);

			// Set period for item
			item.Period(_periodObj);

			for (var i=0; i<times; i++) {
				if (pauseData) {
					intervalStream = new PlaybackStream(pauseData);
					console.warn('intervalStream', '_periodObj', _periodObj.toString(),  'pauseDuration', pauseDuration.toString(), 'itemDuration', itemDuration.toString(),'pauseData', pauseData, 'intervalStream', ko.toJS(intervalStream))
					if (tail) { // Move the trailing part into the first interval
						intervalStream.Items(tail);
						tail = null;
					}
					buffer.push(intervalStream);
					console.log('^intervalStream' + ',' + intervalStream.Duration() + ',' + intervalStream.SlotDuration());
				}
				buffer.push(new itemClass(itemCloneData));
			}
			// Place tail at then end of timeline if repetitions without intervals:
			if (tail) buffer = buffer.concat(tail)
			self.Items(buffer)
			console.log('^item updateItems: ', item.Label())
		}

		// PlaybackStream.iterRanges
		self.iterRanges = function (selectionLo, selectionHi) {
			var offset = self.Offset.peek();
			var duration = self.RequiredDuration.peek();
			var itemRange = TimeRange(offset, duration);
			var period = self.Period.peek();
			var numRepeat = self.RepeatCount.peek() || false;
			var windowing = selectionLo !== void 0 || selectionHi !== void 0;
			var insideRange = true;
			var sentinelFunc, recursiveExpression;

			// Single range only:
			if (period === null) {
				// check limits: ...
				insideRange = windowing &&
					(selectionLo === void 0 || itemRange.hi >= selectionLo ) &&
					(selectionHi === void 0 || itemRange.lo <= selectionHi )
				return iter( insideRange ? [itemRange] : [])
			}

			// Has repetitions:
			sentinelFunc = function (item, index) {
				return (selectionHi && (item.lo > selectionHi)) || (numRepeat && (index > numRepeat))
			}

			recursiveExpression = function (item, index) {
				// find first item ???
				// nearest "further" item is:
				// intervalMs - (now % intervalMs)
				// period - ((selectionLo || offset) % period )
				var offset = (index === 0)
					? period - ((selectionLo || offset) % period )
					: item.lo;
				return TimeRange(offset, duration);
			}
			return iter(recursiveExpression, sentinelFunc)
		}


		/**
		 * PlaybackStream.render
		 *
		 * Renders playback from stream contents.
		 * Wraps assigned record into layer,
		 * adds keyframes to start assigned record.
		 * @param  {array} scene     Mutable list of DOM obects
		 * @param  {array} keyframes Mutable list of events
		 * @param {object} options Clipping settings (optional attributes: clipLo, cipHi)
		 * @return {undefined}           None
		 */
		self.render = function (objects, keyframes, options) {
			var
				self = this,
				items = this.Items.peek(),
				id = this.StorageID.peek(),

				clipLo = self.Offset.peek(),
				clipHi = clipLo + self.RequiredDuration.peek();

			options = options || {};

			if (items.length > 0) {
				var
					count = 0,
					// queue = {'keyframes': self.keyframes().toJS(), 'clipLo': clipLo, 'clipHi': clipHi};
					queue = self.iterKeyframes(options.clipLo, options.clipHi);

				while (queue.next().hasValue) {
					var slot = queue.value();
					var clippingRegion = {'clipLo': slot.lo, 'clipHi': slot.hi}

					// keyframes.push({'id': id, 'lo': slot.lo, 'hi': slot.hi})

					// Render children in each repetiotion of stream:
					objects.push('<!-- Stream: '+id+' (start) -->')

					_.each(this.Items.peek(), function (child) {
						child.render(objects, keyframes, clippingRegion)
					})
					objects.push('<!-- Stream: '+id+' (end) -->')

					count++
				}
			}
		}

	}

	/*////////////////////////////////////////////////////////
	//
	// PlaybackSchedule
	//
	*/////////////////////////////////////////////////////////

	var StructPlaybackSchedule = function (scope, data) {
		// Persistent fields for I/O
		scope = StructPlaybackStream(scope, data);
		var
			Field = ko._FieldFactory_(scope, ko.observable),
			FieldA = ko._FieldFactory_(scope, ko.observableArray);

		/**
		 * zero or some moment
		 */
		Field( 'Start', null, data, optionalDateOrInterval) // <-- stream becomes TRIGGER!

		// Smallest unit in millisecons
		Field( 'Quantum', 1 * 60 * 1000, data);

		return scope;
	}

	/**
	 * Top-level collection container with functions to edit and view items.
	 * Has methods for chart view and for rendering.
	 * @param {object} data Values for initialization (same as persistent properties)
	 */
	var PlaybackSchedule = function (data) {
		var self = this;
		PlaybackStream.call(this, data);
		StructPlaybackSchedule(this, data);

		BaseModel.IFUpdate(self, StructPlaybackSchedule);

		// *** Constant fields ***
		self._rtti = 'class:PlaybackSchedule';

		// Override restrictions properties
		/**
		 * Disallow to sort schedule with another items
		 * @type {Boolean}
		 */
		self.IsSortable = false;

		// self.IsStartLocked.subscribe(function (value) {
		// 	var start = self.Start.peek();
		// 	if (value) {
		// 		if (!start) self.Start(new LocalDateTime(new Date()));
		// 	} else {
		// 		if (start) self.Start(null);
		// 	}
		// })

		self.IsStartLocked = ko.pureComputed({
			'read': function () {
				return (!!self.Start())
			},
			'write': function (locked) {
				var
					start = self.Start.peek();
				if (locked) {
					if (!start) self.Start(LocalDateTime.now());
				} else {
					if (start) self.Start(null);
				}
			}
		})

		self.HasTrigger = ko.pureComputed(function () {
			return self.IsStartLocked()
		})

		//Alias
		self.IsAbsolute = self.IsStartLocked;

		// self.IsResizable = false;

		// self.IsStartLocked = ko.pureComputed({
		// 	read: function () {
		// 		return self.Start() !== null;
		// 	},
		// 	write: function (value) {
		// 		console.log('wetting writable value: IsStartLocked', value)
		// 		if (value) {
		// 			if (!self.Start.peek()) {
		// 				self.Start(new LocalDateTime(new Date()));
		// 			}
		// 		} else {
		// 			self.Start(null);
		// 		}
		// 	}
		// })


		// self.selectChildren

		self.getParentFor = function (item) {
			var result = null;
			function walker(parent) {
				if (parent.Items) {
					var collection = parent.Items.peek();
					if (collection.indexOf(item)>-1) {
						result = parent;
						console.log('getParentFor: parent found in ', parent)
						return parent;
					} else {
						return _.find(collection, walker);
					}
				}
				console.log('getParentFor: continue search...')
				// return null;
			}

			walker(self);
			return result;
		}

		// self.getParentFor = function (item) {
		// 	var result = null;
		// 	function walker(parent) {
		// 		if (parent.Items) {
		// 			if (parent.Items.indexOf(item)>-1) {
		// 				result = parent;
		// 				console.log('getParentFor: parent found')
		// 				return true;
		// 			} else {
		// 				return _.find(parent.Items.peek(), walker);
		// 			}
		// 		}
		// 		console.log('getParentFor: continue search...')
		// 		return false;
		// 	}

		// 	walker(self);
		// 	return result;
		// }

		self.AllItems = ko.pureComputed(function () {
			var buffer = [];
			function walker(node) {
				if (node.Items) {
					_.each(node.Items.peek(), walker)
				} else {
					buffer.push(node)
				}
			}
			var treeLevel = self.Items();
			_.each(treeLevel, walker);
			return buffer;
		})

		self.AllNodesAndItems = ko.pureComputed(function () {
			var buffer = []; // <-- add self?
			function walker(node) {
				console.log('node: ', node);
				buffer.push(node)
				if (node.Items) {
					_.each(node.Items.peek(), walker)
				}
			}
			var treeLevel = self.Items();
			console.log('self.Items', treeLevel)
			_.each(treeLevel, walker);
			console.log('[',buffer,']');
			return buffer;
		})

		// self.reflectToScreen = function (item) {
		// return {
		// 	'style': {
		// 		'left': data.screenLeftPcnt + '%',
		// 		'width': data.screenWidthPcnt + '%',
		// 	},
		// 	'class': data.classNames.join(' ')
		// }
		// }

		self.asTable = ko.pureComputed(function () {
			var buffer = [];
			function walker(node, index, list) {
				buffer.push({
					'item': node,
					'parent': this
				})
				if (node.Items) {
					_.each(node.Items.peek(), walker, node.Items)
				}
			}
			_.each(self.Items(), walker, self.Items);
			return buffer;
		})

		self.addItem = function (item, parent) {
			parent = parent || self;
			if (parent.Items) {
				parent.Items.push(item);
				return item;
			}
			throw new SequencerError('Cannot add item - the parent node is not a Stream object');
		}

		self.removeItem = function (item) {
			var parent = self.getParentFor(item);
			if (parent) {
				parent.Items.remove(item);
			} else {
				throw new Error('Cannot find parent for item');
			}
		}

		self.resizeItem = function (item, newDuration, lockUpdates) {
			var parent, value;
			if (typeof newDuration === 'number') {
				// value in ms:
				console.log('self.resizeItem: newDuration is number', newDuration)
				value = new DateTimeInterval(newDuration)
			} else if (newDuration instanceof DateTimeInterval) {
				console.log('self.resizeItem: newDuration is DateTimeInterval', newDuration)
				value = newDuration
			} else {
				throw new Error('Invalid duration type: ' + typeof newDuration)
			}
			item.SlotDuration(value);
			// force refresh?
			if (!lockUpdates && item !== self) {
				parent = self.getParentFor(item);
				if (parent) {
					parent.updateItems();
				} else {
					throw new Error('Cannot find parent for item');
				}
			}
		}

		self.moveItem = function (item, newOffset, lockUpdates) {
			var parent, value;
			if (typeof newOffset === 'number') {
				// value in ms:
				console.log('self.resizeItem: newOffset is number', newOffset)
				value = new DateTimeInterval(newOffset)
			} else if (newOffset instanceof DateTimeInterval) {
				console.log('self.resizeItem: newOffset is DateTimeInterval', newOffset)
				value = newOffset
			} else {
				throw new Error('Invalid duration type: ' + typeof newOffset)
			}
			item.Offset(value);
			// force refresh?
			if (!lockUpdates) {
				parent = self.getParentFor(item);
				if (parent) {
					parent.updateItems();
				} else {
					throw new Error('Cannot find parent for item');
				}
			}
		}

		self.repeatTreeItem = function (item, period, times) {
			var parent = self.getParentFor(item);
			parent.repeatItem(item, period, times)
		}

		self.createRecursivePattern = function (itemToRepeat, period, times) {
			var
				// itemToRepeat = new PlaybackStream(),
				buffer = self.Items.peek();

			period = period || itemToRepeat.Period.peek();
			times = times || itemToRepeat.RepeatCount.peek();

			buffer.unshift(itemToRepeat);
			self.Items(buffer);
			console.log('->createRecursivePattern, item added:', self.Items.length)
			self.repeatItem(itemToRepeat, period, times)

		}

		self.sortTriggers = function () {
			self.Items.sort(function (left, right) {
					var oLeft = +left.Offset.peek(), oRight = +right.Offset.peek();
					return (oLeft == oRight) ? 0 : (oLeft < oRight ? -1 : 1) })
		}

		// self.lockItemOffset = function (item, offset) {
		// 	var parent = self.getParentFor(item);
		// 	if (parent === self) {
		// 		// Already on in the top level:
		// 		item.IsOffsetLocked(true)
		// 		item.Offset(offset)
		// 	} else {
		// 		// find index of item in collection:
		// 		var index = parent.Items.indexOf(item);
		// 		// cut item and its nex siblings:
		// 		var buff = parent.Items.peek().splice(index);
		// 		// find the right index depending on time offset:
		// 		var nextItem =  _.find(self.Items.peek(), function (item) {
		// 			return (item.Offset.peek() > offset)
		// 		})
		// 		if (nextItem) {
		// 			var targetIndex = self.Items.indexOf(nextItem);


		// 		}
		// 	}
		// }

		/**
		 * Lower "visible" time
		 * @type {[type]}
		 */
		self.selectionLo = ko.observable(null);

		/**
		 * Upper "visible" time
		 * @type {[type]}
		 */
		self.selectionHi = ko.observable(null);

		self.asIterator = function () {
			// body...
			var start = self.Start.peek();
			var duration = self.RequiredDuration.peek();
			var children = self.Items.peek();

			var selLo = self.selectionLo.peek();
			var selHi = self.selectionHi.peek();

			var noLoLimit = (selLo === null);
			var noHiLimit = (selHi === null);

			var filterFunc = function (item) {
				// Allow each item which intersects 'selection' range
				var match = true;
				var itemOffset = item.Offset.peek(), itemDuration = item.SlotDuration.peek() || item.Duration.peek();
				// Chck lower resrictions:
				match = match || noLoLimit || (start + itemOffset >= selLo)
				match = match || noHiLimit || (start + itemOffset + itemDuration <= selHi)
				return match
			}

			var points = _.filter(children, filterFunc);

			function flatten (list) {
				// replace collection with plain items ...
				return _.map(list, function (item) {
					// if (item.Items...)
				})
			}

			// Then run for nested streams, until elementary elements only remains:
			// ...

			return new Iterator(points, stopFunc)

		}

		// *** Methods ***

		self.renderToObj = function () {
			var
				objects = [],
				keyframes = [],
				geometry, behavior, compiled;
			var clippingRegion = {
				'clipLi': self.Start.peek(),
				'clipHi': self.Start.peek() + self.RequiredDuration.peek()
			}
			try {
				self.render(objects, keyframes, clippingRegion )
					//{'clipLo':self.selectionLo.peek(), 'clipHi': self.selectionHi.peek()});
			} catch (e) {
				console.error('renderToObj error:', e)
				throw e;
			}
			geometry = objects.join(' ');
			// behavior = ko.mapping.toJSON(keyframes);
			compiled = {
				'start': 	self.Start.peek(),
				'duration': self.Duration.peek(),
				'scene': geometry,
				'motion': keyframes // unwrap array - it contain single item
			};
			console.log('rendering --->>----', compiled)
			return compiled;
		}

		/**
		 * Topmost method to generate rendering in JSON format
		 * @return {string}              Serialized JSON for player
		 */
		self.renderJSON = function () {
			var serialized = JSON.stringify(self.renderToObj());
				console.log('---JSON---', serialized)
			return serialized
		}

	}


	return {
		TRIGGER_ACTIONS: TRIGGER_ACTIONS,

		StructPlayAndTrigger: StructPlayAndTrigger,
		PlayAndTrigger: PlayAndTrigger,

		StructSequencer: StructSequencer,
		Sequencer: Sequencer,

		SequencerError: SequencerError
	}


});
