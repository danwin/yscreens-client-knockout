define([
    'durandal/app',
    'knockout.all',
    'underscore',
    'viewmodels/model-device',
    'viewmodels/model-channel',
    'shared/tableview'],
function (app, ko, _, ModelLib, mdChannel, TableViewFactory) {
    //Note: This module exports an object.
    //That means that every module that "requires" it will get the same object instance.
    //If you wish to be able to create multiple instances, instead export a function.
    //See the "welcome" module for an example of function export.

    var channelRef = app.IO.gw.reflections['Channels'];
    var itemRef = app.IO.gw.reflections['Devices'];
    var screenBindingsRef = app.IO.gw.reflections['ScreenBindings'];
    var Channel = mdChannel.Channel;

    // Buffer for screen binding records - used also in "lists" member of options!
    var gearsBindings = ko.observableArray([]);


// Request to fill screen bindings list:
    var reloadGearsBindings = function () {
        return screenBindingsRef.enum(app.rqOptions)
            .then(function (response) {
                gearsBindings(response)
                // options.lists.ScreenBindings(response)
            })
            .catch(app.handleException)
    };

    app.on('tableview:activate').then(reloadGearsBindings)


    var options = {

        displayName : 'My Devices',
        itemClass : ModelLib.Device,
        attrFilter : ModelLib.StructDevice,
        entityName: 'Devices',
        dialogViews : {'edit':'views/dlg-edit-device'},
        columns : [
            // name, sortBy, sortState
            // {name:'Device ID', sortBy:'StorageID'},
            // {name:'Label'},
            // {name:'Linked Channel', sortBy: 'ChannelLabel'},
            // {name:'Hardware ID', sortBy: 'fmtLinkedGear'},
            // {name: 'Resolution'},
            // {name: 'Browser'},
            // {name: 'Address'},
            // {name:'Created', sortBy:'CreatedTime'},
            // {name:'Comment'}
            {name:'Label'},
            {name: 'Resolution'},
            {name: 'Browser'},
            {name:'Created', sortBy:'CreatedTime'},
            {name: 'Address'},
            {name:'Linked Channel', sortBy: 'ChannelLabel'},

            {name:'Device ID', sortBy:'fmtShortId'},
            {name:'Hardware ID', sortBy: 'fmtLinkedGear'},
            {name:'Comment'}
        ],
        endpoints : {
            'create':'',
            'read':'',
            'readMany':'',
            'update':'',
            'delete':''
        },
        populate : [
            {'Label':'Device #1', 'Address':'Moscow', 'CreatedTime': '2016-01-01 00:00:00', 'Comment': 'For children'},
            {'Label':'Device #2', 'Address':'Moscow', 'CreatedTime': '2015-12-31 00:00:00', 'Comment': 'Business device'},
            {'Label':'Device #3', 'Address':'Moscow', 'CreatedTime': '2015-11-01 00:00:00', 'Comment': 'Retail'}
        ],
        lists : {
            ScreenTypes : [
                'unknown',
                '800x600, 4:3 (SVGA)',
                '1024x768, 4:3 (XGA)',
                '1152x864, 4:3 (XGA+)',
                '1280x720, 16:9 (WXGA)',
                '1280x768, 5:3 (WXGA)',
                '1280x800, 16:10 (WXGA)',
                '1280x1024, 5:4 (SXGA)',
                '1440x900, 16:10 (WXGA+)',
                '1600x900, 16:9 (HD+)',
                '1600x1200, 4:3 (UXGA)',
                '1680x1050, 16:10 (WSXGA+)',
                '1920x1080, 16:9 (FHD)',
                '1920x1200, 16:10 (WUXGA)',
                '2560x1440, 16:9 (QHD)',
                '2560x1600, 16:10 (WQXGA)',
                '3840x2160, 16:9 (4K UHD)'
            ],
            Browsers: [
                'unknown',
                'Chomium',
                'Firefox',
                'Opera',
                'embedded'
            ],
            ScreenBindings: gearsBindings
        },
        'addMethods': {
            'pointLocation': function (item) {
                app.showDialogEx('viewmodels/dlg-device-location', item)
                    .then(function (Ok) {
                        if (Ok) {
                            itemRef.update(item, app.rqOptions)
                                .catch(app.handleException)
                        }
                    })
/*

obj : Object|string
The object (or moduleId) to display as a dialog.
activationData : Object optional
The data that should be passed to the object upon activation.

 */
            },

            'assignChannel': function (item) {
                var buffer = new (function () {
                    var self = this;
                    self.DeviceLabel = item.Label.peek();
                    self.Channels = ko.observableArray([]);
                    self.SelectedChannel = ko.observable(null);
                    self.activate = function () {
                        return channelRef.enum(app.rqOptions)
                            .then(function (data) {
                                self.Channels( _.map(data, function (item) {
                                    return new Channel(item)
                                }))
                                console.log('Channels records: ', data)
                                return true
                            })
                        };
                    self.viewUrl = 'views/dlg-assign-channel-to-device';
                });

                app.showDialogEx(buffer)
                    .then(function (Ok) {
                        if (Ok) {
                            var selection = buffer.SelectedChannel.peek() || null;
                            item.ChannelInfo(selection)
                        }
                        return itemRef.save(item, app.rqOptions)
                    })
                    .catch(app.handleException)
            },

            'linkToHardware': function (item) {
                var buffer = new (function () {
                    var self = this;
                    self.DeviceLabel = item.Label.peek();
                    self.Gears = ko.observableArray(gearsBindings.peek());
                    self.SelectedGear = ko.observable(null);
                    self.activate = function () {
                        return screenBindingsRef.enum(app.rqOptions)
                            .then(reloadGearsBindings)
                        };
                    self.viewUrl = 'views/dlg-bind-device-gear';
                });

                app.showDialogEx(buffer)
                    .then(function (Ok) {
                        if (Ok) {
                            var selection = buffer.SelectedGear.peek() || null;
                            if (selection) {
                                var LogicalID = selection.LogicalID;
                                item.LogicalID(LogicalID)
                            } else {
                                item.LogicalID(null)
                            }
                            item.HardwareInfo(selection) // <-- session-only data
                        }
                        return itemRef.save(item, app.rqOptions)
                    })
                    .catch(app.handleException)
            }

        }
    };

    return TableViewFactory(options);

});