define(function(require){
	var _ = require('underscore.all');
	var ko = require('knockout.all');
	var app = require('durandal/app');
	var IO = require('viewmodels/io-config');
	var mdUser = require('viewmodels/model-user');

	var ctor = function(){

		this.signUp = function () {
			var dataBuffer = {
				// 'email': ko.observable(''),
				'email': ko.observable(''),
				'password': ko.observable(''),
				'passwordConfirmation': ko.observable('')
			},
			data;

			var filterDialogResult = function (Ok) {

				if (!Ok) throw new Error('Cancelled by user');
				data = ko.toJS(dataBuffer);

				if (data.password !== data.passwordConfirmation) {
					// console.error(data.password, data.passwordConfirmation) 
					throw new Error('Password and password confirmation are different!');
				}
				return _.pick(data, 'email', 'password')
			}

			var createProviderAccount = function (credentials) {
				return new Promise(function (resolve, reject) {
					app.auth.createUser(credentials)
						.then(resolve)
						.catch(function (error) {
							var messages = {
								'auth/email-already-in-use': 
									'Sorry, you cannot use this e-mail address here. Contact administrator, please',
								'auth/invalid-email': 
									'This e-mail address is invalid',
								'auth/operation-not-allowed': 
									'Email-based accounts disabled. Contact administrator, please',
								'auth/weak-password': 
									'This password is weak. Use at least 6 symbols.'							
							};
							var message = (error && error.code) ? messages[error.code] : 'Cannot activate account. Unnown error.';
							reject (message)
						})
				})
				// return app.auth.createUser(credentials)
				// 	// .then(function (rawUser) {
				// 	// 	return rawUser;
				// 	// })
				// 	.catch(function (error) {
				// 		var messages = {
				// 			'auth/email-already-in-use': 
				// 				'Sorry, you cannot use this e-mail address here. Contact administrator, please',
				// 			'auth/invalid-email': 
				// 				'This e-mail address is invalid',
				// 			'auth/operation-not-allowed': 
				// 				'Email-based accounts disabled. Contact administrator, please',
				// 			'auth/weak-password': 
				// 				'This password is weak. Use at least 6 symbols.'							
				// 		};
				// 		var message = (error && error.code) ? messages[error.code] : 'Cannot activate account. Unnown error.';
				// 		return (message)
				// 	})
				// 	.then(function (errMessage) {
				// 		throw new Error(errMessage)
				// 	})
			}


			var checkPreRegistration = function (rawUser) {
				// Assumes that new user is already signed in a system
				return new Promise(function (resolve, reject) {
					// body...
					console.log('~rawUser, data', rawUser, data)
					if (data && rawUser && data.email === rawUser.email) {
						var svc = app.IO.gw.services['UserInfo'];
						// var opt = app.rqOptions; //<- userId assumed to be in auth.user inside options
						var opt = _.extend({}, app.rqOptions, {'pathArgs':{'userEmail':data.email}})
						svc.read(opt)
							.then(function (result) {
								console.log('~query result: ', result)
								resolve(result);
							})
							.catch(function (reason) {
								reject('~Error on query ', reason);
							}) //<-preRegistered user data, if any
					} else reject('Cannot sign in the New user!')
				})

				// console.log('~rawUser, data', rawUser, data)
				// if (data && rawUser && data.email === rawUser.email) {
				// 	var svc = app.IO.gw.services['UserInfo'];
				// 	// var opt = app.rqOptions; //<- userId assumed to be in auth.user inside options
				// 	var opt = _.extend({}, app.rqOptions, {'pathArgs':{'userEmail':data.email}})
				// 	return svc.read(opt)
				// 		.then(function (result) {
				// 			console.log('~query result: ', result)
				// 			return Promise.resolve(result);
				// 		})
				// 		.catch(function (reason) {
				// 			console.error('~Error on query ', reason);
				// 		}) //<-preRegistered user data, if any
				// } else throw new Error('New user was not signed in!')
			}

			var store3rdPartyUID = function (preRegisteredData) {
				console.log('~preRegisteredData', preRegisteredData)
				return new Promise(function (resolve, reject) {
					if (preRegisteredData) {
						// User found (pre-registered)
						var email = app.auth.user.email;
						var uid = app.auth.user.uid;

						console.log('~auth.user', email, uid)

						var oUser = new mdUser.User({'UID': uid, 'AccountEmail':email})
						// Update 3rd-patry UID in the extended database:
						app.IO.gw.reflections['AdmUsers']
							.update(oUser, app.rqOptions)
							.then(resolve)
							.catch(reject);
					} else reject('Sorry, your account was not confirmed by Administrator.')
				})
			}

			var greetings = function () {
				app.showMessage('Welcome to yScreens!', 'Congratulations', ['Yes']);
				return Promise.resolve(true)
			}

			var deleteProviderAccount = function (reason) {
				return new Promise(function (resolve, reject) {
					// User not found in extended DB:
					if (app.auth.user && app.auth.user.email === data.email) { 
						app.auth.deleteCurrentUser()
							.then(function (response) {
								app.showMessage('Sorry, your account was not confirmed by Administrator.');
								app.trace.log('temporary user successfully removed!', response)
								resolve()
							})
							.catch(function (reason) {
								app.trace.log('Error removing temporary user!', reason)
								reject('Error removing temporary account')
							})
					} else reject(reason)
				})

				// // User not found in extended DB:
				// if (app.auth.user && app.auth.user.email === data.email) 
				// 	return app.auth.deleteCurrentUser()
				// 		.then(function (response) {
				// 			app.showMessage('Sorry, your account was not confirmed by Administrator.');
				// 			app.trace.log('temporary user successfully removed!', response)
				// 		})
				// 		.catch(function (reason) {
				// 			app.trace.log('Error removing temporary user!', reason)
				// 		})
				// throw new Error('Account was not confirmed.')
			}

			// var dlgCtx = mdUser.User();
			app.customDialog({
					'buffer': dataBuffer,
					'attrFilter': function (scope, value) {
						return _.extend(scope, value)
					},
					'viewUrl': 'views/dlg-sign-up'				
				})
				.waitConfirm()
				// arg: Ok
				.then(filterDialogResult)
				// arg: {email, password}
				.then(createProviderAccount)
				// arg: rawUser
				.then(checkPreRegistration)
				// arg: preRegisteredData
				.then(store3rdPartyUID)
				// arg: none
				.then(greetings)

				// .then(function (preRegisteredData) {
				// 	console.log('preRegisteredData', preRegisteredData)
				// 	if (preRegisteredData) {
				// 		// User found (pre-registered)
				// 		var email = auth.user.email;
				// 		var uid = auth.user.uid;
				// 		var oUser = new mdUser.User({'UID': uid, 'AccountEmail':email})
				// 		// Update 3rd-patry UID in the extended database:
				// 		return app.IO.gw.reflections['AdmUsers'].update(oUser, app.rqOptions);
				// 	} else {
				// 		// User not found in extended DB:
				// 		if (app.auth.user.email === data.email) 
				// 			return app.auth.deleteCurrentUser()
				// 				.then(function (response) {
				// 					app.showMessage('Sorry, your account was not confirmed by Administrator.');
				// 					app.trace.log('temporary user successfully removed!', response)
				// 				})
				// 				.catch(function (reason) {
				// 					app.trace.log('Error removing temporary user!', reason)
				// 				})
				// 		throw new Error('Account was not confirmed.')
				// 	}
				// })

				// arg: reason, internally uses "data" and "auth.user"
				.catch(deleteProviderAccount)
				// if something happened on deleteProviderAccount
				.catch(app.handleException)


			// needs IO-config, dialog or app.dialog
        //     if (data) {
        //         var serialized = ko.toJS(data)
        //         var pwd = serialized.password;
        //         var email = serialized.ContactEmail;
        //         console.log('creating user >>>');
        //         return (email && pwd) 
        //             ?app.auth.createUser({email: email, password: pwd})
        //             :Promise.reject('Invalid data in account record')
        //     }
        //     return Promise.resolve(data);
		}
		this.canActivate = function () {
			if (app.auth.user) return '#dashboard';
			return true
		}
		this.activate = function () {
			console.log('Landing activate')
		}
	}
	return ctor;
});