//storage-reflections.js
// STUB FOR DEBUGGING
if (typeof define !== "function" || !define.amd) {
	define = function (ignore, modfunc) {
		MODULES.storage_reflections = modfunc(
			_, ko, IO,
			MODULES.model_asset,
			MODULES.model_timeline,
			MODULES.model_display,
			MODULES.model_channel,
			MODULES.model_device,
			MODULES.model_user,
			MODULES.model_exposition,
			MODULES.model_schedule,
			MODULES.model_sequencer
			);
	}
}

//model-display.js

define([
	'underscore.all',
	'knockout.all',
	'shared/io.middleware',
	'viewmodels/model-asset',
	'viewmodels/model-timeline',
	'viewmodels/model-display',

	'viewmodels/model-channel',
	'viewmodels/model-device',
	'viewmodels/model-user',
	'viewmodels/model-exposition',
	'viewmodels/model-schedule',
	'viewmodels/model-sequencer'
	],

function(_, ko, IO,
	mdAsset,
	mdTimeline,
	mdDisplay,
	mdChannel,
	mdDevice,
	mdUser,
	mdExposition,
	mdSchedule,
	mdSequencer){

	// Shortcuts
	var mandatory = _.assertDefined;

	// Common methods:
	var _toJS = function (obj) {
		// Set new "ModifiedTime" if object supports IFUpdate interface
		if (typeof obj.markUpdates === 'function') obj.markUpdates();
		return obj.toJS()
	}
	var _fromJS = function (obj, data) {
		obj.updateValues(data)
		return obj
	}
	var _makeKey = function (objItem) {
		return objItem.StorageID.peek()
	}

	var ItemReflection = IO.ItemReflection;
	var ObjectReflection = IO.ObjectReflection;
	var Asset = mdAsset.Asset;
	var Timeline = mdTimeline.Timeline;
	var Playlist = mdDisplay.Playlist;

	// Moved into methods below ()
	// var Channel = mdChannel.Channel;
	// var Device = mdDevice.Device;
	// var User = mdUser.User;

	var BasicReflection = ItemReflection(null);

	BasicReflection.toJS(_toJS)
	BasicReflection.fromJS(_fromJS);
	BasicReflection.makeKey(_makeKey);
	BasicReflection.itemFactory(function (data) {
		return data // <--- return data "as-is" by default
	});

	// For each specific class - do no forget to assign itemFactory function!
	// Note that data is already in argument already transformed (i.e. not necessary to call "toJS" explicitly)

	var AssetReflection = ItemReflection(null);

	AssetReflection.toJS(function (objItem) {
		// convert to the common "upload" task data
		var data; // <- IFUpdate.toJS() used
		// translate Asset.uploadFile to the common 'uploadData' attribute
		// and encode metadata
		var uploadTask = {};

		// Set new ModifiedTime (Using IFUpdate method):
		objItem.markUpdates();
		data = objItem.toJS() // <- IFUpdate.toJS() used
		// [1] 'uploadData' (if any)
		uploadTask['uploadData'] = objItem.uploadFile.peek();
		// Note that "undefined" values will be ignored by default:)
		// [2] 'matadata' - mandatory attribute with 1 mandatory field 'contentType'
		uploadTask['metadata'] = {
			'contentType': _.popAttr(data, 'MimeType')

			,'md5Hash': _.popAttr(data, 'md5Hash') 	//string 	YES on upload, NO on updateMetadata
			,'cacheControl': _.popAttr(data, 'cacheControl') 	//string 	YES
			,'contentDisposition': _.popAttr(data, 'contentDisposition') 	//string 	YES
			,'contentEncoding': _.popAttr(data, 'contentEncoding') 	//string 	YES
			,'contentLanguage': _.popAttr(data, 'contentLanguage') 	//string 	YES
			// These fields will be ignored in FirebaseStrorage
			,'size': _.popAttr(data, 'FileSize')
			}; //<- at least: contentType
		// [3] - 'customMetadata' - all user-defined data
		uploadTask['customMetadata'] = data
		return uploadTask
	});

	var convertFromJS = function (objItem, response) {
		// decode metadata
		// note that the CDN server can return additional and changed values so update object to appropriate status!
		var _srvMd = response.metadata || {};
		console.log('=>>> response', response, '_srvMd:', _srvMd);
		// Unwrap metadata, and map known fields:
		var data = _.extend({}, _srvMd, response.customMetadata || {}, {
			'FileSize': _srvMd.size,
			'Src': _srvMd.url,
			'SrvFileName': _srvMd.fileName,
			'SrvCreatedTime': _srvMd.created,
			'SrvModifiedTime': _srvMd.updated,
			'MimeType': _srvMd.contentType
			/* Field('StorageID'),
			*  Field('CreatedTime'),
			*  Field('ModifiedTime'): */
		});
		objItem.updateValues(data) // <- IFUpdate.updateValues() used
		return objItem
	}

	AssetReflection.fromJS(convertFromJS);

	AssetReflection.itemFactory(function (data) {
		var item = new Asset(data);
		// Apply data transformation with ".toJS":
		return convertFromJS(item, data)
		// return new Asset(data) // <--- Redefine that before actual call - here Asset is not defined
	});

	AssetReflection.makeKey(function (objItem) {
		try {
			var
				fileName = objItem.FileName.peek(),
				fileExt = fileName.match(/\./gi) ? fileName.split('.').pop() : objItem.MimeType.peek().split('/').pop();
			return [objItem.StorageID.peek(), fileExt].join('.')
		} catch (e) {
			console.error('AssetReflection.makeKey: ', e, 'objItem:', objItem)
			return 0
		}
	});


    var _fmtMBytes = function (value) {
		var mbSize = value / 1048576 //1,048,576 Bytes
		if (mbSize < 1) return Math.ceil(mbSize * 100) / 100 + ' MB';
		if (mbSize < 10) return Math.ceil(mbSize * 10) / 10 + ' MB';
		return Math.ceil(mbSize) + ' MB';
    }

    // To-do: Reflection methods onBefore.., onAfter.. are not thread-safe! Ensure that CRUD operations execeuses sequentially !!! (using _.reduce?)
	AssetReflection.onBeforeSave(function (objItem, rqOptions) {
		// check whether space quota allow upload
		var _auth = mandatory(rqOptions.auth,
			'Auth is required for reflection (AssetReflection.onBeforeSave)');
		var usrKeys = mandatory(_auth.applyCredentials({}).pathArgs,
			'Cannot extract user keys from applyCredentials (AssetReflection.onBeforeSave)');
		var AccountEmail = mandatory(usrKeys.userEmail,
			'Cannot extract user key (email) from credentials (AssetReflection.onBeforeSave)');
		var oUser = new mdUser.User({'AccountEmail': AccountEmail});
		var spaceRequired = objItem.uploadFile.peek().size;
		var spaceQuota, spaceUsed;

		var checkAvailableSpace = function (oUser) {
			var spaceRemains = oUser.SpaceQuota.peek() - oUser.SpaceUsed.peek();
			if (spaceRemains >= spaceRequired) return Promise.resolve(spaceRemains);
			return Promise.reject('This file size (' + _fmtMBytes(spaceRequired) + ') exceeds your free space! ('+ _fmtMBytes(spaceRemains)+')')
		}

		return new Promise(function (resolve, reject) {
			AdmUserReflection.load(oUser, rqOptions)
				.then(checkAvailableSpace)
				.then(resolve)
				.catch(reject)
		})

	});

	AssetReflection.onAfterSave(function (objItem, rqOptions) {

		var _auth = mandatory(rqOptions.auth,
			'Auth is required for reflection (AssetReflection.onBeforeSave)');
		var usrKeys = mandatory(_auth.applyCredentials({}).pathArgs,
			'Cannot extract user keys from applyCredentials (AssetReflection.onBeforeSave)');
		var AccountEmail = mandatory(usrKeys.userEmail,
			'Cannot extract user key (email) from credentials (AssetReflection.onBeforeSave)');
		var oUser = new mdUser.User({'AccountEmail': AccountEmail});

		var updateSpaceUsed = function () {
			var spaceUsed = oUser.SpaceUsed.peek();
			var spaceRequired = objItem.FileSize.peek();
			oUser.SpaceUsed(spaceUsed + spaceRequired);
			return AdmUserReflection.update(oUser, rqOptions)
		}

		return new Promise(function (resolve, reject) {
			AdmUserReflection.load(oUser, rqOptions)
				.then(updateSpaceUsed)
				.then(resolve)
				.catch(reject)
		})
	})


	AssetReflection.onAfterRemove(function (objItem, rqOptions) {
		// update free space remaining
		var _auth = mandatory(rqOptions.auth,
			'Auth is required for reflection (AssetReflection.onBeforeSave)');
		var usrKeys = mandatory(_auth.applyCredentials({}).pathArgs,
			'Cannot extract user keys from applyCredentials (AssetReflection.onBeforeSave)');
		var AccountEmail = mandatory(usrKeys.userEmail,
			'Cannot extract user key (email) from credentials (AssetReflection.onBeforeSave)');
		var oUser = new mdUser.User({'AccountEmail': AccountEmail});

		var updateSpaceUsed = function () {
			var spaceUsed = oUser.SpaceUsed.peek();
			var spaceRequired = objItem.FileSize.peek();
			spaceUsed = spaceUsed - spaceRequired;
			if (spaceUsed < 0) spaceUsed = 0;
			oUser.SpaceUsed(spaceUsed);
			return AdmUserReflection.update(oUser, rqOptions)
		}
		return new Promise(function (resolve, reject) {
			AdmUserReflection.load(oUser, rqOptions)
				.then(updateSpaceUsed)
				.then(resolve)
				.catch(reject)
		})

	});

	var TimelineReflection = ItemReflection(null);
	TimelineReflection.toJS(_toJS)
	TimelineReflection.fromJS(_fromJS);
	TimelineReflection.makeKey(_makeKey);
	TimelineReflection.itemFactory(function (data) {
		return new Timeline(data) // <--- return data "as-is" by default
	});

	var PlaylistReflection = ItemReflection(null);
	PlaylistReflection.toJS(_toJS)
	PlaylistReflection.fromJS(_fromJS);
	PlaylistReflection.makeKey(_makeKey);
	PlaylistReflection.itemFactory(function (data) {
		return new Playlist(data) // <--- return data "as-is" by default
	});


	var ChannelReflection = ItemReflection(null);
	ChannelReflection.toJS(_toJS)
	ChannelReflection.fromJS(_fromJS);
	ChannelReflection.makeKey(_makeKey);
	ChannelReflection.itemFactory(function (data) {
		return new mdChannel.Channel(data) // <--- return data "as-is" by default
	});

	// Reflection for plain JS object - screen binding with "Magneto displays"
	var ScreenBindingReflection = ItemReflection(null);
	ScreenBindingReflection.toJS(function (obj) {return _.omit(obj, 'LogicalID')});
	ScreenBindingReflection.fromJS(function (obj, data) {return _.extend(obj, data)});
	ScreenBindingReflection.makeKey(function (item) {
		return item['LogicalID']
	});
	ScreenBindingReflection.itemFactory(function (data, key) {
		return _.extend({}, data, {'LogicalID': key}) // <--- return data "as-is" by default, but transform representation LogicalIDValue:{...} to {'LogicalID':LogicalIDValue, ...}
	});


	var DeviceReflection = ItemReflection(null);
	DeviceReflection.toJS(_toJS)
	DeviceReflection.fromJS(_fromJS);
	DeviceReflection.makeKey(_makeKey);
	DeviceReflection.itemFactory(function (data) {
		return new mdDevice.Device(data) // <--- return data "as-is" by default
	});

	DeviceReflection.onAfterLoad(function (objItem, rqOptions) {
		console.log('onAfterLoad---->', rqOptions)
		if (objItem === null) {
			return Promise.resolve();
		} else {
			var deviceObj = objItem, rqOpt = rqOptions;
			var LogicalID = objItem.LogicalID.peek();
			var StorageID = objItem.StorageID.peek();
			if (!LogicalID) return Promise.resolve() // if no Binding established:
			//.... append new data?????????
			return new Promise(function (resolve, reject) {
				var rscEndpoint = ScreenBindingReflection.endpoint.child(LogicalID)
				var rscTransport = ScreenBindingReflection.transport;

				IO.Service(rscEndpoint, rscTransport).read(rqOpt).then(function (response) {
						deviceObj.HardwareInfo(response)
						console.log('NOW QUERY GETS: ', ko.toJS(deviceObj))
						return deviceObj;
					})
					.then(resolve)
					.catch(reject)

				})
		}
	})

	var processDependentRecord = function (objItem, rqOptions) {
		console.log('processDependentRecord---->', rqOptions)

		var _auth = mandatory(rqOptions.auth,
			'Auth is required for reflection (DeviceReflection.onBeforeSave)');
		var pathArgs = mandatory(_auth.applyCredentials({}).pathArgs,
			'Cannot extract user keys from applyCredentials (DeviceReflection.onBeforeSave)');

		var deviceObj = objItem, rqOpt = rqOptions;
		var LogicalID = objItem.LogicalID.peek();
		var StorageID = objItem.StorageID.peek();
		if (!LogicalID) return Promise.resolve() // if no Binding established:

		return new Promise(function (resolve, reject) {
			var rscEndpoint = ScreenBindingReflection.endpoint.child(LogicalID)
			var rscTransport = ScreenBindingReflection.transport;

			// Store also the channel info for the fast lookup from Magneto Device:
			var ChannelInfo = deviceObj.ChannelInfo.peek();
			var ChannelID = (ChannelInfo === null) ? null : ChannelInfo.StorageID.peek()

			IO.Service(rscEndpoint, rscTransport).read(rqOpt).then(function (response) {
				var verb = (response === null) ? 'save' : 'update';
				ScreenBindingReflection[verb]({
						'LogicalID': LogicalID,
						'ysData':{
							'StorageID':StorageID,
							'ChannelID': ChannelID,
							'rqOptions':{'pathArgs': pathArgs}
						}
					}, rqOptions)
					.then(resolve)
					.catch(reject)
			})
		})
	}

	var removeDependentRecord = function (objItem, rqOptions) {
		console.log('removeDependentRecord---->', rqOptions)
		var deviceObj = objItem, rqOpt = rqOptions;
		var LogicalID = objItem.LogicalID.peek();
		var StorageID = objItem.StorageID.peek();
		if (!LogicalID) return Promise.resolve() // if no Binding established:

		return new Promise(function (resolve, reject) {
			var rscEndpoint = ScreenBindingReflection.endpoint.child(LogicalID)
			var rscTransport = ScreenBindingReflection.transport;
			IO.Service(rscEndpoint, rscTransport).delete(rqOpt)
				.then(resolve)
				.catch(reject)
			})
	}

	DeviceReflection.onAfterSave(processDependentRecord)
	DeviceReflection.onAfterUpdate(processDependentRecord)
	DeviceReflection.onAfterRemove(removeDependentRecord)

	//--------



	var UserReflection = ObjectReflection(null);
	UserReflection.toJS(_toJS)
	UserReflection.fromJS(_fromJS);
	// UserReflection.makeKey(_makeKey);
	// UserReflection.itemFactory(function (data) {
	// 	return new mdUser.User(data) // <--- return data "as-is" by default
	// });

	var ExpositionReflection = ItemReflection(null);
	ExpositionReflection.toJS(_toJS)
	ExpositionReflection.fromJS(_fromJS);
	ExpositionReflection.makeKey(_makeKey);
	ExpositionReflection.itemFactory(function (data) {
		return new mdExposition.Exposition(data) // <--- return data "as-is" by default
	});

	var ScheduleReflection = ItemReflection(null);
	ScheduleReflection.toJS(_toJS)
	ScheduleReflection.fromJS(_fromJS);
	ScheduleReflection.makeKey(_makeKey);
	ScheduleReflection.itemFactory(function (data) {
		return new mdSchedule.Schedule(data) // <--- return data "as-is" by default
	});

	// PlayAndTrigger
	var SequencerReflection = ItemReflection(null);
	SequencerReflection.toJS(_toJS)
	SequencerReflection.fromJS(_fromJS);
	SequencerReflection.makeKey(_makeKey);
	SequencerReflection.itemFactory(function (data) {
		return new mdSequencer.PlayAndTrigger(data) // <--- return data "as-is" by default
	});


	var AdmUserReflection = ItemReflection(null);
	AdmUserReflection.toJS(_toJS)
	AdmUserReflection.fromJS(_fromJS);
	AdmUserReflection.makeKey(function (obj) {
		return obj.AccountEmail.peek() // Use Primary Email as main identifier for user
	});
	AdmUserReflection.itemFactory(function (data) {
		return new mdUser.User(data) // <--- return data "as-is" by default
	})

	var UserPhotoReflection = ItemReflection(null);
	UserPhotoReflection.toJS(function (objItem) {
		// extract only necessary fields and convert to the common "upload" task data
		var uploadTask = {};

		// Set new ModifiedTime (Using IFUpdate method):
		objItem.markUpdates();
		// [1] 'uploadData' (if any)

		var file = objItem.uploadFile.peek();
		uploadTask['uploadData'] = file; //+
		// Note that "undefined" values will be ignored by default:)
		// [2] 'matadata' - mandatory attribute with 1 mandatory field 'contentType'
		uploadTask['metadata'] = {
			'contentType': file.type
			// ,'cacheControl': _.popAttr(data, 'cacheControl') 	//string 	YES
			// These fields will be ignored in FirebaseStrorage
			,'size': file.size
			}; //<- at least: contentType
		// [3] - 'customMetadata' - all user-defined data
		uploadTask['customMetadata'] = {}
		return uploadTask
	})
	UserPhotoReflection.fromJS(function (objItem, response) {
		// Update only 1 property - PhotUrl, another properties remain unchanged
		// note that the CDN server can return additional and changed values so update object to appropriate status!
		var _srvMd = response.metadata || {};

		objItem.PhotoUrl(_srvMd.url);
		// !!! do not use IFUpdate.updateValues here!!!!!
		return objItem
	});
	UserPhotoReflection.makeKey(function (obj) {
		return obj.AccountEmail.peek() // Use Primary Email as main identifier for user
	});
	// This is not necessary? It is not planned to use "enum" for photos now
	UserPhotoReflection.itemFactory(function (data) {
		return new mdUser.User(data) // <--- return data "as-is" by default
	})

	return {
		BasicReflection: BasicReflection,
		AssetReflection: AssetReflection,

		TimelineReflection: TimelineReflection,
		PlaylistReflection: PlaylistReflection,

		ChannelReflection: ChannelReflection,

		ScreenBindingReflection: ScreenBindingReflection,

		DeviceReflection: DeviceReflection,
		UserReflection: UserReflection,

		AdmUserReflection: AdmUserReflection,

		UserPhotoReflection: UserPhotoReflection,

		ExpositionReflection: ExpositionReflection,

		ScheduleReflection: ScheduleReflection,
		SequencerReflection: SequencerReflection
	}
});