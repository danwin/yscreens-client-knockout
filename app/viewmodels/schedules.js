//schedules.js
define([
	'durandal/app',
	'knockout.all',
	'viewmodels/model-schedule',
    'shared/tableview'
],
function (app, ko, ModelLib, TableViewFactory) {

	// var Schedule = ModelLib.Schedule;
	// var Trigger = ModelLib.Trigger;

    function linkToScheduleEditor(item) {
        return 'index.html#schedule-editor/'+item.StorageID.peek()
    }

    function linkToScheduleAssistant(item) {
        return 'index.html#schedule-assistant/'+item.StorageID.peek()
    }

    var options = {

        displayName : 'My Schedules',
        itemClass : ModelLib.Schedule,
        entityName : 'Schedules', // <-- data partition on the server
        attrFilter : ModelLib.StructSchedule,
        dialogViews : {'edit':'views/schedule-editor-dialogs/dlg-edit-schedule-attrs'},
        // new handlers (used in view):
        addMethods : {
            'linkToScheduleEditor': linkToScheduleEditor,
            'linkToScheduleAssistant': linkToScheduleAssistant
        },
        columns : [
            // name, sortBy, sortState
            {name:'Label'},
            {name: 'Begins', sortBy: 'Start'},
            {name: 'Duration', sortBy: 'RequiredDuration'},
            {name: 'Contents'},
            {name:'Created', sortBy:'CreatedTime'},
            {name:'Modified', sortBy:'ModifiedTime'},
            {name:'Comment'}
        ],
        endpoints : {
            'create':'',
            'read':'',
            'readMany':'',
            'update':'',
            'delete':''
        },
        populate : [
            {'Label':'Schedule #1', 'CreatedTime': '2016-01-01 00:00:00', 'Comment': 'For children'},
            {'Label':'Schedule #2', 'CreatedTime': '2015-12-31 00:00:00', 'Comment': 'Business channel'},
            {'Label':'Schedule #3', 'CreatedTime': '2015-11-01 00:00:00', 'Comment': 'Retail'}
        ]
    };

    return TableViewFactory(options);

//-------------------------------------------

  //   var ctor = function () {
  //       var self = this;
  //       this.displayName = 'Schedule Editor';

  //       var itemRef = app.IO.gw.reflections['Schedules'];
  //       var recordRef = app.IO.gw.reflections['Expositions'];

  //       this.schedule = new ModelLib.PlaybackSchedule();
  //       this.schedule.Items.subscribe(function () {
  //       	var list = self.schedule.Items.peek();
  //       })

  //       // alias:
  //       this.items = this.schedule.Items;

		// /* Interior drawing: */
  //       this.timepoints = ko.observableArray(['0:00','12:00']);

  //       var _throttleOptions = { rateLimit: { timeout: 500, method: "notifyWhenChangesStop" } };
  //   	this.windowStart = ko.observable(0).extend(_throttleOptions);
  //   	this.windowDuration = ko.observable(0).extend(_throttleOptions);
  //   	this.windowStep = ko.observable(1).extend(_throttleOptions);

  //   	var _epoch = (function () {
  //   		// Use: August 2016 becase 1/08/2016 was Monday
  //   		var d = new Date(2016, 7, 1, 0,0,0);
  //   		return d.getTime();
  //   	})();


  //       var _handleExtents = function () {
  //       	var start = self.windowStart.peek(),
  //       		duration = self.windowDuration.peek(),
  //       		end = start + duration,
  //       		step = self.windowStep.peek(),
  //       		buffer = [],
  //       		mode = 'hours';

  //       	var fmtTimestamp = function (valueMs, mode) {
  //   			var d = new Date(_epoch + valueMs);
  //       		if (mode === 'hours') {
  //       			var h = d.getHours(),
  //       				m = d.getMinutes(),
  //       				mm = (m<10) ? '0'+m : m;
  //       			// console.log('...date...', valueMs, d)
  //       			return h + ':' + mm
  //       		}
  //       		if (mode === 'days') {
  //       			var wdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  //       			// console.log('...date...', valueMs, d)
  //       			// console.log('getDay', d.getDay())
  //       			return wdays[d.getDay()]
  //       		}
  //       		if (mode === 'weeks') {
  //       			return 'Week' + (Math.floor(d.getDate() / 7) + (d.getMonth() - 7) * 4 + 1) /*getMonth - 7 ... bacasue _epoch starts ant August*/
  //       		}
  //       	}
  //       	if (Math.abs(1 - duration/(1000 * 24 * 3600)) < 1.5) {
  //       		mode = 'hours'
  //       	} else if (Math.abs(1 - duration/(1000 * 24 * 3600 * 7)) < 1.5) {
  //       		mode = 'days'
  //       	} else {
  //       		mode = 'weeks'
  //       	}

  //       	while (start <= end) {
  //       		buffer.push( fmtTimestamp( start, mode ))
  //       		start = start + step
  //       	}
  //       	// console.log(buffer)
  //       	self.timepoints(buffer)
  //       }

  //       this.windowStart.subscribe(_handleExtents);
  //       this.windowDuration.subscribe(_handleExtents);
  //       this.windowStep.subscribe(_handleExtents);

  //       // this.items = ko.observableArray([]);

  //       this.records = ko.observableArray([]);

  //       this.stateLegend = ko.observable('');

  //       this.markedItems = ko.observableArray([]);
  //       this.markedItems.subscribe(function () {
  //           if (self.markedItems().length > 0) {
  //               self.stateLegend('&nbsp;You have selected <span class="badge">'+ self.markedItems().length + '</span> element(s)')
  //           } else {
  //               self.stateLegend('')
  //           }
  //       });
  //       this.mark = function (item) {
  //           var index = self.markedItems.indexOf(item);
  //           if (index < 0) {
  //               self.markedItems.push(item);
  //           } else {
  //               self.markedItems.splice(index, 1);
  //           }
  //           console.log(ko.toJS(self.markedItems));
  //           return true;
  //       };


  //       var _populate = function (list) {
  //       	for (var i=0; i<3; i++)
  //       		self.schedule.addRecord(new Trigger({'RecordInfo':{'Label': 'Record#'+i}, 'Duration': (i+1)*3600*1000}))
  //       }

  //       this.activate = function (scheduleId) {
  //       	console.log('...schedules...activate...')

  //       	// Set window extents
		// 	self.windowStart(0);
		// 	self.windowDuration(1000 * 24 * 3600 /* * 60*/);
		// 	self.windowStep(1000 * 3600 /* * 24  * 7*/);

  //       	var queue = [];
  //       	// Enum schedules
  //       	self.schedule.StorageID(scheduleId || 0)
  //       	_populate();
  //       	if (scheduleId) queue.push(
  //       		itemRef
		// 			.load(self.schedule, app.rqOptions)
		// 			// .then(function (data) {
		// 			// 	if (data.length == 0) _populate(data);
		// 			// 	self.items(data)
		// 			// 	return data
		// 			// })
  //       		)
  //       	// Enum records
  //       	queue.push(
  //       		recordRef
		// 			.enum(app.rqOptions)
		// 			.then(function (data) {
		// 				// var lookup = _.map(data, function (item) {
		// 				// 	return {'label': item.Label.peek(), 'id': item.StorageID.peek()}
		// 				// })
		// 				// console.log('...record found: ...', lookup)
		// 				self.records(data)
		// 				return data
		// 			})
  //       		)
  //       	return Promise.all(queue)
  //       		.then(function (resuls) {
  //       			console.log('...then...', resuls)
  //       		})
  //       		.catch(function (reason) {
  //       			console.error('...schedules...', reason)
  //       		})
  //       		.catch(app.handleException);
  //       }

  //       this.compositionComplete = function () {
  //       	// updateTimetable()
  //       }

  //       /*
  //       CRUD
  //       */

  //       this.add = function () {
  //       	var item = new ModelLib.PlaybackSchedule();

  //       	app.customDialog({
  //       		'buffer': item,
  //       		'attrFilter': ModelLib.StructPlaybackSchedule,
  //       		'viewUrl': 'views/dlg-add-schedule-playback',
  //       		'lookups': {'records': self.records.peek()}
  //       	}).waitConfirm()
  //       	.then(function (Ok) {
  //       		if (Ok) {
  //       			self.schedule.addRecord(item);
  //       		}
  //       	})
  //       	.catch(app.handleException)
  //       	// body...
  //       }

	 //        // common code to remove item
	 //        var _doRemove = function (item, collectionList, selectionList) {
	 //        	return new Promise(function (resolve, reject) {
	 //        		try {
		// 	            console.log('Removing item:::', item);
		// 	            // To-do: use PropertyReflection here to operate directly on collection...

		// 	            var index = collectionList.indexOf(item);
		//         		if (index >-1) collectionList.splice(index, 1);
		//         		// remove from selection ("markedItems"):
		//         		index = selectionList.indexOf(item);
		//         		if (index >-1) selectionList.splice(index,1)
		//         		resolve(true)
	 //        		} catch (e) {
	 //        			reject(e)
	 //        		}
	 //        	})
	 //        }

	 //        self.remove = function (item) {
	 //            // body...
	 //            var _collection = self.items.peek();

	 //            // ask confirm:
	 //            app.showMessage(
	 //                'Are you sure you want to remove element \"'+ item.Label() + '\"?',
	 //                'Confirm', ['Yes', 'No']
	 //            ).then(function (result) {
	 //                if (result == 'Yes') {
		// 	            console.log('Removing item:::', item);
		// 	            _doRemove(item, _collection, [])
		// 	            	.then(function () {
		// 	                	self.items(_collection)
		// 	                	self.markedItems([])
		// 	            	})
		// 	                .catch(app.handleException)
	 //       //          	itemRef.remove(item, app.rqOptions)
		//       //           	.then(function () {
		//       //           		self.items.splice(index, 1);
		// 						// self.stateLegend('Removed: 1 item');
		//       //           	})
		//       //           	.catch(app.handleException);
	 //                }
	 //            });
	 //        };

	 //        self.removeMany = function (item) {
	 //            // body...
	 //            var
	 //                _selection = self.markedItems.peek();
	 //                _collection = self.items.peek();
	 //            if (!_selection.length) return;
	 //            // ask confirm:
	 //            app.showMessage(
	 //                'Are you sure you want to remove '+ _selection.length + ' element(s)?',
	 //                'Confirm', ['Yes', 'No']
	 //            ).then(function (result) {
	 //                if (result != 'Yes') return;

	 //                // Promise.all(_.map(_selection, function (item) {
	 //                // 	return _doRemove(item, _collection, _selection)
	 //                // }))

	 //                // Do sych execution due to external variables are not thread-safe
	 //                _.reduce(_selection, function (promise, item) {
	 //                	return promise.then(function () {
	 //                		return _doRemove(item, _collection, _selection)
	 //                	})
	 //                }, Promise.resolve())
	 //                .then(function () {
	 //                	self.items(_collection)
	 //                	self.markedItems(_selection)
	 //                })
	 //                .catch(app.handleException)

	 //            });
	 //        };

  //       /*
  //       End of CRUD
  //       */

  //       var __widthPx__; // <-- stores a start value before resizing
  //       this.handleStartResize = function (argument) {
		// 	var record_obj = ko.dataFor(this);
		// 	__widthPx__ = $(this).width();
  //       }
  //       this.handleResize = function (event, ui) {
		// 	var context = ko.contextFor(this),
		// 		model = context.$root,
		// 		record_obj = ko.dataFor(this),
		// 		oldWidth = __widthPx__,
		// 		newWidth = $(this).width(),
		// 		duration = record_obj.Duration.peek(),
		// 		newValue = (duration + (newWidth - oldWidth)/oldWidth * duration),
		// 		quantum = self.schedule.Quantum.peek();
  //       	// ui.element.css({height:null})
  //       	// if (event.type === 'stopresize')

  //   		//align new value to the integer count of quantum
  //   		newValue = Math.round(newValue / quantum) * quantum;

  //   		console.log('oldWidth', oldWidth, 'newWidth', newWidth, 'duration', duration, 'newValue', newValue);

  //       	self.schedule.resizeTrigger(record_obj, newValue);

  //       }

  //   };

    //Note: This module exports a function. That means that you, the developer, can create multiple instances.
    //This pattern is also recognized by Durandal so that it can create instances on demand.
    //If you wish to create a singleton, you should export an object instead of a function.
    //See the "flickr" module for an example of object export.

    return ctor;
});