//schedule-editor.js
define([
	'durandal/app',
	'plugins/dialog',
	'plugins/router',
	'underscore.all',
	'jquery',
	'knockout.all',
	'viewmodels/model-schedule',
	'viewmodels/model-exposition',
	'datetime',
	'shared/datetime-helpers',
	'nouislider',

	'knockout.sortable',
	'shared/ko.resizable'
],
function (app, dialog, router, _, $, ko, mdSchedule, mdExposition, mdDateTime, dt) {

	var
		Schedule = mdSchedule.Schedule
		,PlaybackItem = mdSchedule.PlaybackItem
		,PlaybackFrame = mdSchedule.PlaybackFrame
		,PlaybackStream = mdSchedule.PlaybackStream
		,StructPlaybackItem = mdSchedule.StructPlaybackItem
		,StructPlaybackFrame = mdSchedule.StructPlaybackFrame
		,StructPlaybackStream = mdSchedule.StructPlaybackStream
		,DateTimeInterval = mdDateTime.DateTimeInterval
		,LocalDateTime = mdDateTime.LocalDateTime

		,Exposition = mdExposition.Exposition;


	var vecToMs = DateTimeInterval.vectorToMilliseconds;

	var ctor = function () {
		var self = this;
		this.displayName = 'Schedule Editor';

		this.Changed = ko.observable(false)

		var itemRef = app.IO.gw.reflections['Schedules'];
		var recordRef = app.IO.gw.reflections['Expositions'];

		//
		this.schedule = new mdSchedule.Schedule({
			'Label':'Schedule', 'IsStartLocked':true,
			'Start': (LocalDateTime.now().add(new DateTimeInterval({'hours':2, 'minutes':10, 'seconds':15})))
/*			'Start': (new LocalDateTime(new Date())).add(new DateTimeInterval({'hours':2, 'minutes':10, 'seconds':15}))
*/		}); // <-- dummy data illed for debug
		this.schedule.Items.subscribe(function () {
			var list = self.schedule.Items.peek();
		})

		// alias:
		this.items = this.schedule.Items;

		var _throttleOptions = { rateLimit: { timeout: 500, method: "notifyWhenChangesStop" } };
												// LocalDateTime - override valueOf to return module also!


		this.TimeBinding = ko.observable('Relative')
		/**
		 * Measurement units for domain (enumeration)
		 * @type {string}
		 */
		this.TimeUnits = ko.observable('Hours');

		/**
		 * Start of time axis in viewport
		 * @type {LocalDateTime/DateTimeInterval}
		 */
		this.ViewportStart = ko.observable(LocalDateTime.now())//.extend(_throttleOptions);
		this.ViewportOffset = ko.observable(vecToMs({'seconds':0}))//.extend(_throttleOptions);
		this.ViewportStep = ko.observable(vecToMs({'hours':1}))//.extend(_throttleOptions);
		this.ViewportDuration = ko.observable(vecToMs({'days':1}))//.extend(_throttleOptions);

		this.cmpZeroOffset = ko.pureComputed(function () {
			// ---> viewportZeroTime ---> scheduleZeroTime
			var
				viewportZeroTime = self.ViewportStart(),
				viewportOffset = self.ViewportOffset(),
				isAbsoluteAxis = (self.TimeBinding.peek() === 'Absolute'),
				isAbsoluteSchedule = self.schedule.IsAbsolute.peek(),
				// note that view can be absolute;
				scheduleZeroTime = self.schedule.Start(),
				units = self.TimeUnits(),
				result = new LocalDateTime();

			return (isAbsoluteAxis) ? scheduleZeroTime.subtract(viewportZeroTime).add(viewportOffset) : viewportOffset;
			// return (viewportZeroTime - scheduleZeroTime) + viewportOffset;

			if (isAbsoluteAxis) {
				// Absolute mode
				if (isAbsoluteSchedule) {
					console.log('cmpZeroOffset=====>', scheduleZeroTime.subtract(viewportZeroTime).toDelimitedString())
					return scheduleZeroTime.subtract(viewportZeroTime)
				} else throw new Error('Cannot show relative schedule in absolute axis!')
			} else {
				// Relative mode
					console.log('cmpZeroOffset=====>', new DateTimeInterval( scheduleZeroTime - scheduleZeroTime % units + viewportOffset).toDelimitedString())
				return new DateTimeInterval( scheduleZeroTime - scheduleZeroTime % units + viewportOffset)

				// return viewportOffset;
				// if (isAbsoluteSchedule) {
				// 	// Absolute schedule in a relative axis (axis aligned to hours, days, ...)
				// 	// From absolute start of schedule - use only a fractional portion.
				// 	return new DateTimeInterval( scheduleZeroTime % units + viewportOffset)
				// } else {
				// 	return viewportOffset;
				// }
			}
		})

		// Accessors/mutators for drop-down lists with a sring values:
		// this.VpStepMs = ko.pureComputed({
		// 	'read': function () {return self.ViewportStep().toString()},
		// 	'write': function (value) {
		// 		self.ViewportStep(new DateTimeInterval(parseInt(value, 10)))
		// 	}
		// })
		this.VpOffsetMsStr = ko.pureComputed({
			'read': function () {return self.ViewportOffset().toString()},
			'write': function (value) {
				self.ViewportOffset(new DateTimeInterval(parseInt(value, 10)))
			}
		})
		this.VpDurationMsStr = ko.pureComputed({
			'read': function () {return self.ViewportDuration().toString()},
			'write': function (value) {
				self.ViewportDuration(new DateTimeInterval(parseInt(value, 10)))
			}
		})

		var MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		var DAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

		function strMonth(index) {
			return MONTHS[index-1]
		}
		function strDay(index) {
			return DAYS[index]
		}
		function strWeek(index) {
			return 'W'+index
		}

		var createAxisLabelRenderer = function (zeroTime, step, mode, units) {
			function _2digits(v) {return (v < 10) ? '0'+v : v}
			var
				isAbsoluteAxis = mode === 'Absolute';
			// var
			// 	absStart = self.ViewportStart.peek(), // <- instanceof LocalDateTime
			// 	offset = self.cmpZeroOffset.peek(),
			// 	// zeroTime = absStart;
			// 	zeroTime = (offset)
			// 		? absStart.subtract(offset)
			// 		: absStart; // <-- always subtract to shift to the right

			switch (units) {
				case 'Hours':
					if (isAbsoluteAxis) {
						return function (time, step, edge) {
							return (edge || time.hours === 0)
								? [time.date, ' ', strMonth(time.month), ' ',time.hours, ':', _2digits(time.minutes)].join('')
								: [time.hours, _2digits(time.minutes)].join(':')
						}
					} else {
						return function (time, step, edge) {
							return [time.hours, _2digits(time.minutes)].join(':')
						}
					}

				case 'Days':
					if (isAbsoluteAxis) {
						return function (time, step, edge) {
							return (edge || time.date === 1)
								? [time.date, strMonth(time.month)].join('-')
								: time.date
						}
					} else {
						return function (time, step, edge) {
							return time.date
						}
					}

				case 'Days of Week':
					if (isAbsoluteAxis) {
						return function (time, step, edge) {
							return (edge || time.date === 1)
								? [strDay(time.toDate().getDay()), time.date, strMonth(time.month)].join(' ')
								: strDay(time.toDate().getDay())
						}
					} else {
						// var firstDay = zeroTime.toDate().getDay()
						return function (time, step, edge) {
							console.log('Label:::: ', time, time.date % 7)
							return strDay(time.date % 7)
						}
					}

				case 'Weeks':
					if (isAbsoluteAxis) {
						var zeroWeek = Math.ceil(zeroTime.date / 7);
						return function (time, step, edge) {
							return (edge)
								? [strWeek(Math.ceil(time.date / 7) - zeroWeek), time.date, strMonth(time.month)].join(' ')
								: strWeek(Math.ceil(time.date / 7) - zeroWeek)
						}
					} else {
						return function (time, step, edge) {
							return strWeek(Math.ceil(time.date / 7))
						}
					}
				// 'Month': similar to 'Weeks'

				case 'Months':
					if (isAbsoluteAxis) {
						return function (time, step, edge) {
							return (edge)
								? [time.date, strMonth(time.month)].join(' ')
								: strMonth(time.month)
						}
					} else {
						var zeroMonth = zeroTime.month;
						return function (time, step, edge) {
							return strMonth(time.month - zeroMonth)
						}
					}

				default:
					throw new Error('Invalid units!')
			}

		}

		// this.TimeDomainLabels = ko.observableArray(['0:00','12:00']);
		this.TimeDomainLabels = ko.pureComputed(function () {
			var
				absStart = self.ViewportStart(), // <- instanceof LocalDateTime
				___ = function () {
					console.log('==================> absStart', absStart)
				}(),
				offset = self.ViewportOffset(),// self.cmpZeroOffset(),
				start = (offset)
					? absStart.add(offset)
					: absStart, // <-- always subtract to shift to the right
				cursor = start, // <-- initial value
				duration = self.ViewportDuration(),
				end = start.add(duration),
				step = self.ViewportStep(),
				mode = self.TimeBinding.peek(), // <--ViewportStart,
				units = self.TimeUnits() // <--,
				// ____test = console.log('TimeDomainLabels-------------------->', start),
				labels = [],
				_fmtTimestamp = createAxisLabelRenderer(start, step, mode, units)
				;

			while (cursor && cursor <= end) { // <- we can write arythmetic operation becase .valueOf returns module!
				// labels.push( fmtTimestamp( start.module(), mode ))
				labels.push(_fmtTimestamp( cursor, step, cursor == start || cursor == end))
				// try {
					console.log('TimeDomainLabels-------------------->', cursor)
					cursor = cursor.add(step)
				// } catch (e) {console.error(e, 'start', start, 'step', step)}
			}
			// console.log(labels)
			return labels

		})

	/*////////////////////////////////////////////////////////
	//
	// Lookups
	//
	*/////////////////////////////////////////////////////////

		this.lkpTimeBinding = ['Relative', 'Absolute']

		// Lookup list for TimeUnits:
		this.lkpTimeUnits = ['Hours', 'Days of Week', 'Days', 'Weeks', 'Months']
		// this.lkpTimeUnits = ko.pureComputed(function () {
		// 	// var timeBinding = self.TimeBinding();
		// 	// if (timeBinding === 'Relative')
		// 	// 	return ['Hours', 'Days of Week', 'Days', 'Weeks']
		// 	// if (timeBinding === 'Absolute')
		// 		return ['Hours', 'Days of Week', 'Days', 'Weeks', 'Months']
		// });

		this.lkpWindowOffset = ko.pureComputed(function () {
			var units = self.TimeUnits();
			var isAbsoluteAxis = self.TimeBinding.peek() === 'Absolute';
			switch (units) {
				case 'Hours':
					return _.map(_.range(0,24), function (timePoint) {
						return {'label': timePoint+':00', 'value': vecToMs({'hours':timePoint})}
					})
				case 'Days':
					return _.map(_.range(0,31), function (timePoint) {
						return {'label': timePoint+1, 'value': vecToMs({'days': timePoint})}
					})
				//---
				case 'Days of Week':
					var wdShift = (isAbsoluteAxis)
						? self.ViewportStart.peek().toDate().getDay() : 0;
					return _.map(DAYS, function (value, index) {
						return {'label': value, 'value': vecToMs({'days': index - wdShift})}
					})
				case 'Weeks': // 52 full weeks per year:
					return _.map(_.range(0,52), function (timePoint) {
						return {'label': timePoint+1, 'value': vecToMs({'weeks': timePoint})}
					})
				case 'Months': // Absolute:
					var now = new Date()
					var cursor = new Date();
					// cursor.setDate(1); // <-- point to start of month
					return _.map(MONTHS, function (value, index) {
						cursor.setMonth(index)
						return {'label': value, 'value': cursor.getTime() - now.getTime()}
					})
				default:
					throw new Error('Unknown units: '+units)
			}
		})


		this.lkpWindowDuration = ko.pureComputed(function () {
			var units = self.TimeUnits();
			switch (units) {
				case 'Hours':
					return _.map(_.range(12,25), function (timePoint) {
						return {'label': timePoint+':00', 'value': vecToMs({'hours': timePoint})}
					})
				case 'Days':
					return _.map(_.range(1,32), function (timePoint) {
						return {'label': timePoint, 'value': vecToMs({'days':timePoint})}
					})
				case 'Days of Week':
					return _.map({'1 week':1, '2 weeks':2, '3 weeks':3, '4 weeks':4}, function (value, key) {
						return {'label': key, 'value': vecToMs({'weeks': value})}
					})
				case 'Weeks': // 52 full weeks per year:
					return _.map(_.range(1,52), function (value) {
						return {'label': value, 'value': vecToMs({'weeks': value})}
					})
				case 'Months': // Absolute:
					// Incrementl number of days for beginning of a month:
					return _.map({'1 month':5, '1 Quarter':15, '1/2 Year':30, 'Year':54}, function (value, key) {
						return {'label': key, 'value': vecToMs({'weeks':value})}
					})
				default:
					throw new Error('Unknown units: '+units)
			}
		})

	/*////////////////////////////////////////////////////////
	//
	// Subscriptions
	//
	*/////////////////////////////////////////////////////////

		/* Interior drawing: */

		var _epoch = (function () {
			// Use: August 2016 becase 1/08/2016 was Monday
			var d = new Date(Date.UTC(2016, 7, 1, 0,0,0));
			return d.getTime();
		})();

		// Notification listeners:
		// this.TimeBinding.subscribe(function (newValue) {
		// 	var step = self.ViewportStep.peek(), offset, now;
		// 	if ('Relative' === newValue) {
		// 		self.ViewportStart(new LocalDateTime(0))
		// 	} else if ('Absolute' === newValue) {
		// 		now = new LocalDateTime(new Date());
		// 		offset = new LocalDateTime(now - now % step);
		// 		self.ViewportStart(offset);
		// 	} else throw new Error('Invalid value of TimeBinding: '+newValue);
		// })

		ko.computed(function () {
			var
				mode = self.TimeBinding(),
				step = +self.ViewportStep(),
				now = +LocalDateTime.now(),
				zero;
			if ('Relative' === mode) {
				zero = new LocalDateTime(0)
				console.log('zero===', zero.toDelimitedString())
				self.ViewportOffset(0); // <- clear
			} else if ('Absolute' === mode) {
				zero = new LocalDateTime(Math.floor(now/step) * step)  //new LocalDateTime(now - now % step);
				console.log('zero===', new LocalDateTime(Math.floor(now/step) * step), Math.floor(now/step) * step, zero, step, now)
				self.ViewportOffset(0); // <- clear
			} else throw new Error('Invalid value of TimeBinding: '+mode);
			self.ViewportStart(zero);
		})

		// this.TimeUnits.subscribe(function () {
		// 	var timeUnits = self.TimeUnits.peek();
		// 	var result = {
		// 		'Hours': 	{'step': {'hours': 1}, 'duration':{'hours': 12}},
		// 		'Days of Week': 	{'step': {'days': 1}, 'duration':{'days': 7}},
		// 		'Days': 	{'step': {'days': 1}, 'duration':{'days': 31}},
		// 		'Weeks': 	{'step': {'weeks': 1}, 'duration':{'weeks': 52}},
		// 		'Months': {'step': {'weeks': 4}, 'duration':{'week':52}}
		// 	}[timeUnits];
		// 	if (!result) {
		// 			throw new Error('Invald time units selected: '+timeUnits)
		// 	}
		// 	self.ViewportStep(new DateTimeInterval( result.step));
		// 	self.ViewportDuration(new DateTimeInterval( result.duration));
		// });

		ko.computed(function () {
			var timeUnits = self.TimeUnits();
			var result = {
				'Hours': 	{'step': {'hours': 1}, 'duration':{'hours': 12}},
				'Days of Week': 	{'step': {'days': 1}, 'duration':{'days': 7}},
				'Days': 	{'step': {'days': 1}, 'duration':{'days': 31}},
				'Weeks': 	{'step': {'weeks': 1}, 'duration':{'weeks': 52}},
				'Months': {'step': {'weeks': 4}, 'duration':{'weeks':52}}
			}[timeUnits];
			if (!result) {
					throw new Error('Invald time units selected: '+timeUnits)
			}
			self.ViewportStep(new DateTimeInterval( vecToMs( result.step)));
			self.ViewportDuration(vecToMs( result.duration).toString());
		})

		// this.ViewportStart.subscribe(_handleExtents);
		// this.ViewportDuration.subscribe(_handleExtents);
		// this.ViewportStep.subscribe(_handleExtents);


		// self.resizableElementOptions = {
		// 		animate: false
		// 		,ghost: false
		// 		,handles:{'e':'.resize-handle.ui-resizable-e','w':'.resize-handle.ui-resizable-w'}
		// 		,start: self.handleStartResize
		// 		,resize: self.handleResize
		// 		,stop: self.handleResizeComplete
		// 		,distance: 1
		// 	};
		self.sortableElementOptions = {
				handle: '.sort-handle'
				,axis: 'y'
				,helper: function (event, element) {
					return $('<div>')
						.css({})
						// .addClass('sort-helper')
						.html('<span class="badge"><i class="fa fa-arrows-v"></i>&nbsp;Select order...</span>');
				}
			}

		/**
		 * Get coordinates/dimensions for the screen projection
		 * @param  {PlaybackItem} item             item to display
		 * @param  {string} dimension             Name of property with domain units: 'Duration', 'SlotDuration', 'TimeRemainder', 'TimeOverrun'
		 * @return {object}                  Attributes for DOM object: "left", "width"
		 */
		self.attributesFor = function (item, dimension) {
			var viewportDuration = self.ViewportDuration();

			var
				absStart = self.ViewportStart(), // <- instanceof LocalDateTime
				offset = parseInt(self.ViewportOffset(), 10),
				ViewportStart = (offset) ? absStart.subtract(offset) : absStart; // <-- always subtract to shift to the right

			// var viewportStart = self.ViewportStart() - self.ViewportOffset();
			var domainWidth = item[dimension];

			switch (dimension) {
				case 'SlotDuration':
					return (item === self.draggingItem.peek()) ? {
						'left': (self.cmpZeroOffset() + item.Offset())*100/viewportDuration + '%'
					} : {
						'left': (self.cmpZeroOffset() + item.Offset())*100/viewportDuration + '%',
						'width': (item.SlotDuration() || 0)*100/viewportDuration + '%'
					}
				case 'Duration':
					return {
						'left': (self.cmpZeroOffset() + item.Offset())*100/viewportDuration + '%',
						'width': (item.Duration() || 0)*100/viewportDuration + '%'
					}
				case 'TimeRemainder':
					return {
						'left': (self.cmpZeroOffset() + item.Offset() + item.Duration())*100/viewportDuration + '%',
						'width': item.TimeRemainder()*100/viewportDuration + '%',
					}
				case 'TimeOverrun':
					return {
						'left': (self.cmpZeroOffset() + item.Offset() + item.SlotDuration())*100/viewportDuration + '%',
						'width': item.TimeOverrun()*100/viewportDuration + '%',
					}
				default:
					throw new Error('schedule-editor.attributesFor() gots invalid "dimension" argument: ' + dimension)
			}

			// var style = {
			// 	'left': item.Offset()*100/viewportDuration + '%'
			// };

			// if (item !== self.draggingItem.peek()) {
			// 	style['width'] = item.Duration()*100/viewportDuration + '%'
			// }

			// return  style;
		};

		self.fmtStartFor = function (item) {
			var value =  (item === self.schedule && self.schedule.IsAbsolute()) ? item.Start() : item.Offset()
			return value.toDelimitedString()
			// var
			// 	// absStart = self.ViewportStart(), // <- instanceof LocalDateTime
			// 	// offset = self.ViewportOffset(),
			// 	// start =
			// 	// start = (offset) ? absStart.subtract(offset) : absStart; // <-- always subtract to shift to the right

			// 	absStart = self.ViewportStart(), // <- instanceof LocalDateTime
			// 	offset = self.cmpZeroOffset(),
			// 	start = (offset)
			// 		? absStart.add(offset)
			// 		: absStart; // <-- always subtract to shift to the right

			// // var start = self.schedule.Start() || (new DateTimeInterval(0));
			// try {
			// 	// console.log('fmtStartFor-------------------->', start)
			// 	start = start.add(item.Offset());
			// } catch (e) {console.error(e, 'start', start, 'step', step)}
			// return start.toDelimitedTimeString();
		}


//********************
	// self['soTimeRange'] = ko.computed(function () {
	// 	// options for TimeRange slider depends on _DayNight
	// 	// switch, so implementing options as observable

	// 	var mode = self._DayNight();

	// 	var range, start, filter,
	// 		mediaRate = ($( window ).width() < 480) ? 2 : 1;

	// 	var filter12 = function (value) {
	// 		// 0 - no value, 1- large value, 2 - small value;
	// 		return (value % (60 * mediaRate) == 0) ? 1 : 0;
	// 	};

	// 	var filter24 = function (value) {
	// 		// 0 - no value, 1- large value, 2 - small value;
	// 		if (value == 0 || value == 1440) return 0;
	// 		return (value % (180) == 0) ? 1 : 0;
	// 	};

	// 	range = {
	// 		'day': {'min': 7 * 60, 'max': 19 * 60},
	// 		'night': {'min': 19 * 60, 'max': (19 + 12) * 60},
	// 		'day+night': {'min': 0, 'max': 24 * 60}
	// 	};

	// 	step = {'day': 15,	'night': 15,	'day+night': 30};

	// 	filter = {'day': filter12, 'night': filter12, 'day+night': filter24};

	// 	start = [self.StartTime.peek(), self.EndTime.peek()];

	// 	return {
	// 		start: start, // Handle start position
	// 		tooltips: [timeFormatterHMMA, timeFormatterHMMA],
	// 		step: step[mode], // Slider moves in increments of '10'
	// 		margin: 20, // Handles must be more than '20' apart
	// 		connect: true, // Display a colored bar between the handles
	// 		direction: 'ltr', // Put '0' at the bottom of the slider
	// 		orientation: 'horizontal', // Orient the slider vertically
	// 		behaviour: 'tap-drag', // Move handle on tap, bar is draggable
	// 		range: range[mode],
	// 		pips: { // Show a scale with the slider
	// 			mode: 'steps',
	// 			filter: filter[mode],
	// 			format: timeFormatterHA,
	// 			density: step,
	// 			stepped: true
	// 		}
	// 	}

	// });

//********************


		// this.items = ko.observableArray([]);

		this.records = ko.observableArray([]);

		this.stateLegend = ko.observable('');

		this.markedItems = ko.observableArray([]);
		this.markedItems.subscribe(function () {
			if (self.markedItems().length > 0) {
				self.stateLegend('&nbsp;You have selected <span class="badge">'+ self.markedItems().length + '</span> element(s)')
			} else {
				self.stateLegend('')
			}
		});
		this.mark = function (item) {
			var index = self.markedItems.indexOf(item);
			if (index < 0) {
				self.markedItems.push(item);
			} else {
				self.markedItems.splice(index, 1);
			}
			console.log(ko.toJS(self.markedItems));
			return true;
		};


		var _populate = function (list) {
			var buffer = []
			for (var i=0; i<3; i++) {
				buffer.push(new PlaybackStream({
					'Label': 'Label#'+i,
					'RecordInfo':{'Label': 'Record#'+i},
					'SlotDuration': new DateTimeInterval((i+1)*3600*1000),
					'Items': [
						{'SlotDuration':600000*1, 'Label': 'Some label 1.'+i},
						{'SlotDuration':600000*2, 'Label': 'Some label 2.'+i},
						{'SlotDuration':600000*3, 'Label': 'Some label 3.'+i},
						{'SlotDuration':600000*4, 'Label': 'Some label 4.'+i, 'Items': [{'Label':'c1'},{'Label':'c2'}]}
					]
				}))
			}
			self.schedule.Items(buffer)
			// self.schedule.updateValues({'Items': buffer})
		}

		this.activate = function (scheduleId) {
			console.log('...schedules...activate...')

			// Set window extents
			// self.ViewportStart(new LocalDateTime(0));
			// self.ViewportDuration(vecToMs({'hours': 24}) /* * 60*/);
			// self.ViewportStep(vecToMs({'hours': 1}) /* * 24  * 7*/);

			var queue = [];
			// Enum schedules
			self.schedule.StorageID(scheduleId || 0)

			if (scheduleId) { queue.push(
				itemRef
					.load(self.schedule, app.rqOptions)
					.catch(app.handleException)
					// .then(function (data) {
					// 	if (data.length == 0) _populate(data);
					// 	self.items(data)
					// 	return data
					// })
				);
			} else _populate();

			// Enum records
			queue.push(
				recordRef
					.enum(app.rqOptions)
					.then(function (data) {
						var records = _.map(data, function (item) {
							return new Exposition(item)
						})
						// var lookup = _.map(data, function (item) {
						// 	return {'label': item.Label.peek(), 'id': item.StorageID.peek()}
						// })
						// console.log('...record found: ...', lookup)
						console.log('Exposition records: ', records)
						self.records(records)
						return data
					})
				)
			return Promise.all(queue)
				.then(function (resuls) {
					console.log('...then...', resuls)
				})
				.catch(function (reason) {
					console.error('...schedules...', reason)
				})
				.catch(app.handleException);
		}

		this.compositionComplete = function () {
			// updateTimetable()
		}

		/*
		CRUD
		*/

		this.assignPlayback = function (item) {
			// var item = new mdSchedule.PlaybackStream();

			app.customDialog({
				'buffer': item,
				'attrFilter': mdSchedule.StructPlaybackFrame,
				'constructorFunc': PlaybackFrame,
				'viewUrl': 'views/schedule-editor-dialogs/dlg-assign-playback',
				'lookups': {
					'records': self.records.peek(),
					'formatOptionsLegend': function (item) {
						return item.Label() +' [' + (new DateTimeInterval( item.Duration())) + ']'
					}
				}
			}).waitConfirm()
			.then(function (Ok) {
				if (Ok) {

					// self.schedule.addRecord(item);
					self.Changed(true)
				}
			})
			.catch(app.handleException)
			// body...
		}

		// this.lkpPeriods = [
		// 	{'label':'Hourly', 'value': new DateTimeInterval({'hours':1})},
		// 	{'label':'Daily', 'value': new DateTimeInterval({'days':1})},
		// 	{'label':'Weekly', 'value': new DateTimeInterval({'weeks':1})},
		// 	{'label':'Exactly...', 'value': new DateTimeInterval({'hours':0})}
		// ]

		this.lkpPeriods = [
			{'label':'Hourly', 'value': 3600},
			{'label':'Daily', 'value': 86400},
			{'label':'Weekly', 'value': 604800}
			// {'label':'Exactly...', 'value': new DateTimeInterval({'hours':0})}
		]

		this.addRecursiveStream = function () {
			// var buffer = {
			// 	StreamDuration: ko.observable(new DateTimeInterval(0)).extend({ numeric: 0 }),
			// 	Period: ko.observable(new DateTimeInterval(0)).extend({ numeric: 0 }),

			// }
			var buffer = new PlaybackStream({'SlotDuration':3600000, 'Period':3600000})
			buffer['viewUrl'] = 'views/dlg-edit-repetition';
			buffer['lookups'] = {
					'lkpPeriods': self.lkpPeriods,
					'lkpFmtPeriodLabel': function(item){
							console.log('lkpPeriods======>', item, parseInt(item.value/1000))
							switch ( parseInt(item.value) ) {
								case 3600:
								case 86400:
								case 604800:
									return item.label;
								default:
									return 'Exactly...';
							}
					}
				}

			app.showDialogEx(buffer)
			.then(function (Ok) {
				console.log('dialog result', Ok)
				if (Ok) {

					var item = new mdSchedule.PlaybackStream(buffer.toJS()); // <--
					self.schedule.createRecursivePattern(item); // <- item, period, times
					self.Changed(true)
				}
			})
			.catch(app.handleException)
		}

		/**
		 * Dialog data to edit trigger start/offset:
		 * @param {[type]} data [description]
		 */
		function TriggerData(data) {
				this.Start = ko.observable(data.Start);
				this.Offset = ko.observable(data.Offset);
				this.IsAbsolute = data.IsAbsolute;
				this['viewUrl'] = 'views/schedule-editor-dialogs/dlg-edit-trigger';
		}

		// Also make: editTrigger?
		this.addTriggeredStream = function () {
			var buffer = new TriggerData({
				'Start': new LocalDateTime.now(),
				'Offset': new DateTimeInterval(0), /*Initial value: edge start*/
				'IsAbsolute': self.schedule.IsAbsolute.peek()
			})

			app.showDialogEx(buffer)
			.then(function (Ok) {
				console.log('dialog result', Ok)
				if (Ok) {
					var offset = buffer.IsAbsolute
						? buffer.Start.peek().subtract(self.schedule.Start.peek())
						: buffer.Offset.peek();//.add(self.schedule.Offset.peek());
					var item = new mdSchedule.PlaybackStream({
						'Offset': offset,
						'IsOffsetLocked': true
					}); // <--
					console.log('Offset:', offset.toString(),
						'was: ', buffer.Start.peek(), self.schedule.Start.peek())
					self.schedule.Items.push(item); // <- item, period, times
					self.schedule.sortTriggers()
					self.Changed(true)
				}
			})
			.catch(app.handleException)
		}

		this.editTrigger = function (item) {
			var
					itemIsSchedule = (item === self.schedule),
					IsAbsolute = itemIsSchedule || self.schedule.IsAbsolute.peek(),
					Offset = itemIsSchedule ? 0 : item.Offset.peek(),
					scheduleStart = self.schedule.Start.peek(),
					Start = (itemIsSchedule) ? scheduleStart : scheduleStart.add(Offset);
			var buffer = new TriggerData({
				'IsAbsolute': IsAbsolute,
				'Start': Start,
				'Offset': Offset
			})
			return app.showDialogEx(buffer)
			.then(function (Ok) {
				if (Ok) {
					var Offset, parent;
					if (itemIsSchedule) {
						self.schedule.IsStartLocked(true)
						self.schedule.Start(buffer.Start.peek())
						self.schedule.updateItems()
					} else {
						if (IsAbsolute) {
							Offset = buffer.Start.peek().subtract(scheduleStart)
						} else {
							Offset = buffer.Offset
						}
						console.log('Offset:', Offset.toString())
						item.Offset(Offset)
						item.IsOffsetLocked(true)

						// If item is not on the top level:
						parent = self.schedule.getParentFor(item)
						if (parent && parent !== self.schedule) {
							parent.Items.remove(item);
							self.Items.push(item)
						}
						self.schedule.sortTriggers()
					}
					// If schedule itself:
					self.Changed(true)
				}
			})
			.catch(app.handleException)
		}

		this.removeTrigger = function (item) {
			if (item instanceof Schedule) {
				item.IsStartLocked(false);
			} else {
				item.IsOffsetLocked(false);
			}
			self.schedule.sortTriggers()
			self.schedule.updateItems();
			self.Changed(true)
		}
//---

		this.editDuration = function (item) {
			// var item = new mdSchedule.PlaybackStream();

			app.customDialog({
				'buffer': item,
				'attrFilter': mdSchedule.StructPlaybackFrame,
				'constructorFunc': PlaybackFrame,
				'viewUrl': 'views/schedule-editor-dialogs/dlg-edit-duration'
			}).waitConfirm()
			.then(function (Ok) {
				if (Ok) {
					self.Changed(true)
				}
			})
			.catch(app.handleException)
			// body...
		}


		this.splitItem = function (item) {
			var dlgBuffer = (function () {
					var model = {};
					var duration = item.RequiredDuration.peek();
					model.Duration = ko.observable(duration)
					model.maxSubitemDuration = Math.floor(duration/2)
					model.SplitCount = ko.observable(2)
					model.SubitemDuration = ko.observable(model.maxSubitemDuration)
					model.UseSplitCount = ko.observable("yes")
					model.Remainder = ko.pureComputed(function () {
							return new DateTimeInterval( model.Duration() % model.SubitemDuration() )
					})
					model.CopyContents = ko.observable(true);

					model.SplitCount.subscribe(function () {
						if (model.UseSplitCount.peek() === 'yes') {
							model.SubitemDuration(new DateTimeInterval( Math.floor( model.Duration.peek() / model.SplitCount.peek())) )
						}
					})

					model.SubitemDuration.subscribe(function () {
						if (model.UseSplitCount.peek() === 'no') {
							model.SplitCount(Math.floor( model.Duration.peek() / model.SubitemDuration.peek()))
						}
					})

					model.SubitemDuration.subscribe(function (newValue) {
						if (newValue > model.maxSubitemDuration || newValue < 60000) { // <-- loop over the "max" value
							model.SubitemDuration(new DateTimeInterval( model.maxSubitemDuration ))
						}
					})

					model.SplitCount.subscribe(function (newValue) {
						if (newValue < 2) {
							model.SplitCount(2)
						}
					})

					return model
				})()
			dlgBuffer['viewUrl'] = 'views/schedule-editor-dialogs/dlg-timeslot-split';

			app.showDialogEx(dlgBuffer)
			.then(function (Ok) {
				var
					parent = self.schedule.getParentFor(item),
					collection = parent.Items.peek(),
					index = parent.Items.indexOf(item),
					dontDuplicateContents = !dlgBuffer.CopyContents.peek(),
					child, data, contents;

				console.log('dialog result', Ok)

				if (Ok) {
					// Remove original item from collection:
					collection.splice(index, 1);

					// Set new duration:
					item.SlotDuration(dlgBuffer.SubitemDuration.peek())

					data = item.toJS();

					if (item instanceof PlaybackFrame) {
						for (var i=0, len=dlgBuffer.SplitCount.peek(); i<len; i++) {
							// Clear contents for 2nd+ child if necessary:
							if (dontDuplicateContents && i === 1) data.RecordInfo = {}
							child = new PlaybackItem(data);
							collection.splice(index, 0, child)
						}
					} else if (item instanceof PlaybackStream) {
						for (var i=0, len=dlgBuffer.SplitCount.peek(); i<len; i++) {
							// Clear contents for 2nd+ child if necessary:
							if (dontDuplicateContents && i === 1) data.Items = [];
							child = new PlaybackStream(data);
							collection.splice(index, 0, child)
						}
					}

					parent.Items(collection)

					self.Changed(true)
				}
			})
			.catch(app.handleException)
		}

		// 	app.customDialog({
		// 		'buffer': buffer,
		// 		'attrFilter': StructPlaybackStream,
		// 		'constructorFunc': PlaybackStream,
		// 		'viewUrl': 'views/dlg-edit-repetition',
		// 		'lookups': {
		// 			'lkpPeriods': self.lkpPeriods,
		// 			'lkpFmtPeriodLabel': function(item){
		// 					console.log('lkpPeriods======>', item, parseInt(item.value/1000))
		// 					switch ( parseInt(item.value) ) {
		// 						case 3600:
		// 						case 86400:
		// 						case 604800:
		// 							return item.label;
		// 						default:
		// 							return 'Exactly...';
		// 					}
		// 			}
		// 		}
		// 	}).waitConfirm()
		// 	.then(function (Ok) {
		// 		if (Ok) {
		// 			var item = new mdSchedule.PlaybackStream({

		// 			});
		// 			self.schedule.createRecursivePattern(); // <- item, period, times
		// 			self.Changed(true)
		// 		}
		// 	})
		// 	.catch(app.handleException)
		// }

		this.addDailyGrid = function () {
			var buffer = {};
			var period = new DateTimeInterval({'hours': 24});

			buffer.Start = ko.observable(new LocalDateTime({'hours':0, 'minutes':0}))
			buffer.Stop = ko.observable(new LocalDateTime({'hours':23, 'minutes': 59}))
			buffer.SlotDuration = ko.pureComputed(function () {
				var interval = buffer.Stop().subtract(buffer.Start());
				if (interval < 0) {
					interval = interval.add(new DateTimeInterval({'hours': 24}))
				}
				return interval;
			})
			buffer.fmtDuration = ko.pureComputed(function () {
				return buffer.SlotDuration().toString();
			})

			buffer.CloneByReference = ko.observable(false)
			buffer.viewUrl = 'views/dlg-edit-shd-daygrid'
			buffer.Cancel = function () {dialog.close(buffer)}
			buffer.Ok = function () {dialog.close(buffer, true)}

			app.showDialog(buffer)
			.then(function (Ok) {
				if (Ok) {
					var item = new mdSchedule.PlaybackStream({
						'Offset': buffer.Start.peek(),
						'SlotDuration': buffer.SlotDuration.peek(),
						'IsDurationLocked': true,
						'IsOffsetLocked': true
					});
					self.schedule.createRecursivePattern(item, period, 7);
					self.Changed(true)
				}
			})
			// .catch(app.handleException)
		}

		this.editComment = function (item) {

			app.customDialog({
				'buffer': item,
				'attrFilter': mdSchedule.StructPlaybackItem,
				'constructorFunc': PlaybackItem,
				'viewUrl': 'views/schedule-editor-dialogs/dlg-edit-comment'
			}).waitConfirm()
			.then(function (Ok) {
				if (Ok) {
					self.Changed(true)
				}
			})
			.catch(app.handleException)
			// body...
		}

		this.renderSchedule = function () {
			// WAS: schedule.publish
			self.publish()
				.then(function () {
					self.stateLegend('Schedule rendered')
				})
				.catch(app.handleException);
		}

		// common code to remove item
		var _doRemove = function (item, collectionList, selectionList) {
			return new Promise(function (resolve, reject) {
				try {
					console.log('Removing item:::', item);
					// To-do: use PropertyReflection here to operate directly on collection...

					var index = collectionList.indexOf(item);
					if (index >-1) {
						collectionList.splice(index, 1);
						self.Changed(true)
					}
					// remove from selection ("markedItems"):
					index = selectionList.indexOf(item);
					if (index >-1) selectionList.splice(index,1)
					resolve(true)
				} catch (e) {
					reject(e)
				}
			})
		}

		self.remove = function (item) {
			// body...
			var _collection = self.items.peek();

			// ask confirm:
			app.showMessage(
				'Are you sure you want to remove element \"'+ item.Label() + '\"?',
				'Confirm', ['Yes', 'No']
			).then(function (result) {
				if (result == 'Yes') {
					self.schedule.removeItem(item);
				}
			});
		};

		self.removeMany = function (item) {
			// body...
			var
				_selection = self.markedItems.peek();
				_collection = self.items.peek();
			if (!_selection.length) return;
			// ask confirm:
			app.showMessage(
				'Are you sure you want to remove '+ _selection.length + ' element(s)?',
				'Confirm', ['Yes', 'No']
			).then(function (result) {
				if (result != 'Yes') return;

				// Promise.all(_.map(_selection, function (item) {
				// 	return _doRemove(item, _collection, _selection)
				// }))

				// Do sych execution due to external variables are not thread-safe
				_.reduce(_selection, function (promise, item) {
					return promise.then(function () {
						return _doRemove(item, _collection, _selection)
					})
				}, Promise.resolve())
				.then(function () {
					self.items(_collection)
					self.markedItems(_selection)
				})
				.catch(app.handleException)

			});
		};

		self.save = function () {
			itemRef.save(self.schedule, app.rqOptions)
				.then(function () {
					self.Changed(false)
					self.stateLegend('Saved')
				})
				.catch(app.handleException)
		}

		// Publish rendered movie
		self.publish = function () {
			var js = self.schedule.renderJSON();

			var moviesSvc = app.IO.gw.services['Expositions'];
			var itemRef = app.IO.gw.reflections['Expositions'];

			var xp = new mdExposition.Exposition()
			// Apply values:
			// Use same Id as for original playist:
			xp.StorageID(self.schedule.StorageID.peek())
			// Mark type of the source entity - Playlists
			xp.SourceEntity('Schedules')
			xp.json(js)
			xp.Label(self.schedule.Label.peek())
			xp.js(JSON.parse(js))

		  return moviesSvc.signIn()
			  .then(function () {
			  	return itemRef.save(xp, app.rqOptions);
			  })
		}

		self.linkToPreview = ko.pureComputed(function () {
			var uId = app.auth.user && app.auth.user.uid || '';
			return 'app/test/test-player-edl.html?schId='+self.schedule.StorageID() + '&usr='+uId;
		})


		self.edit = function (item) {
			// body...
		}

		self.closeEditor = function () {
			router.navigate('schedules', {trigger: true, replace:true})
		}

		self.canDeactivate = function (argument) {

			if (!self.Changed()) return true;

			var title = 'Warning';
			var msg = 'Do you want to leave this page and lose all of your edits to this form?';
			return app.showMessage(msg, title, ['Yes', 'No'])
				.then(function (selectedOption) {
					return selectedOption === 'Yes';
				});
		}

		/*
		End of CRUD
		*/

		self.draggingItem = ko.observable(null);

		var __draggingWidth__ = null;

		// this.itemElementAttrs = function (trigger) {
		// 	var triggerOffset = trigger.Start() // <-- for recurrent ?
		// 		,windowOffset = self.ViewportStart()
		// 		,ViewportDuration = self.ViewportDuration()
		// 		,leftMargin = triggerOffset > windowOffset ? (triggerOffset - windowOffset)*100/ViewportDuration : 0
		// 		,attrs = {}
		// 		,style = ['left:' + leftMargin + '%']
		// 		,duration = trigger.Duration().module()
		// 		,isInfinite = (duration === Infinity)
		// 		,visibleWidth;
		// 	if (trigger !== self.draggingItem()) {
		// 		if (duration === Infinity) {}
		// 		visibleWidth = (duration === Infinity)
		// 			? 100 /*%*/ - leftMargin
		// 			: duration*100/ViewportDuration - leftMargin
		// 		style.push('width:' + visibleWidth + '%');
		// 	}
		// 	attrs['style'] = style.join(';')
		// 	if (isInfinite) attrs['class'] = 'infinite'

		// 	return attrs;
		// }
		//
		var TRACE = console;

		// var __widthPx__; // <-- stores a start value before resizing
		this.handleStartResize = function (argument) {
			var record_obj = ko.dataFor(this);

			// TRACE.log('"draggingItem" object: ', ko.toJS(record_obj))
			// TRACE.log('"this" value: ', this)
			// TRACE.log('"this" width: ', $(this).width())

			self.draggingItem(record_obj)
			// __widthPx__ = $(this).width();
			__draggingWidth__ = $(this).width()
		}

		this.handleResizeComplete = function (event, ui) {
			// var context = ko.contextFor(this),
			// 	record_obj = ko.dataFor(this),
			// 	oldWidth = __draggingWidth__,
			// 	newWidth = $(this).width(),

			// 	// ___ = console.log('',record_obj, '',record_obj.Duration()),

			// 	duration = record_obj.Duration.peek().module(),
			// 	newDuration = duration*(newWidth/oldWidth),
			// 	// newDuration = duration*(1 + (newWidth - oldWidth)/oldWidth),
			// 	quantum = self.schedule.Quantum.peek();
   //      	// ui.element.css({height:null})
   //      	// if (event.type === 'stopresize')

   //      	// console.log('event', event)
   //      	// console.log('ui', ui)

   //  		//align new value to the integer count of quantum
   //  		newDuration = Math.round(newDuration / quantum) * quantum;

   //  		// console.log('COMPLETE: oldWidth', oldWidth, 'newWidth', newWidth, 'duration', duration, 'newDuration', newDuration);

   //  		// setTimeout(function () {
	  //       	self.schedule.resizeItem(record_obj, newDuration);

				var record_obj = ko.dataFor(this),
					duration = record_obj.SlotDuration.peek();
	  // self.handleResize.call(this, event, ui);
			self.draggingItem(null);

			self.schedule.resizeItem(record_obj, duration, false);
				self.Changed(true)
			// }, 100)

			__draggingWidth__ = null;

		}

		this.handleResize = function (event, ui) {
			var context = ko.contextFor(this),
				record_obj = ko.dataFor(this),
				newWidth = $(this).width(),
				oldWidth = __draggingWidth__,
				duration = record_obj.SlotDuration.peek(),
				newDuration = duration*(newWidth/oldWidth),



				// ___ = console.log('handleResize: record_obj.duration:',  duration, 'newDuration:', newDuration, 'oldWidth: ', oldWidth, 'newWidth', newWidth, 'ratio: ', newWidth/oldWidth),

				quantum = self.schedule.Quantum.peek();
			// ui.element.css({height:null})
			// if (event.type === 'stopresize')

			// console.log('oldWidth', oldWidth, 'newWidth', newWidth, 'duration', duration, 'newDuration', newDuration);

			console.log('resizing: ', record_obj.Label(), 'from: ',duration, ' to: ', newDuration, 'W0:', oldWidth, 'W1:', newWidth);
			//align new value to the integer count of quantum
			newDuration = Math.round(newDuration / quantum) * quantum;

			// console.log('quantum applied:', 'newDuration', newDuration, 'quantum', quantum)

			// console.log('oldWidth', oldWidth, 'newWidth', newWidth, 'duration', duration, 'newDuration', newDuration);

			self.schedule.resizeItem(record_obj, Math.round(newDuration), true);
			__draggingWidth__ = newWidth;
		}

	};

	//Note: This module exports a function. That means that you, the developer, can create multiple instances.
	//This pattern is also recognized by Durandal so that it can create instances on demand.
	//If you wish to create a singleton, you should export an object instead of a function.
	//See the "flickr" module for an example of object export.

	return ctor;
});