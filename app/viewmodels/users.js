define([
    'durandal/app', 
    'knockout.all', 
    'underscore.all',
    'viewmodels/model-user', 
    'shared/tableview'], 
		function (app, ko, _, ModelLib, TableViewFactory) {
    //Note: This module exports an object.
    //That means that every module that "requires" it will get the same object instance.
    //If you wish to be able to create multiple instances, instead export a function.
    //See the "welcome" module for an example of function export.

    var options = {

        displayName : 'Users',
        itemClass : ModelLib.User,
        entityName : 'AdmUsers', // <-- data partition on the server
        attrFilter : ModelLib.StructUser,
        dialogViews : {
            'add':'views/dlg-add-user',
            'edit':'views/dlg-edit-user'
        },
        columns : [
            // name, sortBy, sortState
            {name:'Login email', sortBy:'AccountEmail'},

            // {name:'Nickname', sortBy:'Label'},

            {name:'Last name', sortBy:'LastName'},
            {name:'First name', sortBy:'FirstName'},


            {name:'phone', sortBy:'ContactPhone'},
            {name:'Address', sortBy:'BillingAddress'},

            {name:'Locked', sortBy:'IsLocked'},
            {name:'Admin', sortBy:'IsAdmin'},

            {name:'Created', sortBy:'CreatedTime'},
            {name:'Modified', sortBy:'ModifiedTime'},

            {name:'Space', sortBy:'SpaceUsed'},

            {name:'Comment'}
        ],
        endpoints : {
            'create':'',
            'read':'',
            'readMany':'',
            'update':'',
            'delete':''
        },

        addMethods: {
            'toggleLockUser': function (item) {
                var oUser = item;
                var flag = oUser.IsLocked.peek();
                var verb = (flag) ? 'unlock' : 'lock';

                console.log(ko.toJS(item));

                if (oUser.AccountEmail.peek() === app.auth.user.email) {
                    app.showMessage('You cannot lock your own account!', 'Error');
                    return false;
                }

                app.showMessage(
                    'Are you sure to '+verb+' user \"'+oUser.AccountEmail.peek()+'\"?', 
                    'Confirmation', 
                    ['Yes', 'No'])
                .then(function (result) {
                    console.log(result)
                    if (result !== 'Yes') return

                    oUser.IsLocked(!flag);

                    app.IO.gw.reflections['AdmUsers']
                        .update(oUser, app.rqOptions)
                        .catch(app.handleException)                        
                })
                return false;
            }
        },

        // onBeforeAdd: function (data) {
        //     if (data) {
        //         var serialized = ko.toJS(data)
        //         var pwd = serialized.password;
        //         var email = serialized.ContactEmail;
        //         console.log('creating user >>>');
        //         return (email && pwd) 
        //             ?app.auth.createUser({email: email, password: pwd})
        //             :Promise.reject('Invalid data in account record')
        //     }
        //     return Promise.resolve(data);
        // },

        onBeforeRemove: function (oUser) {
            var gw = app.IO.gw;
            var uid = oUser.UID.peek();

            var removeNode = function (entityName) {
                var transactionOpt = {'pathArgs':{'userId':uid}}
                var collection = gw.reflections[entityName].enum(transactionOpt);
                var removeItem = function (item) {
                    console.log('>>> removing', item);
                    return collection.remove(item, transactionOpt)
                }
                console.log('removing collection for: ', entityName, collection);
                return Promise.all(_.map(collection, removeItem))
            }

            var removeAll = function () {
                return Promise.all(
                    _.map(gw.userResources, removeNode)
                )
            }

            var notifySuccess = function () {
                app.showMessage('All data for user heve been removed!', 'Success', ['Ok']);
                return Promise.resolve(true);
                // throw new Error('Debug: break here!');
            }

            return (new Promise(function (resolve, reject) {
                app.showMessage(
                    'Do you really want to remove user and all his resources?<br>This cannot be undone!', 'Warning', ['Yes', 'No']
                    ).then(function (result) {
                        if (result !== 'Yes') reject(new Error('Declined by user'));
                        resolve(true);       
                    });
            }))
            .then(removeAll)
            .then(notifySuccess)
            // .catch(app.handleException);
            // return Promise.resolve(oUser);
        },

        populate : [
        ],

        lists: {
            'QuotaList': [
                {'label': '1 Mb', 'value': 1048576 * 1},
                {'label': '10 Mb', 'value': 1048576 * 10},
                {'label': '100 Mb', 'value': 1048576 * 100},
                {'label': '1 Gb', 'value': 1048576 * 1000}
            ]
        }
    };

    // Set up jQuery plugins on dialogs?
    app.on('customDialog:compositionComplete').then(function () {
        app.formManager.installHelpers({
            JQ_SEL_MASKED_FIELD: '.modal-body input[data-inputmask-alias]',
            JQ_SEL_FIELD_FEEDBACK: '.modal-body span.form-control-feedback',
            JQ_SEL_USER_MESSAGE: null, //'section.user-message',
            JQ_SEL_REQUIRED: '.modal-body *[required]',
            // JQ_CLASS_MASKED: 'masked-field',
            JQ_CLASS_INCOMPLETE: '.modal-body incomplete-field'
        });
    })




    return TableViewFactory(options);

});