define(['durandal/app', 'viewmodels/model-user', 'knockout.all'], 
    function (app, mdUser, ko) {
    var ctor = function () {
        var self = this;
        this.displayName = 'Settings';
        this.description = '';
        this.features = [
        ];

        this.userData = new mdUser.User();

        this.getPhotoUrl = ko.pureComputed(function () {
            var photoUrl = self.userData.PhotoUrl();
            return photoUrl ? photoUrl : 'img/person-icon.png'
        })

        this.activate = function () {
            this.userData.AccountEmail(app.auth.user.email)
            app.IO.gw.reflections['AdmUsers'].load(this.userData)
                .catch(app.handleException)
        }

        this.compositionComplete = function () {
            app.formManager.installHelpers({
                JQ_SEL_MASKED_FIELD: 'form#user-data input[data-inputmask-alias]',
                JQ_SEL_FIELD_FEEDBACK: 'form#user-data span.form-control-feedback',
                JQ_SEL_USER_MESSAGE: null, //'section.user-message',
                JQ_SEL_REQUIRED: 'form#user-data *[required]',
                // JQ_CLASS_MASKED: 'masked-field',
                JQ_CLASS_INCOMPLETE: 'form#user-data incomplete-field'
            });
        }

        // Event handler, use "self" inside
        this.updateInfo = function () {
            // Check user photo?
            var queue = [];

            var UserPhotoReflection = app.IO.gw.reflections['UserPhoto'];

            var updateAuthUserFields = function () {
                var photoUrl = self.userData.PhotoUrl.peek() || null;
                var displayName = self.userData.Label.peek() || self.userData.AccountEmail.peek();
                console.log('Url updated, update profile in raw auth', ko.toJS(self.userData))
                return app.auth.updateProfile({
                    'photoUrl': photoUrl,
                    'displayName': displayName
                })
            }

            var doPhotoUpload = function () {
                    return new Promise(function (resolve, reject) {
                        // Upload photo to CDN
                        UserPhotoReflection
                            // store
                            .save(self.userData, app.rqOptions)
                            // load updated version with actual photo url:
                            .then(function (response) {
                                console.log('Photo uploaded, update object', ko.toJS(self.userData))
                                return UserPhotoReflection.load(self.userData, app.rqOptions)
                            })
                            .then(function (result) {
                                console.log('Refreshed userData (with metadata)', ko.toJS(result))
                                return Promise.resolve(result)
                            })
                            .then(resolve)
                                    // .catch(reject)
                            .catch(reject)
                    })
                }

            var updateUserInfo = function () {
                return new Promise(function (resolve, reject) {
                    app.IO.gw.reflections['AdmUsers']
                        .update(self.userData, app.rqOptions)
                        .then(resolve)
                        .catch(reject)
                })            
            }

            var showOk = function () {
                    app.showMessage('Data has been updated!', 'Status', ['Ok'])
                }

            if (self.userData.uploadFile.peek()) {
                doPhotoUpload()
                    .then(updateUserInfo)
                    .then(updateAuthUserFields)
                    .then(showOk)
                    .catch(app.handleException)
            } else {
                updateUserInfo()
                    .then(updateAuthUserFields)
                    .then(showOk)
                    .catch(app.handleException)
            };

            return false;

        }
    };

    //Note: This module exports a function. That means that you, the developer, can create multiple instances.
    //This pattern is also recognized by Durandal so that it can create instances on demand.
    //If you wish to create a singleton, you should export an object instead of a function.
    //See the "flickr" module for an example of object export.

    return ctor;
});