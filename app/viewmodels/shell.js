define([
	'underscore.all'
	,'knockout.all'
	,'plugins/router'
	,'plugins/dialog'
	,'durandal/app'
	,'viewmodels/user-check' //<-- install userCtx interceptor
	],

function (_, ko, router, dialog, app, checkUser) {

	var TRACE_MODULE = false;

	var shell = {
		router: router,
		user: ko.observable(app.auth.user), /*Init user with initial value from Auth*/
		// Stores user context (isLocked, isAdmin, ...)
		userCtx: ko.observable(app.auth.user && app.auth.user.Ctx || null)
	};

	// For debug purposes only:
	if (TRACE_MODULE) {
		shell.user.subscribe(function (newValue) {
			console.log('[trace] shell.user changed: ', newValue, shell.user.peek())
		})
		// For debug purposes only:
		shell.userCtx.subscribe(function (newValue) {
			console.log('[trace]  shell.userCtx changed: ', newValue, shell.userCtx.peek())
		})
	}

	// Intercept events from user-check:
	app.on('auth:userCtx:success').then(function (Ctx) {
		if (TRACE_MODULE) console.log('auth:userCtx processed in "shell"', Ctx);
		var oldUserValue = shell.user.peek();
		var newUserValue = app.auth.user;
		shell.user(newUserValue); //<-- fixed to intercept case when Ctx resolved on startup by promise but onAuthStateChanged not fired
		shell.userCtx(Ctx);
		if (!Ctx) {
			router.navigate('', {trigger: true, replace:true});
		} else {
			// User only signed in:
			if (!oldUserValue && newUserValue)
				router.navigate('dashboard', {trigger: true, replace:true});
		}
		// if (router && router.activeInstruction()) {
		// 	var activeRoute = router.activeInstruction().config.route;
		// 	// redirect rot and landing to dashboard:
		// 	if (['', 'landing'].indexOf(activeRoute) >-1)
		// 		router.navigate('dashboard', {trigger: true, replace:true});
		// 	// if (router.activeInstruction.peek().config.route !== '')
		// 	// 	router.navigate('', {trigger: true, replace:true});
		// }
	});


	app.on('auth:userCtx:error').then(function (error) {
		// var activeInstruction = router && router.activeInstruction() || null;
		// var allowAnonymous =
		// 	activeInstruction && (
		// 		activeInstruction.config.allowAnonymous || ['', 'landing'].indexOf(activeInstruction.config.route > -1)
		// 	) || false;
		shell.userCtx(null);
		app.handleException(error).then(function () {
			router.navigate('', {trigger: true, replace:true});
		});
		// if (!allowAnonymous)
		// 		router.navigate('', {trigger: true, replace:true});
	});

	return _.extend(shell, {
		search: function() {
			//It's really easy to show a message box.
			//You can add custom options too. Also, it returns a promise for the user's response.
			app.showMessage('Search not yet implemented...');
		},

		userName: ko.pureComputed(function () {
			var user = shell.user();
			return user ? user.displayName : 'unathorized';
		}),

		userPhoto: ko.pureComputed(function () {
			var user = shell.user();
			return (user && user.photoUrl) ?
				'<span style="background-image=\"'+user.photoUrl+'\"></span>'
				: '<i class="glyphicon glyphicon-user"></i>';
		}),

		signIn: function () {
			var buffer = {
				'viewUrl': 'views/dialog-login',
				userName: '',
				password: '',
				Ok: function () {dialog.close(this, true)},
				Cancel: function () {dialog.close(this, false)},
				/*Autofocus username input:*/
				// compositionComplete: function () {
				// 		setTimeout(function () {document.getElementById('login-username').focus()
				// 	}, 100);
				// }
			};
			app.showDialog(buffer)
				.then(function (result) {
					if (result)
						return app.auth.signIn({
								email: buffer.userName,
								password: buffer.password
							}).then(checkUser)
							.then(function (userCtx) {
								if (!userCtx) throw new Error('Account not found');
								if (userCtx.isLocked) {
									app.showMessage('Sorry, your account is locked. Contact admnistrator, please.')
										.then(shell.signOut);
								} else {
									router.navigate('dashboard', {trigger: true, replace:true});
								}
							})
							.catch(app.handleException)
				})
			// return false;
		},

		signOut: function () {
			app.auth.signOut().then(function (result) {
				if (TRACE_MODULE) console.log('signOut', result);
				shell.user(null)
				shell.userCtx(null)
				// router.navigate('landing');
			})
			.catch(app.handleException);
			return false;
		},

		isSigned: ko.pureComputed(function () {
			var currentUser = shell.user();
			return !!currentUser;
		}),

		isUserNotLocked: ko.pureComputed(function () {
			return !shell.user() || !shell.userCtx() || !shell.userCtx().isLocked;
		}),

		compositionComplete: function () {
			// // Subscribe to notification:
			// app.on('auth:stateChanged', function (data) {
			// 	console.log('auth:stateChanged', data);
			// 	shell.user(data);
			// 	// reload page because auth state changed:
			// 	// if (!shell.isSigned()) router.navigate('landing');

			// 	// router.navigate( (shell.isSigned()) ? '' : 'landing');
			// 	// if (shell.isSigned()) { router.navigate('') } else {};
			// });
		},
		activate: function () {

			router.map([
				{ route: ['landing', ''], title:'Getting started with yScreens!', moduleId: 'viewmodels/landing', nav: false, icon:'',
				allowAnonymous: true },

				{ route: 'dashboard', title:'Dashboard', moduleId: 'viewmodels/dashboard', hash:'#dashboard', nav: true, icon: 'fa-tachometer' },
				{ route: 'assets', title:'Assets', moduleId: 'viewmodels/assets', nav: true, icon: 'fa-cubes' },
				{ route: 'playlists', title:'Playlists', moduleId: 'viewmodels/playlists', nav: true, icon: 'fa-film' },
				{ route: 'channels', title:'Channels', moduleId: 'viewmodels/channels', nav: true, icon: 'fa-plug' },
				{ route: 'displays', title:'Displays', moduleId: 'viewmodels/devices', nav: true, icon: 'fa-television' },

				{ route: 'records', title:'Records', moduleId: 'viewmodels/records', nav: true, icon: 'fa-database' },

				{ route: 'schedules', title:'Schedules', moduleId: 'viewmodels/schedules', nav: true, icon: 'fa-calendar' },

				{ route: 'schedule-editor(/:scheduleId)', title:'Schedule Editor', moduleId: 'viewmodels/schedule-editor', hash:'#schedule-editor', nav: false, icon: 'fa-calendar' },

				{ route: 'schedule-assistant(/:scheduleId)', title:'Schedule Assistant', moduleId: 'viewmodels/schedule-assistant', hash:'#schedule-assistant', nav: false, icon: 'fa-calendar' },

				{ route: 'settings', title:'Settings', moduleId: 'viewmodels/settings', nav: false, icon: 'fa-cog' },
				{ route: 'about', title:'About', moduleId: 'viewmodels/about', nav: false, icon: 'fa-info-circle' },

				// Admin-only pages (set nav: false):
				{ route: 'accounts', title:'User Accounts', moduleId: 'viewmodels/users', nav: false, icon: 'fa-users', hash: '#accounts',
				adminOnly: true},


				// A samle view which not included in a direct navigation:
				// { route: 'composer(/:id)', title:'Playlist Composer', moduleId: 'appComposer/composer', hash: '#composer', nav: false, icon:'' },


				{ route: 'test-dlg', title:'Test dialog', moduleId: 'test/test-dlg', nav: false, icon:'' }


			]).buildNavigationModel();

			// Guard routes

				        	// return (app.auth.user && app.auth.user.Ctx.isSubscriber) ? true : 'index.html#landing';


			router.guardRoute = function (instance, instruction) {
				var user = app.auth.user;
				var target = instruction.config.route;
				var allowAnonymous = instruction.config.allowAnonymous;
				var requireAdmin = instruction.config.adminOnly;

				// console.log('=============== guardRoute ====================', instruction, target);


				// For authehticated user - replace root by "dashboard":
				if (user && ['', 'landing'].indexOf(target) > -1) return 'dashboard';

				/*
				Public pages:
				*/

				// Allow regardless the authentication status:
				if (allowAnonymous) return true;

				// User not authenticated, required resource doe not allow anonymous, so go to the root:
				if (!user) return '';

				// User authenticated but Admin priveleges required:
				if (requireAdmin) return (user.Ctx.isAdmin)

				// All other cases (allow):
				return true;
			}
			// 	 // From this guardRoute you can return a Boolean whether user is allowed or not, or a view that needs to be called if user is not allowed. You can even return a promise. Checkout the DurandalJS site for more information.
			// 	 // http://durandaljs.com/documentation/api.html#class/Router/method/guardRoute

			// 	// if user signed in:
			// 	if (app.auth.isSigned()) {
			// 		// if current location was landing - jump to dashboard:
			// 		if (instruction.config.route ==='landing') return '';
			// 		// otherwise allow any:
			// 		return true
			// 	};

			// 	// User is not signed in:
			// 	// Allow landing if it is a target:
			// 	if (instruction.config.route ==='landing') return true

			// 	// otherwise lways redirect "guests" on landing:
			// 	return 'landing';
			// }
			//

			// Subscribe to notification:
			app.on('auth:stateChanged').then(function (data) {
				if (TRACE_MODULE) console.log('auth:stateChanged', data);
				shell.user(data);
			});

			return app.auth.retriveSession()
				.then(checkUser)
				.then(function (response) {
					app.trace.log('auth: checkUser promise resolved', response)
					// Pass pushState arg to router.activate (SEO-friendly ursl?)
					// return { pushState : true }
					return null;
				})
				.then(app.auth.retriveToken) // <- To-do: solve task with .retrieveToken more clear... It is necessary
				.then(router.activate)
				.catch(app.handleException)
		}
	});
});