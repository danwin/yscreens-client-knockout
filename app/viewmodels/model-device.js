define([
	'underscore.all',
	'knockout.all',
	'viewmodels/model-base',
	'viewmodels/model-channel',
	], function(_, ko, BaseModel, mdChannel){

	/*////////////////////////////////////////////////////////
	//
	// Device
	//
	*/////////////////////////////////////////////////////////

	// To-do: Client device protection - via Auth for devices. Create tokens via JS RSA lib. (see: http://kjur.github.io/jsrsasign/; https://github.com/firebase/quickstart-js/blob/master/auth/exampletokengenerator/auth.html)
	// Use Firebase Auth caps

	var StructDevice = function(scope, data) {
		// Persistent fields for I/O
		scope = scope || {};
		var
			Field = ko._FieldFactory_(scope, ko.observable),
			FieldA = ko._FieldFactory_(scope, ko.observableArray);

		/* Field('StorageID'),
		*  Field('CreatedTime'),
		*  Field('ModifiedTime'): */
		BaseModel.StructPersistent(scope, data);

		Field( 'Label', 'New Device', data);

		Field( 'LogicalID', null, data);

		Field( 'Resolution', '-', data);
		Field( 'Browser', '-', data);


		Field( 'Lat', null, data);
		Field( 'Lon', null, data);
		Field( 'Address', '', data);

		Field( 'Comment', '', data);

		FieldA( 'Tags', [], data);

		// Default playlist (visible when no schedule defined)
		Field( 'ChannelInfo', null, data, function (args) {
			return (args)
				? _.pick(mdChannel.StructChannel({}, args), 'StorageID', 'Label', 'Tags')
				: null;
		});

		return scope;
	}

	var Device = function (data) {
		var self = this;

		// Session-ony resource Id for message binding
		_.applyReferenceID(self, 'Device_');

		StructDevice(self, data);

		// Implement "updateValues" method which binded with the appropriate factory:
		BaseModel.IFUpdate(self, StructDevice);

		// *** Constant fields ***

		self._rtti = 'class:Device';
		_.assertTrue(self !== window,
			'"'+self._rtti+'"" function is a constructor, call it with "new"!');

		// *** Editor context ***

		// Session-only field, contains uploaded data from request to "Magneto Device"
		self.HardwareInfo = ko.observable();

		self.ChannelID = ko.pureComputed(function () {
			return (self.ChannelInfo()) ? self.ChannelInfo.peek().StorageID.peek() : null;
		})

		self.ChannelLabel = ko.pureComputed(function () {
			console.warn('ChannelInfo', ko.toJS(self.ChannelInfo));
			return (self.ChannelInfo()) ? self.ChannelInfo.peek().Label.peek() : '';
		})

		self.fmtLinkedGear = ko.pureComputed(function () {
			var key = self.LogicalID();
			return key ? '..'+key.substring(key.length-6) : ''
		})
	}

	return {
		StructDevice: StructDevice,
		Device: Device
	}

});