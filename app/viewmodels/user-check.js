//Check user rights
define([
	'underscore.all',
    'durandal/app', 
    'knockout.all', 
    'viewmodels/model-user',
    'shared/io.middleware',
    'viewmodels/io-config'
    ], 
function (_, app, ko, ModelLib, IO) {

	var User = ModelLib.User;

	var UserCtx = IO.UserCtx;

	var lastEmail = null;
	var userCtx = null;

	var lock = 0;

	var userChanged = function (user) {
		// still no user:
		if (lastEmail === null && !user) return false;

		// same user (compare by email):
		if (user && lastEmail === user.email) return (!userCtx); //<-- do not update userCtx if already defined

		// sign out (session was active):
		if (lastEmail !== null && !user) {
			userCtx = null;
			lastEmail = null;
			return true;
		}
		// sign in (session was inactive):
		if (lastEmail === null && !!user) {
			lastEmail = user.email;
			return true
		}
		// change session to another user:
		if (lastEmail !== user.email) {
			lastEmail = user.email;
			return true;			
		}
		return false;

		// if (user) {
		// 	if (lastEmail && lastEmail === user.email) return false
		// 	_.assertDefined(user.email, 'user check error - no email for user specified!')
		// 	lastEmail = user.email;
		// 	return true
		// } else { // no user
		// 	if (lastEmail) { // user was signed
		// 		lastEmail = null;
		// 		return true
		// 	}
		// 	return false
		// }
		// // var noUser = !user;
		// // user = user || {};

		// // if (lastEmail === user.email || noUser && !lastEmail) return false; // <-- same user
		// // lastEmail = noUser ? null : user.email;
		// // return true;  // <-- changed! new context detected
	}

	var notifyChanges = function (message, argument) {
		//~app.trace.log(message, argument);
		app.trigger(message, argument);
	}

	var notifySuccess = notifyChanges.bind(null, 'auth:userCtx:success'); // assumed argument: userCtx
	var notifyError = notifyChanges.bind(null, 'auth:userCtx:error'); // assumed arument: e

	var checkUser = function (user) {
		lock++;
		return new Promise(function (resolve, reject) {
			var _resolve = function (arg) {
				lock--;
				resolve(arg)
				notifySuccess(userCtx) 
			}
			var _reject = function (arg) {
				lock--;
				reject(arg)
				notifyError(arg) 
			}

			//~app.trace.log('##########  checkUser:', lock, user)

			if (!userChanged(user)) {
				//~app.trace.log('checkUser: user is same, return result: ', userCtx);
				_resolve(userCtx);
				return
			} // <- already known

			// No user:
			if (!user) {
				userCtx = null;
				_resolve(userCtx)
				return				
			}

			// User is active:
			try {
			    var userTableRef = _.assertDefined(app.IO.gw.reflections['AdmUsers'], 
			    	'Cannot define user rights: table reflection is missed');

				// AccountEmail is a key for User:
				var email = user.email;
				var extUser = new User({'AccountEmail': email});
			} catch (e) {
				app.trace.error('Error on access to reflection?', e, user, userCtx);
				_reject(e)
				return
			} 

			// Try to call users x table
			userTableRef.load(extUser)
				.then(function () {
					var extUserInfo = ko.toJS(extUser); // <-unwrap observables
					// Define user context:

					// Update "local" value:
					userCtx = new UserCtx({
						isLocked: extUserInfo.IsLocked,
						isAdmin: extUserInfo.IsAdmin,
						// Define that user's email registered:
						isSubscriber: extUserInfo.AccountEmail === user.email,
					});

					// Update "global" value:
					if (app.auth.user) {
						_.extend(app.auth.user.Ctx, userCtx);
						
						// Fix known issue with photoUrl === null on Firebase:
						app.auth.user.photoUrl = extUser.getPhotoUrl()
					}

					_resolve(userCtx);
					return
					//Use Promise.all! at loading process
				})
				.catch(function (reason) {
					//~app.trace.log('auth: userCtx error', reason);
					_reject(new app.IO.AuthError('Cannot define user context', reason))
					return
				});

		})
	}

	app.on('auth:stateChanged').then(function (user) {
		//~app.trace.log('=============>lock: ', lock);
		if (lock) {
			//~app.trace.log('checkUser: lock!=0, ignoring,...');
			return
		}
		checkUser(user)
		// .then(notifySuccess) //<-- Promise(userCtx)
		.catch(function (reason) {
			notifyError(reason)
			app.handleException(reason);
		})
	})


	//~app.trace.log('userCtx interceptor installed');

	// return only one function:
	return checkUser;
});
