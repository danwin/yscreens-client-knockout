// dlg-device-location.js
define([
	'plugins/dialog',
    'durandal/app',
    'knockout.all',
    'underscore',
    'leaflet',
    'viewmodels/model-device'],
function (dialog, app, ko, _, Leaflet,  mdDevice) {

	var Device = mdDevice.Device;

	var ctor = function () {

		var self = this;

		this.viewUrl = 'views/dlg-device-location';

		this.deviceInstance = null;
		this.Label = ko.observable('');


		this.marker = null;

		this.hasLocation = ko.pureComputed(function () {
			var lat = self.deviceInstance.Lat.peek();
			var lon = self.deviceInstance.Lon.peek();
			return (!!lat && !!lon);
		})

		var createMarkerObj = function (latlng) {
		    // var geojsonFeature = {
		    //     "type": "Feature",
		    //         "properties": {},
		    //         "geometry": {
		    //             "type": "Point",
		    //             "coordinates": [latlng.lat, latlng.lng]
		    //     }
		    // }
		    // var marker;

		    // L.geoJson(geojsonFeature, {
		    //     pointToLayer: function(feature, latlng){
		    //         marker = L.marker(latlng, {
		    //             title: "Resource Location",
		    //             alt: "Resource Location",
		    //             riseOnHover: true,
		    //             draggable: true,
		    //         }).bindPopup("<input type='button' value='Delete this marker' class='marker-delete-button'/>");
		    //         // marker.on("popupopen", onPopupOpen);
		    //         return marker;
		    //     }
		    // }).addTo(self.map);

            var customDefault = L.icon({
                iconUrl: 'vendor/leaflet/css/images/marker-icon.png',
                shadowUrl: 'vendor/leaflet/css/images/marker-shadow.png',
            });

		    self.marker = L.marker(latlng, {
		    	icon: customDefault,
                title: "Resource Location",
                alt: "Resource Location",
                riseOnHover: true,
                draggable: true,

				    radius: 10,
		    })
		    self.marker.addTo(self.map);

		    // self.marker = marker;
		    return self.marker;
		}

		var handleClick = function (e){
		    // Add marker to map at click location; add popup window
		    if (!self.marker) {
		    	var marker = createMarkerObj(e.latlng);
			    marker.on('dragend', function(event){
		            var marker = event.target;
		            var position = marker.getLatLng();
		            console.log(position);
		            marker.setLatLng(position,{/*id:uni,*/draggable:true}).bindPopup(position).update();
			    });
			    // self.map.addLayer(marker);
		    }
		}

		this.accept = function () {
			if (self.marker) {
	            // Update buffer;
	            var newLoc = self.marker.getLatLng();
	            self.deviceInstance.Lat(newLoc.lat);
	            self.deviceInstance.Lon(newLoc.lng);
	            console.log('Updated: ', ko.toJS(self.deviceInstance))
			}
			dialog.close(self, true)
		}

		this.cancel = function () {
			dialog.close(self, false)
		}

		this.activate = function (data) {
			if (data instanceof Device) {
				console.log('Data is: ', data);
				self.deviceInstance = data;
				self.Label(data.Label.peek())
				console.log('Label is: ', self.Label.peek())
			} else throw new Error('"dlg-device-location": passed data is not Device object!')
		}

        this.compositionComplete = function () {
            // body...
            console.log('compositionComplete: map-view:::::', $('#map-view'));
            self.map = new L.Map('map-view');

            // create the tile layer with correct attribution
            var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
            var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
            var osm = new L.TileLayer(osmUrl, {minZoom: 3, maxZoom: 12, attribution: osmAttrib, subdomains: 'abc'}).addTo(self.map);

            // start the map in Sweden:
            self.map.setView(new L.LatLng(63, 17),4);
            // self.map.addLayer(osm);

            if (self.hasLocation.peek()) {
				var lat = self.deviceInstance.Lat.peek();
				var lon = self.deviceInstance.Lon.peek();
            	createMarkerObj(L.latLng(lat, lon))
            }

            self.map.on('click', handleClick);

            // Mark place if device already has location:
            // ...

        }
	}


	return ctor;

})
