
//display.js
/*
To-do: implement HardwareId for asset management and connection protection.

To-do: Client device protection - via Auth for devices. Create tokens via JS RSA lib. (see: http://kjur.github.io/jsrsasign/; https://github.com/firebase/quickstart-js/blob/master/auth/exampletokengenerator/auth.html)

To-do: own namespaces for each interface? e.g.: self.ifControllable.request()
To-do: internal "on()" - for
To-do: move ping from "control" to "echo", change ping/pong also to request/response

 */


(function (scope) {
	var thisModName = 'DisplayServer';

	function raise (message) {throw new Error(thisModName + ' ' +message)}

	// Import:
	var IControllable = scope['IControllable'] || raise('requires IControllable!');
	var ISwitchable = scope['ISwitchable'] || raise('requires ISwitchable!');
	var IPingable = scope['IPingable'] || raise('requires IPingable!');
	var IOnline = scope['IOnline'] || raise('requires IOnline!');
	var IPluggable = scope['IPluggable'] || raise('requires IPluggable!');
	var IMenu = scope['IMenu'] || raise('requires IMenu!');

	var _extend = scope.extend;

	// Export:
	scope[thisModName] = DisplayServer;


	// Перечисляем "допустимые" состояния для табло. Обрати внимание - значения "совместимы" с именами классов CSS - начинаются не с цифры и т.п. Этот массив просто для проверки что мы присваиваем "законное" состояние, предусмотренной в системе (используя [...].indexOf(state) >-1 )
	var POSSIBLE_STATES = {
		'state-trace': true, /* New device - no registration record in database*/
		'state-service-menu': true, /* Show on-screen menu*/
		'state-idle': true, /* */
		'state-ready': true,
		'state-calling': true, /*  */
		'state-customer-session': true, /*  */
		'state-maintenance': true,
		'state-error': true,
		'state-signed-off': true, /* Authentication failure */
		'state-offline': true /* Network problem - no connection */
		}


	function DisplayServer(data) {
		data = data || {}
		// Properties
		var self = {}

		// Setting for Properties:
		data.dataDirectory = data.dataDirectory || 'displays';
		data.storageKeyForDeviceId = data.storageKeyForDeviceId || 'ys-device-id';
		data.POSSIBLE_STATES = POSSIBLE_STATES,

		IPingable(self, data)
		IControllable(self, data)
		ISwitchable(self, data)
		IPluggable(self, data)
		IMenu(self, data)
		IOnline(self, data)

		// New properties:
		_extend(self, {
			'credentials': {
				'email': 'display@qms.app',
				'password': 'passwd'
			}
		})

		// New Methods:
		_extend(self, {
			'setText': function (text) {
				document.getElementById('root').innerHTML = text || ''
			}

		});


		/**
		 * To be called on event 'data changed in DB'
		 * @param  {object} data Firebase snapshot - object with .val() method
		 * @return {undefined}      None
		 */
		var handleBufferChanges = function (snapshot) {
			var timestamp = (new Date()).getTime();
			try {
				var buffer = snapshot.val();
				if (buffer === null) {
					// Device is not registered or registration removed
					console.log('CHECK DEVICE REGISTRATION')
					var key = self.getKey()
					var maskedId = key.substring(key.length-6)
					self.setText('Device<br>#'+maskedId);
					self.setState('state-trace');
				} else {
					console.log('data received', buffer)
					self.setText(buffer.content)
					self.setState(buffer.state);
					self.ACK(timestamp)
					// lastChangesTimestamp = timestamp;
				}
			} catch (e) {
				self.NACK(timestamp, e.message)
				self.setState('state-error')
			}
		}

		var commandDispatcher = function (command, args) {
			switch (command) {
				case 'TRACE':
					console.warn('commandDispatcher: command', command);
					// Use pushText, pushState, popState, popText; run TRACE only XXX seconds
					self.setText('#'+self.getKey());
					self.setState('state-trace');
					return true; // command recognized and performed
				}
		}

		// Initialize object:
		self.listenToChildNode ('buffer', handleBufferChanges, 'value')

		self.installCommandDispatcher(commandDispatcher);

		var autoSignIn = function () {
			return self.signIn(self.credentials)
		}

		// retrieveDeviceId();
			self.retrieveSession()
				.catch(autoSignIn)
				.then(function () {
					console.log('Magneto Display:  Signed!')
				})
				.catch(function (reason) {
					console.error('Auth error: ', reason)
					self.setState('state-signed-off')
				});

		return self;
	}

}(Magneto))
