(function (scope) {
	var thisModName = 'IAuthentic';

	function raise (message) {throw new Error(thisModName + ' ' +message)}

	// Import:
	var firebase = window['firebase'] || raise('requires Firebase!');

	var _extend = scope.extend;

	// Export:
	scope[thisModName] = IAuthentic;
	scope.constants['ERR_AUTH_TIMEOUT'] = ++scope.constants.lastValue

	//authentic.js
	function IAuthentic(self, data) {
		data = data || {};

		_extend(self, {
			'auth': firebase.auth()
		})

		_extend(self, {
			/**
			 * Sign In
			 * @return {Promise} Promise, with result Firebase.User
			 */
			'signIn': function (credentials) {
				return self.auth.signInWithEmailAndPassword(credentials.email, credentials.password)
			},
			'signOut': function () {
				return self.auth.signOut()
			},
			/**
			 * Add listener to the onAuthStateChanged
			 * @param  {Function} callback Handler with argument User
			 */
			'listenToAuthState': function (callback) {
				self.auth.onAuthStateChanged(callback)
			},

			/**
			 * Wait until first connection
			 * @return {[type]} [description]
			 */
			'waitConnection': function () {
				return new Promise(function (resolve, reject) {
					var cancelOnTimeout = function () {
						reject(scope.constants.ERR_AUTH_TIMEOUT)
					}
					setTimeout(cancelOnTimeout, 15000)
					self.listenToAuthState(function (user) {
						if (user) resolve(user) // <--- resolve outer promise
					})
				})
			},

			'retrieveSession': function () {
				// If already authenticated:
				if (self.auth.currentUser) Promise.resolve(self.auth.currentUser)
				return self.waitConnection()
			}
		})

	}
})(Magneto)