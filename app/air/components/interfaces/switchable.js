//switchable.js

(function (scope) {
	var thisModName = 'ISwitchable';

	function raise (message) {throw new Error(thisModName + ' ' +message)}

	// Import: (nothing)
	var _extend = scope.extend;

	// Export:
	scope[thisModName] = ISwitchable;

	/*
	Has state
	 */

	 // Private vars and functions:

	// Для хранения стейта можем обойтись без отдельной переменной - пишем напрямую в атрибут класса:
	var setAppState = function (domId, stateCode) {
		document.getElementById(domId).className = stateCode;
	}

	// И читаем оттуда же. Функция пока не используется, записана чтобы не забыть в будущем
	var getAppState = function (domId) {
		return document.getElementById(domId).className
	}

	var dispatchers = []; // <-- listens to all notifications about state changes
	var subscribers = {}; // <-- listens to specific states notifications about changes

	function ISwitchable(self, data) {
		data = data || {}


		_extend(self, {
			'domId': data.domId || 'root',
			'POSSIBLE_STATES': data.POSSIBLE_STATES || {}
		})

		_extend(self, {
			/**
			 * Apply state to device
			 * @param {string} stateCode
			 */
			'setState': function (stateCode) {
				if (self.POSSIBLE_STATES[stateCode] === void 0)
					throw new Error('Invalid state!');
				setAppState(self.domId, stateCode)

				// run dispatchers:
				for (var i=0, len=dispatchers.length; i<len; i++) {
					dispatchers[i](stateCode)
				}
				// run named subscribers
				var subsForState = subscribers[stateCode];
				if (!!subsForState) {
					for (var i=0, len=subsForState.length; i<len; i++) {
						subsForState[i]()
					}
				}
			},

			'getState': function () {
				return getAppState(self.domId)
			},

			/**
			 * Register handler to listen for a specific state. Handler will be called immediatelly when state changed.
			 * @param  {any} stateCode State constant
			 * @param  {function} handler  Callback (no aruments)
			 */
			'on': function (stateCode, handler) {
				if (!subscribers[stateCode]) {
					subscribers[stateCode] = []
				}
				if (typeof handler === 'function') {
					subscribers[stateCode].push(handler)
				} else raise('"handler" argument for ".on" method must be a function')
			},

			/**
			 * Register handler to any state change. Handler will be called immediatelly when state changed.
			 * @param  {function} handler Callback with argument 'stateCode'
			 */
			'onStateChange': function (handler) {
				if (typeof handler === 'function') {
					dispatchers.push(handler)
				} else raise('"handler" argument for ".onStateChange" method must be a function')
			}
		})

	}
})(Magneto)