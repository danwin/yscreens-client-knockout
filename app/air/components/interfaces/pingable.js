//pingable.js

(function (scope) {
	var thisModName = 'IPingable';

	function raise (message) {throw new Error(thisModName + ' ' +message)}

	// Import:
	var IPersistent = scope['IPersistent'] || raise('requires IPersistent!');

	// Export:
	scope[thisModName] = IPingable;

	// Reflects its presence in a system
	function IPingable(self, data) {
		data = data || {}

		// Implements "Persistent" interface:
		IPersistent(self, data)

		var handleEchoRequests = function (snapshot) {
			var pingValue = snapshot.val();
			console.warn('handleEchoRequests', pingValue)
			// Respond to ping if request exists:
			if (pingValue !== null) {
				// var targetRef = self.database.ref(self.instanceDataPath()).child('echo')
				// console.warn('**handleEchoRequests: ', targetRef.toString())
				// Clear ping packet:
				self.saveToChildNode('echo/ping', null)
					.catch(scope.handleException)

				// targetRef.child('ping').set(null)
				// Write response (PONG) packet with timestamp:
				self.saveToChildNode('echo/pong', +(new Date()))
					.catch(scope.handleException)
				// targetRef.child('pong').set(+(new Date()))
			}
		}

		console.log('*listenToChildNode, echo/ping')
		self.listenToChildNode('echo/ping', handleEchoRequests, 'value')

	}

})(Magneto)

// (function (root, factory) {
//     if (typeof define === "function" && define.amd) { // AMD mode
//         define(["interfaces/persistent"], factory);
//     } else if (typeof exports === "object") { // CommonJS mode
//         module.exports = factory(
//         	require("./persistent"))
//     } else {
//     	if (root.Magneto === void 0) root.Magneto = {};
//         root.Magneto['IPingable'] = factory(
//         	root.Magneto.IPersistent); // Plain JS
//     }
// }(this, function (IPersistent) {

// 	// ------ 8< -------

// 	// Reflects its presence in a system
// 	function IPingable(self, data) {
// 		data = data || {}
// 		self = self || {}

// 		// Implements "Persistent" interface:
// 		IPersistent(self, data)

// 		var handleEchoRequests = function (data) {
// 			var buffer = data.val();
// 			console.warn('handleEchoRequests', data.val())
// 			if (buffer !== null) {
// 				var targetRef = self.database.ref(self.instanceDataPath()).child('control')
// 				// Respond to ping if request exists:
// 				if (buffer.ping) {
// 					// Clear ping packet:
// 					targetRef.child('ping').set(null)
// 					// Write response (PONG) packet with timestamp:
// 					targetRef.child('pong').set(+(new Date()))
// 				}
// 			}
// 		}

// 		self.listenToChildNode('control', handleEchoRequests, eventFilter)

// 		return self;
// 	}

// 	// ------ 8< -------

// 	return IPingable;

// })(Magneto)


