// controllable.js

(function (scope) {
	var thisModName = 'IControllable';

	function raise (message) {throw new Error(thisModName + ' ' +message)}

	// Import:
	var IPersistent = scope['IPersistent'] || raise('requires IPersistent!');

	var _extend = scope.extend;

	// Export:
	scope[thisModName] = IControllable;

	/**
	 * Implements 2-way command channel over IPersisten interface
	 * @param {object} self Instance scope (defined)
	 * @param {object} data Optional values for attributes
	 */
	function IControllable(self, data) {
		data = data || {};

		// Implements interface "Authentic":
		IPersistent(self, data);

		_extend(self, {
			/**
			 * Positive response on command
			 * @param {number} timestamp UTC Time (number of milliseconds) when command is handled
			 */
			'ACK': function (timestamp, result) {
				var targetRef = self.database
					.ref(self.instanceDataPath())
					.child('control/response')
				result = result || 'Ok';
				// Clear NACK flag in db record:
				targetRef.child('NACK').set(null).catch(scope.handleException)
				// Write ACK flag to db record
				targetRef.child('ACK').set({
					'timestamp'	: timestamp,
					'result'	: result
				}).catch(scope.handleException)
			},

			/**
			 * Negative reponse.
			 * @param {string} errorMessage Сообщение об ошибке, необязателен.
			 */
			'NACK': function (timestamp, details) {
				var targetRef = self.database
					.ref(self.instanceDataPath())
					.child('control/response')
				details = details || null;
				targetRef.child('ACK').set(null).catch(scope.handleException)
				targetRef.child('NACK').set({
					'timestamp'	: timestamp,
					'details'	: details
				}).catch(scope.handleException)
			},

			'listenCommandsChannel': function (channelName, callback, eventFilter) {
				eventFilter = eventFilter || 'value';
				self.listenToChildNode (channelName, callback, eventFilter)
			},

			'installCommandDispatcher': function (commandDispatcher) {
				var dispatcher = commandDispatcher

				var callback = function (snapshot) {
					var buffer = snapshot.val();
					if (buffer) {
						var timestamp = (new Date()).getTime();
						var command = buffer.command;
						var args = buffer.args;
						if (command) {
							try {
								if (dispatcher(command, args))
									self.ACK(timestamp, {'command': command});

							} catch (e) {
								self.NACK(timestamp, {'command': command, 'error': e })
							}
						}
					}
				}

				self.listenToChildNode ('control/request', callback, 'value')
			}

		})
	}

})(Magneto)