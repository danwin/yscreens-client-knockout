// controllable.js

(function (scope) {
	var thisModName = 'IPluggable';

	function raise (message) {throw new Error(thisModName + ' ' +message)}

	// Import:
	var IPersistent = scope['IPersistent'] || raise('requires IPersistent!');

	var _extend = scope.extend;

	// Export:
	scope[thisModName] = IPluggable;

	var UUID = function () {
		// Implementation suggested: http://stackoverflow.com/a/2117523
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		    return v.toString(16);
		}).toUpperCase();
	}

	/**
	 * Implements PnP functions
	 * @param {object} self Instance scope (defined)
	 * @param {object} data Optional values for attributes
	 */
	function IPluggable(self, data) {
		data = data || {};

		// Implements interface "Authentic":
		IPersistent(self, data);


		var _deviceId = data.deviceId || null;

		// New properties
		_extend(self, {
			'storageKeyForDeviceId': 	data.storageKeyForDeviceId || 'MagnetoDevId'
			// 'deviceType': null
		})

		// New methods:
		_extend(self, {
			'getKey': function () {
				return _deviceId || localStorage.getItem(self.storageKeyForDeviceId) || self.generateDeviceId();
			},

			'generateDeviceId': function () {
				_deviceId = UUID();
				console.log('*generateDeviceId', self.storageKeyForDeviceId, _deviceId)
				localStorage.setItem(self.storageKeyForDeviceId, _deviceId)
				return _deviceId
			},

			// ??? detects "registered or not" or offer tools only? ISwitchable?
			'setAnnounce': function (data) {
				data = (data === void 0) ? +(new Date()) : data;
				self.saveToChildNode('announce', data)
					.catch(scope.handleException)
			},

			'removeAnnounce': function (data) {
				console.log('*removeAnnounce')
				self.saveToChildNode('announce', null)
					.catch(scope.handleException)
			}

		})

		// install data listener
		var observeBindingState = function (snapshot) {
			var ownRecord = snapshot.val();
			console.log('*observeBindingState', ownRecord)
			if (!ownRecord) {
				console.log('*observeBindingState', 'announce ON')
				// If recors for device does not exist - create new record with only "anounce" attribute
				self.setAnnounce()
			}
			// if (ownRecord) {
			// 	// record in the "dataDirectory/getKey()" exists
			// 	console.log('*observeBindingState', 'announce OFF')
			// 	var keysCount = 0;
			// 	for (var key in ownRecord) {
			// 		if (ownRecord.hasOwnProperty(key)) keysCount++
			// 	}
			// 	// If the announce record exists and own record contains something else instead of "announce"
			// 	if (ownRecord.announce && keysCount > 1) self.removeAnnounce()

			// } else {
			// 	// record in the "dataDirectory/getKey()" does NOT exists
			// 	console.log('*observeBindingState', 'announce ON')

			// 	self.setAnnounce()

			// }
		}

		self.listenToOwnNode(observeBindingState, 'value')

	}

})(Magneto)