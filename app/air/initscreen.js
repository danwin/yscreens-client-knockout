//initscreen.js
//

var DEBUG = false;

if (DEBUG) {
	var PLAYLISTS = {
		'test-playlist-1': {
			scene: document.getElementById('test-playlist-1').innerHTML,
			motion: [{
				'hiddenMs':0,
				'ascentMethod': 'show',
				'ascentMs': 1000,
				'visibleMs': 2000,
				'descentMethod': 'hide',
				'descentMs': 1000,
				'duration': 4000,
				'DOMselector': '#slide-1-1'
			},{
				'hiddenMs':4000,
				'ascentMethod': 'show',
				'ascentMs': 1000,
				'visibleMs': 2000,
				'descentMethod': 'hide',
				'descentMs': 1000,
				'duration': 4000,
				'DOMselector': '#slide-1-2'
			}]
		},
		'test-playlist-2': {
			scene: document.getElementById('test-playlist-2').innerHTML,
			motion: [{
				'hiddenMs':0,
				'ascentMethod': 'show',
				'ascentMs': 1000,
				'visibleMs': 2000,
				'descentMethod': 'hide',
				'descentMs': 1000,
				'duration': 4000,
				'DOMselector': '#slide-2-1'
			},{
				'hiddenMs':4000,
				'ascentMethod': 'show',
				'ascentMs': 1000,
				'visibleMs': 2000,
				'descentMethod': 'hide',
				'descentMs': 1000,
				'duration': 4000,
				'DOMselector': '#slide-2-2'
			}]
		}
	}

 	var SCHEDULE =  {
 		"start":(datetime.LocalDateTime.now().add(5000)).toFixedString(), /*"20170119T202018",*/
 		"duration":10800000,

 		"scene":"<!-- Stream: time-stream-1 (start) -->\
 			<!-- schedule-frame \"item 1\"\" start -->\
 			<section id=\"time-frame-1\" class=\"schedule-frame\"></section>\
 			<section id=\"time-frame-2\" class=\"schedule-frame\"></section>\
 			<!-- schedule-frame stop -->\
 			<!-- Stream: time-stream-1 (end) -->",

 		"motion":[
 			{"id":"time-frame-1","mediaId":"test-playlist-1","lo":0,"hi":8000},
 			{"id":"time-frame-2","mediaId":"test-playlist-2","lo":8000,"hi":16000}
 		]
 	}

	var mockContentRequestDispatcher = function (xpId) {
		// mocks "schedule" response
		return Promise.resolve({'js': PLAYLISTS[xpId]})
	}
}



(function () {

	var _cfg = MODULES.config;
	var dbResourceName = _cfg.storages.db;
	var pathRoot = dbResourceName.split('//')[1] || ''; // contains second part of resource addess: firebase-db://ys-data ---> ys-data
	var srvStorageConfig = _cfg.storages.config;

	var auth = IO.AuthFactory(srvStorageConfig)
	var _dataTsp = IO.Transport.factory(dbResourceName, {}).init(srvStorageConfig);

	// Records / renderings:
	// Both the rendered schedules and rendered playlists are in same location:
	var epRenderings = IO.Endpoint('usr/:userId/records');

	// Playback runtime:
	var domID = 'display';

	var playbackEngine;

	// Device:
	var deviceInstance = null;

	// Constant values can be any, only to distinct them:
	var LAST_CONSTANT = 0,
		NEEDS_CONFIGURATION = ++LAST_CONSTANT,
		NO_BINDING = ++LAST_CONSTANT,
		NO_CHANNEL_ASSIGNED = ++LAST_CONSTANT,
		NO_SCHEDULE_IN_CHANNEL = ++LAST_CONSTANT,
		RECORD_NOT_A_SCHEDULE = ++LAST_CONSTANT,
		RECORD_NOT_A_PLAYLIST = ++LAST_CONSTANT;

	// PHASE N: Retrieve userId, compile rqOptions if possible...
	var initDevice = function () {
		var rqOptions = {}
		console.log('USER DATA:', auth.user)
		var userId = localStorage.getItem('ys-uid');
		if (userId) {
			rqOptions = {'pathArgs': {'userId': userId}}
		} else {
			// try to extract user data from auth object
			rqOptions = auth.applyCredentials({})
			if (rqOptions.pathArgs.userEmail === 'player@device.service')
				Promise.reject(NEEDS_CONFIGURATION)
			userId = rqOptions.pathArgs.userId;
			localStorage.setItem('ys-uid', userId)
		}
		console.log(Magneto)
		deviceInstance = Magneto.DisplayServer({
			'dataDirectory': pathRoot + '/usr/'+userId+'/magneto-network/displays',
			'credentials': {
				'email': 'player@device.service',
				'password': 'playback-password'
			}
		})
		console.log(deviceInstance, Magneto)
		// Returns promise which waits for display connection and resolves promise with result "rqOptions":
		return deviceInstance.waitConnection().then(function () {
			console.log('USER DATA: after waitConnection', auth.user)
			return rqOptions
		})
	}

	// PHASE n + 1. We are online now
	var retrieveChannelKey = function (rqOptions) {
		var userId = rqOptions.pathArgs.userId;
		// 1. Load device record
		var epDevices = IO.Endpoint('usr/:userId/magneto-network/displays/'+deviceInstance.getKey())
		var devSrv = IO.Service(epDevices, _dataTsp);
		return devSrv.read(rqOptions)
			.then(function (deviceRec) {
				if (!deviceRec) return Promise.reject(NO_BINDING)
				var ysData = deviceRec.ysData;
				if (ysData && ysData.ChannelID) {
					// Return data for subsequent requests:
					/*'ysData':{'StorageID':StorageID, 'ChannelID': ChannelID*/
					return Promise.resolve({
						'ChannelID': ysData.ChannelID,
						'rqOptions': rqOptions
					})
				}
				// No ChannelID in display record
				return Promise.reject(NO_CHANNEL_ASSIGNED)
			})
	}

	var retrieveScheduleKey = function (result) {
		// Channel.ScheduleInfo.StorageID
		var ChannelID = result.ChannelID;
		var rqOptions = result.rqOptions;
		var epChannel = IO.Endpoint('usr/:userId/channels');
		return IO.Service(epChannel, _dataTsp).child(ChannelID).read(rqOptions)
			.then(function (channelData) {
				if (channelData.ScheduleInfo && channelData.ScheduleInfo.StorageID) {
					return Promise.resolve({
						'ScheduleRenderingID': channelData.ScheduleInfo.StorageID,
						'rqOptions': rqOptions
					})
				}
				return Promise.reject(NO_SCHEDULE_IN_CHANNEL)
			})
	}

	/**
	 * Request schedule content
	 * @param  {[type]} schId [description]
	 * @return {[type]}       [description]
	 */
	var retrieveScheduleContent = function (result) {
		var ScheduleRenderingID = result.ScheduleRenderingID;
		var rqOptions = result.rqOptions;
		var serviceRenderings = IO.Service(epRenderings, _dataTsp);

		return serviceRenderings.child(ScheduleRenderingID)
			.read(rqOptions)
			.then(function (response) {
				console.log('data retrieved:', response);
				if (response.SourceEntity === 'Schedules')
					return Promise.resolve({
						'ScheduleScript': response["js"],
						'rqOptions': rqOptions
					})
				return Promise.reject(RECORD_NOT_A_SCHEDULE)
			})
	}

	var mockRetrieveScheduleContent = function (result) {
		return Promise.resolve({
			'ScheduleScript': SCHEDULE,
			'rqOptions': result.rqOptions
		})
	}

	// PHASE: Resolve playback data
	var retrieveProgrammeRendering = function (result) {
		var ScheduleScript = result.ScheduleScript;
		var rqOptions = result.rqOptions;
		var serviceRenderings = IO.Service(epRenderings, _dataTsp);

		var playlistRequestDispatcher = function (xpId) {
			return serviceRenderings.child(xpId)
				.read(rqOptions)
				.then(function (response) {
					console.log('data retrieved:', response);
					if (response.SourceEntity === 'Playlists') {
						return response;
					} else {
						return Promise.reject(RECORD_NOT_A_PLAYLIST)
					}
				})
		}
		playbackEngine = new window.rtl_orchestrator(domID, {
			'contentRequestDispatcher': (DEBUG) ? mockContentRequestDispatcher : playlistRequestDispatcher
		});

		return playbackEngine.fromJS(ScheduleScript)
	}

	var mockRetrieveProgrammeRendering = function (result) {
		var mockContentRequestDispatcher = function (xpId) {
			return Promise.resolve(PLAYLISTS[xpId])
		}
		playbackEngine = new window.rtl_orchestrator(domID, {
			'contentRequestDispatcher':  mockContentRequestDispatcher
		});

		return playbackEngine.fromJS(ScheduleScript)
	}


	var handleAnimations = function () {
		return playbackEngine.observeTasksList();
	}


	// Init Firebase
	IO.initFirebaseApp(srvStorageConfig)

	if (DEBUG) {
		retrieveScheduleContent = mockRetrieveScheduleContent;
		retrieveProgrammeRendering = mockRetrieveProgrammeRendering;
	}
	__rtl_preloader__
		.then(initDevice)
		// initDevice()
		.then(retrieveChannelKey)
		.then(retrieveScheduleKey)
		.then(retrieveScheduleContent)
		.then(retrieveProgrammeRendering)
		.then(handleAnimations)
		.catch(function (reason) {
			var errMessage;
			switch (reason) {
				// GO THROUGH CONSTANTS
				case Magneto.constants.ERR_AUTH_TIMEOUT:
					errMessage = 'Cannot link to server'
					break
				case NO_BINDING:
					errMessage = 'Device binding not establihed'
					break
				case NEEDS_CONFIGURATION:
					errMessage = 'Device not configured'
					break
				case NO_CHANNEL_ASSIGNED:
					errMessage = 'No channel selected'
					break
				case NO_SCHEDULE_IN_CHANNEL:
					errMessage = 'Channel is empty'
					break
				case RECORD_NOT_A_SCHEDULE:
					errMessage = 'Error in schedule record'
					break
				case RECORD_NOT_A_PLAYLIST:
					errMessage = 'Error in playist record'
					break
				default:
					errMessage = 'Unknown error'
			}
			// if (deviceInstance) {
			// 	deviceInstance.setText(errMessage)
			// 	deviceInstance.setState('state-error')
			// }
			console.error('Promise rejected: ', errMessage, reason)
			// To-do schedule periodical polling to resolve new settings if any
		})

	// MENU

/*
			<section id="menu-page-main">
				<p><button onclick="maintenanceMode()">SignIn for setup...</button></p>
				<p>Press Esc to close menu...</p>
			</section>

			<section id="menu-page-user-mode" style="display: none;">
				<p><button onclick="storeValues()">Auto-tune</button></p>
				<p><button onclick="resetToFactory()">Reset to factory</button></p>
				<p><button onclick="restart()">Restart</button></p>
			</section>

			<section id="menu-page-auth" style="display: none">
				<p>User</p>
				<p><input type="text" name="usrname"></p>
				<p>Password</p>
				<p><input type="password" name="pwd"></p>
				<p><button onclick="signInAsUser()">Sign In</button></p>
			</section>

			<section id="menu-page-alert" style="display: none">
				<p id="alert-message">...</p>
				<p><button onclick="goBack()">Back</button></p>
			</section>
 */

	var menuWindow = document.getElementById('menu')
	var menuPageMain = document.getElementById('menu-page-main')
	var menuPageAuth = document.getElementById('menu-page-auth')
	var menuPageUserMode = document.getElementById('menu-page-user-mode')
	var menuPageAlert = document.getElementById('menu-page-alert')
	var pages = [menuPageMain, menuPageAuth, menuPageUserMode, menuPageAlert]

	window.showPage = function (element) {
		pages.forEach(hidePage);
		element.style.display = ''
	}

	window.goBack = function () {
		showPage(menuPageMain)
	}

	window.hidePage = function (element) {
		element.style.display = 'none'
	}

	window.resetToFactory = function () {
		localStorage.setItem('ys-uid', null)
	}

	window.maintenanceMode = function () {
		showPage(menuPageAuth)
	}


	window.signInAsUser = function () {
		var usrname = document.getElementById('usrname').value
		var pwd = document.getElementById('pwd').value
		deviceInstance.signOut().then(function () {
			return deviceInstance.signIn({email:usrname, password:pwd})
				.then(function () {
					showPage(menuPageUserMode)
				})
				.catch(function (reason) {
					menuAlert('Auth error', reason)
				})
		})
	}

	window.storeValues = function () {
		var rqOptions = auth.applyCredentials({})
		if (rqOptions.pathArgs.userEmail === 'player@device.service') {
			alert('Cannot Auto-tune with this account')
			return
		}
		var userId = rqOptions.pathArgs.userId;
		localStorage.setItem('ys-uid', userId)
	}

	window.restart = function () {
		auth.signOut().then(function () {
			document.location.reload()
		})
	}

//****************************************


	// var userId = _.getQueryVariable('usr');
	// // var exposureId = 'a0gshb39pqp58fdgqvypxj';


	// // Both the rendered schedules and rendered playlists are in same location:
	// var epRenderings = IO.Endpoint('usr/:userId/records');
	// var serviceRenderings = IO.Service(epRenderings, _dataTsp);


}())