﻿requirejs.config({

    urlArgs: "bust=" +  (new Date()).getTime(), // debug
    //~ urlArgs: "version=1.2.3", // production

    // baseUrl: '../vendor',

    waitSeconds: 15, // timeout settings

    paths: {

        // 'lib':          '../vendor',

        // 'app':          '../app',
        // 'shared':       '../app/shared',
        'Q':            '../vendor/q', // Kris Kowal promises lib
        'json2' :       '../vendor/json2',

        'jquery':       '../vendor/jquery-1.11.3.min',
        'jquery.ui':        '../vendor/jquery-ui/jquery-ui.1.11.4.interactions.min',

        // folder, not js (for ko.mapping(!)):
        'jquery-ui':        '../vendor/jquery-ui',

        // aliases for jquery-fileupload:
        'jquery-fileupload':  '../vendor/jquery-fileupload/js',
        'jquery.ui.widget': '../vendor/jquery-fileupload/js/vendor/jquery.ui.widget',

        'jquery.inputmask': '../vendor/jquery.inputmask.bundle',
        'jquery.autocomplete': '../vendor/jquery.autocomplete',
        // groups jquery wit a minimal plugins (ntfbus)
        // do not include jquery-ui because it is not always necessary
        'jquery.all':       'shared/jquery-loader',

        'knockout':         '../vendor/knockout-3.3.0',
        'knockout.mapping':     '../vendor/knockout.mapping.2.4.1',

        'knockout.sortable':    '../vendor/knockout-sortable',
        // mapping for knockout-sortanle:
        // 
        // 'jquery-ui/ui/widgets/sortable': '../vendor/jquery-ui/sortable',
        // 'jquery-ui/ui/widgets/draggable': '../vendor/jquery-ui/draggable',
        // 'jquery-ui/ui/widgets/core': '../vendor/jquery-ui/core',
        // 'jquery-ui/ui/widgets/mouse': '../vendor/jquery-ui/mouse',
        // 'jquery-ui/ui/widgets/widget': '../vendor/jquery-ui/widget',
        // 'jquery-ui/core': '../vendor/jquery-ui/core',
        // 'jquery-ui/mouse': '../vendor/jquery-ui/mouse',
        // 'jquery-ui/widget': '../vendor/jquery-ui/widget',



        // this module groups knockout.kernel', 'knockout.mapping', preloads 'jquery':
        'knockout.all':         'shared/knockout-loader',

        'underscore':       '../vendor/underscore-min',
        'underscore.all':   'shared/underscore-loader',
        
        'bootstrap':        '../vendor/bootstrap/js/bootstrap.min',
        'amplify':        '../vendor/amplify-1.1.2/amplify',
        'leaflet':         '../vendor/leaflet/js/leaflet',
        'chart':          '../vendor/chartjs/js/Chart.min',
        'knockout.chart':   '../vendor/knockout.chart',

        'firebase':         '../vendor/firebase/firebase',

        'd3js':             '../vendor/d3/d3.min.js',

        'message.bus':  'shared/message.bus',

        'nouislider':   '../vendor/nouislider/nouislider.min',
        // 'vendor.timetable':   '../vendor/timetable/timetable.min',

        'datetime': 'shared/datetime',
        'iterator': 'shared/iterator',
        // 'templates':        '../templates'
        // 
        'html5-simple-date-input-polyfill': '../vendor/html5-simple-date-input-polyfill/html5-simple-date-input-polyfill',

        // DURANDAL:
        'text': '../vendor/text',
        'durandal':'../vendor/durandal/js',
        'plugins' : '../vendor/durandal/js/plugins',
        'transitions' : '../vendor/durandal/js/transitions'

        // appViewModel: './module' // local module with appViewModel
    },
    packages: [{
        name: 'moment',
        // This location is relative to baseUrl. Choose bower_components
        // or node_modules, depending on how moment was installed.
        location: 'vendor/moment',
        main: 'moment'
    }],
    shim: { // bootstrap not AMD-compatible, so 'shim' is necessary
        'knockout': {
            deps: ['jquery'] // see: http://blog.scottlogic.com/2014/02/28/developing-large-scale-knockoutjs-applications.html
            ,exports: 'ko'
        },
        'knockout.mapping': {
            deps: ['json2', 'knockout']  // <- way to inject "json2" script
        },
        // 'knockout.sortable': {
        //     deps: ['knockout','jquery.ui']
        // },
        'bootstrap': {
            deps: ['jquery'], // dependency of jquery
            exports: 'jQuery'
        },
        'jquery.ui': {
            deps: ['jquery']
        },
        'amplify': {
            deps: ['jquery'],
            exports: 'amplify'
        },
        'vendor.timetable': {
            deps: [],
            exports: 'Timetable'
        }

    }
    //,
    
    // //~ // global dependencies
    // deps: ['jquery', 'knockout', 'knockout.mapping', 'jqueryui', 'bootstrap'],
    
    // //~ // callback
    // callback: function($, ko, mapping){
    //  ko.mapping = mapping;
    // }
});


// Custom addition to setup "console.log", "_TRACE_", etc.,
function setupConsole(config){
    // Some ides from: http://tobyho.com/2012/07/27/taking-over-console-log/
    var console = window.console;

    if (window._TRACE_) 
        throw "'window._TRACE_' already defined!";
    window._TRACE_ = console ?
        function () {console.log.apply(console, arguments);}
        :
        _.noop;

    if (window._ERR_) 
        throw "'window._ERR_' already defined!";
    window._ERR_ = function () {
            throw Array.prototype.slice.apply(arguments).join(' ')
        };

    if (!console) {
        window.console = {
            'log': _.noop, 
            'warn': _.noop, 
            'error': _.noop
        };
        return;
    }
    function intercept(method){
        var original = console[method]
        console[method] = function(){
            // do sneaky stuff
            if (original.apply){
                // Do this for normal browsers
                original.apply(console, arguments)
            }else{
                // Do this for IE
                var message = Array.prototype.slice.apply(arguments).join(' ')
                original(message)
            }
        }
    }
    var methods = ['log', 'warn', 'error']
    for (var i = 0; i < methods.length; i++)
        intercept(methods[i])
}

// var setupConsole = function () {}


define([
        'durandal/system', 
        'durandal/app', 
        'durandal/viewLocator',
        'plugins/dialog', 
        'shared/config', 
        'components/custom-dialog',
        'viewmodels/io-config',
        'shared/form.manage',
        //----
        'bootstrap',
        'html5-simple-date-input-polyfill',
        'datetime',
        'shared/input-time-interval',
        ],  
    function (
        system, 
        app, 
        viewLocator, 
        dialog,
        config, 
        customDialog, 
        IO,
        formManager) {
    //>>excludeStart("build", true);
    system.debug(true);
    //>>excludeEnd("build");

    // *** Allow inspection for debugging: ***
    if (system.debug()) window.__durandal_app__ == app;

    // Move to module (?):
    // setupConsole(config);

    // Setup IO and Auth as soon as possible:
    // Set common IO facilities:
    app.IO = IO;

    // Shortcut - "Global" authentication instance:
    app.auth = IO.gw.sessions.default;
    // Relay notification for all subscribers:
    app.auth.onAuthStateChanged(function (user) {
        console.log('main.js: firing auth:stateChanged');
        app.trigger('auth:stateChanged', user);
        if (user) app.trigger('auth:stateChanged:userDefined', user)
    });
    // Set "global" options for requests:
    app.rqOptions = {
        'auth': app.auth
    };

    // Tools to manage jQuery/Bootstrap forms:
    app.formManager = formManager;

    // Global customer-specific details
    app.globals = {}
    app.globals.contacts = {
        supportEmail: 'support@ys.com',
        adminEmail: 'admin@ys.com',
        supportPhone: '+00000000',
        adminPhone: '+00000000'
    }

    app.title = 'yScreens';

    app.configurePlugins({
        router: true,
        dialog: true,
        widget: {
            // register widget from folder "widgets/mapview"
            'kinds': ['map-widget', 'piechart-widget']
        }
    });

    // add custom dialog as app "plugin":
    app.customDialog = customDialog;

    app.showDialogEx = function(obj, activationData, context) {
        console.log('Running patched dialog!!!')
        return new Promise(function (resolve, reject) {
            try {
                obj.Ok = function () {
                    dialog.close(obj, true);
                    return false;
                }
                obj.Cancel = function () {
                    dialog.close(obj, false);
                    return false;
                }

                dialog.show(obj, activationData, context)
                    .then(function (dialogResult) {
                        resolve(dialogResult)
                    })
            } catch (e) {
                reject(e)
            }
        })
    }

    app.showMessageEx = function(obj, activationData, context) {
        console.log('Running patched message!!!')
        return new Promise(function (resolve, reject) {
            try {
                dialog.showMessage(message, title, options)
                    .then(function (dialogResult) {
                        resolve(dialogResult)
                    })
            } catch (e) {
                reject(e)
            }
        })
    }

    // // Replace jQuery "promises" which default for Durandal with "Q"
    // system.defer = function (action) {
    //     var deferred = Q.defer();
    //     action.call(deferred, deferred);
    //     var promise = deferred.promise;
    //     deferred.promise = function() {
    //       return promise;
    //     };
    //     return deferred;
    // };

    // Common handler for exceptions (returns Durandal promise):
    app.handleException = function (e) {
        // By default: logs error;
        // to-do: pop-up dialog, +error monitoring?
        
        // e can be: { "error": { "code": 404, "message": "Not Found. Could not delete object" } }
        e = e || {};
        app.trace.log('app.handleException:', e);
        app.showMessage(e.message || e.toString(), 'Error'); // <-- allow to pass method in promise handlers
        if (system.debug()) throw e;
    }

    // Emulate "console.log" for debugging
    app.trace = {
        log: console.log,
        warn: console.warn,
        error: console.error
    }

    // CUSTOM CODE (END)

    app.start().then(function() {
        //Replace 'viewmodels' in the moduleId with 'views' to locate the view.
        //Look for partial views in a 'views' folder in the root.
        viewLocator.useConvention();

        //Show the app by setting the root view model for our application with a transition.
        app.setRoot( 'viewmodels/shell', 'entrance');
    });

});