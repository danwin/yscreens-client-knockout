// custom-dialog.js
define(['plugins/dialog', 'durandal/app', 'knockout'], function (dialog, app, ko) {

	if (typeof Promise === 'undefined') {
		new Error('Your browser does not support Promise API. Polyfill is necessary!');
	} else {
		var _promise = function (action) {
			// action is a function with 2 args: resolve, reject
			return new Promise(action);
		}
	}

	function assertDefined (value, name) {
		if (typeof value == 'undefined')
			throw new Error('Requied argument "' +name+ '" is "undefined" in "customDialog" options!');
		return value;
	}

	function CustomDialog (options) {
		// body...
		var
			self = this,
			customStyle = (typeof options.cssClass == 'string') ? options.cssClass : null,
			sourceModel = assertDefined(options.buffer, 'buffer'),
			attrFilter = assertDefined(options.attrFilter, 'attrFilter'),
			constructorFunc = options.constructorFunc,

			changesInfo = {
				hasChanges : false,
				changed : {},
				subscriptions: []
			},
			// flag if at least one property was changed:
			// common listener to changes:

			// changesRouter = function (attrName) {
			// 	changesInfo.changed[attrName] = true;
			// 	changesInfo.hasChanges = true;
			// 	console.log('changed ', attrName);
			// },
			// fake model to detect only observable properties:

			// referenceModel = attrFilter({}, sourceModel),
			// referenceModel = attrFilter({}, {}),

			// tmp vars:
			referenceModel, attr, listener, instance;

		console.log('====0')

		referenceModel = attrFilter({}, {}),

		console.log('====01')

		if (referenceModel === void 0) throw new Error('"referenceModel" is not defined: check the validity of attrFilter function - it must return reference to the model scope.')

		// *** PRIVATE ***

		var copyChangedValues = function () {
			// copy only properties which are changed:
			for (var prop in changesInfo.changed) {
				sourceModel[prop](self[prop].peek());
				console.log('copying changed value: ', prop);
			}
		}

		// call on destruction:
		var disposeAll = function () {
			// clear subscriptions:
			while (changesInfo.subscriptions.length > 0) {
				changesInfo.subscriptions.pop().dispose();
			}
			if (customStyle) {
				$('.dialog-modal').removeClass(customStyle)
			}
		}

		// *** PUBLIC ***

		self.viewUrl = assertDefined(options.viewUrl, 'viewUrl');
		self.lookups = options.lookups || {};

		self.handleButton = function (caption) {
			dialog.close(self, caption);
			app.trigger('customDialog:'+caption, {'view': options.viewUrl, 'model': self});
		}
		self.Ok = function () {
			dialog.close(self, 'Ok');
			app.trigger('customDialog:Ok', {'view': options.viewUrl, 'model': self});
			return false;
		}
		self.Cancel = function () {
			dialog.close(self, 'Cancel');
			app.trigger('customDialog:Cancel', {'view': options.viewUrl, 'model': self});
			return false;
		}

		var _toPromise = function (action) {
			return _promise(function (resolve, reject) {
				var response;
				try {
					instance.then(function (response) {
						resolve(action(response))
					})
					// response = instance.then(action);
					// resolve(response)
				} catch (e) {
					reject(e)
				}
			})
		}

		// *** main methods to dispatch dialog:
		// self.waitConfirm = function (changedObservable) {
		// 	var wrapper = function (result) {
		// 		console.log('listeners after: ', ko.toJS(changesInfo));
		// 		console.log('changedObservable >>> ', ko.toJS(changedObservable));
		// 		try {
		// 			if (result === 'Ok' && changesInfo.hasChanges) {
		// 				copyChangedValues();
		// 				// mark changed flag or call "on confirm" handler function
		// 				if (changedObservable) {changedObservable(true)}
		// 			}
		// 		} finally {
		// 			disposeAll();
		// 		}
		// 		return true;
		// 	}

		// 	// // method handles "Ok" press only
		// 	// var r = _bindCatch(instance.then(wrapper));
		// 	// console.log('r->',r)
		// 	// return r;
		// 	return _toPromise(wrapper)
		// }


		self.waitConfirm = function (changedObservable) {
			return new Promise(function (resolve, reject) {
				instance.then(function (modalResult) {
					try {
						if (modalResult === 'Ok') {
							if (changesInfo.hasChanges) {
								copyChangedValues();
								if (typeof changedObservable === 'function')
									changedObservable(true)
							}
							resolve(true)
						} else resolve(false)
					} catch (e) {
						reject(e)
					}
					disposeAll();
				})
			})
		}

		// self.then = function (handler) {
		// 	// as "traditional" promise interface
		// 	var wrapper = function (result) {
		// 		try {
		// 			handler(result)
		// 		} finally {
		// 			disposeAll();
		// 		}
		// 	}
		// 	return _toPromise(wrapper)
		// }

		self.then = function (handler) {
			return new Promise(function (resolve, reject) {
				instance.then(function (modalResult) {
					resolve(modalResult)
					disposeAll();
				})
			});
		}

		// intercept when dialog DOM is ready:

		self.compositionComplete = function () {
			// notification: "show"
			app.trigger('customDialog:show', {'view': options.viewUrl, 'model': self});
			app.trigger('customDialog:compositionComplete');
		}

		self.deactivate = function () {
			app.trigger('customDialog:deactivate');
		}

		// *** initialization ***

		console.log('====1')

		// Instantiate "filtered" fields and values in new instance: (NOTE: call constructor without NEW - insert properties directly in current namespace):
		constructorFunc ? constructorFunc.call(self, ko.toJS(sourceModel)) :
		attrFilter(self, ko.toJS(sourceModel));
		// attrFilter(self, sourceModel);
		console.log('====2')

		// Connect listeners:
		function newListener (attrName) {
			return function () {
				changesInfo.changed[attrName] = true;
				changesInfo.hasChanges = true;
				console.log('changed ', attrName);
			};
		}

		console.log('referenceModel', ko.toJS(referenceModel))
		for (var prop in referenceModel) {
			attr = referenceModel[prop];
			if (referenceModel.hasOwnProperty(prop) && ko.isObservable(attr)) {
				// listener = function () { changesRouter(prop); }
				// subscripe listener to the same own property, store reference:
				changesInfo.subscriptions.push(this[prop].subscribe(newListener(prop)));
				console.log('listening to: ', prop);
				console.log('current value: ', this[prop]());
			}
		}

		console.log('listeners: ', ko.toJS(changesInfo));

		// instantiate dialog window and show it:
		instance = app.showDialog(self);

		// customize css, including dimensions"
		if (customStyle) {
			$('.dialog-modal').addClass(customStyle)
		}

	}

	/**
	 * Executes a custom dialog to view/edit a ViewModel.
	 * Template based on html file (url passed in options).
	 * Use bindins for buttons inside template: data-bind="click:Ok", or "Cancel".
	 *
	 * Recommended usage: install this function into "app" namespace like plugin (usually in "main.js").
	 *
	 * @function customDialog
	 * @param {object} options
	 * @param {object} options.buffer View model to pass and to receive results.
	 * @param {object} attrFilter Implements a "factory" function to instantiate only a persistent subset of buffer properties (without computed props, notifications, etc.). Called with arguments: ({}, ViewModel).
	 * @param {string} options.baseUrl The base url to load the view ("html") from, for ex.: 'views/template1'
	 * @param {string} [options.lookups] Optional "coupled" models. Use weak references like ID.
	 * @param {string} [options.cssClass] Optional name of custom CSS class to customize dialog appearance.
	 * Handling of user buttons - use a special handler in data-bind="click:handleButton('MyButton')"
	 */

	// var customDialog = function (options) {
	// 	// arguments: {buffer: ViewModel, attrFilter:StructModel, viewUrl:'', lookups:{}}
	// 	// a sample of StructModel see, e.g., in model-assets.js
	// 	var
	// 		buffer = assertDefined(options.buffer, 'buffer'),
	// 		attrFilter = assertDefined(options.attrFilter, 'attrFilter'),
	// 		viewUrl = assertDefined(options.viewUrl, 'viewUrl'),
	// 		dialogModel = {},
	// 		changesInfo = {
	// 			listenerFuncs : [],
	// 			changed : {}
	// 		},
	// 		attr,
	// 		listener,
	// 		instance,
	// 		caption;

	// 	// Instantiate "filtered" fields and values in new instance:
	// 	dialogModel = attrFilter({}, buffer);

	// 	var changesRouter = function (attrName) {
	// 		dialogModel.changesInfo.changed[attrName] = true;
	// 	}

	// 	// Connect listeners:
	// 	for (var prop in dialogModel) {
	// 		attr = dialogModel[prop];
	// 		if (dialogModel.hasOwnProperty(prop) && ko.utils.isObservable(attr)) {
	// 			listener = function (argument) {
	// 				changesRouter(prop);
	// 			}
	// 			changesInfo.listenerFuncs.push(listener);
	// 			attr.subscribe(listener);
	// 		}
	// 	}

	// 	dialogModel.changesInfo = changesInfo;

	// 	dialogModel.handleButton = function (caption) {
	// 		dialog.close(dialogModel, caption);
	// 	}

	// 	dialogModel.viewUrl = viewUrl;
	// 	dialogModel.lookups = options.lookups || {};

	// 	dialogModel.Ok = function () {
	// 		dialog.close(dialogModel, 'Ok');
	// 	}
	// 	dialogModel.Cancel = function () {
	// 		dialog.close(dialogModel, 'Cancel');
	// 	}

	// 	instance = app.showDialog(dialogModel);

	// 	return {
	// 		waitConfirm : function (changedObservable) {
	// 			// method handles "Ok" press only
	// 			var wrapper = function (result) {
	// 				if (result === 'Ok') {
	// 					buffer(attrFilter({}, dialogModel));
	// 					// mark changed flag or call "on confirm" handler function
	// 					if (changedObservable) changedObservable(true);
	// 				}
	// 			}
	// 			instance.then(wrapper);
	// 		},
	// 		then : function (handler) {
	// 			// as "traditional" promise interface
	// 			instance.then(handler);
	// 		}
	// 	}
	// };

	var customDialog = function (options) {

		return new CustomDialog(options);

	}

	return customDialog;
});