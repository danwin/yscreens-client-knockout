// Source: https://github.com/liorwohl/html5-simple-date-input-polyfill
// Changes: UMD wrapper for module, 2) automatic updates for calendar when input changed programmatically; 3) default value - "today" unless "value" attribute missed;

(function(root, factory) {
    if (typeof define === "function" && define.amd) { // AMD mode
      define([], factory);
    } else if (typeof exports === "object") { // CommonJS mode
      module.exports = factory();
    } else {
      factory(); // Plain JS
    }

}(this, function() {

  // use isNaN polyfill
  !(typeof Number.isNaN == 'function') ||
    (Number.isNaN = function (value) {
      return value !== null // Number(null) => 0
        && (value != value // NaN != NaN
          || +value != value // Number(falsy) => 0 && falsy == 0...
        )
  });

  var _2digits = function (value, label) {
    return (value < 10) ? '0' + value : value.toString()
  }

  var renderDateStr = function (dateObj, hasDateEditor, hasTimeEditor) {
    var result = [];
    //~~~console.log('...rendering...', dateObj)
    if (hasDateEditor) {
      result.push([
          dateObj.getFullYear(),
          _2digits(dateObj.getMonth() + 1, 'month'),
          _2digits(dateObj.getDate(), 'date')
        ].join('-')
      )
    }
    if (hasTimeEditor) {
      result.push([
          _2digits(dateObj.getHours(), 'hours'),
          _2digits(dateObj.getMinutes(), 'minutes'),
        ].join(':')
      )
    }
    //~//~~~console.log(result)
    return result.join(' ');
  }

  var timeMask = /(\d{1,2}):(\d{1,2})(?:(\:\d{1,2}(?:(\.\d{1,3})*))*)/gi

  /**
   * Decodes time-only strings like "00:00", "00:00:00", "00:00:00.000"
   * (Date.parse usually fails on such strings)
   * @param  {string} s See above
   * @return {number}   Milliseconds
   */
  var timeStrToMilliseconds = function (s) {
    var h, m, sec, time;
    time = s.match(timeMask);
    ////~~~console.log('time is:', time);
    if (time && time[0]) {
      time = time[0].split(':');
      time.reverse();
      h = time.pop();
      h = (h) ? parseInt(h) : 0;
      // //~~~console.log('h', h)
      m = time.pop();
      m = (m) ? parseInt(m) : 0;
      // //~~~console.log('m', m)
      sec = time.pop();
      sec = (sec) ? parseFloat(sec) : 0;
      // //~~~console.log('sec', sec)
      return h * 3600000 + m * 60000 + sec * 1000
    }
    return NaN;
  }

  var _tzOffset = (new Date()).getTimezoneOffset() * 60000;

  var TimestampFactory = function (timeStr) {
    var
      ms = timeStrToMilliseconds(timeStr),
      date = new Date(ms + _tzOffset); // <-- Date constructor uses UTC interval in milliseconds
    //~~~console.log('TimestampFactory !!!', timeStr, date);
    return date; // <- ignore year, month, date - use only a time fragment from this object
  }

  // string Id of the current visible editorsContainer;
  var _activeSId = null;
  var _activeExtender = null;

  var hideOpened = function (e) {
    var editorsContainer, sId;
    e = e ? e : window.event;
    //~//~~~console.log('GLOBAL blur', e.target, e.relatedTarget)
    if (e.target && /*e.relatedTarget &&*/ e.target.getAttribute('data-disable-autoclose') !== _activeSId /*e.relatedTarget.getAttribute('data-disable-autoclose')*/) {
      //~//~~~console.log(e.target.getAttribute('data-disable-autoclose'), e.relatedTarget.getAttribute('data-disable-autoclose'))
      // sId = e.target.getAttribute('data-disable-autoclose');
      sId = _activeSId;
      editorsContainer = document.getElementById('datetime-editor-group-'+sId);
      if (editorsContainer) {
        //~~~console.log('!editor found, closing:', editorsContainer)
        editorsContainer.style.display = 'none';

        _activeExtender.fromBufferToInputValue /*selectDate*/()
        _activeExtender.updateOutputProperty()

        _activeSId = null
        _activeExtender = null
      }
    }
    return false;
  }

  function calanderExtender (theInput) {

    var self = this;

    // Set or update last Id (class-wide value)
    calanderExtender.lastId = calanderExtender.lastId ? (++calanderExtender.lastId) : 1;

    // Own value to create unique Id's for elements
    this.sId = calanderExtender.lastId.toString();

    this.theInput = theInput;

    this.theInput.setAttribute('data-datetime-sid', self.sId);
    this.theInput.setAttribute('data-disable-autoclose', self.sId);
    this.container = null;
    this.theDateEditorDiv = null;

    // Choose editors by  input[type] check
    var inputType = this.theInput.getAttribute('data-x-type') || this.theInput.getAttribute('type'); //<- warning: for browsers which have no support for input[type=date] the value element.type will be "text"! but the attribute value for 'type' will be as specified!
    //~//~~~console.log(inputType)
    this.hasDateEditor = ('date,datetime,datetime-local'.split(',').indexOf(inputType) >-1);
    this.hasTimeEditor = ('datetime,datetime-local,time'.split(',').indexOf(inputType) >-1);

    // Set "now":
    this.selectedDate = new Date();

    this.init = function () {
      if (!this.updateBufferFromInput()) {
        // this.selectedDate.setTime(new Date().getTime());
        this.updateOutputProperty();
        this.theInput.value = renderDateStr(this.selectedDate, this.hasDateEditor, this.hasTimeEditor)
      }
      this.createCal();
    };

    // set input.valueAsDate:
    this.updateOutputProperty = function () {
        this.theInput.valueAsDate = this.selectedDate;
    }

    //update selectedDate with the date from the input, return true if changed
    this.updateBufferFromInput = function () {
      if (this.theInput.value) {
        var possibleNewDate = (this.hasDateEditor) ?
          new Date(this.theInput.value)
          :
          TimestampFactory(this.theInput.value);
        var numValue = possibleNewDate.getTime();

        //~~~console.log('this.updateBufferFromInput: detected new value', possibleNewDate, this.theInput.value)
        // if (Date.parse(this.theInput.value) && possibleNewDate.toDateString() !== this.selectedDate.toDateString()) {
        if (!Number.isNaN(numValue) && numValue !== this.selectedDate.getTime()) {
          this.selectedDate = possibleNewDate;
          return true;
        }
      }
      //~~~console.log('this.updateBufferFromInput: NO new value', this.theInput.value)
      return false;
    };

    //update the calendar and/or time if the date changed manually in the input
    this.updateControls = function () {
      if (self.updateBufferFromInput()) {
        self.updateOutputProperty();
        if (self.hasDateEditor) self.updateCalendarSelecteds();
        if (self.hasTimeEditor) self.updateTimeDisplay();
        //~~~console.log('this.updateControls: ', self.selectedDate)
      }
      //~~~console.log('---this.updateControls:--- ', self.selectedDate)
    }

    this.incrementTime = function (timeUnits, increment) {
      // var date = new Date(self.selectedDate.getTime()), value;
      var date = self.selectedDate, value;
      if (timeUnits === 'hours') {
        value = date.getHours() + increment;
        if (value < 0) value = 23;
        if (value > 23 ) value = 0;
        date.setHours(value)
      } else if (timeUnits === 'minutes') {
        value = date.getMinutes() + increment;
        if (value < 0) value = 59;
        if (value > 59 ) value = 0;
        date.setMinutes(value)
      } else {
        throw new Error('Invalid time units to edit: ' + timeUnits)
      }

      unitDisplay = document.getElementById(timeUnits+'-display-'+self.sId);
      unitDisplay.innerHTML =_2digits(value)

      // self.fromBufferToInputValue selectDate()
      // self.updateOutputProperty()

      // self.updateTimeDisplay();
    }

    this.resetDateTime = function () {
      self.resetTime();
      self.resetDate();
    }

    this.resetTime = function () {
      var now = new Date();
      self.selectedDate.setHours(now.getHours());
      self.selectedDate.setMinutes(now.getMinutes());
      // self.updateOutputProperty();
      // self.theInput.value = renderDateStr(self.selectedDate, self.hasDateEditor, self.hasTimeEditor)

      // self.fromBufferToInputValue selectDate()
      // self.updateTimeDisplay();

            // self.selectedDate.setDate(parseInt(this.innerHTML));
            // self.updateOutputProperty();
            // self.fromBufferToInputValue /*selectDate*/();


    }

    this.resetDate = function () {
      var now = new Date();
      self.selectedDate.setFullYear(now.getFullYear());
      self.selectedDate.setMonth(now.getMonth());
      self.selectedDate.setDate(now.getDate());
      // self.updateOutputProperty();
      // self.fromBufferToInputValue /*selectDate*/();
      // self.theInput.value = renderDateStr(self.selectedDate, self.hasDateEditor, self.hasTimeEditor)

      // var monthSelect = document.getElementById('month-select-'+self.sId);
      // if (monthSelect) monthSelect.value = now.getMonth()

      // var yearSelect = document.getElementById('year-select-'+self.sId);
      // if (yearSelect) yearSelect.value = now.getFullYear()

      self.updateCalendarSelecteds(); // <- update selection in drop-down lists, implicit call to .fromBufferToInputValue /*selectDate*/()
      // self.fromBufferToInputValue /*selectDate*/()
      // self.createMonthTable();
      // self.updateTimeDisplay();
    }

    var appentTabPage = function (parent, className) {
      // Group with tab handle, lable and page for content
      var element = document.createElement('div');
      element.className = className;
      parent.appendChild(element)
      return element;

    }

    //create the calendar html and events
    this.createCal = function () {
      var element;
      //creating a container div around the input, the calendar will also be there
      this.container = document.createElement('div');
      this.container.className = 'calanderContainer';
      this.container.style.display = 'inline-block';
      this.container.setAttribute('data-disable-autoclose', self.sId); //<- prevent auto-close on click
      this.theInput.parentNode.replaceChild(this.container, this.theInput);
      this.container.appendChild(this.theInput);


      // //the calendar div
      // this.theDateEditorDiv = document.createElement('div');
      // this.theDateEditorDiv.className = 'calendar';
      // this.theDateEditorDiv.style.display = 'none';
      // this.container.appendChild(this.theDateEditorDiv);

      // CHANGES ARE HERE:::::::::::::::::::
      // Create pull-down:
      this.editorsContainer = document.createElement('div')
      this.editorsContainer.setAttribute('data-disable-autoclose', self.sId); //<- prevent auto-close on click
      this.editorsContainer.id = 'datetime-editor-group-'+self.sId;
      this.editorsContainer.className = 'calendar';
      this.editorsContainer.style.display = 'none'; // <-

      this.container.appendChild(this.editorsContainer)

      if (this.hasDateEditor) { // needs "date" tab
        //~//~~~console.log('...adding the calendar...')
        this.theDateEditorDiv = appentTabPage(this.editorsContainer, 'date-editor-container');
        // this.theDateEditorDiv.className = 'calendar'; // <-

        //the year and month selects inside the calendar
        this.creathYearAndMonthSelects();

        //the days table inside the calendar
        this.createMonthTable();
      }
      if (this.hasTimeEditor) { // needs time tabs
        //~//~~~console.log('...adding the time editors...')
        this.theTimeEditorDiv = appentTabPage(this.editorsContainer, 'time-editor-container');
        this.createTimeEditor();
      }

      // Special event which allows to set value directly by custom event:
      this.theInput.addEventListener('setDateValue', function (e) {
        // self.selectedDate.setTime(e.detail.date.getTime())
        //~~~console.log('++++ setDateValue received', self.selectedDate)
        // self.selectedDate.setTime(e.detail.date.getTime());
        self.theInput.value = renderDateStr(new Date(e.detail.date.getTime()), self.hasDateEditor, self.hasTimeEditor)
        console.warn('input after event: ', self.theInput.value, self.selectedDate)
        self.updateControls()
        console.warn('input after self.updateControls(): ', self.theInput.value, self.selectedDate)
      });

      function openEditor () {
        if (_activeSId !== self.sId) {
          var editorsContainer = document.getElementById('datetime-editor-group-'+_activeSId);
          if (editorsContainer) editorsContainer.style.display = 'none';
          _activeSId = self.sId
        }
        // Update editor
        self.updateControls()
        // Display editor
        self.editorsContainer.style.display = '';
        _activeExtender = self;
      }

      //open the calendar when the input get focus, also on various click events to capture it in all corner cases
      this.theInput.addEventListener('focus', openEditor);
      this.theInput.addEventListener('mouseup', openEditor);
      this.theInput.addEventListener('mousedown', openEditor);

      // Align string to mask with leading zeros if date is in a short form:
      this.theInput.addEventListener('blur', function (e) {
        var text = self.theInput.value;
        if (text.length < 10) {
          //~//~~~console.log('Updating!!!')
          self.theInput.value = renderDateStr(self.selectedDate, self.hasDateEditor, self.hasTimeEditor)
        }

        // e = e ? e : window.event;
        // // if focus moves out of controls group:
        // //~//~~~console.log('blur', e.target, e.relatedTarget)
        // if (!e.relatedTarget.getAttribute('data-disable-autoclose') || e.relatedTarget.getAttribute('data-disable-autoclose' !== self.sId)) {
        //   //~//~~~console.log('Loosing focus, hiding editor:')
        //   self.editorsContainer.style.display = 'none';
        // }
      })

      this.theInput.addEventListener('keyup', self.updateControls);

      // "Ctrl" listener:
      this._isCtrlKeyDown = false;

      this.theInput.addEventListener('keyup',   function (evt) {
        var e = evt ? evt : window.event;
        if (17 === e.keyCode) {
          self._isCtrlKeyDown = false;
          //~//~~~console.log('Ctrl OFF')
        }
      })

      var _stopEvent = function (e) { //Stop the event
          //e.cancelBubble is supported by IE - this will kill the bubbling process.
          e.cancelBubble = true;
          e.returnValue = false;

          //e.stopPropagation works in Firefox.
          if (e.stopPropagation) {
            e.stopPropagation();
            e.preventDefault();
          }
          return false
      }

      self.onKeyDown = function (evt) {
        // use "self" here instead of "this"
        function isTextSelected(input){ // <--- http://stackoverflow.com/questions/9065828/javascript-check-if-text-selected
           var startPos = input.selectionStart;
           var endPos = input.selectionEnd;
           var doc = document.selection;

           if(doc && doc.createRange().text.length != 0) {
              return true;
           } else if (!doc && input.value.substring(startPos,endPos).length != 0) {
              return true;
           }
           return false;
        }

        var e = evt ? evt : window.event;
        var key = e.charCode || e.keyCode || e.which;
        var keychar = String.fromCharCode(key);
        var text = self.theInput.value;

        // ----------- 8< --------------------
        // Simple stub to allow only Tab key
        if (key === 9) return true;
        return _stopEvent(e)
        // ----------- 8< --------------------

        // Always allow Ctrl key:
        if (key === 17) {
          self._isCtrlKeyDown = true;
          //~//~~~console.log('Ctrl ON')
          return true;
        }

        // Allow combinations: Ctrl+C, Ctrl+A, Ctrl+Z, Ctrl+X:
        if (self._isCtrlKeyDown && [65,67,88,90,99].indexOf(key)>-1) return true;
        // Allow tab, Enter, End, Home, <-, ->, Backspace, Delete:
        //~//~~~console.log('[].indexOf', key, [9, 13, 35, 36, 37, 39].indexOf(key))
        if ([9, 13, 35, 36, 37, 39, 8, 46].indexOf(key)>-1) return true;

        var maxLen = 0;
        if (self.hasDateEditor) maxLen = 10;
        if (self.hasTimeEditor) maxLen += (maxLen > 0) ? 6 : 5;

        // if complete and no text is selected:
        if (text.length >=maxLen && !isTextSelected(self.theInput))  return _stopEvent(e);

        // year/months part is complete, the next char can be both the "-" and the digit:
        if (self.hasDateEditor && (text.length === 4 || text.length === 7)) {
          if (keychar === '-') return true; // <- user inputs delimiter explicitly
          self.theInput.value += '-'; // <- user goes to next digit, so insert delimiter
        }
        // date part is complete, the next char can be both the " " and the digit (time part begins):
        if (self.hasDateEditor && self.hasTimeEditor && text.length === 10) {
          if (keychar === ' ') return true; // <- user inputs delimiter explicitly
          self.theInput.value += ' '; // <- user goes to next digit, so insert delimiter
        }
        // hours part is complete, the next char can be both the ":" and the digit:
        if (self.hasTimeEditor && (text.length === ( (self.hasDateEditor) ? 13 : 2)) ) {
          if (keychar === ':') return true; // <- user inputs delimiter explicitly
          self.theInput.value += ':'; // <- user goes to next digit, so insert delimiter
        }

        if (/[0-9]/.test(keychar)) return true; // <-- allow digits
        return _stopEvent(e); // <- in all another cases - prohibit all
      }

      // TO-DO: issues remains: FIRST CHAR not validated???
      this.theInput.addEventListener('keydown', self.onKeyDown)

      this.theInput.addEventListener('change', self.updateControls)

      //close the calendar when clicking outside of the input or calendar
      // TO-DO: handle also "blur"/"focus" events to hide menu...
      // document.getElementsByTagName('body')[0].addEventListener('focus', function (e) {
      //   // //~//~~~console.log('..we are here: ',e.target, e.target.parentNode, e.target.parentNode.parentNode)

      //   // CHECK whether a target belongs to this control!! fires handlers in all instances of <input[date]>

      //   // switch (e.target.getAttribute('data-fire-action')) {
      //   //   case 'hours-increase':
      //   //     self.incrementTime('hours', +1)
      //   //     break
      //   //   case 'hours-decrease':
      //   //     self.incrementTime('hours', -1)
      //   //     break
      //   //   case 'minutes-increase':
      //   //     self.incrementTime('minutes', +1)
      //   //     break
      //   //   case 'minutes-decrease':
      //   //     self.incrementTime('minutes', -1)
      //   //     break
      //   //   case 'apply-now':
      //   //     self.resetTime()
      //   //     break
      //   //   case 'apply-today':
      //   //     self.resetDate()
      //   //     break;
      //   // }

      //   if (e.target === self.theInput || e.target && e.target.getAttribute('data-disable-autoclose') === self.sId) return;

      //   // if (e.target.parentNode === self.container ||
      //   //     e.target.parentNode.parentNode === self.container ||
      //   //     e.target.parentNode.parentNode === self.editorsContainer ||
      //   //     e.target.parentNode === self.theDateEditorDiv ||
      //   //     (e.target.parentNode && e.target.parentNode.hasAttribute('data-disable-autoclose')) ||
      //   //     (e.target.parentNode.parentNode && e.target.parentNode.parentNode.hasAttribute('data-disable-autoclose'))
      //   // ) {
      //   //   return
      //   // }
      //   self.editorsContainer.style.display = 'none';
      //   if (_activeSId === self.sId) {
      //     //~~~console.log('closing editor for: ', _activeSId);
      //     _activeSId = null;
      //   }
      // }, true);
    };

    //create the year and month selects html
    this.creathYearAndMonthSelects = function () {
      var btnToday = document.createElement('button');
      btnToday.setAttribute('data-disable-autoclose', self.sId);
      btnToday.innerHTML = 'Today';
      btnToday.onclick = self.resetDate;
      // btnToday.setAttribute('href', 'javascript:void(0)')
      this.theDateEditorDiv.appendChild(btnToday);

      //the year selector inside the calendar
      var yearSelect = this.createRangeSelect(new Date().getFullYear() - 80, new Date().getFullYear() + 20, this.selectedDate.getFullYear());
      yearSelect.className = 'yearSelect';
      yearSelect.setAttribute('data-disable-autoclose', self.sId);
      yearSelect.id = 'year-select-'+self.sId;
      this.theDateEditorDiv.appendChild(yearSelect);

      yearSelect.onchange = function () {
        //~//~~~console.log('...yearSelect.onchange...')
        self.selectedDate.setYear(this.value);
        self.updateOutputProperty();
        self.fromBufferToInputValue /*selectDate*/();
        self.createMonthTable();

        //~//~~~console.log('...yearSelect.onchange')
        self.theInput.focus();
      };

      //the month selector inside the calendar
      var monthsNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
      var monthSelect = this.createRangeSelect(0, 11, this.selectedDate.getMonth(), monthsNames);
      monthSelect.className = 'monthSelect';
      monthSelect.setAttribute('data-disable-autoclose', self.sId);
      monthSelect.id = 'month-select-'+self.sId;
      this.theDateEditorDiv.appendChild(monthSelect);

      monthSelect.onchange = function () {
        self.selectedDate.setMonth(this.value);
        self.updateOutputProperty();
        self.fromBufferToInputValue /*selectDate*/();
        self.createMonthTable();

        //~//~~~console.log('...monthSelect.onchange')
        self.theInput.focus();
      };
    };

    //update the year and month selects with the right selected value (if date changed externally)
    this.updateCalendarSelecteds = function () {
      // this.theDateEditorDiv.querySelector('.yearSelect').value  = this.selectedDate.getFullYear();
      // this.theDateEditorDiv.querySelector('.monthSelect').value = this.selectedDate.getMonth();

      // var now = new Date();
      var monthSelect = document.getElementById('month-select-'+self.sId);
      if (monthSelect) monthSelect.value = self.selectedDate.getMonth()

      var yearSelect = document.getElementById('year-select-'+self.sId);
      if (yearSelect) yearSelect.value = self.selectedDate.getFullYear()

      this.createMonthTable();
    };

    this.updateTimeDisplay = function () {
      var hoursDisplay, minutesDisplay, secondsDisplay;
      if (self.hasTimeEditor) {
        hoursDisplay = document.getElementById('hours-display-'+self.sId);
        if (hoursDisplay) hoursDisplay.innerHTML = _2digits(self.selectedDate.getHours());
        minutesDisplay = document.getElementById('minutes-display-'+self.sId);
        if (minutesDisplay) minutesDisplay.innerHTML = _2digits(self.selectedDate.getMinutes());
        secondsDisplay = document.getElementById('seconds-display-'+self.sId);
        if (secondsDisplay) secondsDisplay.innerHTML = _2digits(self.selectedDate.getSeconds());
      }
    }

    this.createTimeEditor = function () {
      function createButton(caption, handler) {
        var button = document.createElement('button');
        button.innerHTML = caption;
        button.setAttribute('data-disable-autoclose', self.sId);
        button.onclick = handler;
        return button;
      }
      var container = self.theTimeEditorDiv;
      container.setAttribute('data-disable-autoclose', self.sId);

      var topBar = document.createElement('div');
      topBar.className = 'time-editor-topbar';

      var cell = document.createElement('div')
      cell.className = 'cell'
      var button = createButton('Now', self.resetDateTime);
      cell.appendChild(button)
      topBar.appendChild(cell)

      cell = document.createElement('div')
      cell.className = 'cell'
      button = createButton('Close', hideOpened);
      button.setAttribute('data-disable-autoclose', 'false');
      cell.appendChild(button)
      topBar.appendChild(cell)

      container.appendChild(topBar)

      // button = createButton('Close', self.resetTime);
      // cell.appendChild(button)

      var editor = document.createElement('div');
      editor.className = 'time-edit';
      editor.setAttribute('data-disable-autoclose', self.sId);

      // var cell = document.createElement('div')
      // cell.className = 'cell'
      // var button = createButton('Now', self.resetTime);
      // cell.appendChild(button)
      // editor.appendChild(cell)

      // cell = document.createElement('div')
      // cell.className = 'cell'
      // button = createButton('Close', hideOpened);
      // cell.appendChild(button)
      // editor.appendChild(cell)

      // // Newline
      // var element = document.createElement('div')
      // element.className = 'newline'
      // editor.appendChild(element)
      // // container.appendChild(topBar)

      // Hours ++
      var cell = document.createElement('div');
      cell.className = 'cell';
      button = createButton('+', function () {
        self.incrementTime('hours', +1)
      })
      cell.appendChild(button)
      editor.appendChild(cell)

      // Minutes ++
      var cell = document.createElement('div');
      cell.className = 'cell';
      button = createButton('+', function () {
        self.incrementTime('minutes', +1)
      })
      cell.appendChild(button)
      editor.appendChild(cell)

      // Newline
      var element = document.createElement('div')
      element.className = 'newline'
      editor.appendChild(element)

      // Time display
      element = document.createElement('div');
      element.className = 'cell colspan time-display';
      element.innerHTML = '<span id="hours-display-'+self.sId+'">00</span>\
            <span>:</span>\
            <span id="minutes-display-'+self.sId+'">00</span>'
      editor.appendChild(element);

      // Newline
      var element = document.createElement('div')
      element.className = 'newline'
      editor.appendChild(element)


      // Hours --
      var cell = document.createElement('div');
      cell.className = 'cell';
      button = createButton('-', function () {
        self.incrementTime('hours', -1)
      })
      cell.appendChild(button)
      editor.appendChild(cell)

      // Minutes --
      var cell = document.createElement('div');
      cell.className = 'cell';
      button = createButton('-', function () {
        self.incrementTime('minutes', -1)
      })
      cell.appendChild(button)
      editor.appendChild(cell)

      container.appendChild(editor)
      this.updateTimeDisplay();


      // var html = '<div><button data-fire-action="apply-now" data-disable-autoclose="true">Now</button></div>\
      //   <div class="time-edit" data-disable-autoclose="true">\
      //     <div class="cell"><button data-fire-action="hours-increase">+</button></div>\
      //     <div class="cell"><button data-fire-action="minutes-increase">+</button></div>\
      //     <div class="newline"></div>\
      //     <div class="cell colspan time-display">\
      //       <span id="hours-display">00</span>\
      //       <span>:</span>\
      //       <span id="minutes-display">00</span>\
      //     </div>\
      //     <div class="newline"></div>\
      //     <div class="cell"><button data-fire-action="hours-decrease">-</button></div>\
      //     <div class="cell"><button data-fire-action="minutes-decrease">-</button></div>\
      // </div>'

      // this.theTimeEditorDiv.innerHTML = html;
      // this.updateTimeDisplay();
    }

    //create the days table
    this.createMonthTable = function () {
      var year = this.selectedDate.getFullYear(); //get the year (2015)
      var month = this.selectedDate.getMonth(); //get the month number (0-11)
      var startDay = new Date(year, month, 1).getDay(); //first weekday of month (0-6)
      var maxDays = new Date(this.selectedDate.getFullYear(), month + 1, 0).getDate(); //get days in month (1-31)

      //if there was a table before, remove it
      var oldTables = this.theDateEditorDiv.getElementsByTagName('table');
      if (oldTables.length > 0) {
        this.theDateEditorDiv.removeChild(oldTables[0]);
      }

      //the table and header for the month days
      var theTable = document.createElement('table');
      theTable.className = 'table-condensed'; //<- use bootstap style if css is attached
      theTable.innerHTML = '<tr><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr>';
      this.theDateEditorDiv.appendChild(theTable);

      //create the days cols according to the selected month days
      var aRow;
      var aCell;
      for (var cellNum = 0; cellNum < maxDays + startDay; cellNum++) {

        //crate a table row in the begining and after each 7 cells
        if (cellNum % 7 === 0) {
          aRow = theTable.insertRow(-1);
        }

        aCell = aRow.insertCell(-1);

        if (cellNum + 1 > startDay) {

          var dayNum = cellNum + 1 - startDay;
          aCell.innerHTML = dayNum;
          if (dayNum === this.selectedDate.getDate()) {
            aCell.className = 'selected';
          }

          //when clicking on a day in the days table
          aCell.addEventListener('click', function () {

            //mark the dey with 'selected' css class
            self.theDateEditorDiv.querySelector('.selected').className = '';
            this.className = 'selected';

            self.selectedDate.setDate(parseInt(this.innerHTML));
            self.updateOutputProperty();
            self.fromBufferToInputValue /*selectDate*/();
            self.theInput.focus();

            if (!self.hasTimeEditor) self.editorsContainer.style.display = 'none';

          });
        }
      }
    };

    //copy the selected date to the input field
    this.fromBufferToInputValue /*selectDate*/ = function () {

      this.theInput.value = renderDateStr(this.selectedDate, this.hasDateEditor, this.hasTimeEditor);

      //make angular see the change
      var fakeEvent = document.createEvent('KeyboardEvent');
      fakeEvent.initEvent("change", true, false);
      this.theInput.dispatchEvent(fakeEvent);
    };

    //helper function to create html select tags
    this.createRangeSelect = function (min, max, selected, namesArray) {
      var aOption;
      var curNum;
      var theText;

      var theSelect = document.createElement('select');

      for (curNum = min; curNum <= max; curNum++) {
        aOption = document.createElement('option');
        theSelect.appendChild(aOption);

        if (namesArray) {
          theText = namesArray[curNum - min];
        } else {
          theText = curNum;
        }

        aOption.text = theText;
        aOption.value = curNum;

        if (curNum === selected) {
          aOption.selected = true;
        }
      };

      return theSelect;
    }

    this.init();
  }

  //return false if the browser dont support input[type=date]
  function checkDateInputSupport () {
    var input = document.createElement('input');
    input.setAttribute('type','date');

    var notADateValue = 'not-a-date';
    input.setAttribute('value', notADateValue);

    return !(input.value === notADateValue);
  }

  function applyDateTimeExtender(dateInput) {
    new calanderExtender(dateInput);
    //mark that it have extender installed:
    dateInput.classList.add('has-dateinput-extender');
    dateInput.setAttribute('data-has-dateinput-extender', 'true');
  }

  //will add the calanderExtender to all inputs in the page
  function addCalanderExtenderToDateInputs (filterSelector) {
    //get and loop all the input[type=date]s in the page that dont have "has-dateinput-extender" class yet
    var dateInputs = document.querySelectorAll(filterSelector || 'input[type=date]:not(.has-dateinput-extender),input[type^=datetime]:not(.has-dateinput-extender),input[type=time]:not(.has-dateinput-extender)');
    [].forEach.call(dateInputs, function (dateInput) {
      //call calanderExtender function on the input
      applyDateTimeExtender(dateInput);
    });
  }

  var isDateTimeInput = function (element) {
    var inputType = element.getAttribute('data-x-type') || element.getAttribute('type') || ''; //<- warning: for browsers which have no support for input[type=date] the value element.type will be "text"! but the attribute value for 'type' will be as specified!
    //~//~~~console.log(inputType)
    return !!({'date':1, 'datetime':1, 'datetime-local': 1, 'time': 1}[inputType]);
  }

  // Apply extender to all inputs with 'data-x-type'='date', 'time', ...:
  addCalanderExtenderToDateInputs('input[data-x-type=date]:not(.has-dateinput-extender),input[data-x-type^=datetime]:not(.has-dateinput-extender),input[data-x-type=time]:not(.has-dateinput-extender)');
  //run the above code on any <input type='date'> in the document, also on dynamically created ones
  //check if type=date is supported or if not mobile, they have built-in support for type='date'
  if (!checkDateInputSupport() && typeof window.orientation === 'undefined') {
    addCalanderExtenderToDateInputs();
    //this is also on mousedown event so it will capture new inputs that might joined to the dom dynamically
    document.getElementsByTagName('body')[0].addEventListener('focus', function (event) {
      var e = event || window.event, target = e.target;
      if (target && target.tagName === 'INPUT'
          && isDateTimeInput(target)
          && !target.hasAttribute('data-has-dateinput-extender')) applyDateTimeExtender(target)
      // //~~~console.log('extender applied to: ', target)
    }, true);

  }

  // Add delegated listener to handle status when editor group loses focus:
  document.getElementsByTagName('body')[0].addEventListener('focus', hideOpened, true) //<- pass true to ensure event visibility in nested elements
  document.getElementsByTagName('body')[0].addEventListener('click', hideOpened, true) //<- pass true to ensure event

  // Add delegated listener to handle status when editor group loses focus:
  // document.getElementsByTagName('body')[0].addEventListener('click', hideOpened, true) //<- pass true to ensure event visibility in nested elements

  // return {
  //   'applyDateTimeExtender': applyDateTimeExtender,
  //   'DateTimeExtender': calanderExtender
  // }; // <-- allow to use manual control on any input

}));