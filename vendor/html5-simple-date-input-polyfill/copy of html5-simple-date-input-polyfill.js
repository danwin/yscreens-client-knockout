// Source: https://github.com/liorwohl/html5-simple-date-input-polyfill
// Changes: UMD wrapper for module, 2) automatic updates for calendar when input changed programmatically; 3) default value - "today" unless "value" attribute missed;

(function(root, factory) {
    if (typeof define === "function" && define.amd) { // AMD mode
      define([], factory);
    } else if (typeof exports === "object") { // CommonJS mode
      module.exports = factory();
    } else {
      factory(); // Plain JS, "rtl" is in window scope
    }

}(this, function(_) {

  var _2digits = function (value) {
    return (value < 10) ? '0' + value : value.toString()
  }

  var renderDateStr = function (dateObj, hasDateEditor, hasTimeEditor) {
    var result = [];
    console.log('...rendering...', hasDateEditor, hasTimeEditor)
    if (hasDateEditor) {
      result.push([
          dateObj.getFullYear(), 
          _2digits(dateObj.getMonth() + 1), 
          _2digits(dateObj.getDate())
        ].join('-')
      )
    }
    if (hasTimeEditor) {
      result.push([
          _2digits(dateObj.getHours()),
          _2digits(dateObj.getMinutes()),
        ].join(':')
      )
    }
    return result.join(' ');
  }

  // var renderPartialDateStr = function (textVal) {
  //   if (textVal.length === 10) return false;
  //   var args = textVal.split('-');
  //   if (textVal.length > 4 && textVal.chatAt(4) !== '-') 
  // }

  function calanderExtender (theInput) {

    var self = this;

    this.theInput = theInput;
    this.container = null;
    this.theCalDiv = null;
    this.selectedDate = new Date();

    // Choose editors by  input[type] check     
    var inputType = this.theInput.getAttribute('type'); //<- warning: for browsers which have no support for input[type=date] the value element.type will be "text"! but the attribute value for 'type' will be as specified!
    console.log(inputType)
    this.hasDateEditor = ('date,datetime,datetime-local'.split(',').indexOf(inputType) >-1);
    this.hasTimeEditor = ('datetime,datetime-local,time'.split(',').indexOf(inputType) >-1);

    this.init = function () {
      if (!this.getDateFromInput()) { 
        this.selectedDate.setTime(new Date().getTime());
        this.theInput.value = renderDateStr(this.selectedDate, this.hasDateEditor, this.hasTimeEditor)
      }
      this.createCal();
    };

    //update selectedDate with the date from the input, return true if changed
    this.getDateFromInput = function () {
      if (this.theInput.value) {
        var cleanedText = this.theInput.value.replace(/-$/,''); // remove trailing "-" when input text is a partial
        var possibleNewDate = new Date(this.theInput.value);
        if (Date.parse(this.theInput.value) && possibleNewDate.toDateString() !== this.selectedDate.toDateString()) {
          this.selectedDate = possibleNewDate;
          return true;
        }
      }
      return false;
    };

    // Id generator: "class-wide" visibility
    calanderExtender._lastTabGroupId = calanderExtender._lastTabGroupId || 0;
    /**
     * Creates tabbed control (container for tab pages)
     * @return {element} Tab control - the parent container for tab pages
     */
    var appentTabsGroup = function (parent, groupName) {
      // <div class='tabstrip'>
      groupName = groupName || 'tab-group'
      var container = document.createElement('div');
      container.className = 'tabstrip';
      parent.appendChild(container);

      // <ul> as collection for tabPages
      var element = document.createElement('ul');
      element.setAttribute('data-tabsgroup-name', groupName + (++calanderExtender._lastTabGroupId))
      container.appendChild(element);
      return element;
    }

    // Id generator for tab pages - "class-wide" visiblity
    calanderExtender._lastTabId = calanderExtender._lastTabId || 0;
    /**
     * Creates single page in the tab control, with a tab handle and caption
     * @param {element} collection <ul> to which new page will be attached
     * @param {string} id         tab Id
     * @param {string} caption    Caption for a tab handle
     * @return {element} Container for page content
     */
    var appentTabPage = function (parent, caption) {
      // Group with tab handle, lable and page for content
      var groupName = parent.getAttribute('data-tabsgroup-name');
      var tabItem = document.createElement('li');
      tabItem.setAttribute('data-disable-autoclose', 'true'); //<- prevent auto-close on click
      var index = (++calanderExtender._lastTabId);
      var element;

      // <input type="radio">
      element = document.createElement('input');
      element.id = 'tabstrip-'+index;
      element.setAttribute('type', 'radio');
      element.setAttribute('name', groupName);
      if (!parent.innerHTML) element.setAttribute('checked', 'checked'); //<-show the first page
      // element.setAttribute('id', 'tabstrip-'+index);
      console.log(parent, parent.innerHTML)
      tabItem.appendChild(element);

      // <label>, tab handle with caption
      element = document.createElement('label');
      element.setAttribute('for', 'tabstrip-'+index)
      element.innerHTML = caption;
      tabItem.appendChild(element);

      // <div>, page "body", container for content
      element = document.createElement('div');
      tabItem.appendChild(element);
      // return reference to the page container, to add children
      parent.appendChild(tabItem);
      return element; 
    }

    //create the calendar html and events
    this.createCal = function () {
      var element;
      //creating a container div around the input, the calendar will also be there
      this.container = document.createElement('div');
      this.container.className = 'calanderContainer';
      this.container.style.display = 'inline-block';
      this.container.setAttribute('data-disable-autoclose', 'true'); //<- prevent auto-close on click
      this.theInput.parentNode.replaceChild(this.container, this.theInput);
      this.container.appendChild(this.theInput);


      // //the calendar div
      // this.theCalDiv = document.createElement('div');
      // this.theCalDiv.className = 'calendar';
      // this.theCalDiv.style.display = 'none';
      // this.container.appendChild(this.theCalDiv);

      // CHANGES ARE HERE:::::::::::::::::::
      // Create pull-down:
      this.menuContainer = document.createElement('div')
      this.menuContainer.setAttribute('data-disable-autoclose', 'true'); //<- prevent auto-close on click
      this.menuContainer.className = 'calendar';
      this.menuContainer.style.display = 'none'; // <-

      this.container.appendChild(this.menuContainer)

      // Create tabs container
      this.tabsPanel = appentTabsGroup(this.menuContainer, 'date-tabs');

      if (this.hasDateEditor) { // needs "date" tab
        console.log('...adding the calendar...')
        this.theCalDiv = appentTabPage(this.tabsPanel, 'Day');
        // this.theCalDiv.className = 'calendar'; // <-
      }
      if (this.hasTimeEditor) { // needs time tabs
        console.log('...adding the time editors...')
        this.theHoursDiv = appentTabPage(this.tabsPanel, 'Hours');
        this.theMinutesDiv = appentTabPage(this.tabsPanel, 'Minutes');
      }

      //the year and month selects inside the calendar
      this.creathYearAndMonthSelects();

      //the days table inside the calendar
      this.createMonthTable();

      //open the calendar when the input get focus, also on various click events to capture it in all corner cases
      this.theInput.addEventListener('focus', function () { self.menuContainer.style.display = ''; });
      this.theInput.addEventListener('mouseup', function () { self.menuContainer.style.display = ''; });
      this.theInput.addEventListener('mousedown', function () { self.menuContainer.style.display = ''; });

      // Align string to mask with leading zeros if date is in a short form:
      this.theInput.addEventListener('blur', function () {
        var text = self.theInput.value;
        if (text.length < 10) {
          console.log('Updating!!!')
          self.theInput.value = renderDateStr(self.selectedDate, self.hasDateEditor, self.hasTimeEditor)
        }
        // self.menuContainer.style.display = 'none';
      })

      //update the calendar if the date changed manually in the input
      var _updateOnChanges = function () {
        if (self.getDateFromInput()) {
          self.updateSelecteds();
        }
      }

      this.theInput.addEventListener('keyup', _updateOnChanges);

      // "Ctrl" listener:
      this._isCtrlKeyDown = false;

      this.theInput.addEventListener('keyup',   function (evt) {
        var e = evt ? evt : window.event;        
        if (17 === e.keyCode) {
          self._isCtrlKeyDown = false;
          console.log('Ctrl OFF')
        } 
      })

      var _stopEvent = function (e) { //Stop the event
          //e.cancelBubble is supported by IE - this will kill the bubbling process.
          e.cancelBubble = true;
          e.returnValue = false;
  
          //e.stopPropagation works in Firefox.
          if (e.stopPropagation) {
            e.stopPropagation();
            e.preventDefault();
          }
          return false
      }

      // TO-DO: issues remains: FIRST CHAR not validated???
      this.theInput.addEventListener('keydown', function (evt) {
        function isTextSelected(input){ // <--- http://stackoverflow.com/questions/9065828/javascript-check-if-text-selected
           var startPos = input.selectionStart;
           var endPos = input.selectionEnd;
           var doc = document.selection;

           if(doc && doc.createRange().text.length != 0) {
              return true;
           } else if (!doc && input.value.substring(startPos,endPos).length != 0) {
              return true;
           }
           return false;
        }
        
        var e = evt ? evt : window.event;
        var key = e.charCode || e.keyCode || e.which;
        var keychar = String.fromCharCode(key);
        var text = self.theInput.value;

        // Always allow Ctrl key:
        if (key === 17) {
          self._isCtrlKeyDown = true;
          console.log('Ctrl ON')
          return true;
        }

        // Allow combinations: Ctrl+C, Ctrl+A, Ctrl+Z, Ctrl+X:
        if (self._isCtrlKeyDown && [65,67,88,90,99].indexOf(key)>-1) return true;
        // Allow tab, Enter, End, Home, <-, ->, Backspace, Delete:
        console.log('[].indexOf', key, [9, 13, 35, 36, 37, 39].indexOf(key))
        if ([9, 13, 35, 36, 37, 39, 8, 46].indexOf(key)>-1) return true;

        var maxLen = 0;
        if (self.hasDateEditor) maxLen = 10;
        if (self.hasTimeEditor) maxLen += (maxLen > 0) ? 6 : 5;

        // if complete and no text is selected:
        if (text.length >=maxLen && !isTextSelected(self.theInput))  return _stopEvent(e); 

        // year/months part is complete, the next char can be both the "-" and the digit:
        if (self.hasDateEditor && (text.length === 4 || text.length === 7)) {
          if (keychar === '-') return true; // <- user inputs delimiter explicitly
          self.theInput.value += '-'; // <- user goes to next digit, so insert delimiter
        }
        // date part is complete, the next char can be both the " " and the digit (time part begins):
        if (self.hasDateEditor && self.hasTimeEditor && text.length === 10) {
          if (keychar === ' ') return true; // <- user inputs delimiter explicitly
          self.theInput.value += ' '; // <- user goes to next digit, so insert delimiter
        }
        // hours part is complete, the next char can be both the ":" and the digit:
        if (self.hasTimeEditor && (text.length === ( (self.hasDateEditor) ? 13 : 2)) ) {
          if (keychar === ':') return true; // <- user inputs delimiter explicitly
          self.theInput.value += ':'; // <- user goes to next digit, so insert delimiter
        }

        if (/[0-9]/.test(keychar)) return true; // <-- allow digits
        return _stopEvent(e); // <- in all another cases - prohibit all
      })

      this.theInput.addEventListener('change', _updateOnChanges)

      //close the calendar when clicking outside of the input or calendar
      // TO-DO: handle also "blur"/"focus" events to hide menu...
      document.addEventListener('click', function (e) {
        console.log('..we are here: ',e.target.parentNode, e.target.parentNode.parentNode)
        if (e.target.parentNode === self.container ||
            e.target.parentNode.parentNode === self.container || 
            e.target.parentNode.parentNode === self.menuContainer ||
            e.target.parentNode === self.theCalDiv ||
            (e.target.parentNode && e.target.parentNode.hasAttribute('data-disable-autoclose'))
        ) return;
        self.menuContainer.style.display = 'none';
      });
    };

    //create the year and month selects html
    this.creathYearAndMonthSelects = function () {
      //the year selector inside the calendar
      var yearSelect = this.createRangeSelect(new Date().getFullYear() - 80, new Date().getFullYear() + 20, this.selectedDate.getFullYear());
      yearSelect.className = 'yearSelect';
      yearSelect.setAttribute('data-disable-autoclose', 'true');
      this.theCalDiv.appendChild(yearSelect);

      yearSelect.onchange = function () {
        console.log('...yearSelect.onchange...')
        self.selectedDate.setYear(this.value);
        self.selectDate();
        self.createMonthTable();

        console.log('...yearSelect.onchange')
        self.theInput.focus();
      };

      //the month selector inside the calendar
      var monthsNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
      var monthSelect = this.createRangeSelect(0, 11, this.selectedDate.getMonth(), monthsNames);
      monthSelect.className = 'monthSelect';
      monthSelect.setAttribute('data-disable-autoclose', 'true');
      this.theCalDiv.appendChild(monthSelect);
      
      monthSelect.onchange = function () {
        self.selectedDate.setMonth(this.value);
        self.selectDate();
        self.createMonthTable();

        console.log('...monthSelect.onchange')
        self.theInput.focus();
      };
    };

    //update the year and month selects with the right selected value (if date changed externally)
    this.updateSelecteds = function () {
      this.theCalDiv.querySelector('.yearSelect').value  = this.selectedDate.getFullYear();
      this.theCalDiv.querySelector('.monthSelect').value = this.selectedDate.getMonth();
      this.createMonthTable();
    };

    //create the days table
    this.createMonthTable = function () {
      var year = this.selectedDate.getFullYear(); //get the year (2015)
      var month = this.selectedDate.getMonth(); //get the month number (0-11)
      var startDay = new Date(year, month, 1).getDay(); //first weekday of month (0-6)
      var maxDays = new Date(this.selectedDate.getFullYear(), month + 1, 0).getDate(); //get days in month (1-31)

      //if there was a table before, remove it
      var oldTables = this.theCalDiv.getElementsByTagName('table');
      if (oldTables.length > 0) {
        this.theCalDiv.removeChild(oldTables[0]);
      }

      //the table and header for the month days
      var theTable = document.createElement('table');
      theTable.className = 'table-condensed'; //<- use bootstap style if css is attached
      theTable.innerHTML = '<tr><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr>';
      this.theCalDiv.appendChild(theTable);

      //create the days cols according to the selected month days
      var aRow;
      var aCell;
      for (var cellNum = 0; cellNum < maxDays + startDay; cellNum++) {

        //crate a table row in the begining and after each 7 cells
        if (cellNum % 7 === 0) {
          aRow = theTable.insertRow(-1);
        }

        aCell = aRow.insertCell(-1);

        if (cellNum + 1 > startDay) {

          var dayNum = cellNum + 1 - startDay;
          aCell.innerHTML = dayNum;
          if (dayNum === this.selectedDate.getDate()) {
            aCell.className = 'selected';
          }

          //when clicking on a day in the days table
          aCell.addEventListener('click', function () {

            //mark the dey with 'selected' css class
            self.theCalDiv.querySelector('.selected').className = '';
            this.className = 'selected';

            self.selectedDate.setDate(parseInt(this.innerHTML));
            self.selectDate();
            self.theInput.focus();
          });
        }
      }
    };

    //copy the selected date to the input field
    this.selectDate = function () {

      this.theInput.value = renderDateStr(this.selectedDate, this.hasDateEditor, this.hasTimeEditor);

      //make angular see the change
      var fakeEvent = document.createEvent('KeyboardEvent');
      fakeEvent.initEvent("change", true, false);
      this.theInput.dispatchEvent(fakeEvent);
    };

    //helper function to create html select tags
    this.createRangeSelect = function (min, max, selected, namesArray) {
      var aOption;
      var curNum;
      var theText;

      var theSelect = document.createElement('select');

      for (curNum = min; curNum <= max; curNum++) {
        aOption = document.createElement('option');
        theSelect.appendChild(aOption);

        if (namesArray) {
          theText = namesArray[curNum - min];
        } else {
          theText = curNum;
        }

        aOption.text = theText;
        aOption.value = curNum;

        if (curNum === selected) {
          aOption.selected = true;
        }
      };

      return theSelect;
    }

    this.init();
  }

  //return false if the browser dont support input[type=date]
  function checkDateInputSupport () {
    var input = document.createElement('input');
    input.setAttribute('type','date');

    var notADateValue = 'not-a-date';
    input.setAttribute('value', notADateValue); 

    return !(input.value === notADateValue);
  }

  //will add the calanderExtender to all inputs in the page
  function addCalanderExtenderToDateInputs () {
    //get and loop all the input[type=date]s in the page that dont have "haveCal" class yet
    var dateInputs = document.querySelectorAll('input[type=date]:not(.haveCal),input[type^=datetime]:not(.haveCal),input[type=time]:not(.haveCal)');
    [].forEach.call(dateInputs, function (dateInput) {
      //call calanderExtender function on the input
      new calanderExtender(dateInput);
      //mark that it have calendar
      dateInput.classList.add('haveCal');
    });
  }

  //run the above code on any <input type='date'> in the document, also on dynamically created ones 
  //check if type=date is supported or if not mobile, they have built-in support for type='date'
  if (!checkDateInputSupport() && typeof window.orientation === 'undefined') {
    addCalanderExtenderToDateInputs();
    //this is also on mousedown event so it will capture new inputs that might joined to the dom dynamically
    document.querySelector('body').addEventListener('mousedown', function (event) {
      addCalanderExtenderToDateInputs();
    });
  }

}));