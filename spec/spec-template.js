define([
  // all specs should require the SpecHelper
  // with jasmine setup and plugins
  'spec/SpecHelper',

  // spec dependencies
  'Player',
  'Song'
],
function (jasmine, Player, Song) {
  // spec code
});