define([
  // all specs should require the SpecHelper
  // with jasmine setup and plugins
  'spec/SpecHelper',

  // spec dependencies
  'jsapp/model-asset'
],
function (jasmine, asset) {
  // spec code
});