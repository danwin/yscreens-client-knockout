// env-stub.js

TestEnvironment = {
	'cookies': {},
	'state': null,
	'error': false,
	'bodyCSS': {},
	'urlQuery': {},
	'urlHash': '',

	'inputEmail': '',
	'inputToken': '',

	// ajax:
	'respondSuccess': true,
	'statusCode': 200
};
