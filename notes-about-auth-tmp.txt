endpoint: has .../{userId}/... argument

application: extands request options with "userId"

?do not create new "class" like "authorized endpoint or service"


io, io.middleware:

"createOptions"???
translate "attrs"-> {pathArgs:{attrs}}

?internal behaviour of service???

pathArgs, app and service

---
"app" "auth" can inject "userId" into "rqOptions.pathArgs" OR into transport "rqOptions.qryArgs"
---

svc.options()  method, ...
svc.read(app.rqOptions)
OR
app.read(svcName) ... ???
... inside: function read() { var opts = {pathArgs:{userId: app.auth.userId}}; returnsvc.read(opts) }

1. auth has ...userId
2. auth can wrap service to keep options with current userId.
3. auth simulates Service protocol? Yes, because it is AUTHENTICATED service
4. at the same time: auth implements signIn/signOut methods, ...
5. abstract auth and Firebase auth;
6. auth is entrity from "io" ???
aSerice = auth.wrap(aService);
// service can has options...

[to-do]:

1.remove signIn/signOut from transports???
2. observable User in app
3. escape user id in the path!